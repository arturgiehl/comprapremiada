<?PHP
$this->Html->css('Seo.keywords','stylesheet/less',array('inline'=>false));
echo $this->Form->create('SeoConfig');
$delete_icon=$this->Html->image('Painel.icons/delete.png');
if(isset($message)) echo $this->Html->tag('h1', $message, array('class'=>'message blue','data-delay-hide'=>3));
?>
<fieldset class="box">
    <legend>Configurações SEO</legend>
    
    <?PHP
        echo $this->Form->input('title',array('label'=>'Título do site'));
        echo $this->Form->input('description',array('label'=>'Descrição do site'));
//        echo $this->Form->input('author',array('label'=>'Autor'));
//        echo $this->Form->input('analytics',array('label'=>'Google analytics (somente o ID)'));
    ?>
    
</fieldset>

<fieldset class="box">
    <legend>Keywords</legend>
    <nav class="buttons">
        <?PHP
        echo $this->Html->link('Adicionar keyword',array('plugin'=>'seo','controller'=>'keywords','action'=>'add','admin'=>true),array('id'=>'add-keyword'));
        ?>
    </nav>
    <ul id="keywords">
        <?PHP
        foreach($keywords as $keyword){
            $link=$this->Html->link($delete_icon,array('plugin'=>'seo','controller'=>'keywords','action'=>'delete','admin'=>true,$keyword),array('escape'=>false));
            echo $this->Html->tag('li', $keyword.$link);
        }
        ?>
    </ul>
</fieldset>
<?PHP
echo $this->Form->end('Atualizar');
?>

<script type="text/javascript">
jQuery(function($){
    var icons={
        delete:'<?=$delete_icon?>',
    }
    $("#add-keyword").on('click',function(){
//        if($("#keywords>li").length>=10){
//            alert('Você só pode cadatrar 10 keywords');
//            return false;
//        }
        var kw=prompt('Digite uma keyword');
        if(!kw) return false;
        var url=$(this).attr('href')+'/'+kw;
        $.getJSON(url,function(data){
            if(data.status=='ok'){
                var dlink=base+'/admin/seo/keywords/delete/'+kw;
                $("#keywords").append('<li>'+kw+'<a href="'+dlink+'">'+icons.delete+'</a></li>');
            }
        });
        return false;
    });
    
    $("#keywords a").on('click',function(){
        var $this=$(this);
        $.getJSON($(this).attr('href'),function(data){
            if(data.status=='ok'){
                $this.parent().detach();
                return false;
            }
        });
        return false;
    });    
});
</script>