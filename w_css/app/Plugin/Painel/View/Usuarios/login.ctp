<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Sistema Administrativo - AMEXCOM</title>
        <?PHP
        echo $this->Html->css('Painel.login.less.css?' . time(), 'stylesheet/less');
        echo $this->Html->script(array('Painel.less','Painel.jquery'));
        ?>
    </head>
    <body> 
        <div id="login">
            <div class="left">
                <?PHP echo $this->Html->image('Painel.login/logo.png',array('ADMX Digital')) ?>
            </div>
            <div class="right">
                <h1><?PHP echo __('Bem-vindo a Administração de Sites da AMEXCOM') ?></h1>
                <?PHP
                if(isset($message)) echo '<h3>'.$message.'</h3>';
                
                echo $this->Form->create('User',array('id'=>'login-form','url'=>array('plugin'=>'painel','controller'=>'usuarios','action'=>'login')));
                echo $this->Form->input('username',array('label'=>'Usuário','value'=>'','autocomplete'=>'off'));
                echo $this->Form->input('password',array('label'=>'Senha','value'=>'','autocomplete'=>'off'));
                echo $this->Form->submit('Login');
                echo $this->Html->link('Esqueci minha senha','#',array('id'=>'recover'));
                echo $this->Form->end();
                
                echo $this->Form->create('User',array('id'=>'recover-form','url'=>array('plugin'=>'painel','controller'=>'usuarios','action'=>'recover')));
                echo $this->Form->input('email',array('label'=>'E-mail'));
                echo $this->Form->submit('Recuperar Senha');
                echo $this->Html->link('Painel de Login','#',array('id'=>'pannel'));
                echo $this->Form->end();
                ?>
            </div>
        </div>
        <script type="text/javascript">
            jQuery(function($){
                $('#recover').click(function(){
                    $('#login-form').hide();
                    $('#recover-form').show();
                    return false;
                });
                $('#pannel').click(function(){
                    $('#login-form').show();
                    $('#recover-form').hide();
                    return false;
                });
            });
        </script>
    </body>
</html>