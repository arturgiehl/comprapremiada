<?PHP
class TelefonesController extends TelefonesAppController{
    
    public $paginate = array('limit'=>16,'order'=>array('created'=>'DESC'));
    
    public function view(){
        $this->autoRender = true;
        $this->layout = false;
        
        $telefone['Telefone'] = array(
                                    'ip' => $this->getIP(),
                            );
        $this->Telefone->save($telefone);
    }
  
    public function admin_index(){
        $this->layout = "Painel.admin";
        $posts = $this->paginate('Telefone');
        $this->set('posts',$posts);
        $this->set('total',$this->Telefone->find('count'));
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->Telefone->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }   
    
    public function geoip(){
        $this->autoRender = true;
        $this->layout = false;
        
        $json_return = file_get_contents('http://www.telize.com/geoip/'.$_GET['ip'].'');
        $json = json_decode($json_return);
        $this->set('json',$json);
    }
    
    public function getIP(){  
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  
            $ip = $_SERVER['HTTP_CLIENT_IP'];  
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){  
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
        }else  {  
            $ip = $_SERVER['REMOTE_ADDR'];  
        }  
        return $ip;  
    }
    
}