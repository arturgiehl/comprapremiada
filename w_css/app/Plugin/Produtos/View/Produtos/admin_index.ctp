<style>
    .menu-destaque {
        margin-right: 200px;
    }
</style>

<fieldset class="box">
    <legend>Produtos</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar',array('action'=>'add')); ?>
    </nav>    
    <?PHP
        $value = !empty($_GET['s']) ? $_GET['s'] : '';
        $options=array('type' => 'get', 'id' => 'search');
        if(!empty($url)) $options['url']=$url;
//        echo $this->Form->create('Search',$options);
        echo $this->Form->create('Search',array('url'=>array('plugin'=>'produtos', 'controller'=>'produtos', 'action'=>'index'),'type' => 'get'));
        echo $this->Form->input('s', array('div'=>false,'label'=>false,'placeholder'=>'BUSCA','value'=>$value));
        echo $this->Form->end(array('label' => 'OK', 'div' => false));
    ?>
<!--    <ul class="menu menu-destaque">
        <li><a href="javascript:void(0);">Filtrar por destaque</a>
            <ul>
                <li><?PHP // echo $this->Html->link('Todos',array('action'=>'index')); ?></li>
                <?PHP
//                    echo $this->Html->tag('li',$this->Html->link('Sim',array('action'=>'index','d','1')));
//                    echo $this->Html->tag('li',$this->Html->link('Não',array('action'=>'index','d','n')));
                ?>
            </ul>
        </li>
    </ul>-->
    <ul class="menu">
        <li><a href="javascript:void(0);">Filtrar por Categoria</a>
            <ul>
                <li><?PHP echo $this->Html->link('Todos',array('action'=>'index')); ?></li>
                <?PHP
//                pre($list_ensinos);
                    foreach($categorias as $categoria){
                        echo $this->Html->tag('li',$this->Html->link($categoria['CategoriaProduto']['title'],array('action'=>'index','c',$categoria['CategoriaProduto']['id'])));
                    }
                ?>
            </ul>
        </li>
    </ul>
    <table class="tables">
        <thead>
            <tr>
                <td class="thumb"><a>Imagem</a></td>
                <td><?PHP echo $this->Paginator->sort('title','Título')?></td>
                <td><?PHP echo $this->Paginator->sort('preco','Pontos')?></td>
                <td><?PHP echo $this->Paginator->sort('cat_id','Categoria')?></td>
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP 
                $destaque = array('0'=>'Não', '1'=>'Sim');
                foreach($posts as $post){ 
            ?>
            <tr>
                <td><?PHP if($post['Produto']['imagem']) echo $this->Fill->image($post['Produto']['imagem'],120,80,'ffffff'); ?></td>
                <td><?PHP echo $post['Produto']['title']?></td>
                <td><?PHP echo number_format($post['Produto']['preco'], 0, '', '.'); ?></td>
                <td><?PHP echo $post['CategoriaProduto']['title']?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['Produto']['id']))?></td>
                <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['Produto']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>