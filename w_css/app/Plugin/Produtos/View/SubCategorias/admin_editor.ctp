<?PHP
echo $this->Form->create('SubCategoria',array('url'=>array('action'=>'add')));
?>

<fieldset class="box">
    <legend>Categoria:</legend>
    <?PHP
        echo $this->Form->input('categoria_id',array('required'=>'required','type'=>'select','options'=>$categorias,'label'=>false));
    ?>
</fieldset>

<fieldset class="box">
    <legend>Sub Categoria</legend>
    <?PHP
        echo $this->Form->input('title',array('label'=>'Título'));    
    ?>
</fieldset>

<?PHP
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>