<div class="bg-gray bgpb">
    <section class="container-fix top-content">
        <?PHP
            $this->Html->addCrumb('Inicial','/'); 
            $this->Html->addCrumb('Carrinho',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index')); 
            echo $this->element('breadcrumb');

            echo '<h1 class="h1p">Carrinho de troca</h1>';
        ?>
        
        <div class="row">
            <div class="row cabecalho-produto">
                <div class="seven columns">
                    Produto
                </div>
                <div class="three columns qtd">
                    Quantidade
                </div>
                <div class="two columns opc">
                    &nbsp;
                </div>
            </div>
            <?PHP
                foreach ($produtos as $produto) {
//                    pre($produto);
                    if($produto['Produto']['imagem']){
                        if(file_exists('img/media_cache/fill/100x60/ffffff/'.$produto['Produto']['imagem'].'')){
                            $image = $this->Html->image('media_cache/fill/100x60/ffffff/'.$produto['Produto']['imagem'].'',array('alt'=>$produto['Produto']['title']));
                        }else{
                            $image = $this->Fill->image($produto['Produto']['imagem'],100,60,'ffffff',array('alt'=>$produto['Produto']['title']));
                        }
                    }else{
                        $image = '';
                    }

                    echo '<div class="row list-orcamento">';
                        echo '<div class="seven columns title-produto">';
                            echo $image.' <span>'.$produto['Produto']['title'].' - <b class="pontos">'.$produto['Produto']['preco'].' pts</b></span>';
                        echo '</div>';
                        echo '<div class="three columns quantidade">';
                            echo '<div class="row">';
                                echo '<div class="seven columns left-qtd">';
                                    echo $this->Form->input('qntd',array('id'=>'qtd_'.$produto['ProdutoSessao']['id'],'label'=>false,'div'=>false,'value'=>$produto['ProdutoSessao']['quantidade'],'readonly'=>true,'maxlength'=>2));
                                echo '</div>';
                                echo '<div class="five columns right-qtd">';
                                    echo '<div class="row">';
                                        echo $this->Html->link('<i class="fas fa-angle-up"></i>','javascript:void(0)',array('escape'=>false,'class'=>'arrowQrd qtd_up','id'=>$produto['ProdutoSessao']['id'],'data-pi'=>$produto['Produto']['id'],'data-sum'=>'1'));
                                    echo '</div>';
                                    echo '<div class="row">';
                                        echo $this->Html->link('<i class="fas fa-angle-down"></i>','javascript:void(0)',array('escape'=>false,'class'=>'arrowQrd qtd_down','id'=>$produto['ProdutoSessao']['id'],'data-pi'=>$produto['Produto']['id'],'data-sum'=>'0'));
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        echo '</div>';
                        echo '<div class="two columns delete">';
                            echo $this->Html->link('<i class="fas fa-window-close"></i>',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'delete_orc','idp'=>$produto['ProdutoSessao']['id']),array('escape'=>false,'title'=>'Remover Produto'));
                        echo '</div>';
                    echo '</div>';
                }
            ?>
            
            <div class="row row-form-orc">
                <p>Verifique corretamente seus produtos antes de solicitar a troca dos mesmos.</p>
                <?PHP
//                    pre($this->Session->read('Cliente'));
                    
                    echo $this->Form->create('Orcamento',array('class'=>'formc','url'=>array('plugin'=>'produtos','controller'=>'orcamento','action'=>'send')));
                    
                        echo $this->Form->hidden('sessao_id',array('value'=>$_SESSION['SessaoOrcamento']));
                        echo $this->Form->hidden('cliente_id',array('value'=>$this->Session->read('Cliente.id')));
                        echo $this->Form->hidden('cnpj',array('value'=>$this->Session->read('Cliente.cnpj')));
                        echo $this->Form->hidden('nome_fantasia',array('value'=>$this->Session->read('Cliente.nome_fantasia')));
                        echo $this->Form->hidden('nome',array('value'=>$this->Session->read('Cliente.nome')));
                        echo $this->Form->hidden('cpf',array('value'=>$this->Session->read('Cliente.cpf')));
                        echo $this->Form->hidden('sexo',array('value'=>$this->Session->read('Cliente.sexo')));
                        echo $this->Form->hidden('data_nascimento',array('value'=>$this->Session->read('Cliente.data_nascimento')));
                        echo $this->Form->hidden('telefone1',array('value'=>$this->Session->read('Cliente.telefone1')));
                        echo $this->Form->hidden('telefone2',array('value'=>$this->Session->read('Cliente.telefone2')));
                        echo $this->Form->hidden('cep',array('value'=>$this->Session->read('Cliente.cep')));
                        echo $this->Form->hidden('cidade',array('value'=>$this->Session->read('Cliente.cidade')));
                        echo $this->Form->hidden('estado',array('value'=>$this->Session->read('Cliente.estado')));
                        echo $this->Form->hidden('endereco',array('value'=>$this->Session->read('Cliente.endereco')));
                        echo $this->Form->hidden('bairro',array('value'=>$this->Session->read('Cliente.bairro')));
                        echo $this->Form->hidden('numero',array('value'=>$this->Session->read('Cliente.numero')));
                        echo $this->Form->hidden('complemento',array('value'=>$this->Session->read('Cliente.complemento')));
                        echo $this->Form->hidden('email',array('value'=>$this->Session->read('Cliente.email')));
                        
                        echo $this->Form->submit('Enviar solicitação de troca',array('div'=>false,'class'=>'bs-orc'));
                        echo '<div class="row msg-send-form"> Enviando dados...</div>';
                    echo $this->Form->end(); 
                ?>
            </div>
            
        </div>
    </section>
</div>


<a class="popup-loader" href="<?PHP echo $this->base ?>/load"></a>
