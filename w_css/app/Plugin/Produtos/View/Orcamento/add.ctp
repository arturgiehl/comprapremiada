<div class="popup-add-product">
    <?PHP 
        if($ponto){
            echo '<p class="p-pontos">Você não tem pontos suficientes para trocar este produto, verifique seu carrinho e sua pontuação.</p>';
        }else{
            if($add){
                echo '<p>Produto já está no carrinho de troca</p>';
            }else{
                echo '<p class="padd">Produto adicionado ao carrinho de troca</p>';
            }
        }
        
        echo $this->Html->link('<i class="fas fa-shopping-cart"></i> Ver meu carrinho',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index'),array('escape'=>false,'class'=>'viewc')); 
        echo $this->Html->link('<i class="fas fa-window-close"></i> Fechar','javascript:void(0)',array('escape'=>false,'class'=>'close')); 
    ?>
</div>

<script>
    $(document).ready(function(){
        $('.close').click(function(){
            window.history.pushState('', '/', window.location.pathname);
            $.magnificPopup.close(); 
        });
    });
</script>