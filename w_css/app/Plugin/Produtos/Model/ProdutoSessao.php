<?PHP
class ProdutoSessao extends ProdutosAppModel {
    
    public $useTable  = 'produto_session';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(ProdutoSessao.created,'%d/%m/%Y')",
        'mdate'=>"DATE_FORMAT(ProdutoSessao.modified,'%d/%m/%Y')",
    );
    
}