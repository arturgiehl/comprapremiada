<?PHP
class SubCategoria extends ProdutosAppModel{

    var $useTable = 'produtos_sub_categorias';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(SubCategoria.created,'%d/%m/%Y')",
        'mdate'=>"DATE_FORMAT(SubCategoria.modified,'%d/%m/%Y')",
    );
    
    public $validate = array(
        'title'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
    );
    
    public $actsAs = array(
        'Painel.Slug'=>array('title'=>'slug'),
    );
    
    public $belongsTo = array(
        'Categoria' => array(
            'className' => 'Produtos.CategoriaProduto',
            'foreignKey' => 'categoria_id',
            'fields' => array('id','title')
        ),
    );    
    
}