<?PHP
class Produto extends ProdutosAppModel{
    
    var $name = 'Produto';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Produto.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Produto.modified,'%d/%m/%Y %H:%i')",
        'ddata'=>"DATE_FORMAT(Produto.created,'%d/%m/%Y')",
    );
    
    public $validate = array(
        'title'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
    );
    
    public $actsAs = array(
        'Painel.Gallery',
        'Painel.Videos',
        'Painel.Downloads',
        'Painel.Slug'=>array('title'=>'slug'),
        'Searchable'=>array('title','texto','preco')
    );
    
    public $belongsTo = array(
        'CategoriaProduto'=>array(
            'className'=>'Produtos.CategoriaProduto',
            'foreignKey'=>'cat_id'
        ),
    ); 
    
}