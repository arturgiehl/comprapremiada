<?PHP
class Orcamento extends ProdutosAppModel {
    
    public $useTable  = 'pedidos_troca';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Orcamento.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Orcamento.modified,'%d/%m/%Y %H:%i')",
    );
    
//    public $validate=array(
//        'nome'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
//        'email'=>array('rule'=>'email','message'=>'Não é um e-mail válido'),
//        'telefone'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
//        'mensagem'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
//    );
    
}