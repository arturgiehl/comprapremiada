<?PHP
class ProdutosAppController extends AppController{
    
    public $uses = array(
                    'Produtos.Produto',
                    'Produtos.CategoriaProduto',
                    'Produtos.ProdutoSessao',
                    'Produtos.Orcamento',
                    'Produtos.SubCategoria',
                    'Clientes.Cliente',
                );
    
}