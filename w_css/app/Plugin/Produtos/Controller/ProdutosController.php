<?PHP
class ProdutosController extends ProdutosAppController {
    
    public $paginate = array('limit'=>30,'order'=>array('created'=>'DESC'));
        
    public function index(){
        $categoria = '';
        if(isset($this->params->categoria) && isset($this->params->subslug)){
            $sub_categoria = $this->SubCategoria->find('first',array('conditions'=>array('SubCategoria.slug'=>$this->params->subslug)));
//            pre($sub_categoria);
            if($sub_categoria){
                $this->Seo->title($sub_categoria['SubCategoria']['title']);
                
                $this->paginate['limit'] = 24;
                if(isset($this->params->page)){ 
                    $this->paginate['page'] = $this->params->page;   
                }
                $this->paginate['order'] = array('Produto.created'=>'DESC');
                $this->paginate['conditions'] = array('Produto.cat_id'=>$sub_categoria['Categoria']['id'],'Produto.subcategoria_id'=>$sub_categoria['SubCategoria']['id']);
                $posts = $this->paginate('Produto'); 
            }
        }else if(isset($this->params->categoria)){
            $categoria = $this->CategoriaProduto->find('first',array('conditions'=>array('CategoriaProduto.slug'=>$this->params->categoria)));
            if($categoria){
                $this->Seo->title($categoria['CategoriaProduto']['title']);
                
                $this->paginate['limit'] = 24;
                if(isset($this->params->page)){ 
                    $this->paginate['page'] = $this->params->page;   
                }
                $this->paginate['order'] = array('Produto.created'=>'DESC');
                $this->paginate['conditions'] = array('Produto.cat_id'=>$categoria['CategoriaProduto']['id']);
                $posts = $this->paginate('Produto'); 
            }
        }else{
            $this->Seo->title('Produtos');
            
            if(isset($this->params->page)){ 
                $this->paginate['page'] = $this->params->page;   
            }
            $this->paginate['limit'] = 88;
//            $this->paginate['conditions'] = array('Produto.ativo'=>'1');
            $this->paginate['order'] = array('Produto.created'=>'DESC');
            $posts = $this->paginate('Produto'); 
        }
        $this->set('posts',$posts);    
        $this->set('categoria',$categoria);    
        
        $this->set('list_categorias',$this->CategoriaProduto->find('all',array('order'=>array('title ASC'))));
    }
    
    public function view(){
        $slug = $this->params->slug;
        $post = $this->Produto->find('first',array('conditions'=>array('Produto.slug'=>$slug)));
        if($post){
            $this->set('post',$post);
        
            $this->Seo->title($post['Produto']['title']);
            $this->set('og_title',$post['Produto']['title']); 
            if($post['Produto']['texto']){
                $description = $post['Produto']['texto'];
                if((strlen($post['Produto']['texto'])) > 144){
                    $description = substr($post['Produto']['texto'],0, 144).'...';
                }
                $this->Seo->description(strip_tags(html_entity_decode($description)));
                $this->set('og_description',strip_tags(html_entity_decode($description))); 
            }
            if($post['Produto']['imagem']){
                $this->set('og_image',$post['Produto']['imagem']); 
            }
        }else{
            $this->redirect(array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'));
        }
        
//        $outras = $this->Produto->find('all',array('conditions'=>array('Produto.id != "'.$post['Produto']['id'].'"'),'order'=>'RAND()','limit'=>3));
//        $this->set('outras',$outras);
    }
    
    public function admin_index($tipo = null, $filter = null){
        $this->layout = 'Painel.admin';
        
        if($tipo === 'c' && $filter) {
            $this->paginate['conditions'] = array('Produto.cat_id' => $filter);
        }

        if($tipo == 'd' && $filter) {
            if($filter === 'n'){
                $this->paginate['conditions'] = array('Produto.destaque' => "0");
            }else{
                $this->paginate['conditions'] = array('Produto.destaque' => $filter);
            }
        }
        
        if(isset($_GET['s']) && $_GET['s']){
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Produto.title LIKE' => "%".$_GET['s']."%", 
                                                                'Produto.texto LIKE' => "%".$_GET['s']."%",
                                                                'Produto.preco LIKE' => "%".$_GET['s']."%",
                                                            ));
            $this->paginate['limit'] = 90;
        }
        
        $posts = $this->paginate('Produto');
        $this->set('posts',$posts);
        $this->set('categorias',$this->CategoriaProduto->find('all',array('order'=>'CategoriaProduto.title ASC')));
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        
        $categorias[''] = 'Selecione a categoria';
        $categorias[] = $this->CategoriaProduto->find('list',array('fields'=>array('id','title'),'order'=>array('title ASC')));
        $this->set('categorias',$categorias);
        
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            
            if(isset($categorias[0])){
                $this->request->data['Produto']['categoria'] = $categorias[0][$this->data['Produto']['cat_id']];
            }
            
            if($this->Produto->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        $registro = $this->Produto->read('*',$id);
        
        $categorias[''] = 'Selecione a categoria';
        $categorias[] = $this->CategoriaProduto->find('list',array('fields'=>array('id','title'),'order'=>array('title ASC')));
        
        $sub_categorias[''] = 'Sub Categoria';
        $sub_categorias[] = $this->SubCategoria->find('list',array(
                                                                'conditions' => array('SubCategoria.categoria_id' => $registro['Produto']['cat_id']),
                                                                'fields' => array('id','title'),
                                                                'order' => array('title ASC')
                                                        ));
        
        $this->set('categorias',$categorias);
        $this->set('sub_categorias',$sub_categorias);
        
        $this->data = $registro; 
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->Produto->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }   
    
    public function change_value($valor){
        if($valor){
            $value_bd = str_replace('.', '', trim($valor));
            $value_bd = str_replace(',', '.', $value_bd);
            return $value_bd;
        }
    }
    
    public function admin_get_sub_categorias(){
        $this->autoRender = true;
        $this->layout = false;
        
        $sub_categorias[''] = 'Sub Categoria';
        $sub_categorias[] = $this->SubCategoria->find('list',array(
                                                                'conditions' => array('SubCategoria.categoria_id' => $_POST['catid']),
                                                                'fields' => array('id','title'),
                                                                'order' => array('title ASC')
                                                        ));
        $this->set('sub_categorias',$sub_categorias);
    }
    
}