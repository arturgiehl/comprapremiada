<?PHP
class OrcamentoController extends ProdutosAppController{
    
    public $paginate = array('limit'=>10,'order'=>'id DESC');
    
    public function add(){
        $this->autoRender = true;
        $this->layout = false;
        $add = true;
        $ponto = false;
        
        if($this->Session->read('Cliente')){
            if(!$this->Session->check('SessaoOrcamento')){
                $session_id = $this->cramdom();
               $this->Session->write('SessaoOrcamento',$session_id); 
            }

            if(isset($this->params->idp) && $this->params->idp){
                $produto = $this->Produto->find('first',array('conditions'=>array('Produto.id'=>$this->params->idp)));

//                $total_pontos = $this->Produto->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0" ;');
                $total_notas = $this->Produto->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0" ;');
                $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$this->Session->read('Cliente.id'))));
                $total_session = $this->Produto->query('SELECT SUM(pontos * quantidade) AS TOTAL FROM tb_produto_session WHERE cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND session_id = "'.$_SESSION['SessaoOrcamento'].'" ;');
                
                $total_pontos = $total_notas[0][0]['TOTAL'] - $cliente['Cliente']['pontos_trocados'];
                
                $total_check = $total_session[0][0]['TOTAL'] + $produto['Produto']['preco'];
//                $total_check = number_format($total_session[0][0]['TOTAL'] + $produto['Produto']['preco'], 0, '', '.');
//                pre($total_pontos);

                if($total_check > $total_pontos){
                    $ponto = true;
                }else{
                    $verifica = $this->Orcamento->query('SELECT COUNT(*) AS TOTAL FROM tb_produto_session WHERE produto_id = "'.$this->params->idp.'" AND session_id = "'.$_SESSION['SessaoOrcamento'].'"');
                    if($verifica[0][0]['TOTAL'] == 0){
                        $produto_sessao['ProdutoSessao'] = array(
                                                                'id' => '',
                                                                'session_id' => $_SESSION['SessaoOrcamento'],
                                                                'produto_id' => $this->params->idp,
                                                                'cnpj' => $this->Session->read('Cliente.cnpj'),
                                                                'quantidade' => '1',
                                                                'pontos' => $produto['Produto']['preco'],
                                                            );
                        if($this->ProdutoSessao->save($produto_sessao)){
                            $add = false;
                        }
                    }
                }
            }
        }
        
        $this->set('add',$add);
        $this->set('ponto',$ponto);
    }
    
    public function delete_orc(){
        $this->autoRender = false;
        if($this->ProdutoSessao->delete($this->params->idp)){ 
            $this->redirect(array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index'));
        }
    }
    
    public function qtdeup(){
        $this->autoRender = true;
        $this->layout = false;
        $retorno['msg'] = '';
        $retorno['ponto'] = '0';
        $quantidade = $_GET['quantidade'];
        
        $produto = $this->Produto->find('first',array('conditions'=>array('Produto.id'=>$_GET['produtoId'])));
//        $total_pontos = $this->Produto->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0" ;');
//        $total_session = $this->Produto->query('SELECT SUM(pontos * quantidade) AS TOTAL FROM tb_produto_session WHERE cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND session_id = "'.$_SESSION['SessaoOrcamento'].'" ;');
//        $total_check = $total_session[0][0]['TOTAL'] + $produto['Produto']['preco'];

        $total_notas = $this->Produto->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0" ;');
        $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$this->Session->read('Cliente.id'))));
        $total_session = $this->Produto->query('SELECT SUM(pontos * quantidade) AS TOTAL FROM tb_produto_session WHERE cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND session_id = "'.$_SESSION['SessaoOrcamento'].'" ;');
        $total_pontos = $total_notas[0][0]['TOTAL'] - $cliente['Cliente']['pontos_trocados'];
        $total_check = $total_session[0][0]['TOTAL'] + $produto['Produto']['preco'];
        
        if($total_check > $total_pontos){
            $retorno['ponto'] = '1';
        }else{
            if($_GET['sum'] === '0'){
                if($_GET['quantidade'] > 1){
                    $quantidade = $_GET['quantidade'] - 1;
                }else{
                    $quantidade = 1;
                }
            }else{
                $quantidade = $_GET['quantidade'] + 1;
            }
            
            $this->ProdutoSessao->query('UPDATE tb_produto_session SET quantidade = "'.$quantidade.'"  WHERE id = "'.$_GET['produtoSessaoId'].'" ');
        }
        
        $retorno['quantidade'] = $quantidade;
        $this->set('retorno',$retorno);
//        $this->set('quantidade',$quantidade);
    }
    
    public function loader(){
        $this->layout = false;
    }
    
    private function cramdom(){
        $novo_valor= "";
        $valor = "abcdefghijklmnopqrstuvwxyz0123456789";
        srand((double)microtime()*1000000);
        for ($i = 0; $i < 10; $i++){
            $novo_valor.= $valor[rand()%strlen($valor)];
        }
        $novo_valor .= $novo_valor.date('dmYHis');
        return $novo_valor;
    }

    public function index($id = null){
        $this->Seo->title('Meu Carrinho');
        
        if(!$this->Session->check('SessaoOrcamento')){
           $session_id = $this->cramdom();
           $this->Session->write('SessaoOrcamento',$session_id); 
        }
        
        $this->Produto->recursive = -1;
        $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        Produto.preco,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$_SESSION['SessaoOrcamento'].'"');
        
        if(count($produtos_session) > 0){
            $this->set('produtos',$produtos_session);
        }else{
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<script type="text/javascript">';
            echo 'alert("ATENÇÃO: Nenhum produto adicionado ao carrinho");';
            echo 'window.location.href="'.$this->base.'/produtos";';
            echo '</script>';
            exit;
        }
        
    }

    public function send(){
        $this->Seo->title('Enviar Orçamento');
        
        if($this->data && $this->request->is('post')){
            $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        Produto.preco,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$_SESSION['SessaoOrcamento'].'"');
            
            $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$this->Session->read('Cliente.id'))));
            $total_pontos = $cliente['Cliente']['pontos_trocados'];
            
            foreach ($produtos_session as $ps) {
                $total_pontos = $total_pontos + $ps['Produto']['preco'] * $ps['ProdutoSessao']['quantidade'];
            }
//            pre($total_pontos);
            $save_ponto['Cliente'] = array(
                                        'id' => $this->data['Orcamento']['cliente_id'],
                                        'pontos_trocados' => $total_pontos
                                    );
            $this->Cliente->save($save_ponto);
            
            if($this->Orcamento->save($this->data)){
                $this->send_mail($this->data,$produtos_session);
                $this->Session->delete('SessaoOrcamento');
                $this->set('add_sucess',true);
            }else{
                $this->message_empty('ERRO: Não foi possível enviar seus dados, tente novamente mais tarde'); 
            }
        }
    }
    
    private function send_mail($data,$produtos){
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtp');
        $mail->template('Contatos.pedido');
  
        $mail->to('web@amexcom.com.br');
//        $mail->bcc('web@amexcom.com.br');
        
        $mail->replyTo($data['Orcamento']['email']);
        $mail->emailFormat('html');
        $mail->subject('Solicitação de troca - '.date('d/m/Y - H:i').'');
        $mail->viewVars(array('data'=>$data['Orcamento'],'produtos'=>$produtos));
        return $mail->send();
    }
   

//ADMIN ==========================================================================================================================================
    public function admin_index(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = 'Orcamento.created DESC';
        $this->set('posts',$this->paginate('Orcamento'));
    }
    
    public function admin_view($id){
        $this->layout = 'Painel.admin';
        $post = $this->Orcamento->read('*',$id);
        
        $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$post['Orcamento']['sessao_id'].'"');
        
        $this->set('post',$post);
        $this->set('produtos',$produtos_session);
    }
    
    public function admin_delete($id){
        $this->autoRender=false;
        if($this->Orcamento->delete($id)) $this->redirect(array('action'=>'index'));
    }
    
}