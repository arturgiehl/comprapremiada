<?PHP
class NewslettersController extends ContatosAppController{
    
    public $paginate = array('limit'=>10,'order'=>'id DESC');
    
    public function index(){
        $this->Seo->title('Newsletter');
        if($this->data && $this->request->is('post')){
            
            if($this->data['Newsletter']['nome'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo Nome.'); 
            }else if($this->data['Newsletter']['email'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo E-mail.'); 
            }
            
//            $count_email = $this->Newsletter->find('count',array('conditions'=>array('Newsletter.email'=>$this->data['Newsletter']['email'])));
//            if($count_email > 0){
//                $this->message_empty('ATENÇÃO: Seu e-mail já está cadastrado em nossa base de dados.'); 
//            }
            
            if($this->Newsletter->validates($this->data)){
                if($this->Newsletter->save($this->data)){
                    $this->set('add_sucess',true);
                } else {
                    $this->message_empty("Não foi possível enviar seus dados, tente novamente mais tarde"); 
                }
            }
        }
    }
        
    public function admin_index(){
        $this->layout="Painel.admin";
        $this->paginate['order']='Newsletter.created DESC';
        $this->set('total',$this->Newsletter->find('count'));
        $this->set('posts',$this->paginate('Newsletter'));
    }
    
    public function admin_view($id){
        $this->layout="Painel.admin";
        $this->set('post',$this->Newsletter->read('*',$id));
    }
    
    public function admin_delete($id){
        $this->autoRender=false;
        if($this->Newsletter->delete($id)) $this->redirect(array('action'=>'index'));
    }
    
    public function message_empty($text){
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<script type="text/javascript">';
        echo 'alert("'.$text.'");';
        echo 'history.go(-1);';
        echo '</script>';
        exit; 
    }
    
    public function admin_report(){
        $this->autoRender=false;
        $dados = $this->Newsletter->find('all', array('order'=>'Newsletter.created DESC') );
    
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: application/octet-stream");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment; filename=\"Newsletter Fipal - ".date('d-m-Y H:i').".csv\";" );
        header("Content-Transfer-Encoding: binary");
        echo "\xEF\xBB\xBF";
        echo "Nome;E-mail;Telefone;Fipal mais próxima;\n";
        foreach($dados as $dado){
            echo $dado['Newsletter']['nome'].';'.$dado['Newsletter']['email'].';'.$dado['Newsletter']['telefone'].';'.$dado['Newsletter']['fipal_proxima']."\n";
        }
    }
    
}