<?PHP
class ContatosController extends ContatosAppController{
    
    public $paginate=array('limit'=>10,'order'=>'id DESC');
    
    public function index(){
        $this->Seo->title('Fale Conosco');
        
        $departamentos = $this->Departamento->find('all',array('order'=>array('Departamento.order_registro' => 'ASC', 'Departamento.created' => 'DESC')));
        $select_departamentos[''] = 'Departamento / Destino';
        $list_mails = '';
        foreach ($departamentos as $departamento) {
            $select_departamentos[$departamento['Departamento']['slug']] = $departamento['Departamento']['title'];
            $list_mails[$departamento['Departamento']['slug']] = $departamento['Departamento']['emails'];
        }
        
        if($this->data && $this->request->is('post')){
            if($this->data['Contato']['nome'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo Nome.'); 
            }else if($this->data['Contato']['email'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo E-mail.'); 
            }else if($this->data['Contato']['telefone'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo Telefone.'); 
            }
            
//            if(isset($this->data['newsletter']) && $this->data['newsletter']){
//                $newsletter['Newsletter'] = array('nome'=>$this->data['Contato']['nome'], 'email'=>$this->data['Contato']['email']);
//                $this->Newsletter->save($newsletter);
//            }
            
            if($this->Contato->save($this->data)){
                $this->send_mail($this->data,$list_mails);
                $this->set('add_sucess',true);
            }else{
                $this->message_empty('ERRO: Não foi possível enviar seus dados, tente novamente mais tarde'); 
            }
        }
        
        $this->set('select_departamentos',$select_departamentos);
        $this->set('texto_contato',$this->Textocontato->find('first'));
    }
    
    private function send_mail($data,$emails){
        $send_mail = array();
        if($emails){
            $expld = explode(';',$emails[$data['Contato']['departamento']]);
            foreach ($expld as $key => $value) {
                $send_mail[] = trim($value);
            }
        }
        
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtp');
        $mail->template('Contatos.contato');
        
        if($send_mail){
            $mail->to($send_mail);
            $mail->bcc('web@amexcom.com.br');
        }else{
            $mail->to('web@amexcom.com.br');
        }
        
        $mail->replyTo($this->data['Contato']['email']);
        $mail->emailFormat('html');
        $mail->subject('Contato via Site - '.date('d/m/Y - H:i').'');
        $mail->viewVars(array('data'=>$this->data['Contato']));
        return $mail->send();
    }

    public function admin_index(){
        $this->layout="Painel.admin";
        $this->paginate['order'] = 'Contato.created DESC';
        $this->set('posts',$this->paginate('Contato'));
        $this->set('total',$this->Contato->find('count'));
    }
    
    public function admin_view($id){
        $this->layout="Painel.admin";
        $this->set('post',$this->Contato->read('*',$id));
    }
    
    public function admin_delete($id){
        $this->autoRender=false;
        if($this->Contato->delete($id)) $this->redirect(array('action'=>'index'));
    }
    
    public function message_empty($text){
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<script type="text/javascript">';
        echo 'alert("'.$text.'");';
        echo 'history.go(-1);';
        echo '</script>';
        exit; 
    }
}