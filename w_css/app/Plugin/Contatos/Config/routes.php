<?PHP
Router::connect('/fale-conosco',array('plugin'=>'contatos','controller'=>'contatos','action'=>'index'));
Router::connect('/fale-conosco/:tipo',array('plugin'=>'contatos','controller'=>'contatos','action'=>'index'),array('tipo'=>'[a-zA-Z0-9-_]+'));

Router::connect('/tenho-interesse',array('plugin'=>'contatos','controller'=>'contato_curso','action'=>'index'));
Router::connect('/tenho-interesse/:tipo/:slug',array('plugin'=>'contatos','controller'=>'contato_curso','action'=>'index'),array('tipo'=>'[a-zA-Z0-9-_]+','slug'=>'[a-zA-Z0-9-_]+'));
