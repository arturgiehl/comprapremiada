<?PHP
class Newsletter extends ContatosAppModel{
    
    public $useTable  = 'newsletter';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Newsletter.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Newsletter.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $validate = array(
        'nome'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'email'=>array('rule'=>'email','message'=>'Não é um e-mail válido'),
    );
    
    public function afterSave($created, $options = array()) {
        parent::afterSave($created);
        $date = date('d/m/Y - H:i');
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtp');
        $mail->template('Contatos.newsletter');
        
        $mail->to('web@amexcom.com.br');
//        $mail->bcc(array('web@amexcom.com.br'));

        $mail->emailFormat('html');
        $mail->subject("Receber Newsletter - $date");
        $mail->viewVars(array('data'=>$this->data['Newsletter']));
        return $mail->send();
    }
}