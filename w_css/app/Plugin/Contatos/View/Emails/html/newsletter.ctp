<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            body{
                width: 100%;
                font-family: Verdana, Geneva, sans-serif;
                font-size: 15px;
                border: 1px solid transparent;
                display: table;
            }
            .maintitle{
                width: 802px;
                height: 38px;
                padding-top: 12px;
                font-size:20px;
                color:#E5122A;
                font-weight:bold;	
                border-radius: 10px 10px 0px 0px;
                text-align: center;
            }
            #corpo {
                width: 800px;
                height: auto;
                display: table;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 10px;
            }
            .linha {
                width: 100%;
                height: auto;
                float: left;
                display: block;
                padding-bottom: 10px;
                margin-top: 10px;
                border-bottom: 1px dotted #e2dfd8;
            }
            .linha_ultima { border-bottom: 0px; }
            .titulo {
                width: 180px;
                float: left;
                text-align: right;
                color:#000;
            }
            .descricao {
                width: 600px;
                float: right;
                color: #7E7E7E;
            }
        </style>

    </head>
    <body>
        <div class="maintitle">Receber Newsletter</div>
        
        <table id="corpo">
            <tr class="linha">
                <td class="titulo">Nome:</td>
                <td class="descricao"><?PHP echo $data['nome']; ?></td>
            </tr>
            
            <tr class="linha linha_ultima">
                <td class="titulo">E-mail:</td>
                <td class="descricao"><?PHP echo $data['email']; ?></td>
            </tr>
        </table>
        <p><b>AMEXCOM</b></p>
    </body>
</html>