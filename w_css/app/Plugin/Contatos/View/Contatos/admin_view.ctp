<?PHP 
    $post = array_shift($post); 
    
    $ufs = array(
                ''=>'Estado', 'AC'=>'Acre', 'AL'=>'Alagoas', 'AP'=>'Amapá', 'AM'=>'Amazonas', 'BA'=>'Bahia', 'CE'=>'Ceará', 'DF'=>'Distrito Federal', 'ES'=>'Espírito Santo',
                'GO'=>'Goiás', 'MA'=>'Maranhão', 'MT'=>'Mato Grosso', 'MS'=>'Mato Grosso do Sul', 'MG'=>'Minas Gerais', 'PA'=>'Pará', 'PB'=>'Paraíba', 'PR'=>'Paraná',
                'PE'=>'Pernambuco', 'PI'=>'Piauí', 'RJ'=>'Rio de Janeiro', 'RN'=>'Rio Grande do Norte', 'RS'=>'Rio Grande do Sul', 'RO'=>'Rondônia', 'RR'=>'Roraima',
                'SC'=>'Santa Catarina', 'SP'=>'São Paulo', 'SE'=>'Sergipe', 'TO'=>'Tocantins'
            );
?>

<fieldset class="box">
    <legend>Dados</legend>    
    <dl class="description">
        <dt>Nome:</dt><dd><?PHP echo $post['nome']?></dd>
        <dt>E-mail:</dt><dd><?PHP echo $post['email']?></dd>
        <dt>Telefone:</dt><dd><?PHP echo $post['telefone']?></dd>
        <dt>Cidade:</dt><dd><?PHP echo $post['cidade']?></dd>
        <dt>Departamento:</dt><dd><?PHP echo $post['departamento']?></dd>
        <dt>Mensagem:</dt><dd><?PHP echo $post['mensagem']; ?></dd>
        <dt>&nbsp;</dt><dd>&nbsp;</dd>
        <dt>Enviado em:</dt><dd><?PHP echo $post['cdate']?></dd>
    </dl>    
</fieldset>
