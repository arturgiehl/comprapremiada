<section class="top-content">
    <div class="container-fix">
        <h1>Fale Conosco</h1>
    </div>
</section>

<?PHP if(isset($add_sucess) && $add_sucess){ ?>
    <div class="msg-add-sucess"> 
        Dados enviados com sucesso! <br>
        Alguém de nossa equipe logo entrará em contato com você!
    </div>
<?PHP }else{ ?>
    
<script async src="https://www.amexserver.com.br/dashboard/grabber/cdn/connect-1.0.0.js?key=a5438237d655bf08af4a8256c770f469"></script> 

<section class="container-fix">
    <?PHP
        $this->Html->addCrumb('Principal','/'); 
        $this->Html->addCrumb('Fale Conosco',array('plugin'=>'contatos','controller'=>'contatos','action'=>'index')); 
        echo $this->element('breadcrumb');
    ?>
    <div class="row low-bc">
        <div class="six columns">
            <?PHP
                $value_select = '';
                if(isset($this->params->tipo) && $this->params->tipo){
                    $value_select = $this->params->tipo;
                }
               
                echo $this->Form->create('Contato',array('class'=>'formc','url'=>array('plugin'=>'contatos','controller'=>'contatos','action'=>'index')));
                    echo $this->Form->input('nome',array('label'=>false,'placeholder'=>'Nome','div'=>false,'data-amex-input'=>'name'));
                    echo $this->Form->input('email',array('label'=>false,'placeholder'=>'E-mail','type'=>'email','div'=>false,'data-amex-input'=>'email'));
                    echo $this->Form->input('telefone',array('label'=>false,'placeholder'=>'Telefone','id'=>'telefone','div'=>false,'maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);','data-amex-input'=>'phone'));
                    echo $this->Form->input('cidade',array('label'=>false,'placeholder'=>'Cidade','div'=>false,'data-amex-input'=>'city'));
                    echo $this->Form->input('departamento',array('type'=>'select','options'=>$select_departamentos,'label'=>false,'required'=>'required','value'=>$value_select,'data-amex-input'=>'cf-departamento'));
                    echo $this->Form->input('mensagem',array('label'=>false,'placeholder'=>'Mensagem','div'=>false,'data-amex-input'=>'message'));
                    echo $this->Form->submit('ENVIAR',array('div'=>false,'class'=>'bs-contact'));
                    echo '<div class="row msg-send-form"> Enviando dados...</div>';
                echo $this->Form->end(); 
        ?>
        </div>
        <div class="six columns">
            <div class="row texto-contato">
                <?PHP echo $texto_contato['Textocontato']['texto']; ?>
            </div>
            <div class="row">
                <iframe id="locationmap" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14468.712723475803!2d-53.4525694!3d-24.9600517!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9bfa337f7b97c20f!2sTupi+Screen+Distribuidor+de+Produtos+para+Serigrafia+e+Comunica%C3%A7%C3%A3o+Visual!5e0!3m2!1spt-BR!2sbr!4v1535995898815" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>


<?PHP } ?>


<script>
    var element = document.getElementById("mn4");
    element.classList.add("m-active");
</script>