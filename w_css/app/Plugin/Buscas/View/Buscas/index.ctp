<div class="bg-gray">
    <section class="container-fix top-content">
        <?PHP
//            $this->Html->addCrumb('Inicial','/'); 
//            $this->Html->addCrumb('Produtos',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index')); 
//            echo $this->element('breadcrumb');
           
            echo '<h1 class="h1p">Busca</h1>';
        ?>
        
        <div class="row low-bc">
            <div class="three columns menu-ln">
                <p class="tfilter">Categorias  <i class="fas fa-angle-down"></i></p>
                <div class="row box-categorias">
                    <?PHP 
                        foreach ($list_categorias as $mcategoria) {
    //                        pre($mcategoria);
                            $as_active = '';
    //                        pre($this->params->subslug);
                            if(isset($this->params->categoria) && $mcategoria['CategoriaProduto']['slug'] === $this->params->categoria && !isset($this->params->subslug)){
                                $as_active = 'as-active';
                            }

                            echo $this->Html->link($mcategoria['CategoriaProduto']['title'].' <i class="fas fa-angle-right"></i>',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index','categoria'=>$mcategoria['CategoriaProduto']['slug']),array('escape'=>false,'class'=>$as_active));
                        }
                    ?>
                </div>
            </div>
            <div class="nine columns">
                <?PHP
                    $count = 1;
                    foreach ($posts as $produto) {
                        if($produto['Produto']['imagem']){
                            if(file_exists('img/media_cache/fill/265x160/ffffff/'.$produto['Produto']['imagem'].'')){
                                $image = $this->Html->image('media_cache/fill/265x160/ffffff/'.$produto['Produto']['imagem'].'',array('alt'=>$produto['Produto']['title']));
                            }else{
                                $image = $this->Fill->image($produto['Produto']['imagem'],265,160,'ffffff',array('alt'=>$produto['Produto']['title']));
                            }
                        }else{
                            $image = $this->Html->image('noimg265x160.jpg',array('alt'=>'Imagem indisponível'));
                        }

                        if($count === 1) echo '<div class="row">';
                            echo '<div class="four columns produto-list">';
                                echo $this->Html->link($image,array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false));
                                echo $this->Html->link($produto['Produto']['title'],array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false,'class'=>'title'));
                                echo $this->Html->link($produto['Produto']['preco'],array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false,'class'=>'price'));
                                echo '<div class="row">';
                                    echo $this->Html->link('<i class="fas fa-plus"></i> Mais detalhes',array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false,'class'=>'pmore'));
//                                    echo '<div class="six columns">';
//                                        
//                                    echo '</div>';
//                                    echo '<div class="six columns">';
//                                        echo $this->Html->link('<i class="fas fa-plus"></i> Trocar',array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false,'class'=>'pmore'));
//                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        if($count === 3){
                            echo '</div>';
                            $count = 0;
                        }
                        $count++;
                    }
                    if($count > 1 && $count <=3) echo '</div>';

                    echo $this->element('Painel.paginator'); 
                ?>
            </div>

        </div>
    </section>
</div>

<script>
    var element = document.getElementById("mn2");
    element.classList.add("m-active");
</script>