<fieldset class="box">
    <legend>Buscas (<?PHP echo $total; ?>)</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Gerar CSV',array('plugin'=>'buscas','controller'=>'buscas','action'=>'report','admin'=>true)) ?>
    </nav>
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('title','Busca')?></td>
                <td><?PHP echo $this->Paginator->sort('ip','IP')?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Data')?></td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP foreach($posts as $post){ ?>
            <tr>
                <td><?PHP echo $post['Busca']['title']?></td>
                <td><?PHP echo $post['Busca']['ip']?></td>
                <td><?PHP echo $post['Busca']['cdate']?></td>
                <!--<td><?PHP // echo $this->Html->link('Excluir',array('action'=>'delete',$post['Busca']['id']),false,'Tem certeza?')?></td>-->
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>
