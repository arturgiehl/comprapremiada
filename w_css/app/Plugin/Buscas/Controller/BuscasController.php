<?PHP
class BuscasController extends BuscasAppController{

    public $paginate = array('limit'=>16,'order'=>array('Busca.order_depoimento' => 'ASC', 'Busca.created' => 'DESC'));
    
    public function open(){
        $this->autoRender = true;
        $this->layout = false;
    }

    public function index(){
        $this->Seo->title('Busca');
        
        $busca = strip_tags($_GET['s']);

        $save['Busca'] = array('id'=>'', 'title'=>$busca, 'ip'=>$this->getIP());
        $this->Busca->save($save);

        $posts = $this->Produto->search($busca);
        
        $this->set(compact('busca','posts'));
        $this->set('list_categorias',$this->CategoriaProduto->find('all',array('fields'=>array('slug','title'),'order'=>array('title ASC'))));
    }
    
    public function admin_index(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('Busca.order_depoimento' => 'ASC', 'Busca.created' => 'DESC');
        $posts = $this->paginate('Busca');
        $this->set('posts',$posts);
        $this->set('total',$this->Busca->find('count'));
    }
    
    public function getIP(){  
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  
            $ip = $_SERVER['HTTP_CLIENT_IP'];  
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){  
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
        }else  {  
            $ip = $_SERVER['REMOTE_ADDR'];  
        }  
        return $ip;  
    }
    
    public function admin_report(){
        $this->autoRender = false;
        $dados = $this->Busca->find('all', array('order'=>'Busca.created DESC') );
        
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: application/octet-stream");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment; filename=\"Buscas - ".date('d-m-Y').".csv\";" );
        header("Content-Transfer-Encoding: binary");
        echo "\xEF\xBB\xBF";
        echo "Busca;IP;Data;\n";
        foreach($dados as $dado){
            echo $dado['Busca']['title'].';'.$dado['Busca']['ip'].';'.$dado['Busca']['cdate']."\n";
        }
    }
    
}