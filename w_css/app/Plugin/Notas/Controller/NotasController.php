<?PHP
class NotasController extends NotasAppController{
    
    public $paginate = array('limit'=>44,'order'=>array('Nota.order_registro'=>'ASC','Nota.created'=>'DESC'));
    
    public function import(){
        $this->autoRender = false;
        $filiais = array('FIAT_FP001_EMI','FIAT_FP002_EMI','FIAT_FP003_EMI','FIAT_FP004_EMI','FIAT_FP005_EMI','FIAT_FP006_EMI','FIAT_FP007_EMI','FIAT_FP009_EMI','JEEP_FP001_EMI','JEEP_FP002_EMI');
        
//        $truncate = $this->Nota->query('TRUNCATE TABLE tb_notas_emitidas;');
  
        foreach ($filiais as $filial) {
//            pre($filial);
            $count_line = 1;
            $arquivo = fopen('http://comprapremiadafipal.com.br/notas/'.$filial.'.txt', 'r');
 
            while(!feof($arquivo)){
                $linha = fgets($arquivo, 1024);
//                echo $linha.'<br/>';
                
                if($count_line > 2){
                    $expline = explode(',', $linha);
//                    pre($expline);
                    
                    if(count($expline) > 2){
                        $nota['Nota'] = array(
                                        'id' => '',
                                        'filial' => $filial,
                                        'cliente_codigo' => trim($expline[0]),
                                        'cliente_nome' => utf8_encode(trim($expline[1])),
                                        'cliente_cnpj' => trim($expline[2]),
                                        'natureza_codigo' => trim($expline[3]),
                                        'natureza_descricao' => utf8_encode(trim($expline[4])),
                                        'nf_numero' => trim($expline[5]),
                                        'data_emissao' => trim($expline[6]),
                                        'valor_nf' => trim($expline[7]),
                                        'cancelada' => 0,
                                    );
                        $this->Nota->save($nota);
                    }else{
                        break;
                    }
                }
                
                $count_line++;
            }

            fclose($arquivo);
        }
        
        echo 'ok!';
    }
    
    public function import_cancel(){
        $this->autoRender = false;
        $filiais = array('FIAT_FP001_CAN','FIAT_FP002_CAN','FIAT_FP003_CAN','FIAT_FP004_CAN','FIAT_FP005_CAN','FIAT_FP006_CAN','FIAT_FP007_CAN','FIAT_FP009_CAN','JEEP_FP001_CAN','JEEP_FP002_CAN');
  
        foreach ($filiais as $filial) {
            $count_line = 1;
            $arquivo = fopen('http://comprapremiadafipal.com.br/notas/'.$filial.'.txt', 'r');
 
            while(!feof($arquivo)){
                $linha = fgets($arquivo, 1024);
                
                if($count_line > 2){
                    $expline = explode(',', $linha);

                    if(count($expline) > 1){
                        echo $expline[5]. '<br>';
                        
                        $nota_emitida = $this->Nota->find('first',array('conditions'=>array('Nota.nf_numero'=>$expline[5])));
                        if(count($nota_emitida) > 0){
                            $update_nota['Nota'] = array('id'=>$nota_emitida['Nota']['id'], 'cancelada'=>1);
                            $this->Nota->save($update_nota);
                        }
                        $nota_cancelada['NotaCancelada'] = array(
                                                            'id' => '',
                                                            'filial' => $filial,
                                                            'cliente_codigo' => trim($expline[0]),
                                                            'cliente_nome' => utf8_encode(trim($expline[1])),
                                                            'cliente_cnpj' => trim($expline[2]),
                                                            'natureza_codigo' => trim($expline[3]),
                                                            'natureza_descricao' => utf8_encode(trim($expline[4])),
                                                            'nf_numero' => trim($expline[5]),
                                                            'data_emissao' => trim($expline[6]),
                                                            'valor_nf' => trim($expline[7]),
                                                        );
                        $this->NotaCancelada->save($nota_cancelada);
                    }else{
                        break;
                    }
                }
                
                $count_line++;
            }

            fclose($arquivo);
        }
        
        echo 'ok!';
    }
    
    public function admin_index(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('Nota.created'=>'DESC');
        $posts = $this->paginate('Nota');
        $this->set('posts',$posts);
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->Nota->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        $this->data=$this->Nota->read('*',$id); 
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->Nota->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }   
    
}