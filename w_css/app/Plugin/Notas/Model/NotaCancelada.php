<?PHP
class NotaCancelada extends NotasAppModel{
    
    var $name = 'NotaCancelada';
    var $useTable = 'notas_canceladas';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Nota.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Nota.modified,'%d/%m/%Y %H:%i')",
    );
    
}