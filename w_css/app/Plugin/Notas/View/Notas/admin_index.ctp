<fieldset class="box">
    <legend>Notas Emitidas</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar',array('action'=>'add')); ?>
    </nav>    
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('title','Pergunta')?></td>
                <!--<td class="created"><?PHP // echo $this->Paginator->sort('created','Criado')?></td>-->
                <!--<td class="modified"><?PHP // echo $this->Paginator->sort('modified','Modificado')?></td>-->
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody class="sortable">
            <?PHP foreach($posts as $post){ ?>
             <tr data-id="<?PHP echo $post['Nota']['id']?>">
                <td><?PHP echo $post['Nota']['title']?></td>
                <!--<td><?PHP // echo $post['Nota']['cdate']?></td>-->
                <!--<td><?PHP // echo $post['Nota']['mdate']?></td>-->
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['Nota']['id']))?></td>
                <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['Nota']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP }?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>
