<?PHP
echo $this->Form->create('Banner',array('url'=>array('action'=>'add')));
$target = array('_self' => 'Mesma Aba', '_blank' => 'Outra Aba');
?>

<fieldset class="box">
    <legend>Banner</legend>
    <?PHP
        echo $this->Form->input('link',array('label'=>'Link (ATENÇÃO: O link deve começar com http:// )'));
        echo $this->Form->input('target',array('type'=>'select','options'=>$target,'label'=>'Abrir link em:'));
    ?>
</fieldset>

<?PHP
echo $this->element('Painel.image',array('label'=>'Banner (1920x500)','name'=>'thumb','reduce'=>'banner'));
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>