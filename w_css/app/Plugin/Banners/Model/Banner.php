<?PHP
class Banner extends BannersAppModel{

    public $virtualFields=array(
        'cdate'=>"DATE_FORMAT(Banner.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Banner.modified,'%d/%m/%Y %H:%i')",
    );
    
}