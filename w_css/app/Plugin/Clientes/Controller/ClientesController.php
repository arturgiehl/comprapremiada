<?PHP
App::import('Vendor', 'sdk/src/facebook');
 
class ClientesController extends ClientesAppController{

    public $paginate = array('limit'=>30,'order'=>array('created'=>'DESC'));

    public function login_register(){
        $this->Seo->title('Faça seu login ou cadastre-se');
 
        if(isset($_GET['referer'])){
            if($_GET['referer'] === 'home'){
                $this->Session->delete('Navigation');
            }else{
                $referer = Router::url('/'.$_GET['referer'], true);
                $this->Session->write('Navigation.referer',$referer);
            }
        }
    }
    
    public function pontos(){
        $this->Seo->title('Consultar Pontuação');
        $pontos = 0;
        
        if($this->data && $this->request->is('post')){
            $cliente = $this->Cliente->query('SELECT count(*) AS TOTAL FROM tb_clientes WHERE cnpj = "'.$this->data['Cliente']['cnpj'].'";');
            if($cliente[0][0]['TOTAL'] > 0){
                $total_pontos = $this->Cliente->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->data['Cliente']['cnpj'].'" AND cancelada = "0" ;');
                if($total_pontos[0][0]['TOTAL']){
                    $pontos = $total_pontos[0][0]['TOTAL'];
                }
                $this->set('pontos',$pontos);
            }else{
                $this->message_empty("ATENÇÃO: CNPJ não cadastrado."); 
            }
        }
    }

    public function add(){
        $this->Seo->title('Cadastro');
        $this->Seo->description('Cadastro');
        if($this->data && $this->request->is('post')){
            
            $verify_name = explode(' ',trim($this->data['Cliente']['nome']));
            if(count($verify_name) < 2){
                $this->message_empty('ATENÇÃO: Favor preencher o seu nome completo.'); 
            }
            
//            $this->valida_email($this->data['Cliente']['email']);
            $this->verify_email($this->data['Cliente']['email']);
            
            $count_pass = strlen($this->data['Cliente']['senha']);
            if($count_pass < 5){
                $this->message_empty('ATENÇÃO: Senha deve conter no mínimo 5 caracteres.'); 
            }
            
            if($this->data['Cliente']['senha'] !== $this->data['Cliente']['repetir_senha']){
                $this->message_empty('ATENÇÃO: As senhas não conferem.'); 
            }
            
            $count_cnpj = $this->Cliente->find('count',array('conditions'=>array('Cliente.cnpj'=>$this->data['Cliente']['cnpj'])));
            if($count_cnpj > 0){
                $this->message_empty('ATENÇÃO: CNPJ já cadastrado em nossa base de dados..'); 
            }
            
            $this->request->data['Cliente']['senha'] = Security::hash($this->data['Cliente']['senha'], 'md5', true);
            $this->request->data['Cliente']['pontos_trocados'] = 0;
            
            $total = $this->Cliente->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->data['Cliente']['cnpj'].'" AND cancelada = "0";');
            $this->request->data['Cliente']['pontos'] = $total[0][0]['TOTAL'];
            
            if($this->Cliente->save($this->data)){
                
                $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Cliente->id)));
                
                $this->Session->write('Cliente',$cliente['Cliente']);
                if($this->Session->check('Cliente')){  
                    $this->set('add_sucess',true);
//                    $this->redirect(array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'));  
//                    if($this->Session->check('Navigation.referer')){
//                        $this->redirect($this->Session->read('Navigation.referer'));  
//                    }
                }else{
                    $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
                }
            }
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
        }
    }
    
    public function login(){
        $this->layout = false;
        if($this->data && $this->request->is('post')){
            
            if($this->data['Login']['email'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo E-mail.'); 
            }else if($this->data['Login']['senha'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo Senha.'); 
            }
            
            $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.email' => $this->data['Login']['email'])));
            $rsv = $this->data['Login']['rsv'];
            
            if($cliente && Security::hash($this->data['Login']['senha'], 'md5', true) === $cliente['Cliente']['senha']){
//                pre($this->Session->check('Navigation.referer'));
//                $total = $this->Cliente->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'" AND cancelada = "0";');
//                $cliente['Cliente']['pontos'] = $total[0][0]['TOTAL'];
                
                $this->Session->write('Cliente',$cliente['Cliente']);
                if($this->Session->check('Cliente')){
                    if($this->request->params['controller']==='clientes' && ($this->request->params['action']==='change' && $this->request->params['action']==='save_new_password')){
                        $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
                    }else{
                        if($this->Session->check('Navigation.referer')){
                            $this->redirect($this->Session->read('Navigation.referer'));  
                        }else{
                            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
                        }
                    }
                }else{
                    $this->redirect($this->referer());
                }
            }else{
                echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                echo '<script type="text/javascript">';
                echo 'alert("ATENÇÃO: E-mail e/ou senha incorreto(s)");';
                echo 'history.go(-1);';
                echo '</script>';
                exit; 
            }
        }
    }
    
    public function logoff(){
        if($this->Session->check('Cliente')){
            if($this->Session->delete('Cliente')){
                $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
            }
        }
    }
    
    public function forget(){
        if($this->data && $this->request->is('post')){
            $email = $this->data['Forget']['email'];

            if($email === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo E-mail'); 
            }
            $this->valida_email($email);

            $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.email'=>$email)));
            if(!$cliente){
                $this->message_empty("ATENÇÃO: E-mail não cadastrado em nossa base de dados."); 
            }else{
                $link = $this->get_token();
                $token = Security::hash($link, 'md5', true);
                $forget['Forget'] = array('id'=>'', 'cliente_id'=>$cliente['Cliente']['id'], 'email'=>$cliente['Cliente']['email'], 'token'=>$token, 'expired'=>0);

                if($this->Forget->save($forget)){
                    App::uses('CakeEmail','Network/Email');
                    $mail = new CakeEmail('smtp');
                    $mail->template('Clientes.forget');
                  
                    $mail->to($email);

                    $mail->emailFormat('html');
                    $mail->subject('Alterar senha - '.date('d/m/Y - H:i').'');
                    $mail->viewVars(array('link'=>$link));
                    if($mail->send()){
                        $this->set('add_sucess',true);
                    }else{
                        $this->message_empty("Não foi possível enviar seus dados, tente novamente mais tarde"); 
                    }
                }else{
                    $this->message_empty("Não foi possível enviar seus dados, tente novamente mais tarde"); 
                }
            }
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
        }
    }

    public function change(){
        $this->Seo->title('Alterar Senha');
        if(isset($this->params->slug) && $this->params->slug){
            $decrypt = Security::hash($this->params->slug, 'md5', true);
           
            $forget = $this->Forget->find('first',array('conditions' => array('Forget.token'=>$decrypt, 'Forget.expired'=>'0')));
            if($forget){
                $date_send = new DateTime($forget['Forget']['created']);
                $date_now = new DateTime(date('Y-m-d H:i:s'));
                $diff = date_diff($date_send, $date_now);

                if($diff->d === 0 && $diff->h === 0 && $diff->i <= 29){
                    $this->set('add_sucess',true);
                    $this->set('slug',$this->params->slug); 
                    $this->set('email',$forget['Forget']['email']); 
                    $this->set('forget_id',$forget['Forget']['id']); 
                    $this->set('cliente_id',$forget['Forget']['cliente_id']); 
                }else{
                    $this->Forget->query('UPDATE tb_forget SET expired = "1" WHERE tb_forget.id = "'.$forget['Forget']['id'].'";');
                    $this->set('add_sucess',false);
                }
            }else{
                $this->set('add_sucess',false);
            }
        }
    }
    
    public function save_new_password(){
        if($this->params->slug && $this->data && $this->request->is('post')){
            if($this->data['Change']['senha'] === '' || $this->data['Change']['repeat_senha'] === ''){
                $this->message_empty("ATENÇÃO: Preencha os campos."); 
            }
            
            $count_pass = strlen($this->data['Change']['senha']);
            if($count_pass < 5){
                $this->message_empty('ATENÇÃO: Senha deve conter no mínimo 5 caracteres.'); 
            }
            
            if($this->data['Change']['senha'] !== $this->data['Change']['repeat_senha']){
                $this->message_empty("ATENÇÃO: As senhas digitadas não conferem."); 
            }

            if($this->params->slug && $this->data['Change']['mail']){
                $senha = Security::hash($this->data['Change']['senha'], 'md5', true);

                $cliente['Cliente'] = array(
                                        'id' => $this->data['Change']['cliente_id'],
                                        'senha' => $senha
                                    );
                
                $forget['Forget'] = array(
                                        'id' => $this->data['Change']['forget_id'],
                                        'expired' => '1'
                                    );
                
                if($this->Cliente->save($cliente) && $this->Forget->save($forget)){
                    $this->set('add_sucess',true); 
                }
            }
        }else{
            $this->redirect(array('plugin'=>'clientes','controller'=>'clientes','action'=>'change','slug'=>$this->params->slug)); 
        }
    }
    
    function get_token(){
        $token = "";
        $valor = "abcdefghijklmnopqrstuvwxyz0123456789";
        srand((double)microtime()*1000000);
        for ($i = 0; $i < 20; $i++){
            if($i === 5){
               $token .= date('dmY'); 
            }else if($i === 8){
                $token .= date('His');
            }else{
                $token .= $valor[rand()%strlen($valor)];
            }
        }
        return $token;
    }
    
    public function valida_cpf($cpf=null) {
        if(empty($cpf)) {
            return false;
        }

        $cpf = preg_replace('[^0-9]', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11) {
            return false;
        }else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
         }else{   
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }

    public function admin_index(){
        $this->layout = "Painel.admin";
        $posts = $this->paginate('Cliente');
        $this->set('posts',$posts);
        $this->set('total',$this->Cliente->find('count'));
    }
    
    public function valida_email($email) {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }else{
            $this->message_empty("ATENÇÃO: O E-mail não é valido."); 
        }
    }
    
    public function message_empty($text){
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<script type="text/javascript">';
        echo 'alert("'.$text.'");';
        echo 'history.go(-1);';
        echo '</script>';
        exit; 
    }
    
    public function verify_email_cpf($email,$cpf){
        $count = $this->Cliente->find('count',array('conditions'=>array('OR'=>array('Cliente.email'=>$email,'Cliente.cpf'=>$cpf))));
        if($count > 0){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<script type="text/javascript">';
            echo 'alert("ATENÇÃO: Você já está cadastrado em nossa base de dados.");';
            echo 'history.go(-1);';
            echo '</script>';
            exit; 
        }else{
            return true;
        }
    }
    
    public function verify_email($email){
        $count = $this->Cliente->find('count',array('conditions'=>array('Cliente.email'=>$email)));
        if($count > 0){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<script type="text/javascript">';
            echo 'alert("ATENÇÃO: E-mail já cadastrado em nossa base de dados.");';
            echo 'history.go(-1);';
            echo '</script>';
            exit; 
        }else{
            return true;
        }
    }
    
}