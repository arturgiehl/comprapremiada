<div class="ls-box">
    <h2 class="ls-title-5 ls-display-inline-block">Clientes (<?PHP echo $total; ?>)</h2>
</div>

<table class="ls-table ls-no-hover ls-table-striped ls-bg-header">
    <thead>
        <tr>
            <th class="ls-data-descending"><?PHP echo $this->Paginator->sort('title','Nome')?></th>
<!--            <th class="ls-data-descending"><?PHP // echo $this->Paginator->sort('cpf','CPF')?></th>
            <th class="ls-data-descending"><?PHP // echo $this->Paginator->sort('telefone','Telefone')?></th>-->
            <th class="ls-data-descending"><?PHP echo $this->Paginator->sort('email','E-mail')?></th>
            <th class="ls-data-descending"><?PHP echo $this->Paginator->sort('created','Criado')?></th>
            <th class="edit-d">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <?PHP 
            foreach($posts as $post){ 
                if($post['Cliente']['id'] !== '5824a488-b34c-46c4-b9bf-4e30327457f4'){
        ?>
            <tr>
                <td><?PHP echo $post['Cliente']['title']?></td>
<!--                <td><?PHP // echo $post['Cliente']['cpf']?></td>
                <td><?PHP // echo $post['Cliente']['telefone']?></td>-->
                <td><?PHP echo $post['Cliente']['email']?></td>
                <td><?PHP echo $post['Cliente']['cdate']?></td>
                <td>
                    <?PHP 
                        if(count($post['Wishlist']) > 0){
                            echo $this->Html->link('Lista',$this->Html->url('/lista-de-desejos/'.$post['Cliente']['slug'],true),array('class'=>'ls-btn ls-btn-xs','target'=>'_blank'));
                        }
                    ?>
                </td>
                <!--<td><?PHP // echo $this->Html->link('Excluir',array('action'=>'delete',$post['Cliente']['id']),array('class'=>'ls-btn-danger ls-btn-xs'),'Tem certeza?')?></td>-->
            </tr>
        <?PHP 
                } 
            } 
        ?>
    </tbody>
</table>
<?PHP echo $this->element('Painel.paginator');?>