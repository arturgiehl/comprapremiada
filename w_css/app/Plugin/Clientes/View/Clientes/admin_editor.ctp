<?PHP
echo $this->Form->create('Cliente',array('url'=>array('action'=>'add')));
?>

<fieldset class="box">
    <legend>Novo Cliente</legend>
    <?PHP
    echo $this->Form->input('nome',array('label'=>'Nome'));
    ?>
</fieldset>

<?PHP
echo $this->element('Painel.image',array('label'=>'Logo','name'=>'thumb'));
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>