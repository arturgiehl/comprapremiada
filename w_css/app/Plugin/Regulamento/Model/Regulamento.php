<?PHP
class Regulamento extends RegulamentoAppModel {
    
    var $useTable = 'regulamento';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Regulamento.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Regulamento.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $actsAs = array(
        'Painel.Gallery',
        'Painel.Videos'
    );
}