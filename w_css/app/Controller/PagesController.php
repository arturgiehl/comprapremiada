<?php
App::uses('AppController', 'Controller');

class PagesController extends AppController {

    public function admin_index() {
        $this->layout = "Painel.admin";
        exit;
    }
     
    public $uses = array(
        'Banners.Banner',
        'Produtos.Produto',
    );
    
    public function home(){
        $this->set('banners',$this->Banner->find('all',array('order'=>array('Banner.order_banner'=>'ASC','Banner.created'=>'DESC'))));
        $this->set('produtos',$this->Produto->find('all',array('order'=>'RAND()','limit'=>12)));
//        pre(Configure::version());
    }
    
    public function notfound(){
        $this->Seo->title('Nada encontrado');
    }

}