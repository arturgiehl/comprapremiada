<?PHP
App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $components = array(
        'Painel.Locker',
        'Seo.Seo',
        'Session',
    );
    
    public $helpers = array(
        'Media.Crop',
        'Media.Fill',
        'Less.Less',
        'Session',
    );
    
    public $uses = array(
        'Clientes.Cliente',
    );
     
    public function beforeFilter() {
        parent::beforeFilter();
        
        $cpontos = '';
        if($this->Session->check('Cliente')){
            $total = $this->Cliente->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0";');
            $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Session->read('Cliente.id'))));
            $cpontos = number_format($total[0][0]['TOTAL'] - $cliente['Cliente']['pontos_trocados'], 0, ',', '.');
        }
        $this->set('cpontos',$cpontos);
    }
    
    public function afterFilter() {
        if($this->response->statusCode() == '404'){
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'notfound')); 
        }
    }

}