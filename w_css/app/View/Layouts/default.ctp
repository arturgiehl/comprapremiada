<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?PHP echo $title_for_layout?></title>
        <?PHP
            echo $this->Html->charset('utf-8');
            echo $this->Html->meta('canonical',$canonical,array('rel'=>'canonical','type'=>null,'title'=>null));
            echo $this->Html->meta(Router::url('/',true).'favicon.png', Router::url('/',true).'img/favicon.png', array('type'=>'icon'));
        ?>
        <!--<meta property="fb:app_id" content="54545454545454" />--> 
        <meta property="og:locale" content="pt_BR">
        <meta property="og:site_name" content="comprapremiadafipal">
        <meta property="og:url" content="<?PHP echo $this->Html->url(false,true); ?>" />  
        <?PHP if(isset($og_title)){ ?> 
            <meta property="og:title" content="<?PHP echo $og_title; ?>" /> 
        <?PHP }else{ ?>   
            <meta property="og:title" content="Compra Premiada" /> 
        <?PHP } ?>   
        <?PHP if(isset($og_description)){ ?> 
            <meta property="og:description" content="<?PHP echo $og_description; ?>" /> 
        <?PHP }else{ ?>  
            <meta property="og:description" content="Compra Premiada Fipal" /> 
        <?PHP } ?>   
        <?PHP if(isset($og_image)){ ?> 
            <meta property="og:image" content="<?PHP echo $this->Html->url('/'.$og_image,true); ?>" /> 
        <?PHP }else{ ?>   
            <meta property="og:image" content="<?PHP echo $this->Html->url('/img/share.jpg',true); ?>" /> 
        <?PHP } ?>   
        <meta property="og:type" content="website" />
    
        <?PHP 
//            echo $this->Html->css('/css/styles');
            echo $this->Less->css('styles'); 
            echo $this->Html->css('https://use.fontawesome.com/releases/v5.0.6/css/all.css'); 
        ?>
        <script>
            var base = '<?= $this->base ?>';
        </script>
    </head>
    <body>
        <?PHP 
            echo $this->element('header'); 
            echo $this->fetch('content'); 
            echo $this->element('footer'); 
        ?>
        
        <script src="<?PHP echo $this->Html->url('/js/less.js',true); ?>"></script>
        <script src="<?PHP echo $this->Html->url('/js/jquery.js',true); ?>"></script>
        <script src="<?PHP echo $this->Html->url('/bxslider/jquery.bxslider.min.js',true); ?>"></script>
        <script src="<?PHP echo $this->Html->url('/js/jquery.magnific-popup.min.js',true); ?>"></script>
        <script src="<?PHP echo $this->Html->url('/js/js.js',true); ?>"></script>
    </body>
</html>