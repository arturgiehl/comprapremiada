<?PHP
echo $this->Form->create('User',array('type'=>'file'));
?>

<fieldset class="box image">
    <legend><?=__('Informações pessoais')?></legend>
    <?PHP
    echo $this->Form->input('name', array('label' => 'Nome'));
    echo $this->Form->input('email');
    echo $this->Form->input('group',array('label'=>'Grupo de acesso','options'=>$groups));
    ?>
</fieldset>

<?PHP
//echo $this->element('Painel.image',array('label'=>'Foto','name'=>'foto'));
?>

<fieldset class="box image">
    <legend><?=__('Informações de login')?></legend>
    <?PHP
    echo $this->Form->input('username', array('label' => 'Usuário'));
    echo $this->Form->input('password', array('label' => 'Senha', 'type' => 'password','value'=>''));
    echo $this->Form->input('password_retype', array('label' => 'Redigite a senha', 'type' => 'password','value'=>''));
    ?>
</fieldset>

<!--<fieldset class="box">                   
    <legend>Pós Graduação</legend>
    <?PHP // echo $this->Form->input('UsuarioPos',array('multiple'=>'checkbox','label'=>false,'options'=>$list_pos_graduacao)); ?>
</fieldset>-->

<?PHP echo $this->Form->end('Enviar'); ?>