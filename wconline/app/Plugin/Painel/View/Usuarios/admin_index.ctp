<fieldset class="box">
    <legend><?PHP echo $title?></legend>    
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar Novo',array('action'=>'add')); ?>
    </nav> 
<!--    <ul class="menu">
        <li>
            <?PHP
//            $name=isset($group_name)?$group_name:'Filtrar por grupo';
//            echo $this->Html->link($name,'javascript:void(0)');
//            echo '<ul>';
//            echo $this->Html->tag('li',$this->Html->link('Todos',array('plugin'=>'painel','controller'=>'usuarios','action'=>'index','admin'=>true)));
//            foreach($groups as $k=>$v):
//                $link=$this->Html->link($v['name'],array('plugin'=>'painel','controller'=>'usuarios','action'=>'filter','admin'=>true,$k));
//                echo $this->Html->tag('li',$link);
//            endforeach;
//            echo '</ul>';            
            ?>
        </li>
    </ul>-->
    
    <table class="tables">
        <thead>
            <tr>
                <!--<td class="thumb"><a>Usuário</a></td>-->
                <td><?PHP echo $this->Paginator->sort('User.name','Nome');?></td>
                <td><?PHP echo $this->Paginator->sort('User.email','E-mail')?></td>
                <td><?PHP echo $this->Paginator->sort('User.username','Login')?></td>
                <td><?PHP echo $this->Paginator->sort('group','Grupo')?></td>
                <!--<td class="edit" style="width:80px;"><?PHP // echo $this->Paginator->sort('active','Ativo')?></td>-->
                <td class="edit"></td>
                <td class="edit"></td>
            </tr>
        </thead>
        <tbody>
            <?PHP
            foreach($users as $user){
                if($user['User']['group'] != 'admx'){
                    $class=$user['User']['active']==1?'':'background-red';
                    $lock=$user['User']['active']==1?'Bloquear':'Liberar';
            ?>
                <tr class="user <?PHP echo $class?>">
<!--                    <td>
                        <?PHP
//                        if($user['User']['foto']){
//                            echo $this->Crop->image($user['User']['foto'],120,80);
//                        } else {
//                            echo $this->Html->image('/painel/img/noimage120x80.png');
//                        }
                        ?>
                    </td>-->
                    <td><?PHP echo $user['User']['name']?></td>
                    <td><?PHP echo $user['User']['email']?></td>
                    <td><?PHP echo $user['User']['username']?></td>
                    <td><?PHP echo $user['User']['group']?></td>
                    <!--<td><?PHP // echo $this->Locker->link($lock,array('action'=>'admin_activate',$user['User']['id']))?></td>-->
                    <td><?PHP echo $this->Locker->link('Editar',array('action'=>'admin_edit',$user['User']['id']))?></td>
                    <td><?PHP echo $this->Locker->link('Excluir',array('action'=>'admin_delete',$user['User']['id']),false,'Tem certeza que deseja excluir este usuário?')?></td>
                </tr>
            <?PHP 
                }
            }
            ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator'); ?>
</fieldset>