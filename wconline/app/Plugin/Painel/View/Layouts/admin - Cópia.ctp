<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="robots" CONTENT="noindex, nofollow">
        <title>ADMX Admin v3.1</title>
        <script type="text/javascript">
            var base = '<?= $this->base ?>';
            var _uploadID=0;
        </script>
        <?PHP
        echo $this->fetch('meta');
        echo $this->Html->css('Painel.reset');
        echo $this->Html->css('Painel.admin.less', 'stylesheet/less');
        echo $this->Html->css('Painel.images.less','stylesheet/less');
        echo $this->Html->css('Painel.videos.less', 'stylesheet/less');
        echo $this->Html->css('Painel.downloads.less','stylesheet/less');
        echo $this->fetch('css');

        echo $this->Html->script('Painel.less');
        echo $this->Html->script('Painel./ckeditor/ckeditor');
        echo $this->Html->script('Painel.jquery');
        echo $this->Html->script('Painel.jquery.ui.min');
        echo $this->Html->script('Painel.admin');
        echo $this->Html->script('Painel.images');
        echo $this->Html->script('Painel.videos');
        echo $this->Html->script('Painel.downloads');
        echo $this->fetch('script');
        ?>
    </head>

    <body>

        <nav id="mainmenu">
            <?PHP echo $this->Html->image('Painel.logo.png'); ?>
            <div class="usuario">
                <?PHP
                if (class_exists("LockerComponent") && LockerComponent::user('username')):
                    $user = LockerComponent::user('username');
                    ?>              
                    <span class="user">Usuário: <span><?= $user ?></span></span>
                <?PHP endif; ?>
                <?PHP echo $this->Html->link('logout', '/logout', array('class' => 'logout')); ?>
                <div class="user-pannel">
                    <?PHP
                    echo $this->Locker->link('alterar dados', array('plugin' => 'painel', 'controller' => 'usuarios', 'action' => 'admin_data'));
                    echo $this->Locker->link('alterar senha', array('plugin' => 'painel', 'controller' => 'usuarios', 'action' => 'admin_password'));
                    ?>
                </div>
            </div>

            <ul class="menu">
                <?PHP
                $menus = Configure::read('Painel.menu');
                foreach ($menus as $key => $menu):
                        $current=false;
                        $plus = '';
                        foreach ($menu as $k => $v):
                            if ($k == 'separator' || $v == 'separator') {
                                if ($plus != '') $plus.='<li><hr></li>';
                            }
                            else $plus.=$this->Locker->link("<li>$k</li>", $v, array('escape' => false));
                            
                            if($this->here==$this->Html->url($v)){
                                $current=true;
                            }
                        endforeach;
                        if ($plus != ""){
                            $more=$current?'open':'';
                            echo '<li class="li '.$more.'"><a href="javascript:void(0);">' . $key . '</a>';
                            echo "<ul>$plus</ul>";
                            echo "</li>";
                        }
                endforeach;
                ?>
            </ul>
        </nav>
        <div id="contents"><?PHP echo $this->fetch('content'); ?></div>
        <?php
        if (Configure::read('read') >= 4)
            echo $this->element('sql_dump');
        ?>
    </body>
</html>