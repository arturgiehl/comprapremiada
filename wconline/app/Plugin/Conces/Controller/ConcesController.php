<?PHP
class ConcesController extends ConcesAppController{

    public $paginate = array('limit'=>16,'order'=>array('Conce.order_depoimento' => 'ASC', 'Conce.created' => 'DESC'));
    
    public function admin_index(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('Conce.order_registro' => 'ASC', 'Conce.created' => 'DESC');
        $posts = $this->paginate('Conce');
        $this->set('posts',$posts);
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->Conce->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        $this->data = $this->Conce->read('*',$id); 
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->Conce->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }   
    
    public function admin_order($id,$order){
        $this->autoRender = false;
        $this->Conce->query('UPDATE tb_concessionarias SET order_registro = "'.$order.'" WHERE id = "'.$id.'";');
    }
    
}