<?PHP
class Conce extends ConcesAppModel {
    
    var $useTable = 'concessionarias';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Conce.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Conce.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $validate = array(
        'title'    =>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'telefone' =>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
    );
    
    public $actsAs = array(
//        'Painel.Gallery',
//        'Painel.Videos',
        'Painel.Slug'=>array('title'=>'slug'),
    );
    
}