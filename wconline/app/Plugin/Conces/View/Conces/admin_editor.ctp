<?PHP
echo $this->Form->create('Conce',array('url'=>array('action'=>'add')));
?>

<fieldset class="box">
    <legend>Concessionária</legend>
    <?PHP
    echo $this->Form->input('title',array('label'=>'Título'));
    echo $this->Form->input('telefone',array('label'=>'Telefone','maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);'));
    ?>
</fieldset>

<?PHP
//echo $this->element('Painel.image',array('label'=>'Imagem','name'=>'imagem'));
//echo $this->element('Painel.gallery',array('label'=>'Imagens'));
//echo $this->element('Painel.videos',array('label'=>'Vídeos'));
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>

<script>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>47 && tecla<58)) return true;
    else{
        if (tecla==8 || tecla==0) return true;
        else  return false;
    }
}

function mascara(o,f){
    v_obj = o;
    v_fun = f;
    setTimeout("execmascara()",1);
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function mtel(v){
    v=v.replace(/\D/g,"");             
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); 
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    
    return v;
}
</script>