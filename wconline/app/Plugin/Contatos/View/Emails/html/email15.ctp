<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; " />
        <title>Compra Premiada</title>

        <style type="text/css">
            body {
                -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6;
            }
            body {
                background-color: #f6f6f6;
            }
        </style>
    </head>

    <body style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background: #f6f6f6; margin: 0; padding: 0;">
        <table class="body-wrap" style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; width: 100%; background: #f6f6f6; margin: 0; padding: 0;">
            <tr style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; margin: 0; padding: 0;">
                <td style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; vertical-align: top; margin: 0; padding: 0;" valign="top"></td>
                <td class="container" width="700" style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;" valign="top">
                    <div class="content" style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; background: #fff; margin: 0; padding: 20px; margin-top: 50px; margin-bottom: 50px; border: 1px solid #e9e9e9;">
                        <p>
                            Olá <b><?PHP echo $nome; ?></b>, você possui um total de <b><?PHP echo $pontos; ?></b> pontos até o momento, acesse o site da campanha e veja todos os produtos disponíveis.
                        </p>
                        
                        <p style="text-align: center; margin-top: 40px;">
                            <a style="text-align: center; font-size: 15px; background-color: #FF3B3F; color: #FFF; padding: 10px 10px 5px 10px; border-radius: 10px; text-decoration: none; font-weight: bold;" moz-do-not-send="true" previewremoved="true" href="http://comprapremiadafipal.com.br/produtos" target="_blank">
                                Clique aqui e veja todos os produtos
                            </a>
                        </p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>
