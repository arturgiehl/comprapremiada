<?PHP
class ContatosAppController extends AppController{
    
    public $uses = array(
        'Contatos.Contato',
        'Contatos.ContatoCurso',
        'Contatos.Newsletter',
        'Departamentos.Departamento',
        'Textocontato.Textocontato',
        'Clientes.Cliente',
        'Produtos.Produto',
    );
    
}