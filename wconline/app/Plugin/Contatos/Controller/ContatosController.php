<?PHP
class ContatosController extends ContatosAppController{
    
    public $paginate=array('limit'=>10,'order'=>'id DESC');
    
    public function email15(){
        $this->layout = false;
        $clientes = $this->Cliente->find('all',array('conditions'=>array('Cliente.mail15' => 0)));
//        pre(count($clientes));
        
        if(isset($this->params->total) && $this->params->total && count($clientes) > 0){
            $total = $this->params->total;
            
            foreach ($clientes as $cliente) {
                if($total >= 1){
                    $cpontos = '';
                    $total_pontos = $this->Cliente->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'" AND cancelada = "0";');
                    if($cliente['Cliente']['aprovado']){
                        $pontos = number_format(number_format($total_pontos[0][0]['TOTAL'], 0, '', '') - $cliente['Cliente']['pontos_trocados'], 0, ',', '.');
                    }
                    
//                    $this->Produto->recursive = -1;
//                    $produtos = $this->Produto->find('all',array('order'=>'RAND()', 'limit'=>3, 'fields'=>array('title','slug','imagem', 'preco')));
                    $produtos = '';

                    App::uses('CakeEmail','Network/Email');
                    $mail = new CakeEmail('smtpsend');
                    $mail->template('Contatos.email15');

                    $mail->to($cliente['Cliente']['email']);
//                    $mail->bcc('artur@amexcom.com.br');

                    $mail->emailFormat('html');
                    $mail->subject('Pontuação Compra Premiada Fipal - '.date('d/m/Y - H:i').'');
                    $mail->viewVars(array('nome'=>$cliente['Cliente']['nome'], 'pontos'=>$pontos, 'produtos'=>$produtos));
       
                    if($mail->send()){
                        $save_send['Cliente'] = array('id'=>$cliente['Cliente']['id'], 'mail15'=>1);
                        $this->Cliente->save($save_send);
                        echo $total.'<br>';
                        $total--;
                    }
                }else{
                    break;
                }
            }
        }else{
            $this->Contato->query('UPDATE tb_clientes SET mail15 = "0";');
            pre('Envios zerados....');
        }
    }
    
    public function index(){
        $this->Seo->title('Fale Conosco');
        
        if($this->data && $this->request->is('post')){
            if($this->data['Contato']['nome'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo Nome.'); 
            }else if($this->data['Contato']['email'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo E-mail.'); 
            }else if($this->data['Contato']['telefone'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo Telefone.'); 
            }
            
            $recaptcha = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lek1NEUAAAAAPb12TVrs4UduZe4AecnRhYD5Vi0&response=".$this->data['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
            $response = json_decode($recaptcha, true);
            if(!$response['success']){
                $this->message_empty('ATENÇÃO: reCaptcha não é válido.'); 
            }
            
            if($this->Contato->save($this->data)){
                $this->send_mail($this->data);
                $this->set('add_sucess',true);
            }else{
                $this->message_empty('ERRO: Não foi possível enviar seus dados, tente novamente mais tarde'); 
            }
        }
    }
    
    private function send_mail($data){
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtp');
        $mail->template('Contatos.contato');
    
        $mail->to('comercial.pecas@fipal.com.br');
        $mail->bcc('artur@amexcom.com.br');
        
        $mail->replyTo($this->data['Contato']['email']);
        $mail->emailFormat('html');
        $mail->subject('Contato - '.date('d/m/Y - H:i').'');
        $mail->viewVars(array('data'=>$this->data['Contato']));
        return $mail->send();
    }

    public function admin_index(){
        $this->layout="Painel.admin";
        $this->paginate['order'] = 'Contato.created DESC';
        $this->set('posts',$this->paginate('Contato'));
        $this->set('total',$this->Contato->find('count'));
    }
    
    public function admin_view($id){
        $this->layout="Painel.admin";
        $this->set('post',$this->Contato->read('*',$id));
    }
    
    public function admin_delete($id){
        $this->autoRender=false;
        if($this->Contato->delete($id)) $this->redirect(array('action'=>'index'));
    }
    
    public function message_empty($text){
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<script type="text/javascript">';
        echo 'alert("'.$text.'");';
        echo 'history.go(-1);';
        echo '</script>';
        exit; 
    }
}