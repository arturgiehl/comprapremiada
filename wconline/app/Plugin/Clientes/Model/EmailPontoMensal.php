<?PHP
class EmailPontoMensal extends ClientesAppModel {
    
    var $name = 'EmailPontoMensal';
    var $useTable = 'email_ponto_mensal';
    var $tablePrefix = '';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(EmailPontoMensal.created,'%d/%m/%Y %H:%i')",
    );
    
}