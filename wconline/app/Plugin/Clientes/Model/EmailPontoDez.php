<?PHP
class EmailPontoDez extends ClientesAppModel {
    
    var $name = 'EmailPontoDez';
    var $useTable = 'email_ponto_dez_dias';
    var $tablePrefix = '';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(EmailPontoDez.created,'%d/%m/%Y %H:%i')",
    );
    
}