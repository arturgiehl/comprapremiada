<?PHP
class ClientesAppController extends AppController {
    
    public $uses = array(
        'Clientes.Cliente',
        'Clientes.Forget',
        'Clientes.Ponto',
        'Clientes.EmailPontoMensal',
        'Clientes.EmailPontoDez',
        'Clientes.PedidoCliente',
        'Clientes.PedidoClienteProduto',
        'Conces.Conce',
        'Notas.Nota',
    );
    
}