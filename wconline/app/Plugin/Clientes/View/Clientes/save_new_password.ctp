<?PHP 
    if(isset($add_sucess) && $add_sucess){ 
?>
    <div class="container-fix">
        <div class="msg-add-sucess"> 
            Nova senha salva com sucesso.
        </div>
    </div>
    <script>
        setTimeout( function() { 
            window.location.href='<?= $this->Html->url('/',true); ?>';
        },3000);
    </script>
<?PHP
}else{
?>
    <div class="bg-gray bgpb">
        <div class="container-fix">
            <div class="box-login-cadastro blc-change">
                <?PHP
                    echo $this->Form->create('Change',array('id'=>'formLogin','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'save_new_password','slug'=>$slug)));
                        echo $this->Form->input('senha',array('label'=>'Senha','type'=>'password','div'=>false));
                        echo $this->Form->input('repeat_senha',array('label'=>'Repetir Senha','type'=>'password','div'=>false));
                        echo $this->Form->hidden('mail',array('value'=>$email));
                        echo $this->Form->hidden('cliente_id',array('value'=>$cliente_id));
                        echo $this->Form->hidden('forget_id',array('value'=>$forget_id));
                        echo $this->Form->submit('Salvar',array('div'=>false));
                    echo $this->Form->end(); 
                ?>
            </div>
        </div>
    </div>
    
    <script>
    $(document).ready(function(){
        $('html,body').animate({scrollTop:$('.bg-content').offset().top}, 900);     
    });
    </script>
<?PHP } ?>
    

