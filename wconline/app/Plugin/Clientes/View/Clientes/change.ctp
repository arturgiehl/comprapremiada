<?PHP 
    if(isset($add_sucess) && $add_sucess){ 
?>
    <div class="bg-gray bgpb">
        <div class="container-fix">
            <div class="box-login-cadastro blc-change">
                <?PHP
                    echo $this->Form->create('Change',array('id'=>'formLogin','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'save_new_password','slug'=>$slug)));
                        echo $this->Form->input('senha',array('label'=>'Senha','type'=>'password','div'=>false));
                        echo $this->Form->input('repeat_senha',array('label'=>'Repetir Senha','type'=>'password','div'=>false));
                        echo $this->Form->hidden('mail',array('value'=>$email));
                        echo $this->Form->hidden('cliente_id',array('value'=>$cliente_id));
                        echo $this->Form->hidden('forget_id',array('value'=>$forget_id));
                        echo $this->Form->submit('Salvar',array('div'=>false));
                    echo $this->Form->end(); 
                ?>
            </div>
        </div>
    </div>
<?PHP
}else{
?>
    <div class="container-fix">
        <div class="clear" style="height: 30px;"></div>
        <div class="msg-add-sucess msg-false-forget"> 
            Desculpe, este link <b>não</b> é <b>válido</b> ou já foi <b>expirado.</b> <br>
            Favor fazer um novo pedido de recuperação de senha
        </div>
    </div>
<?PHP } ?>
    
<!--<script type="text/javascript">
$(document).ready(function(){
    $('html,body').animate({scrollTop:$('.bg-content').offset().top}, 900);     
});
</script>-->
