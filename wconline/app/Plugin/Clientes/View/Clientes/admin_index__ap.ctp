<fieldset class="box">
    <legend>Cadastros de clientes (<?PHP echo $total; ?> Registros)</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Gerar CSV',array('plugin'=>'clientes','controller'=>'clientes','action'=>'report','admin'=>true)) ?>
    </nav>
    <nav class="pannel" style="margin-right: 120px;">
        <?PHP echo $this->Html->link('Adicionar novo cliente',array('action'=>'add')); ?>
    </nav>    
    <?PHP
        $value = !empty($_GET['s']) ? $_GET['s'] : '';
        $options=array('type' => 'get', 'id' => 'search');
        if(!empty($url)) $options['url']=$url;
        echo $this->Form->create('Search',array('url'=>array('plugin'=>'clientes', 'controller'=>'clientes', 'action'=>'index'),'type' => 'get'));
        echo $this->Form->input('s', array('div'=>false,'label'=>false,'placeholder'=>'BUSCA','value'=>$value));
        echo $this->Form->end(array('label' => 'OK', 'div' => false));
    ?>
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('nome_fantasia','Nome Fantasia'); ?></td>
                <td><?PHP echo $this->Paginator->sort('nome','Nome'); ?></td>
                <td><?PHP echo $this->Paginator->sort('cnpj','CNPJ'); ?></td>
                <td><?PHP echo $this->Paginator->sort('total','Total Pontos'); ?></td>
                <td><?PHP echo $this->Paginator->sort('pontos_trocados','Pontos Trocados'); ?></td>
                <td><?PHP echo $this->Paginator->sort('pontos_trocados','Pontuação Atual'); ?></td>
                <!--<td>Total Notas</td>-->
                <td><?PHP echo $this->Paginator->sort('aprovado','Aprovado'); ?></td>
                <td><?PHP echo $this->Paginator->sort('termos','Termos'); ?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Cadastrado'); ?></td>
                <td class="delete">&nbsp;</td>
                <td class="delete">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP 
                foreach($posts as $post){ 
                    $post = array_shift($post); 
//                    pre($post);
                    $total_pontos = $this->requestAction(array('plugin'=>'notas','controller'=>'notas','admin'=>false,'action'=>'get_pontos',$post['cnpj']));
                    $pontos_trocados = $post['pontos_trocados'];
//                    pre($pontos);
            ?>
            <tr>
                <td><?PHP echo $post['nome_fantasia']; ?></td>
                <td><?PHP echo $post['nome']; ?></td>
                <td><?PHP echo $post['cnpj']; ?></td>
                <td><?PHP echo number_format($total_pontos, 0, '', '.'); ?></td>
                <td><?PHP echo number_format($pontos_trocados, 0, '', '.'); ?></td>
                <td><?PHP echo number_format($total_pontos - $pontos_trocados, 0, '', '.'); ?></td>
                <td><?PHP echo $this->Html->image('ativo_'.$post['aprovado'].'.png',array('alt'=>'')); ?></td>
                <td><?PHP echo $this->Html->image('ativo_'.$post['termos'].'.png',array('alt'=>'')); ?></td>
                <td><?PHP echo $post['cdate']; ?></td>
                <td>
                    <?PHP
                        if($post['aprovado']){
                            echo $this->Html->link("Remover",array('action'=>'delete',$post['id']),array('style'=>'color:red;'),'Tem certeza que deseja remover o cadastro?'); 
                        }else{
                            echo $this->Html->link("Remover",array('action'=>'delete',$post['id']),array('style'=>'color:red;'),'Deseja remover o cadastro?'); 
                            echo '&nbsp;&nbsp;&nbsp;';
                            echo $this->Html->link("Aprovar",array('action'=>'aproved',$post['id']),null,"Deseja aprovar o cadastro?"); 
                        }
                    ?>
                </td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['id']),array())?></td>
                <td><?PHP echo $this->Html->link('Ver',array('action'=>'view',$post['id']),array('target'=>'_blank'))?></td>
            </tr>
            <?PHP } ?>
        </tbody>        
    </table>
    <?PHP echo $this->element("Painel.paginator");?>
</fieldset>