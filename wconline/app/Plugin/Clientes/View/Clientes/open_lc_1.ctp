<div class="row bg-login-cadastro" data-role="page" data-dom-cache="true">
<!--    <div class="row">
        <span class="close-box-lc" title="Fechar"><?PHP // echo $this->Html->image('close.svg',array('alt'=>'Fechar')); ?></span>
    </div>-->
    <div class="row">
        <div class="four columns left">
            <div class="row title">
                Login
            </div>
            <div class="row">
                <div class="button-login" id="loginFacebook">
                    <i class="fab fa-facebook-f"></i> Entrar com Facebook
                </div> 
            </div>
            <div class="row">
                <!--<p class="p-entry">Entrar com seu e-mail</p>-->
                <?PHP
                    echo $this->Form->create('Login',array('id'=>'formLogin','class'=>'formtoplogin','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'login')));
                        echo $this->Form->input('email',array('label'=>'E-mail','type'=>'email','div'=>false));
                        echo $this->Form->input('senha',array('label'=>'Senha','type'=>'password','div'=>false));
                        echo $this->Form->hidden('rsv',array('id'=>'rsv','value'=>''));
                        echo '<div class="row bg-bes">';
                            echo '<span class="forget" id="button-forget">esqueci minha senha</span>';
                        echo '</div>';
                        echo $this->Form->hidden('url_redirect',array('id'=>'url_redirect'));
                        echo $this->Form->submit('Acessar',array('div'=>false));
                    echo $this->Form->end(); 

                    echo $this->Form->create('Forget',array('id'=>'formForget','class'=>'formtoplogin','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'forget')));
                        echo $this->Form->input('email',array('label'=>'E-mail','type'=>'email','div'=>false));
                        echo '<div class="row bg-bes">';
                            echo '<span class="forget" id="button-login">fazer login</span>';
                        echo '</div>';
                        echo '<div class="button-sendp" id="send_email_forget">Enviar Pedido</div>';
                        echo '<p id="msg_ef">enviando...</p>';
                    echo $this->Form->end(); 
                ?>
            </div>
        </div>
        <div class="eight columns right">
            <div class="row title">
                Não tenho cadastro
            </div>
            <div class="row">
                <?PHP
                    echo $this->Form->create('Cliente',array('id'=>'formAddClient','class'=>'formtoplogin','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'add')));
                        echo $this->Form->hidden('rsv',array('id'=>'rsv_cli','value'=>''));

                        echo '<div class="row">';
                            echo '<div class="six columns">'. $this->Form->input('title',array('label'=>'Nome Completo','div'=>false)) .'</div>';
                            echo '<div class="six columns">'. $this->Form->input('email',array('label'=>'E-mail <span>(seu e-mail não será divulgado)</span>','type'=>'email','div'=>false)) .'</div>';
                        echo '</div>';
                        echo '<div class="row">';
                            echo '<div class="six columns">'. $this->Form->input('senha',array('label'=>'Senha','type'=>'password','div'=>false)) .'</div>';
                            echo '<div class="six columns">'. $this->Form->input('repetir_senha',array('label'=>'Repetir Senha','type'=>'password','div'=>false)) .'</div>';
                        echo '</div>';

                        echo '<div class="row">';
                            echo '<div class="six columns bg-termos"> &nbsp;';
                            echo '</div>';
                            echo '<div class="six columns">';
                                echo $this->Form->submit('Cadastrar',array('div'=>false));
                                echo '<p id="msg_addc">enviando...</p>';
                            echo '</div>';
                        echo '</div>';

                    echo $this->Form->end(); 
                ?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function(){
    $('.close-box-lc').click(function(){
        window.history.pushState('', '/', window.location.pathname);
        $.magnificPopup.close(); 
    });
    
//    var url_current = window.location.href;
//    var explode = url_current.split("#");
//    if(explode[1]){
//        $('#url_redirect').val(explode[1]);
//    }
    
    $("#button-forget").click(function(){
        $('#formLogin').hide();
        $('#formForget').show();
    });
        
    $("#button-login").click(function(){
        $('#formForget').hide();
        $('#formLogin').show();
    });

    $("#send_email_forget").click(function(){
        $('#send_email_forget').hide();
        $('#msg_ef').show();
        $('#formForget').submit();
    });
                
    $("#add_client").click(function(){
        $('#add_client').hide();
        $('#msg_addc').show();
    });
});
</script>


<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '142805256516009',
            xfbml      : true,
            version    : 'v2.3'
        });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
   
    $('#loginFacebook').click( function(event){
        event.preventDefault();    
        destino = window.location.origin+base+'/c/login-social';
        
        FB.login( function(response){
            if (response.authResponse) {
//                document.cookie = 'dc_userid='+response.authResponse.userID;
//                document.cookie = 'dc_token='+response.authResponse.accessToken;
                window.location.href = destino+'?dct='+response.authResponse.accessToken;
            }else{
//                console.log('O usuário Cancelou o login ou não autozirou.');
            }
        }, {scope: 'user_photos, publish_actions'});    
    //        }, {scope: 'user_photos, email, publish_actions'});    
    });
    
</script>