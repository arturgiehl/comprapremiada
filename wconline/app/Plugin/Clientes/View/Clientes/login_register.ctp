<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Cadastro',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register')); 
                echo $this->element('breadcrumb');
                
//                echo '<h1 class="h1p">Faça seu login ou cadastre-se</h1>';
                echo '<h1 class="h1p">Faça seu login</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix content-mt">
        <div class="row">
            <div class="four columns left">
                <div class="row box-login-cadastro">
                    <h2>Já tenho cadastro</h2>
                    <?PHP
                        echo $this->Form->create('Login',array('id'=>'formLogin','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'login')));
//                            echo $this->Form->input('email',array('label'=>'E-mail','type'=>'email','div'=>false));
                            echo $this->Form->input('cnpj',array('label'=>'CNPJ','type'=>'tel','div'=>false,'type'=>'tel','maxlength'=>14,'onkeypress'=>'return SomenteNumero(event)'));
                            echo $this->Form->input('senha',array('label'=>'Senha','type'=>'password','div'=>false));
                            echo $this->Form->hidden('rsv',array('id'=>'rsv','value'=>''));
                            echo '<div class="row bg-bes">';
                                echo '<span class="forget" id="button-forget">esqueci minha senha</span>';
                            echo '</div>';
                            echo '<div class="row row-buttons">';
                                echo $this->Form->submit('Acessar',array('div'=>false));
                            echo '</div>';
                        echo $this->Form->end(); 

                        echo $this->Form->create('Forget',array('id'=>'formForget','class'=>'aa','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'forget')));
                            echo $this->Form->input('email',array('label'=>'E-mail','type'=>'email','div'=>false));
                            echo '<div class="row bg-bes">';
                                echo '<span class="forget" id="button-login">fazer login</span>';
                            echo '</div>';
                            echo $this->Form->submit('Enviar Pedido',array('div'=>false));
                            echo '<p id="msg_ef" class="msg-send-form">enviando...</p>';
                        echo $this->Form->end(); 
                    ?>
                </div>
            </div>
            <div class="eight columns right">
                <div class="row box-login-cadastro blc2">
                    <h2>Não tenho cadastro</h2>
                    <?PHP
                        $sexo = array(''=>'', 'Masculino'=>'Masculino', 'Feminino'=>'Feminino');
                    
                        echo $this->Form->create('Cliente',array('class'=>'formc','id'=>'formAdd','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'add')));
                            echo '<div class="row">';
                                echo '<div class="six columns">'. $this->Form->input('cnpj',array('label'=>'CNPJ <span>(apenas números)</span>','div'=>false,'type'=>'tel','maxlength'=>14,'onkeypress'=>'return SomenteNumero(event)')) .'</div>';
                                echo '<div class="six columns">'. $this->Form->input('nome_fantasia',array('label'=>'Nome Fantasia da Empresa','div'=>false,'data-amex-input'=>'cf-nome-fantasia')) .'</div>';
                            echo '</div>';
                            
                            echo '<div class="row">';
                                echo $this->Form->input('nome',array('label'=>'Nome do Solicitante','div'=>false));
//                                echo '<div class="six columns">'. $this->Form->input('cpf',array('label'=>'CPF <span>(apenas números)</span>','div'=>false,'type'=>'tel','maxlength'=>11,'onkeypress'=>'return SomenteNumero(event)','data-amex-input'=>'cpf')) .'</div>';
                            echo '</div>';
                            
//                            echo '<div class="row">';
//                                echo '<div class="six columns">'. $this->Form->input('sexo',array('type'=>'select','options'=>$sexo,'label'=>'Sexo','required'=>'required','data-amex-input'=>'cf-sexo')) .'</div>';
//                                echo '<div class="six columns">'. $this->Form->input('data_nascimento',array('label'=>'Data Nascimento <span>(apenas números)</span>','div'=>false,'maxlength'=>10,'type'=>'tel','onkeyup'=>'mascara(this,mdata);','data-amex-input'=>'birthday')) .'</div>';
//                            echo '</div>';
                            
                            echo '<div class="row">';
                                echo '<div class="six columns">'. $this->Form->input('telefone1',array('label'=>'Telefone 1 <span>(apenas números)</span>','div'=>false,'maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);')) .'</div>';
                                echo '<div class="six columns">'. $this->Form->input('telefone2',array('label'=>'Telefone 2 <span>(apenas números)</span>','div'=>false,'maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);')) .'</div>';
                            echo '</div>';
                            
//                            echo '<div class="row">';
//                                echo '<div class="four columns">'. $this->Form->input('cep',array('label'=>'CEP <span>(apenas números)</span> <span id="loadcep">aguarde...</span>','id'=>'cep','div'=>false,'maxlength'=>8,'onkeypress'=>'return SomenteNumero(event)','data-amex-input'=>'cf-cep')) .'</div>';
//                                echo '<div class="five columns">'. $this->Form->input('cidade',array('label'=>'Cidade','div'=>false,'id'=>'cidade','data-amex-input'=>'city')) .'</div>';
//                                echo '<div class="three columns">'. $this->Form->input('estado',array('label'=>'Estado','div'=>false,'id'=>'estado','data-amex-input'=>'state')) .'</div>';
//                            echo '</div>';
                            
//                            echo '<div class="row">';
//                                echo '<div class="six columns">'. $this->Form->input('endereco',array('label'=>'Endereço','div'=>false,'id'=>'endereco','data-amex-input'=>'cf-endereco')) .'</div>';
//                                echo '<div class="four columns">'. $this->Form->input('bairro',array('label'=>'Bairro','div'=>false,'id'=>'bairro','data-amex-input'=>'cf-bairro')) .'</div>';
//                                echo '<div class="two columns">'. $this->Form->input('numero',array('label'=>'Número','div'=>false,'id'=>'numero','data-amex-input'=>'cf-numero')) .'</div>';
//                            echo '</div>';
                            
                            echo '<div class="row">';
                                echo $this->Form->input('qual_vendedor',array('label'=>'Qual seu Vendedor Fipal','div'=>false));
                            echo '</div>';
                            
                            echo '<div class="row">';
                                echo $this->Form->input('concessionaria_id',array('type'=>'select','options'=>$select_concessionaria,'label'=>'Selecione qual sua Fipal','required'=>'required'));
                            echo '</div>';
                            
                            echo '<div class="row">';
                                echo '<div class="four columns">'. $this->Form->input('email',array('label'=>'E-mail','type'=>'email','div'=>false)) .'</div>';
                                echo '<div class="four columns">'. $this->Form->input('senha',array('label'=>'Senha','type'=>'password','div'=>false,'id'=>'senha')) .'</div>';
                                echo '<div class="four columns">'. $this->Form->input('repetir_senha',array('label'=>'Repetir Senha','type'=>'password','div'=>false)) .'</div>';
                            echo '</div>';

                            echo '<label class="label-termos">';
                                echo '<input type="checkbox" name="data[Cliente][termos]" value="1">';
                                echo 'Li e estou de acordo com os '.$this->Html->link('Termos de Uso',array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'termo_uso'),array('escape'=>false,'target'=>'_blank')). ' da plataforma';
                            echo '</label>';
                            
                            echo '<div class="row row-buttons">';
                                echo $this->Form->submit('Cadastrar',array('div'=>false,'class'=>'button-add'));
                                echo '<p id="msg-add" class="msg-send-form">enviando...</p>';
                            echo '</div>';

                        echo $this->Form->end(); 
                    ?>
                </div>
            </div>
        </div>
        
    </section>
</div>

<script async src="https://www.amexserver.com.br/dashboard/grabber/cdn/connect-1.0.0.js?key=f9187596c666ad9fb9e904421a6604d7"></script>