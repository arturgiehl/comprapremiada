<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Minhas Notas',array('plugin'=>'clientes','controller'=>'clientes','action'=>'minhas_notas')); 
                echo $this->element('breadcrumb');
                
                echo '<h1 class="h1p">Histórico de Notas</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix">
        <h2 class="h2-hp">O prazo de validade para utilizar os pontos das notas é de 6 meses.</h2>
        
        <div class="row-table-responsive">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data da emissão</th>
                        <th role="columnheader">Pontos</th>
                        <th role="columnheader">Válido até:</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $total_pontos = 0;
                        foreach ($notas as $nota) {
                            echo '<tr role="row">';
                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                echo '<td role="cell">'.number_format($nota['Nota']['valor_nf'], 0, ',', '.').'</td>';
//                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                echo '<td role="cell">'.date('d/m/Y', strtotime($nota['Nota']['data_e'].' + 6 months')).'</td>';
                            echo '</tr>';
                            
                            $total_pontos = $total_pontos + $nota['Nota']['valor_nf'];
                        }
                    ?>
                </tbody>
            </table>
        </div>
        
        <div class="bg-low-table">
            <table role="table">
                <tbody role="rowgroup">
                    <tr>
                        <td role="cell"><b>Total:</b></td>
                        <td role="cell"><?PHP echo number_format($total_pontos, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Já utilizado:</b></td>
                        <td role="cell">
                            <?PHP 
                                if($cliente['pontos_trocados'] > 0){
                                    echo '-'.number_format($cliente['pontos_trocados'], 0, ',', '.'); 
                                }else{
                                    echo number_format($cliente['pontos_trocados'], 0, ',', '.'); 
                                }
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Disponível:</b></td>
                        <td role="cell"><?PHP echo number_format($total_pontos - $cliente['pontos_trocados'], 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
</div>