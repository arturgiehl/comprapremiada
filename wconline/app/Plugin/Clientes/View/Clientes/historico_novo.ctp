<?PHP

    
?>
<div class="bg-gray bgpb">
    <div class="row bg-bread bgmtm">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Histórico',array('plugin'=>'clientes','controller'=>'clientes','action'=>'historico')); 
                echo $this->element('breadcrumb');
                
                echo '<h1 class="h1p">Histórico de Trocas</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix">
        <h2 class="h2-hp">O prazo de validade para utilizar os pontos das notas é de 6 meses.</h2>
        
        
        
        
        
        <h3 class="h3r">Histórico de trocas geral</h3>
        <div class="tbr-pedido">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data de solicitação</th>
                        <th role="columnheader">Status</th>
                        <th role="columnheader">Produtos</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
                        $total_qtd_geral = 0;
                        $total_pontos_geral = 0;
                        
                        $saldo_total_notas = 0;
                        $saldo_total_usado = 0;
                        
                        $total_disponivel = 0;
                        
                        $arr_notas_usadas = array();
                        $total_expirado = 0;
                        
                        $data_ultima_nota = '';
                        
                        $restante = 0;
                        foreach ($pedidos_geral as $pa) {
                            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));
                            
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">'.$pa['PedidoCliente']['cdate'].'</td>';
                                echo '<td role="cell" class="td20">'.$status[$pa['PedidoCliente']['status']].'</td>';
                                echo '<td role="cell">';
                                    echo '<table class="table-pdts">';
                                        $total_qtd = 0;
                                        $total_pontos = 0;
                                        foreach ($produtos_pedido as $ps) {
                                            echo '<tr>';
                                                echo '<td class="tdf">'.$ps['Produto']['title'].'</td>';
                                                echo '<td>'.$ps['PedidoClienteProduto']['quantidade'].'</td>';
                                                echo '<td>'.number_format($ps['PedidoClienteProduto']['pontos'], 0, ',', '.').'</td>';
                                            echo '</tr>';
                                            
                                            $total_pontos = $total_pontos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                                            $total_qtd = $total_qtd + $ps['PedidoClienteProduto']['quantidade'];
                                        }
                                        echo '<tr>';
                                            echo '<td class="tdta"><b>Total:</b></td>';
                                            echo '<td><b>'.$total_qtd.'</b></td>';
                                            echo '<td><b>'.number_format($total_pontos, 0, ',', '.').'</b></td>';
                                        echo '</tr>';
                                    echo '</table>';
                                echo '</td>';
                            echo '</tr>';
                            
                            $total_qtd_geral = $total_qtd_geral + $total_qtd;
                            $total_pontos_geral = $total_pontos_geral + $total_pontos;
                            
                            $expl_date = explode(' ', $pa['PedidoCliente']['created']);
                           
                            $notas_anteriores_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_notas_anteriores',$pa['PedidoCliente']['cnpj'],$expl_date[0]));
                                  
//                            pre($notas_anteriores_pedido);
                            $total_antes_expirado = 0;
                            foreach ($notas_anteriores_pedido['notas_anteriores'] as $na) {
                                $total_antes_expirado = $total_antes_expirado + $na['Nota']['valor_nf'];
                            }
                            
                            echo '<tr role="row">';
//                                echo '<td role="cell" class="td20"></td>';
//                                echo '<td role="cell" class="td20"></td>';
                                echo '<td role="cell" colspan="3">';
                                    echo '<table class="table-notas">';
                                        $total_notas = 0;
                                        foreach ($notas_anteriores_pedido['notas'] as $nota) {
                                            $firstDate  = new DateTime($nota['Nota']['data_e']);
                                            $secondDate = new DateTime(date('Y-m-d'));
                                            $intvl = $firstDate->diff($secondDate);
//                                            pre($intvl->m);
                                            
                                            if(!in_array($nota['Nota']['id'], $arr_notas_usadas)) { 
                                                $arr_notas_usadas[] = $nota['Nota']['id'];
                                                
                                                echo '<tr>';
                                                    echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                                    echo '<td>'.$nota['Nota']['valor_nf'].'</td>';
                                                    echo '<td>'.$intvl->m.', '.date('d/m/Y', strtotime("6 months",strtotime($nota['Nota']['data_e']))).'</td>';
                                                echo '</tr>';

                                                $total_notas = $total_notas + $nota['Nota']['valor_nf'];
                                                
                                                if($nota['Nota']['valor_nf'] > $total_pontos){
                                                    $restante = $restante + ($total_notas - $total_pontos);
                                                    
                                                }else{
                                                    $restante = (($total_notas + $restante) - $total_pontos);
                                                }
                                                
//                                                if($intvl->m > 6){
//                                                    $total_expirado = $total_expirado + ($nota['Nota']['valor_nf'] - $total_pontos);
//                                                }
                                                
                                                if($intvl->m >= 6){
                                                    $total_expirado = $total_expirado + $nota['Nota']['valor_nf'];
                                                }
                                                
                                                $data_ultima_nota = $nota['Nota']['data_e'];
                                            }
                                        }
                                      
                                        $saldo_total_notas = $saldo_total_notas + ($total_notas - $total_pontos_geral); 
                                        $saldo_total_usado = $saldo_total_usado + $total_pontos;
                                        
                                        $total_disponivel = $total_disponivel + ($total_notas - $total_pontos); 
                                        
//                                        $restante = $restante + ($total_notas - $total_pontos);
                                        
                                                
                                        echo '<tr>';
                                            echo '<td colspan="3"><b>Total notas: '.number_format($total_notas , 0, ',', '.').'</b></td>';
                                        echo '</tr>';
                                        echo '<tr>';
                                            echo '<td colspan="3"><b>Usado: '.number_format($total_pontos, 0, ',', '.').'</b></td>';
                                        echo '</tr>';
                                        echo '<tr>';
                                            echo '<td colspan="3"><b>Total disponivel: '.number_format($total_disponivel , 0, ',', '.').'</b></td>';
                                        echo '</tr>';
//                                        echo '<tr>';
//                                            echo '<td colspan="3"><b>Saldo: '.number_format($saldo_total_notas, 0, ',', '.').'</b></td>';
//                                        echo '</tr>';
//                                        echo '<tr>';
//                                            echo '<td colspan="3"><b>Restante: '.number_format($restante, 0, ',', '.').'</b></td>';
//                                        echo '</tr>';
                                        echo '<tr>';
                                            echo '<td colspan="3"><b>Expirado: '.number_format($total_expirado, 0, ',', '.').'</b></td>';
                                        echo '</tr>';
//                                        echo '<tr>';
//                                            echo '<td colspan="3"><b>Notas antes: '.number_format($total_antes_expirado, 0, ',', '.').'</b></td>';
//                                        echo '</tr>';
                                    echo '</table>';
                                echo '</td>';
                            echo '</tr>';
                        }
                        
                        
                        
//                        pre($data_ultima_nota);
                        $proximas_notas = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_proximas_notas',$cliente['cnpj'],$data_ultima_nota));
                        
//                        pre($proximas_notas);
//                        pre($total_expirado);
//                        foreach ($proximas_notas as $proxima_nota) {
//                            pre($proxima_nota);
//                        }
                    ?>
                </tbody>
            </table>
        </div>
        
<!--        <div class="row">
            <div class="total-low-pedidos">
                <div class="bin b1"><b>Total geral trocado: </b></div>
                <div class="bin b2"><b><?PHP echo $total_qtd_geral; ?></b></div>
                <div class="bin b3"><b><?PHP echo number_format($total_pontos_geral, 0, ',', '.'); ?></b></div>
            </div>
        </div>-->
        
        
        <div class="row">
            <div class="total-low-geral">
                <div class="bin"><b>Disponivel: </b></div>
                <div class="bin "><b><?PHP echo number_format($total_disponivel, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        
        <h3 class="h3r">Notas emitidas a partir de <?PHP echo $data_ultima_nota; ?></h3>
        <div class="row-table-responsive">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data da emissão</th>
                        <th role="columnheader">Pontos</th>
                        <th role="columnheader">Válido até:</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $total_pontos = 0;
                        foreach ($proximas_notas as $nota) {
                            echo '<tr role="row">';
                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                echo '<td role="cell">'.number_format($nota['Nota']['valor_nf'], 0, ',', '.').'</td>';
                                echo '<td role="cell">'.date('d/m/Y', strtotime($nota['Nota']['data_e'].' + 6 months')).'</td>';
                            echo '</tr>';
                            
                            $total_pontos = $total_pontos + $nota['Nota']['valor_nf'];
                        }
                    ?>
                </tbody>
            </table>
        </div>
        
        <div class="bg-low-table">
            <table role="table">
                <tbody role="rowgroup">
                    <tr> <td role="cell"></td><td role="cell"></td><td role="cell"></td> </tr>
                    <tr>
                        <td role="cell"><b>Disponível:</b></td>
                        <td role="cell"><?PHP echo number_format($total_pontos, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        
      
        <div class="row">
            <div class="total-low-geral">
                <div class="bin"><b>Disponivel geral: </b></div>
                <div class="bin "><b><?PHP echo number_format($total_disponivel + $total_pontos, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        <div class="row" style="margin-top: 20px;">
            <div class="three columns">
                <div class="total-low-geral">
                    <div class="bin">Notas antes período:</div>
                    <div class="bin "><b><?PHP echo number_format($pontos_cliente['total_nota_antes'], 0, ',', '.'); ?></b></div>
                </div>
                <div class="total-low-geral">
                    <div class="bin">Notas no período:</div>
                    <div class="bin "><b><?PHP echo number_format($pontos_cliente['total_nota_atuais'], 0, ',', '.'); ?></b></div>
                </div>
                <div class="total-low-geral">
                    <div class="bin">Total:</div>
                    <div class="bin "><b><?PHP echo number_format($pontos_cliente['total_nota_antes'] + $pontos_cliente['total_nota_atuais'], 0, ',', '.'); ?></b></div>
                </div>
            </div>
            <div class="six columns">
                <div class="total-low-geral">
                    <div class="bin">Trocas antes período:</div>
                    <div class="bin "><b><?PHP echo number_format($pontos_cliente['total_trocas_antes6'], 0, ',', '.'); ?></b></div>
                </div>
                <div class="total-low-geral">
                    <div class="bin">Trocas no período:</div>
                    <div class="bin "><b><?PHP echo number_format($pontos_cliente['total_dentro6'], 0, ',', '.'); ?></b></div>
                </div>
                <div class="total-low-geral">
                    <div class="bin">Total:</div>
                    <div class="bin "><b><?PHP echo number_format($pontos_cliente['total_trocas_antes6'] + $pontos_cliente['total_dentro6'], 0, ',', '.'); ?></b></div>
                </div>
            </div>
        </div>
        
        <div class="row" style="margin-top: 20px;">
            <div class="total-low-geral">
                <div class="bin"><b>Notas geral SUM: </b></div>
                <div class="bin "><b><?PHP echo number_format($total_geral, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
    </section>
</div>