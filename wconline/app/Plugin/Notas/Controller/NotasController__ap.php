<?PHP
class NotasController extends NotasAppController{
    
    public $components = array('Help');
    public $paginate = array('limit'=>44,'order'=>array('Nota.order_registro'=>'ASC','Nota.created'=>'DESC'));
    
    public function get_pontos($cnpj){
        $total_pontos = $this->Nota->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$cnpj.'" AND cancelada = "0" ;');
        return number_format($total_pontos[0][0]['TOTAL'], 0, '', '');
    }

    public function import(){
        $this->autoRender = false;
        
        $filiais = array(
            'FIAT_FP001_EMI',
            'FIAT_FP002_EMI',
            'FIAT_FP003_EMI',
            'FIAT_FP004_EMI',
            'FIAT_FP005_EMI',
            'FIAT_FP006_EMI',
            'FIAT_FP007_EMI',
            'FIAT_FP009_EMI',
            'JEEP_FP001_EMI',
            'JEEP_FP002_EMI',
            'JEEP_FP003_EMI'
        );
        
        foreach ($filiais as $filial) {
//            pre($filial);
            $count_line = 1;
            $arquivo = fopen('http://comprapremiadafipal.com.br/notas/'.$filial.'.txt', 'r');
 
            while(!feof($arquivo)){
                $linha = fgets($arquivo, 1024);
//                echo $linha.'<br/>';
                
                if($count_line > 2){
                    $expline = explode(',', $linha);
//                    pre($expline);
                    
                    if(count($expline) > 2){
                        
                        $expl_d = explode('/', trim($expline[6]));
                        $data_e = $expl_d[2].'-'.$expl_d[1].'-'.$expl_d[0]; 
                        
                        $count_nota = $this->Nota->find('count',array('conditions'=>array('Nota.nf_numero'=>trim($expline[5]))));
                        if($count_nota > 0){
                            echo 'existe <br>';
                        }else{
                            $nota['Nota'] = array(
                                'id' => '',
                                'filial'             => $filial,
                                'cliente_codigo'     => trim($expline[0]),
                                'cliente_nome'       => utf8_encode(trim($expline[1])),
                                'cliente_cnpj'       => trim($expline[2]),
                                'natureza_codigo'    => trim($expline[3]),
                                'natureza_descricao' => utf8_encode(trim($expline[4])),
                                'nf_numero'          => trim($expline[5]),
                                'data_emissao'       => trim($expline[6]),
                                'valor_nf'           => trim($expline[7]),
                                'cancelada'          => 0,
                                'data_e'             => $data_e,
                            );
                            $this->Nota->save($nota);
                        }
                    }else{
                        break;
                    }
                }
                
                $count_line++;
            }

            fclose($arquivo);
        }
        
        echo 'ok!';
    }
    
    public function import_cancel(){
        $this->autoRender = false;
        
        $filiais = array(
            'FIAT_FP001_CAN',
            'FIAT_FP002_CAN',
            'FIAT_FP003_CAN',
            'FIAT_FP004_CAN',
            'FIAT_FP005_CAN',
            'FIAT_FP006_CAN',
            'FIAT_FP007_CAN',
            'FIAT_FP009_CAN',
            'JEEP_FP001_CAN',
            'JEEP_FP002_CAN',
            'JEEP_FP003_CAN'
        );
  
        foreach ($filiais as $filial) {
            $count_line = 1;
            $arquivo = fopen('http://comprapremiadafipal.com.br/notas/'.$filial.'.txt', 'r');
 
            while(!feof($arquivo)){
                $linha = fgets($arquivo, 1024);
                
                if($count_line > 2){
                    $expline = explode(',', $linha);

                    if(count($expline) > 1){
                        echo $expline[5]. '<br>';
                        
                        $nota_emitida = $this->Nota->find('first',array('conditions'=>array('Nota.nf_numero'=>$expline[5])));
                        if(count($nota_emitida) > 0){
                            $update_nota['Nota'] = array('id'=>$nota_emitida['Nota']['id'], 'cancelada'=>1);
                            $this->Nota->save($update_nota);
                        }
                        $nota_cancelada['NotaCancelada'] = array(
                                                            'id' => '',
                                                            'filial' => $filial,
                                                            'cliente_codigo' => trim($expline[0]),
                                                            'cliente_nome' => utf8_encode(trim($expline[1])),
                                                            'cliente_cnpj' => trim($expline[2]),
                                                            'natureza_codigo' => trim($expline[3]),
                                                            'natureza_descricao' => utf8_encode(trim($expline[4])),
                                                            'nf_numero' => trim($expline[5]),
                                                            'data_emissao' => trim($expline[6]),
                                                            'valor_nf' => trim($expline[7]),
                                                        );
                        $this->NotaCancelada->save($nota_cancelada);
                    }else{
                        break;
                    }
                }
                
                $count_line++;
            }

            fclose($arquivo);
        }
        
        echo 'ok!';
    }
    
    public function import_natureza(){
        $this->autoRender = false;
        $filiais = array('FIAT_FP001_EMI','FIAT_FP002_EMI','FIAT_FP003_EMI','FIAT_FP004_EMI','FIAT_FP005_EMI','FIAT_FP006_EMI','FIAT_FP007_EMI','FIAT_FP009_EMI');
        
//        $truncate = $this->Nota->query('TRUNCATE TABLE tb_notas_emitidas;');
  
        foreach ($filiais as $filial) {
//            pre($filial);
            $count_line = 1;
            $arquivo = fopen('http://comprapremiadafipal.com.br/notas/'.$filial.'.txt', 'r');
 
            while(!feof($arquivo)){
                $linha = fgets($arquivo, 1024);
//                echo $linha.'<br/>';
                
                if($count_line > 2){
                    $expline = explode(',', $linha);
//                    pre($expline);
                    
                    $count_nota = $this->Nota->find('count',array('Nota.nf_numero'));
                    
                    if(count($expline) > 2){
                        $count_nota = $this->Nota->find('count',array('conditions'=>array('Nota.nf_numero'=>trim($expline[5]))));
                        if($count_nota > 0){
                            pre('existe');
                        }else{
                            $nota['Nota'] = array(
                                            'id' => '',
                                            'filial' => $filial,
                                            'cliente_codigo' => trim($expline[0]),
                                            'cliente_nome' => utf8_encode(trim($expline[1])),
                                            'cliente_cnpj' => trim($expline[2]),
                                            'natureza_codigo' => trim($expline[3]),
                                            'natureza_descricao' => utf8_encode(trim($expline[4])),
                                            'nf_numero' => trim($expline[5]),
                                            'data_emissao' => trim($expline[6]),
                                            'valor_nf' => trim($expline[7]),
                                            'cancelada' => 0,
                                        );
                            $this->Nota->save($nota);
                        }

                    }else{
                        break;
                    }
                }
                
                $count_line++;
            }

            fclose($arquivo);
        }
        
        echo 'ok!';
    }
    
    public function admin_index($tipo = null, $filter = null){
        $this->layout = 'Painel.admin';
        
        $count = $this->Nota->find('count');
                
        if($tipo === 'c' && $filter) {
            $this->paginate['conditions'] = array('Nota.cancelada' => $filter);
            $count = $this->Nota->find('count',array('conditions'=>array('Nota.cancelada'=>$filter)));
        }
        
        if(isset($_GET['s']) && $_GET['s']){
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Nota.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                'Nota.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                'Nota.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                'Nota.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                'Nota.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                'Nota.valor_nf LIKE' => "%".$_GET['s']."%",
                                                            ));
            
            $count = $this->Nota->find('count',array('conditions' => array('OR' => array(
                                                                        'Nota.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                        'Nota.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                        'Nota.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                        'Nota.valor_nf LIKE' => "%".$_GET['s']."%",
                                                                    )) 
                                                    ));
        }
        
        $this->paginate['order'] = array('Nota.created'=>'DESC');
        $this->paginate['limit'] = 80;
        $posts = $this->paginate('Nota');
        $this->set('posts',$posts);
        $this->set('total',$count);
    }
    
    public function admin_index2018($tipo = null, $filter = null){
        $this->layout = 'Painel.admin';
        
        $count = $this->Nota2018->find('count');
                
        if($tipo === 'c' && $filter) {
            $this->paginate['conditions'] = array('Nota2018.cancelada' => $filter);
            $count = $this->Nota2018->find('count',array('conditions'=>array('Nota2018.cancelada'=>$filter)));
        }
        
        if(isset($_GET['s']) && $_GET['s']){
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Nota2018.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                'Nota2018.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                'Nota2018.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                'Nota2018.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                'Nota2018.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                'Nota2018.valor_nf LIKE' => "%".$_GET['s']."%",
                                                            ));
            
            $count = $this->Nota2018->find('count',array('conditions' => array('OR' => array(
                                                                        'Nota2018.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota2018.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota2018.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2018.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2018.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2018.valor_nf LIKE' => "%".$_GET['s']."%",
                                                                    )) 
                                                    ));
        }
        
        $this->paginate['order'] = array('Nota2018.created'=>'DESC');
        $this->paginate['limit'] = 44;
        $posts = $this->paginate('Nota2018');
        $this->set('posts',$posts);
        $this->set('total',$count);
    }
    
    public function admin_index2019($tipo = null, $filter = null){
        $this->layout = 'Painel.admin';
        
        $count = $this->Nota2019->find('count');
                
        if($tipo === 'c' && $filter) {
            $this->paginate['conditions'] = array('Nota2019.cancelada' => $filter);
            $count = $this->Nota2019->find('count',array('conditions'=>array('Nota2019.cancelada'=>$filter)));
        }
        
        if(isset($_GET['s']) && $_GET['s']){
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Nota2019.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                'Nota2019.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                'Nota2019.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                'Nota2019.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                'Nota2019.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                'Nota2019.valor_nf LIKE' => "%".$_GET['s']."%",
                                                            ));
            
            $count = $this->Nota2018->find('count',array('conditions' => array('OR' => array(
                                                                        'Nota2019.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota2019.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota2019.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2019.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2019.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2019.valor_nf LIKE' => "%".$_GET['s']."%",
                                                                    )) 
                                                    ));
        }
        
        $this->paginate['order'] = array('Nota2019.created'=>'DESC');
        $this->paginate['limit'] = 44;
        $posts = $this->paginate('Nota2019');
        $this->set('posts',$posts);
        $this->set('total',$count);
    }
    
    public function admin_index2020($tipo = null, $filter = null){
        $this->layout = 'Painel.admin';
        
        $count = $this->Nota2020->find('count');
                
        if($tipo === 'c' && $filter) {
            $this->paginate['conditions'] = array('Nota2020.cancelada' => $filter);
            $count = $this->Nota2020->find('count',array('conditions'=>array('Nota2020.cancelada'=>$filter)));
        }
        
        if(isset($_GET['s']) && $_GET['s']){
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Nota2020.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                'Nota2020.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                'Nota2020.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                'Nota2020.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                'Nota2020.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                'Nota2020.valor_nf LIKE' => "%".$_GET['s']."%",
                                                            ));
            
            $count = $this->Nota2020->find('count',array('conditions' => array('OR' => array(
                                                                        'Nota2020.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota2020.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota2020.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2020.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2020.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                        'Nota2020.valor_nf LIKE' => "%".$_GET['s']."%",
                                                                    )) 
                                                    ));
        }
        
        $this->paginate['order'] = array('Nota2020.created'=>'DESC');
        $this->paginate['limit'] = 44;
        $posts = $this->paginate('Nota2020');
        $this->set('posts',$posts);
        $this->set('total',$count);
    }
    
    public function admin_index20202($tipo = null, $filter = null){
        $this->layout = 'Painel.admin';
        
        $count = $this->Nota20202->find('count');
                
        if($tipo === 'c' && $filter) {
            $this->paginate['conditions'] = array('Nota20202.cancelada' => $filter);
            $count = $this->Nota2020->find('count',array('conditions'=>array('Nota20202.cancelada'=>$filter)));
        }
        
        if(isset($_GET['s']) && $_GET['s']){
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Nota20202.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                'Nota20202.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                'Nota20202.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                'Nota20202.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                'Nota20202.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                'Nota20202.valor_nf LIKE' => "%".$_GET['s']."%",
                                                            ));
            
            $count = $this->Nota20202->find('count',array('conditions' => array('OR' => array(
                                                                        'Nota20202.cliente_codigo LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota20202.cliente_nome LIKE' => "%".$_GET['s']."%", 
                                                                        'Nota20202.cliente_cnpj LIKE' => "%".$_GET['s']."%",
                                                                        'Nota20202.nf_numero LIKE' => "%".$_GET['s']."%",
                                                                        'Nota20202.data_emissao LIKE' => "%".$_GET['s']."%",
                                                                        'Nota20202.valor_nf LIKE' => "%".$_GET['s']."%",
                                                                    )) 
                                                    ));
        }
        
        $this->paginate['order'] = array('Nota20202.created'=>'DESC');
        $this->paginate['limit'] = 44;
        $posts = $this->paginate('Nota20202');
        $this->set('posts',$posts);
        $this->set('total',$count);
    }
    
    public function admin_canceladas(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('NotaCancelada.created'=>'DESC');
        $posts = $this->paginate('NotaCancelada');
        $this->set('posts',$posts);
    }
    
    public function admin_canceladas2018(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('NotaCancelada2018.created'=>'DESC');
        $posts = $this->paginate('NotaCancelada2018');
        $this->set('posts',$posts);
    }
    
    public function admin_canceladas2019(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('NotaCancelada2019.created'=>'DESC');
        $posts = $this->paginate('NotaCancelada2019');
        $this->set('posts',$posts);
    }
    
    public function admin_canceladas2020(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('NotaCancelada2020.created'=>'DESC');
        $posts = $this->paginate('NotaCancelada2020');
        $this->set('posts',$posts);
    }
    
    public function admin_canceladas20202(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('NotaCancelada20202.created'=>'DESC');
        $posts = $this->paginate('NotaCancelada20202');
        $this->set('posts',$posts);
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->Nota->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        $this->data=$this->Nota->read('*',$id); 
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->Nota->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }   
    
    public function admin_manual(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_manual';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            
            $this->request->data['Nota']['valor_nf']  = $this->change_save_value($this->data['Nota']['valor_nf']);
            $this->request->data['Nota']['cancelada'] = 0;
            $this->request->data['Nota']['manual']    = 1;
            $this->request->data['Nota']['data_e']  = $this->Help->change_save_date($this->data['Nota']['data_emissao']);
            
//            pre($this->data);
            if($this->Nota->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function change_save_value($valor){
        if($valor){
            $value_bd = str_replace('.', '', trim($valor));
            $value_bd = str_replace(',', '.', $value_bd);
            return $value_bd;
        }
    }
    
    public function salvar_data_e(){
        $this->layout = false;
        $this->autoRender = false;
        
        $notas = $this->Nota->find('all');
        
        $count_save = 1;
        foreach ($notas as $nota) {
            if(!$nota['Nota']['data_e']){
                $expld = explode('/', $nota['Nota']['data_emissao']);
                $data_e = $expld[2].'-'.$expld[1].'-'.$expld[0];
                
                $save['Nota'] = array('id'=>$nota['Nota']['id'], 'data_e'=>$data_e);
                if($this->Nota->save($save)){
                    echo 'ok! '.$count_save.' <br>';
                }else{
                    pre('erro '.$nota);
                }
                $count_save++;
            }
        }
    }
    
}