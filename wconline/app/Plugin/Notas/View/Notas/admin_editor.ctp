<?PHP
echo $this->Form->create('Nota',array('url'=>array('action'=>'add')));
?>

<fieldset class="box">
    <legend>Dúvidas Frequentes</legend>
    <?PHP
        echo $this->Form->input('title',array('label'=>'Pergunta'));
        echo $this->Form->input('resposta',array('label'=>'Resposta','class'=>'ckeditor'));
    ?>
</fieldset>

<?PHP
//echo $this->element('Painel.videos',array('label'=>'Vídeo'));
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>