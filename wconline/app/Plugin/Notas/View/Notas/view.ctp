<section class="container-fix">
    <div class="row bg-titlep bg-titlep-n">
        <h1 class="hhp">Especialista Responde</h1>
    </div>
    
    <h2 class="h1-tn"><?PHP echo $post['Faq']['title']; ?></h2>
    
    <div class="row box-vn">
        <?PHP
            echo '<div class="row texto">';
                echo $post['Faq']['resposta'];
            echo '</div>';

            if(count($post['Video']) > 0){
                foreach ($post['Video'] as $video) {
                    echo '<div class="row video">';
                        if($video['title']){
                            echo '<p>'.$video['title'].'</p>';
                        }
                        echo '<iframe src="https://www.youtube.com/embed/'.$video['yid'].'"></iframe> ';
                    echo '</div>';
                }
            }
        ?>
    </div>
    
    <h2 class="h1-tn" style="text-align: left;">Veja mais</h2>
    
    <div class="row faq-links faq-links-view">
        <?PHP
            $cf = 1;
            foreach ($others as $other) {
                echo $this->Html->link($cf.' - '.$other['Faq']['title'],array('plugin'=>'faqs','controller'=>'faqs','action'=>'view','slug'=>$other['Faq']['slug']),array('escape'=>false));
                $cf++;
            }
        ?>
    </div>
</section>