<?PHP
class NotaCancelada2019 extends NotasAppModel{
    
    var $name = 'NotaCancelada2019';
    var $useTable = 'notas_canceladas_2019';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(NotaCancelada2019.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(NotaCancelada2019.modified,'%d/%m/%Y %H:%i')",
    );
    
}