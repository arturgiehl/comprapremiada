<?PHP
class Nota20202 extends NotasAppModel{
    
    var $name = 'Nota20202';
    var $useTable = 'notas_emitidas_2020_2';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Nota20202.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Nota20202.modified,'%d/%m/%Y %H:%i')",
    );
    
}