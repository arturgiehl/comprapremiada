<?PHP
class Nota extends NotasAppModel{
    
    var $name = 'Nota';
    var $useTable = 'notas_emitidas';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Nota.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Nota.modified,'%d/%m/%Y %H:%i')",
    );
    
}