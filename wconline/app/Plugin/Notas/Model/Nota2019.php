<?PHP
class Nota2019 extends NotasAppModel{
    
    var $name = 'Nota2019';
    var $useTable = 'notas_emitidas_2019';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Nota2019.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Nota2019.modified,'%d/%m/%Y %H:%i')",
    );
    
}