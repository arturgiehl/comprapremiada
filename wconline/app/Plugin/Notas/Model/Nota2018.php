<?PHP
class Nota2018 extends NotasAppModel{
    
    var $name = 'Nota2018';
    var $useTable = 'notas_emitidas_2018';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Nota2018.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Nota2018.modified,'%d/%m/%Y %H:%i')",
    );
    
}