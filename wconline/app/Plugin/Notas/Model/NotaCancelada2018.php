<?PHP
class NotaCancelada2018 extends NotasAppModel{
    
    var $name = 'NotaCancelada2018';
    var $useTable = 'notas_canceladas_2018';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(NotaCancelada2018.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(NotaCancelada2018.modified,'%d/%m/%Y %H:%i')",
    );
    
}