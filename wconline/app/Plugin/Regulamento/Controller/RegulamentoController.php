<?PHP
class RegulamentoController extends RegulamentoAppController {
    
    public $paginate = array('limit'=>16,'order'=>array('created'=>'DESC'));
    
    public function index(){
        $this->Seo->title('Regulamento');
        $post = $this->Regulamento->find('first');
        $this->set('post',$post);
    }
    
    public function termo_uso(){
        $this->Seo->title('Termos de Uso');
        $post = $this->Regulamento->find('first');
        $this->set('post',$post);
    }

    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->Regulamento->save($this->request->data)){
                $this->redirect(array('action'=>'edit'));
            }
        }
    }
    
    public function admin_edit(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        $post = $this->Regulamento->find('first');
        $this->data = $this->Regulamento->read('*',$post['Regulamento']['id']); 
    }
    
}