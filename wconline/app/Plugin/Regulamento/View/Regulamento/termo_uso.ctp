<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Termos de Uso',array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'termo_uso')); 
                echo $this->element('breadcrumb');

                echo '<h1 class="h1p">Termos de Uso</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix top-content">
        <div class="row">
            <div class="row texto">
                <?PHP echo $post['Regulamento']['termos_de_uso']; ?>
            </div>
        </div>
    </section>
</div>
