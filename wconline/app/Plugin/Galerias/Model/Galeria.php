<?PHP
class Galeria extends GaleriasAppModel {
    
    var $useTable = 'galerias';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Galeria.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Galeria.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $actsAs = array(
//        'Painel.Gallery',
//        'Painel.Videos',
        'Painel.Slug'=>array('title'=>'slug'),
    );
    
}