<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Galeria de Fotos',array('plugin'=>'galerias','controller'=>'galerias','action'=>'index')); 
                echo $this->element('breadcrumb');

                echo '<h1 class="h1p">Galeria de Fotos</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix top-content">
        <?PHP
            echo '<div class="row bg-gallery">';
                $count = 1;
                foreach ($posts as $gallery) {
//                    pre($gallery);
                    if(file_exists('img/media_cache/fill/220x150/ffffff/'.$gallery['Galeria']['imagem'].'')){
                        $img = $this->Html->image('media_cache/fill/220x150/ffffff/'.$gallery['Galeria']['imagem'].'',array('alt'=>$gallery['Galeria']['title']));
                    }else{
                        $img = $this->Fill->image($gallery['Galeria']['imagem'],220,150,'ffffff',array('alt'=>$gallery['Galeria']['title']));
                    }

                    if($count === 1) echo '<div class="row galeria">';
                        echo '<div class="three columns ig">';
                            echo $this->Html->link($img,'/'.$gallery['Galeria']['imagem'],array('escape'=>false,'title'=>$gallery['Galeria']['title']));
                        echo '</div>';
                    if($count === 4){
                        echo '</div>';
                        $count = 0;
                    }
                    $count++;
                }
                if($count > 1 && $count <=4) echo '</div>';
            echo '</div>';
        ?>
    </section>
</div>
