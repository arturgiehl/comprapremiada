<link rel="stylesheet" href="<?PHP echo $this->base ?>/selectize/selectize.css">
<script src="<?PHP echo $this->base ?>/selectize/selectize.js"></script>

<?PHP echo $this->Form->create('Departamento',array('url'=>array('action'=>'add'))); ?>

    <fieldset class="box">
        <legend>Departamento</legend>
        <?PHP
            echo $this->Form->input('title',array('label'=>'Nome'));
            echo $this->Form->input('emails',array('id'=>'emails','label'=>'E-mails que recebem (ATENÇÃO: digite o e-mail corretamente e aparte enter)'));
        ?>
    </fieldset>

<?PHP
    echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>

<script type="text/javascript">
$(document).ready(function(){
    $('#emails').selectize({
        persist: false,
        createOnBlur: true,
        create: true,
        plugins:['remove_button'],
        delimiter: '; '
    });
});
</script>