<div class="bg-gray">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Produtos',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index')); 
                if($categoria){
                    $this->Html->addCrumb($categoria['CategoriaProduto']['title'],array('plugin'=>'produtos','controller'=>'produtos','action'=>'index','categoria'=>$categoria['CategoriaProduto']['slug'])); 
                }
                echo $this->element('breadcrumb');
                
                if($categoria){
                    echo '<h1 class="h1p">'.$categoria['CategoriaProduto']['title'].'</h1>';
                }else{
                    echo '<h1 class="h1p">Produtos</h1>';
                }
            ?>
        </div>
    </div>
    <section class="container-fix">
        <div class="row top-order">
            <div class="nine columns sc-left">
                <?PHP
                    $value = isset($_GET['s']) ? $_GET['s']:'';
                    echo $this->Form->create('Busca', array('url' => array('plugin'=>'buscas','controller'=>'buscas','action'=>'index'),'id'=>'search','type'=>'get'));
                        echo $this->Form->input('s',array('label'=>false,'div'=>'back-input','value'=>$value,'placeholder'=>'Buscar produto'));
                        echo $this->Form->submit(' ',array('div'=>false));
                    echo $this->Form->end();
                ?>
            </div>
            <div class="three columns sc-right">
                
            </div>
        </div>
        
        <div class="row">
            <div class="three columns menu-ln">
                <div class="row">
                    <p class="tfilter" id="pt">Pontuação  <i class="fas fa-angle-down"></i></p>
                    <div class="row box-categorias" id="box-pt">
                        <?PHP 
                            $arr_pontuacao = array(
                                                '0-999' => 'Até 1.000 pontos',
                                                '1000-2999' => 'De 1.000 a 3.000 pontos',
                                                '3000-4999' => 'De 3.000 a 5.000 pontos',
                                                '5000-9999' => 'De 5.000 a 10.000 pontos',
                                                '10000-29999' => 'De 10.000 a 30.000 pontos',
                                                '30000-49999' => 'De 30.000 a 50.000 pontos',
                                                '50000' => 'Maior que 50.000 pontos',
                                            );
                            
                            foreach ($arr_pontuacao as $key => $value) {
                                $pt_active = '';
                                if(isset($_GET['pontuacao']) && $_GET['pontuacao'] == $key){
                                    $pt_active = 'as-active';
                                }
                                echo $this->Html->link($value.' <i class="fas fa-angle-right"></i>',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index','?'=>array('pontuacao'=>$key)),array('escape'=>false,'class'=>$pt_active));
                            }
                        ?>
                    </div>
                </div>
                
                <div class="row">
                    <p class="tfilter" id="fc">Categorias  <i class="fas fa-angle-down"></i></p>
                    <div class="row box-categorias" id="box-fc">
                        <?PHP 
                            foreach ($list_categorias as $mcategoria) {
                                $as_active = '';
                                if(isset($this->params->categoria) && $mcategoria['CategoriaProduto']['slug'] === $this->params->categoria && !isset($this->params->subslug)){
                                    $as_active = 'as-active';
                                }

                                echo $this->Html->link($mcategoria['CategoriaProduto']['title'].' <i class="fas fa-angle-right"></i>',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index','categoria'=>$mcategoria['CategoriaProduto']['slug']),array('escape'=>false,'class'=>$as_active));
                            }
                        ?>
                    </div>
                </div>
                
                
            </div>
            
            <div class="nine columns">
                <?PHP
                    $count = 1;
                    foreach ($posts as $produto) {
                        if($produto['Produto']['imagem']){
                            if(file_exists('img/media_cache/fill/265x160/ffffff/'.$produto['Produto']['imagem'].'')){
                                $image = $this->Html->image('media_cache/fill/265x160/ffffff/'.$produto['Produto']['imagem'].'',array('alt'=>$produto['Produto']['title']));
                            }else{
                                $image = $this->Fill->image($produto['Produto']['imagem'],265,160,'ffffff',array('alt'=>$produto['Produto']['title']));
                            }
                        }else{
                            $image = $this->Html->image('noimg265x160.jpg',array('alt'=>'Imagem indisponível'));
                        }

                        if($count === 1) echo '<div class="row">';
                            echo '<div class="four columns produto-list">';
                                echo $this->Html->link($image,array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false));
                                echo $this->Html->link($produto['Produto']['title'],array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false,'class'=>'title'));
    //                            echo $this->Html->link(number_format($produto['Produto']['preco'], 0, ',', '.').' pontos <i class="fas fa-shopping-basket"></i>',array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false,'class'=>'price'));
                                echo '<p class="price">'.number_format($produto['Produto']['preco'], 0, ',', '.').' pontos <i class="fas fa-shopping-basket"></i></p>';
                                echo '<div class="row buttons">';
                                    if($this->Session->check('Cliente')){  
                                        echo $this->Html->link('<i class="fas fa-shopping-cart"></i> Incluir no carrinho',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'add','idp'=>$produto['Produto']['id']),array('escape'=>false,'class'=>'addcar add-orc')); 
                                    }else{
                                        echo $this->Html->link('<i class="fas fa-shopping-cart"></i> Incluir no carrinho',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register','?'=>array('referer'=>$this->Html->url('/produtos/'.$produto['Produto']['slug'],true))),array('escape'=>false)); 
                                    }
                                echo '</div>';
                            echo '</div>';
                        if($count === 3){
                            echo '</div>';
                            $count = 0;
                        }
                        $count++;
                    }
                    if($count > 1 && $count <=3) echo '</div>';

                    echo $this->element('Painel.paginator'); 
                ?>
            </div>

        </div>
    </section>
</div>

<script>
    var element = document.getElementById("mn2");
    element.classList.add("m-active");
</script>