<?PHP
class Orcamento2020 extends ProdutosAppModel {
    
    public $useTable  = 'pedidos_troca_2020';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Orcamento2020.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Orcamento2020.modified,'%d/%m/%Y %H:%i')",
    );
    
}