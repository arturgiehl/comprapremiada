<?PHP
class OrcamentoController extends ProdutosAppController{
    
    public $paginate = array('limit'=>30,'order'=>'id DESC');
    
    public function add(){
        $this->autoRender = true;
        $this->layout = false;
        $add = true;
        $ponto = false;
        
        if($this->Session->read('Cliente')){
            if(!$this->Session->check('SessaoOrcamento')){
                $session_id = $this->cramdom();
               $this->Session->write('SessaoOrcamento',$session_id); 
            }

            if(isset($this->params->idp) && $this->params->idp){
                $produto = $this->Produto->find('first',array('conditions'=>array('Produto.id'=>$this->params->idp)));

//                $total_pontos = $this->Produto->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0" ;');
//                $total_notas = $this->Produto->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0" ;');
                $total_notas = $this->Produto->query("SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = ".$this->Session->read('Cliente.cnpj')." AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
                
                $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$this->Session->read('Cliente.id'))));
                $total_session = $this->Produto->query('SELECT SUM(pontos * quantidade) AS TOTAL FROM tb_produto_session WHERE cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND session_id = "'.$_SESSION['SessaoOrcamento'].'" ;');
                
                $total_pontos = $total_notas[0][0]['TOTAL'] - $cliente['Cliente']['pontos_trocados'];
                $total_check = $total_session[0][0]['TOTAL'] + $produto['Produto']['preco'];
//                pre(number_format($total_pontos, 0, '', ''));
//                pre($total_check);

                if($total_check > number_format($total_pontos, 0, '', '') || !$cliente['Cliente']['aprovado']){
                    $ponto = true;
                }else{
                    $verifica = $this->Orcamento->query('SELECT COUNT(*) AS TOTAL FROM tb_produto_session WHERE produto_id = "'.$this->params->idp.'" AND session_id = "'.$_SESSION['SessaoOrcamento'].'"');
                    if($verifica[0][0]['TOTAL'] == 0){
                        $produto_sessao['ProdutoSessao'] = array(
                                                                'id' => '',
                                                                'session_id' => $_SESSION['SessaoOrcamento'],
                                                                'produto_id' => $this->params->idp,
                                                                'cnpj' => $this->Session->read('Cliente.cnpj'),
                                                                'quantidade' => '1',
                                                                'pontos' => $produto['Produto']['preco'],
                                                            );
                        if($this->ProdutoSessao->save($produto_sessao)){
                            $add = false;
                        }
                    }
                }
            }
        }
        
        $this->set('add',$add);
        $this->set('ponto',$ponto);
    }
    
    public function delete_orc(){
        $this->autoRender = false;
        if($this->ProdutoSessao->delete($this->params->idp)){ 
            $this->redirect(array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index'));
        }
    }
    
    public function qtdeup(){
        $this->autoRender = true;
        $this->layout = false;
        $retorno['msg'] = '';
        $retorno['ponto'] = '0';
        $quantidade = $_GET['quantidade'];
        
        $produto = $this->Produto->find('first',array('conditions'=>array('Produto.id'=>$_GET['produtoId'])));
//        $total_pontos = $this->Produto->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0" ;');
//        $total_session = $this->Produto->query('SELECT SUM(pontos * quantidade) AS TOTAL FROM tb_produto_session WHERE cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND session_id = "'.$_SESSION['SessaoOrcamento'].'" ;');
//        $total_check = $total_session[0][0]['TOTAL'] + $produto['Produto']['preco'];

//        $total_notas = $this->Produto->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0" ;');
        $total_notas = $this->Produto->query("SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = ".$this->Session->read('Cliente.cnpj')." AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
        
        
        $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$this->Session->read('Cliente.id'))));
        $total_session = $this->Produto->query('SELECT SUM(pontos * quantidade) AS TOTAL FROM tb_produto_session WHERE cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND session_id = "'.$_SESSION['SessaoOrcamento'].'" ;');
        $total_pontos = $total_notas[0][0]['TOTAL'] - $cliente['Cliente']['pontos_trocados'];
        $total_check = $total_session[0][0]['TOTAL'] + $produto['Produto']['preco'];
        
        if($total_check > number_format($total_pontos, 0, '', '') && $_GET['sum'] === '1'){
            $retorno['ponto'] = '1';
        }else{
            if($_GET['sum'] === '0'){
                if($_GET['quantidade'] > 1){
                    $quantidade = $_GET['quantidade'] - 1;
                }else{
                    $quantidade = 1;
                }
            }else{
                $quantidade = $_GET['quantidade'] + 1;
            }
            
            $this->ProdutoSessao->query('UPDATE tb_produto_session SET quantidade = "'.$quantidade.'"  WHERE id = "'.$_GET['produtoSessaoId'].'" ');
        }
        
        $retorno['quantidade'] = $quantidade;
        $this->set('retorno',$retorno);
//        $this->set('quantidade',$quantidade);
    }
    
    public function loader(){
        $this->layout = false;
    }
    
    private function cramdom(){
        $novo_valor= "";
        $valor = "abcdefghijklmnopqrstuvwxyz0123456789";
        srand((double)microtime()*1000000);
        for ($i = 0; $i < 10; $i++){
            $novo_valor.= $valor[rand()%strlen($valor)];
        }
        $novo_valor .= $novo_valor.date('dmYHis');
        return $novo_valor;
    }

    public function index($id = null){
        $this->Seo->title('Meu Carrinho');
        
        if(!$this->Session->check('SessaoOrcamento')){
           $session_id = $this->cramdom();
           $this->Session->write('SessaoOrcamento',$session_id); 
        }
        
        $this->Produto->recursive = -1;
        $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        Produto.preco,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$_SESSION['SessaoOrcamento'].'"');
        
        if(count($produtos_session) > 0){
            $this->set('produtos',$produtos_session);
        }else{
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<script type="text/javascript">';
            echo 'alert("ATENÇÃO: Nenhum produto adicionado ao carrinho");';
            echo 'window.location.href="'.$this->base.'/produtos";';
            echo '</script>';
            exit;
        }
        
    }

    public function send(){
        $this->Seo->title('Pedido de troca');

        if($this->data && $this->request->is('post') && $this->Session->check('Cliente') && $this->Session->check('SessaoOrcamento')){
            $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        Produto.preco,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$_SESSION['SessaoOrcamento'].'"');
            
            $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$this->Session->read('Cliente.id'))));
            $total_pontos = $cliente['Cliente']['pontos_trocados'];
            
            foreach ($produtos_session as $ps) {
                $total_pontos = $total_pontos + $ps['Produto']['preco'] * $ps['ProdutoSessao']['quantidade'];
            }
            
//            $this->send_mail_cliente($this->data,$produtos_session);
//            $this->send_mail($this->data,$produtos_session);
//            pre($produtos_session);
            
            $save_ponto['Cliente'] = array(
                                        'id' => $this->data['Orcamento']['cliente_id'],
                                        'pontos_trocados' => $total_pontos
                                    );
            $this->Cliente->save($save_ponto);
            
            if($this->Orcamento->save($this->data)){
                $this->send_mail($this->data,$produtos_session);
                $this->send_mail_cliente($this->data,$produtos_session);
                $this->Session->delete('SessaoOrcamento');
                $this->set('add_sucess',true);
            }else{
                $this->message_empty('ERRO: Não foi possível enviar seus dados, tente novamente mais tarde'); 
            }
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
        }
    }
    
    private function send_mail($data,$produtos){
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtp');
//        $mail->template('Contatos.pedido');
        $mail->template('Produtos.pedido_troca');
  
//        $mail->to('artur@amexcom.com.br');
        $mail->to('cascavel.almoxarifado@fipal.com.br');
        $mail->bcc('artur@amexcom.com.br');
        
        $mail->replyTo($data['Orcamento']['email']);
        $mail->emailFormat('html');
        $mail->subject('Solicitação de troca - '.date('d/m/Y - H:i').'');
        $mail->viewVars(array('data'=>$data['Orcamento'],'produtos'=>$produtos));
        return $mail->send();
    }
    
    private function send_mail_cliente($data,$produtos){
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtpsend');
        $mail->template('Produtos.confirmacao_pedido_cliente');
  
        $mail->to($data['Orcamento']['email']);
//        $mail->bcc('artur@amexcom.com.br');
        
        $mail->replyTo($data['Orcamento']['email']);
        $mail->emailFormat('html');
        $mail->subject('Confirmação de pedido - '.date('d/m/Y - H:i').'');
        $mail->viewVars(array('data'=>$data['Orcamento'],'produtos'=>$produtos));
        return $mail->send();
    }
    
    private function send_mail_status_encaminhado($cliente,$produtos){
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtpsend');
        $mail->template('Produtos.pedido_encaminhado');
  
        $mail->to($cliente['email']);
//        $mail->bcc('artur@amexcom.com.br');
        
        $mail->emailFormat('html');
        $mail->subject('Pedido encaminhado para compra');
        $mail->viewVars(array('cliente'=>$cliente,'produtos'=>$produtos));
        return $mail->send();
    }
    
    private function send_mail_status_entregue($cliente,$produtos){
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtpsend');
        $mail->template('Produtos.pedido_entregue');
  
        $mail->to($cliente['email']);
//        $mail->bcc('artur@amexcom.com.br');
        
        $mail->emailFormat('html');
        $mail->subject('Pedido entregue ao seu vendedor');
        $mail->viewVars(array('cliente'=>$cliente,'produtos'=>$produtos));
        return $mail->send();
    }
   

//ADMIN ==========================================================================================================================================
    public function admin_save_status(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_view';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            $orcamento = $this->OrcamentoCliente->find('first',array('conditions'=>array('OrcamentoCliente.id'=>$this->data['Orcamento']['id'])));
            $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        Produto.preco,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$orcamento['OrcamentoCliente']['sessao_id'].'"');
//            pre($this->data);
            if($this->Orcamento->save($this->request->data)){
                if($this->data['Orcamento']['status'] === '2'){
                    $this->send_mail_status_encaminhado($orcamento['Cliente'],$produtos_session);
                }else if($this->data['Orcamento']['status'] === '3'){
                    $this->send_mail_status_entregue($orcamento['Cliente'],$produtos_session);
                }
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_index(){
        $this->layout = 'Painel.admin';
        
        if(isset($_GET['s']) && $_GET['s']){
            $busca = $_GET['s'];
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Orcamento.cnpj LIKE' => "%".$busca."%", 
                                                                'Orcamento.nome_fantasia LIKE' => "%".$busca."%", 
                                                                'Orcamento.nome LIKE' => "%".$busca."%", 
                                                                'Orcamento.cpf LIKE' => "%".$busca."%", 
                                                                'Orcamento.sexo LIKE' => "%".$busca."%", 
                                                                'Orcamento.data_nascimento LIKE' => "%".$busca."%", 
                                                                'Orcamento.telefone1 LIKE' => "%".$busca."%", 
                                                                'Orcamento.telefone2 LIKE' => "%".$busca."%", 
                                                                'Orcamento.cep LIKE' => "%".$busca."%", 
                                                                'Orcamento.cidade LIKE' => "%".$busca."%", 
                                                                'Orcamento.estado LIKE' => "%".$busca."%", 
                                                                'Orcamento.endereco LIKE' => "%".$busca."%", 
                                                                'Orcamento.bairro LIKE' => "%".$busca."%", 
                                                                'Orcamento.email LIKE' => "%".$busca."%", 
                                                            ));
        }
        
        $this->paginate['order'] = 'Orcamento.created DESC';
        $this->set('posts',$this->paginate('Orcamento'));
    }
    
    public function admin_index2018(){
        $this->layout = 'Painel.admin';
        
        if(isset($_GET['s']) && $_GET['s']){
            $busca = $_GET['s'];
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Orcamento2018.cnpj LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.nome_fantasia LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.nome LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.cpf LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.sexo LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.data_nascimento LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.telefone1 LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.telefone2 LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.cep LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.cidade LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.estado LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.endereco LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.bairro LIKE' => "%".$busca."%", 
                                                                'Orcamento2018.email LIKE' => "%".$busca."%", 
                                                            ));
        }
        
        $this->paginate['order'] = 'Orcamento2018.created DESC';
        $this->set('posts',$this->paginate('Orcamento2018'));
    }
    
    public function admin_index2019(){
        $this->layout = 'Painel.admin';
        
        if(isset($_GET['s']) && $_GET['s']){
            $busca = $_GET['s'];
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Orcamento2019.cnpj LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.nome_fantasia LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.nome LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.cpf LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.sexo LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.data_nascimento LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.telefone1 LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.telefone2 LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.cep LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.cidade LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.estado LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.endereco LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.bairro LIKE' => "%".$busca."%", 
                                                                'Orcamento2019.email LIKE' => "%".$busca."%", 
                                                            ));
        }
        
        $this->paginate['order'] = 'Orcamento2019.created DESC';
        $this->set('posts',$this->paginate('Orcamento2019'));
    }
    
    public function admin_index2020(){
        $this->layout = 'Painel.admin';
        
        if(isset($_GET['s']) && $_GET['s']){
            $busca = $_GET['s'];
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Orcamento2020.cnpj LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.nome_fantasia LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.nome LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.cpf LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.sexo LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.data_nascimento LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.telefone1 LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.telefone2 LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.cep LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.cidade LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.estado LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.endereco LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.bairro LIKE' => "%".$busca."%", 
                                                                'Orcamento2020.email LIKE' => "%".$busca."%", 
                                                            ));
        }
        
        $this->paginate['order'] = 'Orcamento2020.created DESC';
        $this->set('posts',$this->paginate('Orcamento2020'));
    }
    
    public function admin_index20202(){
        $this->layout = 'Painel.admin';
        
        if(isset($_GET['s']) && $_GET['s']){
            $busca = $_GET['s'];
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Orcamento20202.cnpj LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.nome_fantasia LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.nome LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.cpf LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.sexo LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.data_nascimento LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.telefone1 LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.telefone2 LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.cep LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.cidade LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.estado LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.endereco LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.bairro LIKE' => "%".$busca."%", 
                                                                'Orcamento20202.email LIKE' => "%".$busca."%", 
                                                            ));
        }
        
        $this->paginate['order'] = 'Orcamento20202.created DESC';
        $this->set('posts',$this->paginate('Orcamento20202'));
    }
    
    public function admin_view($id){
        $this->layout = 'Painel.admin';
        $post = $this->Orcamento->read('*',$id);
        
        $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$post['Orcamento']['sessao_id'].'"');
        
        $this->set('post',$post);
        $this->set('produtos',$produtos_session);
    }
    
    public function admin_view2018($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_view';
        $post = $this->Orcamento2018->read('*',$id);
        
        $produtos_session = $this->Orcamento2018->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$post['Orcamento2018']['sessao_id'].'"');
        
        $this->set('post',$post);
        $this->set('produtos',$produtos_session);
    }
    
    public function admin_view2020($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_view';
        $post = $this->Orcamento2020->read('*',$id);
        
        $produtos_session = $this->Orcamento2020->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$post['Orcamento2020']['sessao_id'].'"');
        
        $this->set('post',$post);
        $this->set('produtos',$produtos_session);
    }
    
    public function admin_view20202($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_view';
        $post = $this->Orcamento20202->read('*',$id);
        
        $produtos_session = $this->Orcamento20202->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$post['Orcamento20202']['sessao_id'].'"');
        
        $this->set('post',$post);
        $this->set('produtos',$produtos_session);
    }
    
    public function admin_delete($id){
        $this->autoRender=false;
        if($this->Orcamento->delete($id)) $this->redirect(array('action'=>'index'));
    }
    
    public function admin_report(){
        $this->autoRender = false;
        $pedidos = $this->Orcamento->find('all', array('order'=>'Orcamento.created DESC') );
        $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
//        pre($pedidos);
        
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: application/octet-stream");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment; filename=\"Pedidos Compra Premiada - ".date('d-m-Y H:i').".csv\";" );
        header("Content-Transfer-Encoding: binary");
        echo "\xEF\xBB\xBF";
        echo "CNPJ;Nome da Empresa;Nome Solicitante;Telefone1;Telefone2;E-mail;Produtos;Status;\n";
        
        foreach($pedidos as $pedido){
            $pp = '';
            $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$pedido['Orcamento']['sessao_id'].'"');
            
            foreach ($produtos_session as $produto) {
                $pp .= '- '.$produto['Produto']['title'].', Quantidade: '.$produto['ProdutoSessao']['quantidade'].' | ';
            }
            
            $tpp = substr($pp, 0, strlen($pp)-2);
            
            echo $pedido['Orcamento']['cnpj'].';'
                .$pedido['Orcamento']['nome_fantasia']
                .';'.$pedido['Orcamento']['nome']
                .';'.$pedido['Orcamento']['telefone1']
                .';'.$pedido['Orcamento']['telefone2']
                .';'.$pedido['Orcamento']['email']
                .';'.$tpp
                .';'.$status[$pedido['Orcamento']['status']]
                ."\n";
        }
    }
    
    public function admin_impression($id){
        $this->layout = false;
        $post = $this->Orcamento->read('*',$id);
        
        $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$post['Orcamento']['sessao_id'].'"');
        
        $this->set('post',$post);
        $this->set('produtos',$produtos_session);
    }
    
    public function admin_impression2018($id){
        $this->layout = false;
        $this->view = 'admin_impression';
        $post = $this->Orcamento2018->read('*',$id);
        
        $produtos_session = $this->Orcamento2018->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$post['Orcamento2018']['sessao_id'].'"');
        
        $this->set('post',$post);
        $this->set('produtos',$produtos_session);
    }
    
    public function admin_impression20202($id){
        $this->layout = false;
        $this->view = 'admin_impression';
        $post = $this->Orcamento20202->read('*',$id);
        
        $produtos_session = $this->Orcamento20202->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$post['Orcamento20202']['sessao_id'].'"');
        
        $this->set('post',$post);
        $this->set('produtos',$produtos_session);
    }
    
}