<?PHP
class SubCategoriasController extends ProdutosAppController{
    
    public $paginate=array('limit'=>30,'order'=>array('order'=>'ASC','created'=>'DESC'));
    
    public function admin_index(){
        $this->layout="Painel.admin";
        $this->paginate['order']=array('order'=>'ASC','created'=>'DESC');
        $posts=$this->paginate('SubCategoria');
        $this->set('posts',$posts);
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        
        $categorias[''] = 'Selecione uma categoria';
        $categorias[] = $this->CategoriaProduto->find('list',array('fields'=>array('id','title'),'order'=>array('title ASC')));
        $this->set('categorias',$categorias);
        
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->SubCategoria->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        
        $categorias[''] = 'Selecione uma categoria';
        $categorias[] = $this->CategoriaProduto->find('list',array('fields'=>array('id','title'),'order'=>array('title ASC')));
        $this->set('categorias',$categorias);
        
        $this->data = $this->SubCategoria->read('*',$id);
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->SubCategoria->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }
    
}