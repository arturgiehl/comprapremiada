    <?PHP 
        $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
        $post = array_shift($post); 
    ?>


    <h3>Pedido Compra Premiada</h3>
    
    <div class="line">
        <div class="name">Cnpj:</div>
        <div class="dado"><?PHP echo $post['cnpj']?></div>
    </div>
    
    <div class="line">
        <div class="name">Nome da Empresa:</div>
        <div class="dado"><?PHP echo $post['nome_fantasia']?></div>
    </div>
    
    <div class="line">
        <div class="name">Nome Solicitante:</div>
        <div class="dado"><?PHP echo $post['nome']?></div>
    </div>
    
    <div class="line">
        <div class="name">Telefone 1:</div>
        <div class="dado"><?PHP echo $post['telefone1']?></div>
    </div>
    
    <div class="line">
        <div class="name">Telefone 2:</div>
        <div class="dado"><?PHP echo $post['telefone2']?></div>
    </div>
    
    <div class="line">
        <div class="name">E-mail:</div>
        <div class="dado"><?PHP echo $post['email']?></div>
    </div>
    
    <div class="line">
        <div class="name">Produtos:</div>
        <div class="dado">
            <?PHP 
                foreach ($produtos as $produto) {
                    echo $produto['Produto']['title'].' - <b>Qtd</b>: '.$produto['ProdutoSessao']['quantidade'].'<br>';
                }
            ?>
        </div>
    </div>

    <div class="line">
        <div class="name">Observação:</div>
        <div class="dado"><?PHP echo $post['observacao']; ?></div>
    </div>
    
    <div class="line" style="border-bottom: 0px;">
        <div class="name">Status:</div>
        <div class="dado"><?PHP echo $status[$post['status']]; ?></div>
    </div>

<style>
    h3 { 
        font-family: 'Verdana'; 
        font-size: 17px; 
        margin-bottom: 20px;
    }
    .line{
        float: left;
        width: 800px;
        clear: both;
        font-family: arial;
        font-size: 13px;
        padding-top: 10px;
        padding-bottom: 10px;
        border-bottom: 1px solid #DDDDDD;
    }
   .name {
        float: left;
        display: inline-block;
        width: 140px;
        font-weight: bold;
    }
    .dado {
        float: left;
        display: inline-block;
        width: 600px;
    }
</style>