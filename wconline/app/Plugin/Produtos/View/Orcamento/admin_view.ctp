<?PHP $post=array_shift($post); ?>

<fieldset class="box">
    <legend>Dados</legend>    
    <dl class="description">
        <dt>CNPJ:</dt><dd><?PHP echo $post['cnpj']; ?></dd>
        <dt>Nome Fantasia:</dt><dd><?PHP echo $post['nome_fantasia']; ?></dd>
        <dt>Nome:</dt><dd><?PHP echo $post['nome']; ?></dd>
        <dt>CPF:</dt><dd><?PHP echo $post['cpf']; ?></dd>
        <dt>Sexo:</dt><dd><?PHP echo $post['sexo']; ?></dd>
        <dt>Data Nascimento:</dt><dd><?PHP echo $post['data_nascimento']; ?></dd>
        <dt>Telefone 1:</dt><dd><?PHP echo $post['telefone1']; ?></dd>
        <dt>Telefone 2:</dt><dd><?PHP echo $post['telefone2']; ?></dd>
        <dt>Cep:</dt><dd><?PHP echo $post['cep']; ?></dd>
        <dt>Cidade:</dt><dd><?PHP echo $post['cidade']; ?></dd>
        <dt>Estado:</dt><dd><?PHP echo $post['estado']; ?></dd>
        <dt>Endereço:</dt><dd><?PHP echo $post['endereco']; ?></dd>
        <dt>bairro:</dt><dd><?PHP echo $post['bairro']; ?></dd>
        <dt>Número:</dt><dd><?PHP echo $post['numero']; ?></dd>
        <dt>E-mail:</dt><dd><?PHP echo $post['email']; ?></dd>
    </dl>    
</fieldset>

<fieldset class="box">
    <legend>Produtos</legend>
    <table>
        <tr>
            <td class="title">Nome</td>
            <td class="title">Qtd</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <?PHP
            $total_produtos = 0;
            foreach ($produtos as $produto) {
                echo '<tr class="trp">';
                    echo '<td class="tdpn">'.$produto['Produto']['title'].'</td>';
                    echo '<td class="tdpq">'.$produto['ProdutoSessao']['quantidade'].'</td>';
                echo '</tr>';
                
                $total_produtos = $total_produtos + ($produto['ProdutoSessao']['quantidade'] * $produto['ProdutoSessao']['pontos']);
            }
        ?> 
        <tr class="trp">
            <td class="tdpn">Total: <?PHP echo number_format($total_produtos, 0, '', '.'); ?></td>
        </tr>
    </table>
</fieldset>

<fieldset class="box">
    <legend>Observação</legend>    
    <dl class="description">
        <dd><?PHP echo $post['observacao']; ?></dd>
    </dl>    
</fieldset>

<?PHP echo $this->Form->create('Orcamento',array('url'=>array('plugin'=>'produtos','controller'=>'orcamento','action'=>'save_status')));
?>

<fieldset class="box">
    <legend>Status:</legend>
    <?PHP 
        $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue ao seu vendedor');
        echo $this->Form->input('status',array('type'=>'select','options'=>$status,'label'=>false,'value'=>$post['status']));
    ?>
</fieldset>

<?PHP
echo $this->Form->hidden('id',array('value'=>$post['id']));
echo $this->Form->hidden('cliente_id',array('value'=>$post['cliente_id']));
echo $this->Form->end('Salvar');
?>

<style type="text/css">

dt { 
    width: 90px !important;
}
    
.title{
    color:#000;
    font-size: 16px;
    font-weight: bold;
}

.trp {
    border-bottom: 1px solid #CCCCCC;
    height: 30px;
    font-size: 15px;
}

.tdpn {
    color: #666666;
    font-size: 15px;
    width: 600px;   
    padding-top: 7px;
}

.tdpq {
    color: #666666;
    width: 50px;    
    font-size: 15px;
}

.tdpv {
    color: #666666;
    width: 80px;    
    font-size: 15px;
}

.tdpt {
    color: #666666;
    width: 150px;    
    font-size: 15px;
}

.tdtotal {
    color:#000;
    font-weight: bold;   
    font-size: 15px;
    padding-top: 7px;
}
</style>