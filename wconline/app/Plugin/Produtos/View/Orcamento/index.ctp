<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Carrinho',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index')); 
                echo $this->element('breadcrumb');

                echo '<h1 class="h1p">Meu carrinho</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix top-content">
        <div class="row">
            <div class="row cabecalho-produto">
                <div class="six columns">
                    Produto
                </div>
                <div class="two columns qtd">
                    Pontos
                </div>
                <div class="two columns qtd">
                    Quantidade
                </div>
                <div class="two columns opc">
                    &nbsp;
                </div>
            </div>
            <?PHP
                $total_car = 0;
                foreach ($produtos as $produto) {
//                    pre($produto);
                    $total_car = $total_car + ($produto['Produto']['preco'] * $produto['ProdutoSessao']['quantidade']);
                    
                    if($produto['Produto']['imagem']){
                        if(file_exists('img/media_cache/fill/100x60/ffffff/'.$produto['Produto']['imagem'].'')){
                            $image = $this->Html->image('media_cache/fill/100x60/ffffff/'.$produto['Produto']['imagem'].'',array('alt'=>$produto['Produto']['title']));
                        }else{
                            $image = $this->Fill->image($produto['Produto']['imagem'],100,60,'ffffff',array('alt'=>$produto['Produto']['title']));
                        }
                    }else{
                        $image = '';
                    }

                    echo '<div class="row list-orcamento">';
                        echo '<div class="six columns title-produto">';
                            echo $image.' <span>'.$produto['Produto']['title'].'</span>';
                        echo '</div>';
                        echo '<div class="two columns pontos">';
                            echo '<p>'.number_format($produto['Produto']['preco'], 0, ',', '.').' <span>pts</span></p>';
                        echo '</div>';
                        echo '<div class="two columns quantidade">';
                            echo '<div class="row">';
                                echo '<div class="seven columns left-qtd">';
                                    echo $this->Form->input('qntd',array('id'=>'qtd_'.$produto['ProdutoSessao']['id'],'label'=>false,'div'=>false,'value'=>$produto['ProdutoSessao']['quantidade'],'readonly'=>true,'maxlength'=>2));
                                echo '</div>';
                                echo '<div class="five columns right-qtd">';
                                    echo '<div class="row">';
                                        echo $this->Html->link('<i class="fas fa-angle-up"></i>','javascript:void(0)',array('escape'=>false,'class'=>'arrowQrd qtd_up','id'=>$produto['ProdutoSessao']['id'],'data-pi'=>$produto['Produto']['id'],'data-sum'=>'1'));
                                    echo '</div>';
                                    echo '<div class="row">';
                                        echo $this->Html->link('<i class="fas fa-angle-down"></i>','javascript:void(0)',array('escape'=>false,'class'=>'arrowQrd qtd_down','id'=>$produto['ProdutoSessao']['id'],'data-pi'=>$produto['Produto']['id'],'data-sum'=>'0'));
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        echo '</div>';
                        echo '<div class="two columns delete">';
                            echo $this->Html->link('<i class="fas fa-window-close"></i>',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'delete_orc','idp'=>$produto['ProdutoSessao']['id']),array('escape'=>false,'title'=>'Remover Produto'));
                        echo '</div>';
                    echo '</div>';
                }
            ?>
            
            <div class="row">
                <div class="dsums">
                    <?PHP 
                        echo '<p class="p1">Meus pontos:</p>';
                        echo '<p class="p2">'.number_format($cpontos, 0, ',', '.').'</p>';
                    ?>
                </div>
                <div class="dsums">
                    <?PHP 
                        echo '<p class="p1">Total carrinho:</p>';
                        echo '<p class="p2">'.number_format($total_car, 0, ',', '.').'</p>';
                    ?>
                </div>
            </div>
            
            <div class="row row-form-orc">
                <div class="row">
                    <?PHP echo $this->Html->link('<i class="fas fa-shopping-cart"></i> Continuar trocando',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false,'class'=>'continue')); ?>
                </div>
                <div class="row">
                    <p>Verifique corretamente seus produtos antes de solicitar a troca dos mesmos.</p>
                    <?PHP
    //                    pre($this->Session->read('Cliente'));

                        echo $this->Form->create('Orcamento',array('class'=>'formc','id'=>'formPedido','url'=>array('plugin'=>'produtos','controller'=>'orcamento','action'=>'send')));

                            echo $this->Form->hidden('sessao_id',array('value'=>$_SESSION['SessaoOrcamento']));
                            echo $this->Form->hidden('cliente_id',array('value'=>$this->Session->read('Cliente.id')));
                            echo $this->Form->hidden('cnpj',array('value'=>$this->Session->read('Cliente.cnpj')));
                            echo $this->Form->hidden('nome_fantasia',array('value'=>$this->Session->read('Cliente.nome_fantasia')));
                            echo $this->Form->hidden('nome',array('value'=>$this->Session->read('Cliente.nome')));
                            echo $this->Form->hidden('cpf',array('value'=>$this->Session->read('Cliente.cpf')));
                            echo $this->Form->hidden('sexo',array('value'=>$this->Session->read('Cliente.sexo')));
                            echo $this->Form->hidden('data_nascimento',array('value'=>$this->Session->read('Cliente.data_nascimento')));
                            echo $this->Form->hidden('telefone1',array('value'=>$this->Session->read('Cliente.telefone1')));
                            echo $this->Form->hidden('telefone2',array('value'=>$this->Session->read('Cliente.telefone2')));
                            echo $this->Form->hidden('cep',array('value'=>$this->Session->read('Cliente.cep')));
                            echo $this->Form->hidden('cidade',array('value'=>$this->Session->read('Cliente.cidade')));
                            echo $this->Form->hidden('estado',array('value'=>$this->Session->read('Cliente.estado')));
                            echo $this->Form->hidden('endereco',array('value'=>$this->Session->read('Cliente.endereco')));
                            echo $this->Form->hidden('bairro',array('value'=>$this->Session->read('Cliente.bairro')));
                            echo $this->Form->hidden('numero',array('value'=>$this->Session->read('Cliente.numero')));
                            echo $this->Form->hidden('complemento',array('value'=>$this->Session->read('Cliente.complemento')));
                            echo $this->Form->hidden('email',array('value'=>$this->Session->read('Cliente.email')));

                            echo '<div class="row row-obs">';
                                echo $this->Form->input('observacao',array('label'=>'Observação <span>(Descreva aqui as características do(s) produto(s), como cor, voltagem, etc.)</span>'));
                            echo '</div>';
                            
                            echo $this->Form->submit('Enviar solicitação de troca',array('div'=>false,'class'=>'bs-orc'));
                            echo '<div class="row msg-send-form"> Aguarde, enviando pedido...</div>';
                        echo $this->Form->end(); 
                    ?>
                </div>
            </div>
            
        </div>
    </section>
</div>


<a class="popup-loader" href="<?PHP echo $this->base ?>/load"></a>
