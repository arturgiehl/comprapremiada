
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Produtos',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index')); 
//                $this->Html->addCrumb($post['Produto']['title'],array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','categoria'=>$post['Produto']['slug'])); 
                echo $this->element('breadcrumb');

                echo '<h1 class="h1p">'.$post['Produto']['title'].'</h1>';
            ?>
        </div>
    </div>
    
    <section class="container-fix top-content">
        <div class="row">
            <div class="five columns box-images">
                <?PHP
                    if($post['Produto']['imagem']){
                        if(file_exists('img/media_cache/fill/470x300/ffffff/'.$post['Produto']['imagem'].'')){
                            $img_left = $this->Html->image('media_cache/fill/470x300/ffffff/'.$post['Produto']['imagem'].'',array('alt'=>'A Empresa','class'=>'img-left'));
                        }else{
                            $img_left = $this->Fill->image($post['Produto']['imagem'],470,300,'ffffff',array('alt'=>'A Empresa','class'=>'img-left'));
                        }
                        echo '<div class="row image-cp">';
                            echo $this->Html->link($img_left,'/'.$post['Produto']['imagem'],array('escape'=>false,'class'=>'al-left'));
                        echo '</div>';
                    }else{
                        echo '<div class="row image-cp">';
                            echo $this->Html->image('noimg470x300.jpg',array('alt'=>'Imagem indisponível'));
                        echo '</div>';
                    }

                    if (count($post['Galleries']) > 0) {
                        echo '<div class="row bg-gallery">';
                            $count = 1;
                            foreach ($post['Galleries']['images'] as $gallery) {
                                if(file_exists('img/media_cache/fill/220x150/ffffff/'.$gallery['path'].'')){
                                    $img = $this->Html->image('media_cache/fill/220x150/ffffff/'.$gallery['path'].'',array('alt'=>$gallery['legend']));
                                }else{
                                    $img = $this->Fill->image($gallery['path'],220,150,'ffffff',array('alt'=>$gallery['legend']));
                                }

                                if($count === 1) echo '<div class="row galeria">';
                                    echo '<div class="four columns ig">';
                                        echo $this->Html->link($img,'/'.$gallery['path'],array('escape'=>false,'title'=>$gallery['legend']));
                                    echo '</div>';
                                if($count === 3){
                                    echo '</div>';
                                    $count = 0;
                                }
                                $count++;
                            }
                            if($count > 1 && $count <=4) echo '</div>';
                        echo '</div>';
                    }


                ?>
            </div>
            <div class="seven columns box-right-info">
                <div class="row row-tp">
                    <?PHP
//                        echo '<h2>'.$post['Produto']['title'].'</h2>';
//                        echo '<span class="line"></span>';
                        
                        if($post['Produto']['preco']){
                            echo '<p class="pontos"><b>'.number_format($post['Produto']['preco'], 0, ',', '.').'</b> pontos</p>';
                            echo '<span class="line"></span>';
                        }
                        
                        echo '<div class="row row-add-car">';
                            if($this->Session->check('Cliente')){  
                                echo $this->Html->link('<i class="fas fa-shopping-cart"></i> <span>Incluir no <b>carrinho</b> de troca</span>',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'add','idp'=>$post['Produto']['id']),array('escape'=>false,'class'=>'add-orc')); 
                            }else{
                                echo $this->Html->link('<i class="fas fa-shopping-cart"></i> <span>Incluir no <b>carrinho</b> de troca</span>',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register','?'=>array('referer'=>$this->request->url)),array('escape'=>false)); 
                            }
                        echo '</div>';
                        
                        if($post['Produto']['texto']){
                            echo '<div class="texto">';
                                echo $post['Produto']['texto'];
                            echo '</div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>


<!--<script>
    var element = document.getElementById("mn2");
    element.classList.add("m-active");
</script>-->