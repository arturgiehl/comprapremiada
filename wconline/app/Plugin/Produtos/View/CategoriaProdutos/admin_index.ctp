<fieldset class="box">
    <legend>Categorias</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar nova',array('action'=>'add')); ?>
    </nav>    
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('title','Título')?></td>
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP 
                foreach($posts as $post){ 
            ?>
            <tr>
                <td><?PHP echo $post['CategoriaProduto']['title']?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['CategoriaProduto']['id']))?></td>
                <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['CategoriaProduto']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>