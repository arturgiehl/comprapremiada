<?PHP
class HelpComponent extends Component {
    
    public function message_empty($text){
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<script type="text/javascript">';
        echo 'alert("'.$text.'");';
        echo 'history.go(-1);';
        echo '</script>';
        exit; 
    }
    
    public function get_ip(){  
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  
            $ip = $_SERVER['HTTP_CLIENT_IP'];  
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){  
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
        }else  {  
            $ip = $_SERVER['REMOTE_ADDR'];  
        }  
        return $ip;  
    }
    
    public function change_save_date($data){
        $expld = explode('/', $data);
        if(count($expld) === 3){
            return $expld[2].'-'.$expld[1].'-'.$expld[0];
        }else{
            self::message_empty('Ocorreu algum erro com a data do evento.');
        }
    }
    
    public function show_mask_date($data){
        $expld = explode('-', $data);
        return $expld[2].'/'.$expld[1].'/'.$expld[0];
    }
    
}