<?PHP
App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $components = array(
        'Painel.Locker',
        'Seo.Seo',
        'Session',
    );
    
    public $helpers = array(
        'Media.Crop',
        'Media.Fill',
        'Less.Less',
        'Session',
    );
    
    public $uses = array(
        'Clientes.Cliente',
        'Clientes.PedidoCliente',
        'Clientes.PedidoClienteProduto',
        'Notas.Nota',
        'Produtos.Orcamento',
        'Conces.Conce'
    );
     
    public function beforeFilter() {
        parent::beforeFilter();
        
        $cpontos = 0;
        $valor_vencer = 0;
        $total_vencidos = 0;
        $pontos_usados = 0;
        $data_vencer = '';
        
        if($this->Session->check('Cliente')){
            $data_cliente = $this->Session->read('Cliente');
            
            $total_pedidos_ativos = 0;
            $pedidos_ativos = $this->PedidoCliente->find('all',array(
                'fields' => array('id','sessao_id'),
                'conditions' => array('PedidoCliente.cnpj = "'.$data_cliente['cnpj'].'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
            ));
            foreach ($pedidos_ativos as $pedido_ativo) {
                $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                    'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                    'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_ativo['PedidoCliente']['sessao_id'])
                ));
                foreach ($produtos_pedido as $ps) {
                    $total_pedidos_ativos = $total_pedidos_ativos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                }
            }
            
            $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$this->Session->read('Cliente.cnpj')."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
            $total_notas_emitidas = $total[0][0]['TOTAL'];
            
//            $notas_vencer = $this->Nota->find('all',array(
//                'fields'     => array('valor_nf','data_e'),
//                'conditions' => array('Nota.cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
//                'order'      => array('Nota.data_e' => 'ASC'),
//            ));
            
            if(isset($data_cliente['termos']) && $data_cliente['termos'] === '1'){
//                $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$this->Session->read('Cliente.cnpj')."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
//                $vencido = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$this->Session->read('Cliente.cnpj')."' AND cancelada = 0 AND data_e <= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
//                $total_vencidos = number_format($vencido[0][0]['TOTAL'], 0, '', '');
//                
//                $pontos_usados = $this->get_pontos_usados($this->Session->read('Cliente.id'));
                
//                $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Session->read('Cliente.id'))));
//                if(isset($cliente['Cliente']['aprovado']) && $cliente['Cliente']['aprovado']){
//                    if((number_format($total[0][0]['TOTAL'], 0, '', '') - $pontos_usados) < 0){
//                        $cpontos = number_format(number_format($total[0][0]['TOTAL'], 0, '', '') - ($pontos_usados - $total_vencidos), 0, ',', '.');
//                    }else{
//                        $cpontos = number_format(number_format($total[0][0]['TOTAL'], 0, '', '') - $pontos_usados, 0, ',', '.');
//                    }
//                }
                
//                $valor_trocado = 0;
//                foreach ($notas_vencer as $nf) {
//                    $valor_trocado = $valor_trocado + $nf['Nota']['valor_nf'];
//                    if($valor_trocado > $cliente['Cliente']['pontos_trocados']){
//                        $valor_vencer = $valor_trocado - $cliente['Cliente']['pontos_trocados'];
//                        $data_vencer = date('d/m/Y', strtotime($nf['Nota']['data_e'].' + 6 months'));
//                        break;
//                    }
//                }
                
//                $this->set('total_pedidos_ativos',$total_pedidos_ativos);
//                $this->set('total_notas_emitidas',$total_notas_emitidas);
                $cpontos = $total_notas_emitidas - $total_pedidos_ativos;
                $this->set('total_pedidos_ativos',$total_pedidos_ativos);
                $this->set('pedidos_ativos',$pedidos_ativos);
//                $this->set('notas_atuais',$notas_atuais);
            }else{
//                pre($this->params['plugin']);
                if($this->params['plugin'] == 'regulamento' && $this->params['controller'] == 'regulamento' && $this->params['action'] == 'termo_uso'){
             
                }else{
                    $this->layout = 'atualizar';
                    $select_concessionaria[''] = 'Selecionar';
                    $select_concessionaria[] = $this->Conce->find('list',array('fields'=>array('id','title'),'order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC')));
                    $this->set('select_concessionaria',$select_concessionaria);
                    $this->set('data_cliente',$data_cliente);
                }
            }
        }
        
        $this->set('cpontos',$cpontos);
        $this->set('valor_vencer',$valor_vencer);
        $this->set('data_vencer',$data_vencer);
        $this->set('total_vencidos',$total_vencidos);
        $this->set('pontos_usados',$pontos_usados);
        $this->set('concessionarias',$this->Conce->find('all',array('order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC'))));
    }

    public function get_pontuacao_disponivel(){
        $total_pedidos_ativos = 0;
        $pedidos_ativos = $this->PedidoCliente->find('all',array(
            'fields' => array('id','sessao_id'),
            'conditions' => array('PedidoCliente.cnpj = "'.$this->Session->read('Cliente.cnpj').'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));
        foreach ($pedidos_ativos as $pedido_ativo) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_ativo['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_pedidos_ativos = $total_pedidos_ativos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$this->Session->read('Cliente.cnpj')."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
        $total_disponivel = $total[0][0]['TOTAL'] - $total_pedidos_ativos;
        return $total_disponivel;
    }


    public function afterFilter() {
        if($this->response->statusCode() == '404'){
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'notfound')); 
        }
    }

    public function get_pontos_usados($cliente_id = null) {
        $pedidos = $this->Orcamento->find('all',array(
            'conditions' => array('Orcamento.cliente_id = "'.$cliente_id.'"', "DATE_FORMAT(Orcamento.created,'%Y%m%d') >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;")
        ));
//        pre($pedidos);
        
        $total_pontos = 0;
        foreach ($pedidos as $pedido) {
            $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        Produto.preco,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$pedido['Orcamento']['sessao_id'].'"');
            foreach ($produtos_session as $ps) {
                $total_pontos = $total_pontos + ($ps['Produto']['preco'] * $ps['ProdutoSessao']['quantidade']);
            }
        }
        return $total_pontos;
    }
    
}