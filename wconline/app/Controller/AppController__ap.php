<?PHP
App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $components = array(
        'Painel.Locker',
        'Seo.Seo',
        'Session',
    );
    
    public $helpers = array(
        'Media.Crop',
        'Media.Fill',
        'Less.Less',
        'Session',
    );
    
    public $uses = array(
        'Clientes.Cliente',
        'Notas.Nota',
        'Conces.Conce'
    );
     
    public function beforeFilter() {
        parent::beforeFilter();
        
        $cpontos = 0;
        $valor_vencer = 0;
        $data_vencer = '';
        
        if($this->Session->check('Cliente')){
            $data_cliente = $this->Session->read('Cliente');
            
//            $nota_vencer = $this->Nota->find('first',array(
//                'conditions' => array('Nota.cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
//                'order'      => array('Nota.data_e' => 'ASC'),
//            ));
            
            $notas_vencer = $this->Nota->find('all',array(
                'fields'     => array('valor_nf','data_e'),
                'conditions' => array('Nota.cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
//            pre($nota_vencer);
            
            if(isset($data_cliente['termos']) && $data_cliente['termos'] === '1'){
//                $total = $this->Cliente->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->Session->read('Cliente.cnpj').'" AND cancelada = "0";');
                $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = ".$this->Session->read('Cliente.cnpj')." AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
                $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Session->read('Cliente.id'))));
                if(isset($cliente['Cliente']['aprovado']) && $cliente['Cliente']['aprovado']){
    //                $cpontos = number_format($total[0][0]['TOTAL'] - $cliente['Cliente']['pontos_trocados'], 0, ',', '.');
                    $cpontos = number_format(number_format($total[0][0]['TOTAL'], 0, '', '') - $cliente['Cliente']['pontos_trocados'], 0, ',', '.');
                }
                
                $valor_trocado = 0;
                foreach ($notas_vencer as $nf) {
                    $valor_trocado = $valor_trocado + $nf['Nota']['valor_nf'];
                    if($valor_trocado > $cliente['Cliente']['pontos_trocados']){
                        $valor_vencer = $valor_trocado - $cliente['Cliente']['pontos_trocados'];
                        $data_vencer = date('d/m/Y', strtotime($nf['Nota']['data_e'].' + 6 months'));
                        break;
                    }
                }
//                pre($nota_vencer);
//                pre($cliente['Cliente']['pontos_trocados']);
            }else{
//                pre($this->params['plugin']);
                if($this->params['plugin'] == 'regulamento' && $this->params['controller'] == 'regulamento' && $this->params['action'] == 'termo_uso'){
             
                }else{
                    $this->layout = 'atualizar';
                    $select_concessionaria[''] = 'Selecionar';
                    $select_concessionaria[] = $this->Conce->find('list',array('fields'=>array('id','title'),'order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC')));
                    $this->set('select_concessionaria',$select_concessionaria);
                    $this->set('data_cliente',$data_cliente);
                }
            }
        }
        
        $this->set('cpontos',$cpontos);
        $this->set('valor_vencer',$valor_vencer);
        $this->set('data_vencer',$data_vencer);
        $this->set('concessionarias',$this->Conce->find('all',array('order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC'))));
    }
    
    public function afterFilter() {
        if($this->response->statusCode() == '404'){
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'notfound')); 
        }
    }

}