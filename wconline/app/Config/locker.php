<?PHP
class LOCKER_CONFIG{
    
    public $groups = array(
        'admx' => array('name'=>'Admx','loginRedirect'=>'/admin/produtos/orcamento','logoutRedirect'=>'/'),
        'admin' => array('name'=>'Administradores','loginRedirect'=>'/admin/produtos/orcamento','logoutRedirect'=>'/'),
    );
    
    public $prefixes = array(
        'admin' => array('admin', 'admx'),
    );
    
    public $sectors = array(
        'seo.site' => array(
                'admin_index' => array('admx','admin'),
                'admin_blocked' => array('admx','admin'),
                'admin_inactive' => array('admx','admin'),
                'admin_delete' => array('admx','admin'),
                'admin_edit' => array('admx','admin'),
                'admin_add' => array('admx','admin'),
        ),
    );
}