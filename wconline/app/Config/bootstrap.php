<?php
CakePlugin::load('Less');
CakePlugin::load('Clientes',array('bootstrap'=>true,'routes'=>true));
CakePlugin::load('Notas',array('bootstrap'=>true,'routes'=>true));
CakePlugin::load('Contatos',array('bootstrap'=>true,'routes'=>true));
CakePlugin::load('Buscas',array('bootstrap'=>true,'routes'=>false));
CakePlugin::load('Produtos',array('bootstrap'=>true,'routes'=>true));
CakePlugin::load('Galerias',array('bootstrap'=>true,'routes'=>true));
CakePlugin::load('Conces',array('bootstrap'=>true,'routes'=>false));
CakePlugin::load('Regulamento',array('bootstrap'=>true,'routes'=>true));
CakePlugin::load('Aempresa',array('bootstrap'=>false,'routes'=>false));
CakePlugin::load('Departamentos',array('bootstrap'=>false,'routes'=>false));

CakePlugin::load('Banners',array('bootstrap'=>true,'routes'=>false));

CakePlugin::load('Painel',array('bootstrap'=>true,'routes'=>true));
CakePlugin::load('Seo',array('bootstrap'=>true));
CakePlugin::load('Media',array('routes'=>true));
CakePlugin::load('GoogleMaps');
############################# DO NOT EDIT ############################################
Cache::config('default', array('engine' => 'File'));
Configure::write('Dispatcher.filters', array('AssetDispatcher','CacheDispatcher'));
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array('engine' => 'FileLog','types' => array('notice', 'info', 'debug'),'file' => 'debug',));
CakeLog::config('error', array('engine' => 'FileLog','types' => array('warning', 'error', 'critical', 'alert', 'emergency'),'file' => 'error',));