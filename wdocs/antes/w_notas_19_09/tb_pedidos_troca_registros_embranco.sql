-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 02/09/2019 às 15:23
-- Versão do servidor: 5.6.41-84.1
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `fipalcom_bd18`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_pedidos_troca`
--

CREATE TABLE `tb_pedidos_troca` (
  `id` char(36) NOT NULL,
  `sessao_id` varchar(66) NOT NULL,
  `cliente_id` varchar(36) NOT NULL,
  `cnpj` varchar(66) NOT NULL,
  `nome_fantasia` varchar(66) NOT NULL,
  `nome` varchar(66) NOT NULL,
  `cpf` varchar(66) NOT NULL,
  `sexo` varchar(66) NOT NULL,
  `data_nascimento` varchar(66) NOT NULL,
  `telefone1` varchar(66) NOT NULL,
  `telefone2` varchar(66) NOT NULL,
  `cep` varchar(66) NOT NULL,
  `cidade` varchar(66) NOT NULL,
  `estado` varchar(66) NOT NULL,
  `endereco` varchar(66) NOT NULL,
  `bairro` varchar(66) NOT NULL,
  `numero` varchar(66) NOT NULL,
  `complemento` varchar(66) NOT NULL,
  `email` varchar(66) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Fazendo dump de dados para tabela `tb_pedidos_troca`
--

INSERT INTO `tb_pedidos_troca` (`id`, `sessao_id`, `cliente_id`, `cnpj`, `nome_fantasia`, `nome`, `cpf`, `sexo`, `data_nascimento`, `telefone1`, `telefone2`, `cep`, `cidade`, `estado`, `endereco`, `bairro`, `numero`, `complemento`, `email`, `created`, `modified`, `status`) VALUES
('5c524a0c-a948-417d-8b69-4a04c0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:48:48', '2019-08-31 11:48:48', '3'),
('5c525dc4-2210-47dd-895d-4f91c0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:41:32', '2019-08-31 11:41:32', '3'),
('5c52f865-403c-4ab7-96b8-4fe3c0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:46:18', '2019-08-31 11:46:18', '3'),
('5c54dcab-1428-475e-9a4b-4d98c0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:48:54', '2019-08-31 11:48:54', '3'),
('5c54dcb9-34c8-4f60-871c-447ec0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:48:59', '2019-08-31 11:48:59', '3'),
('5c54dcfd-9504-44fb-a6f2-442ec0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:49:04', '2019-08-31 11:49:04', '3'),
('5c54dd08-45e8-49f9-98e6-447fc0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:49:11', '2019-08-31 11:49:11', '3'),
('5c54dd29-e408-43d9-a3fb-42dbc0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:49:17', '2019-08-31 11:49:17', '3'),
('5c632701-79c4-4ff6-8316-46c1c0b9d769', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2019-08-31 11:45:47', '2019-08-31 11:45:47', '3'),
('5d289602-1554-484b-8307-fbcfc0b9d769', 'pq6led6xe5pq6led6xe512072019111305', '5bb25c12-fb60-47de-b1b6-4b8fc0b9d769', '13453994000143', 'Fabiano Polimentos', 'Eder Fernando Levandowski', '03278604563', 'Masculino', '31/05/1988', '(45) 99817-0303', '(45) 99817-0303', '85904210', 'Toledo', 'PR', 'Rua Carlos Barbosa', 'Vila Industrial', '2727', '', 'pecas.fabianopolimentos@outlook.com', '2019-07-12 11:15:30', '2019-08-05 17:10:15', '3'),
('5d2be1a7-c4a0-4e16-b44c-4bc2c0b9d769', 'solcc8tyuysolcc8tyuy14072019231231', '5c09612d-9bd8-4ac9-8e2a-48d9c0b9d769', '11336041000170', 'AUTO MECANICA SANTAN', 'ALEXANDRE LUIZ SANTANA', '01671144929', 'Masculino', '14/05/1970', '(44) 3441-3363', '(44) 3441-3363', '87890000', 'Terra Rica', 'PR', 'AVENIDA JAMES PATRICK CLARK', 'CENTRO', '584', '', 'automecanicasantana@hotmail.com', '2019-07-14 23:15:03', '2019-08-05 17:10:24', '3'),
('5d3afab6-249c-4f44-89b3-d4c5c0b9d769', '3nppmtwjyd3nppmtwjyd26072019100550', '5bb26583-efc8-4a8f-9d40-40e5c0b9d769', '77968980000145', 'FLORENCA', 'FIAT FLORENCA', '03865454992', 'Masculino', '28/05/1982', '(41) 99794-9946', '(41) 3213-1599', '80310270', 'Curitiba', 'PR', 'Rua Pretextato Taborda Júnior', 'Santa Quitéria', '840', '', 'fabio.warpechoski@florenca.com.br', '2019-07-26 10:05:58', '2019-08-31 11:42:56', '3'),
('5d3f9117-5530-4764-aa49-4790c0b9d769', 'kv9djarnxpkv9djarnxp29072019213622', '5d0780a0-a304-422b-95af-1b13c0b9d769', '17339445000101', 'r.candiotto mecanica', 'cleberson seminotti', '', '', '', '(45) 3035-5118', '(45) 99906-3970', '', '', '', '', '', '', '', 'seminottiautocar@hotmail.com', '2019-07-29 21:36:39', '2019-08-31 11:44:57', '3'),
('5d44de02-27b8-4483-91f7-4e4dc0b9d769', 'ighco52tqpighco52tqp02082019220601', '5d0780a0-a304-422b-95af-1b13c0b9d769', '17339445000101', 'r.candiotto mecanica', 'cleberson seminotti', '', '', '', '(45) 3035-5118', '(45) 99906-3970', '', '', '', '', '', '', '', 'seminottiautocar@hotmail.com', '2019-08-02 22:06:10', '2019-08-31 11:45:23', '3'),
('5d498851-58a0-453e-a98c-3fc3c0b9d769', 'uhfzbka4i8uhfzbka4i806082019110029', '5d4979b5-61b0-44c6-afbe-4c3fc0b9d769', '10713453000119', 'CARBONERA & CIA LTDA', 'ERICA RENATA', '', '', '', '(45) 3039-5848', '', '', '', '', '', '', '', '', 'f1.autocenter@hotmail.com', '2019-08-06 11:01:53', '2019-08-31 11:40:18', '3'),
('5d4ddebb-138c-4053-a8d5-3765c0b9d769', 'f6w7mdozq9f6w7mdozq909082019175913', '5bb25c12-fb60-47de-b1b6-4b8fc0b9d769', '13453994000143', 'Fabiano Polimentos', 'Eder Fernando Levandowski', '03278604563', 'Masculino', '31/05/1988', '(45) 99817-0303', '(45) 99817-0303', '85904210', 'Toledo', 'PR', 'Rua Carlos Barbosa', 'Vila Industrial', '2727', '', 'pecas.fabianopolimentos@outlook.com', '2019-08-09 17:59:39', '2019-08-31 11:52:38', '3'),
('5d517676-b2ac-4a51-a1f7-4c57c0b9d769', 'w94d2cl2bcw94d2cl2bc12082019112343', '5bbcf8ed-fc00-4f65-8e02-4a8dc0b9d769', '09721199000111', 'Automotiva peças', 'Geovan Sepp', '02251154922', 'Masculino', '06/04/1978', '(44) 3528-0201', '(44) 3528-0201', '85935000', 'Assis Chateaubriand', 'PR', 'Av. Brasil', 'Jardim Paraná', '522', '', 'automotiva10@uol.com.br', '2019-08-12 11:23:50', '2019-08-12 11:23:50', NULL),
('5d532237-d07c-4c05-b0aa-4c6dc0b9d769', 'zfbqwe0f8zzfbqwe0f8z13082019174347', '5bb37841-e56c-48d4-9b29-495bc0b9d769', '81193823000182', 'cjv serviços ', 'Guilherme Deves', '10306152924', 'Masculino', '24/12/1997', '(45) 3036-4550', '(45) 99854-7265', '85818270', 'Cascavel', 'PR', 'Rua Espanha', 'Cascavel Velho', '833', '', 'pecasguilhermerodao12@gmail.com', '2019-08-13 17:48:55', '2019-08-31 11:47:43', '3'),
('5d56f3fe-b874-4b0a-b05b-4b5cc0b9d769', 'b46a4emdr0b46a4emdr016082019152032', '5bbb9663-94fc-45bb-9ac1-2281c0b9d769', '76209634000110', 'KRAMER CENTRO AUTOMO', 'JULIA KRAMER PEROTTI', '04885622956', 'Feminino', '28/02/1986', '(45) 3253-1696', '(45) 99972-7603', '85930000', 'Nova Santa Rosa', 'PR', 'AV. SANTO CRISTO', 'CENTRO', '1150', '', 'kramercentroautomotivo@gmail.com', '2019-08-16 15:20:46', '2019-08-31 11:52:55', '3'),
('5d5ad816-2d50-46b4-a467-4b38c0b9d769', 'f4ftp9n7a9f4ftp9n7a919082019140209', '5bb218c5-3f70-412f-ab4f-6829c0b9d769', '08047895000121', 'MJ CARS CHAPEAÇÃO', 'Frank Ferreira Lima Pain Alves', '01034639927', 'Masculino', '12/07/1990', '(45) 99978-4529', '(45) 3278-6376', '85904210', 'Toledo', 'PR', 'Rua Carlos Barbosa', 'Vila Industrial', '3025', '', 'oficinatoledo@outlook.com', '2019-08-19 14:10:46', '2019-08-31 11:51:31', '3'),
('5d5c06d4-2b3c-476e-afac-4e02c0b9d769', 'tjrdqwq8rmtjrdqwq8rm20082019114146', '5c051ef6-3bec-4807-95d1-3d6dc0b9d769', '09676506000190', 'NEYCAR', 'OSVALDO GOBBE DA SILVA JUNIOR', '04622320932', 'Masculino', '09/11/1986', '(44) 3045-6262', '(44) 3045-6264', '87708140', 'Paranavaí', 'PR', 'Rua Hermenegildo Souza Medeiros', 'Jardim Paraíso', '98', '', 'neycar_am@hotmail.com', '2019-08-20 11:42:28', '2019-08-31 11:52:08', '3'),
('5d5c3530-e27c-4040-af10-4436c0b9d769', 'lw43r3dth6lw43r3dth620082019145951', '5bb27298-3848-4c1a-9232-40c2c0b9d769', '04073826000131', 'COMAUTO ', 'MARCELO DE SOUZA BARROSO ', '02447477929', 'Masculino', '26/09/1977', '(44) 3425-1122', '(44) 3425-5396', '87900000', 'Loanda', 'PR', 'AV PARANA', 'PQ INDUST II', '2336', '', 'pecas.comauto1@gmail.com', '2019-08-20 15:00:16', '2019-08-31 11:50:26', '3'),
('5d5d63b6-df48-4940-989a-4451c0b9d769', 'ne9vc2p5snne9vc2p5sn21082019122853', '5bb0f8fd-99fc-4823-9979-4ba9c0b9d769', '01906960000188', 'Dirceu de Souza Leal', 'Dirceu de Souza Leal', '63538873968', 'Masculino', '09/10/1963', '(45) 99129-7175', '(45) 3235-1905', '85485000', 'Três Barras do Paraná', 'PR', 'Rua castelo branco', 'Centro', '341', '', 'chapeacaoleal@gmail.com', '2019-08-21 12:31:02', '2019-08-31 11:48:13', '3'),
('5d5d63ce-9c0c-4562-b1a4-4a72c0b9d769', 'ne9vc2p5snne9vc2p5sn21082019122853', '5bb0f8fd-99fc-4823-9979-4ba9c0b9d769', '01906960000188', 'Dirceu de Souza Leal', 'Dirceu de Souza Leal', '63538873968', 'Masculino', '09/10/1963', '(45) 99129-7175', '(45) 3235-1905', '85485000', 'Três Barras do Paraná', 'PR', 'Rua castelo branco', 'Centro', '341', '', 'chapeacaoleal@gmail.com', '2019-08-21 12:31:26', '2019-08-31 11:48:25', '3'),
('5d5ddf63-c0a8-47e0-840e-4705c0b9d769', 'b31a8jk8wsb31a8jk8ws21082019211757', '5d5d5ce4-1ad4-4940-a156-4782c0b9d769', '19794799000163', 'EXPRESSO PATO BRANCO', 'EXPRESSO PATO BRANCO', '', '', '', '(46) 3225-1485', '(46) 99109-2720', '', '', '', '', '', '', '', 'FINANCEIRO@EXPRESSOPATOBRANCO.COM.BR', '2019-08-21 21:18:43', '2019-08-31 11:50:55', '3'),
('5d5ed741-edb0-42e7-b5e2-4f10c0b9d769', 'tcz01uxbvttcz01uxbvt22082019145607', '5d5e9d83-f598-40d8-a102-4d87c0b9d769', '01418903000150', 'MULT DIESEL ', 'SERGIO CESAR ZAMPARONE', '', '', '', '(44) 3423-8040', '(44) 3423-1156', '', '', '', '', '', '', '', 'multdiesel@pvai.com.br', '2019-08-22 14:56:17', '2019-08-22 14:56:17', NULL),
('5d629bee-57a4-45a1-aac7-4d70c0b9d769', 'wstz0cgcuowstz0cgcuo25082019113025', '5bbb898f-7510-432c-9ea4-492fc0b9d769', '19583382000151', 'reparação maffissoni', 'rafael hornes dos santos', '03720836924', 'Masculino', '08/10/1982', '(46) 3223-1006', '(46) 3223-4106', '85506090', 'Pato Branco', 'PR', 'Rua Amambaí', 'Pinheirinho', 'pato', '', 'pecas@maffissoni.com.br', '2019-08-25 11:32:14', '2019-08-31 11:51:50', '3'),
('5d62b8b7-4460-4e5e-a4bc-41adc0b9d769', '5l7n2f4u1z5l7n2f4u1z25082019133431', '5c2f45aa-2fac-432f-abdc-496dc0b9d769', '79855482000111', 'pasa e cia', 'amarildo pasa', '12345678912', 'Masculino', '01/01/2001', '(46) 3225-3122', '(46) 3225-3122', '85504000', 'Pato Branco', 'PR', 'Avenida Tupi', 'Bortot', '780', '', 'mecanicapasa@hotmail.com', '2019-08-25 13:35:03', '2019-08-31 11:53:09', '3'),
('5d642406-47bc-413e-a44c-4ef7c0b9d769', '6ftmwmju4x6ftmwmju4x26082019150637', '5bb277ec-420c-4c72-9b59-4a2fc0b9d769', '01662148000154', 'chapeação daniel', 'daniel dall orsoletta & cia ltda', '05494347982', 'Masculino', '14/06/1986', '(46) 3055-2145', '(46) 9912-0908', '85604070', 'francisco beltrão', 'pr', 'são roque', 'cango', '455', '', 'chapeacaodaniel@wln.com.br', '2019-08-26 15:25:10', '2019-08-31 11:47:21', '3'),
('5d645932-9380-4bfe-89fe-436bc0b9d769', '2npmr15kru2npmr15kru26082019190743', '5c09612d-9bd8-4ac9-8e2a-48d9c0b9d769', '11336041000170', 'AUTO MECANICA SANTAN', 'ALEXANDRE LUIZ SANTANA', '01671144929', 'Masculino', '14/05/1970', '(44) 3441-3363', '(44) 3441-3363', '87890000', 'Terra Rica', 'PR', 'AVENIDA JAMES PATRICK CLARK', 'CENTRO', '584', '', 'automecanicasantana@hotmail.com', '2019-08-26 19:12:02', '2019-08-31 11:52:21', '3'),
('5d6575e3-4e1c-4ed8-8fce-49f2c0b9d769', 'g0lr6j8pwvg0lr6j8pwv27082019151709', '5c0572ac-6e30-48f8-8972-4f8bc0b9d769', '16945361000140', 'Premium Reparação', 'Luiz Bonatto', '07299670933', 'Masculino', '01/07/1989', '(45) 3223-4963', '(45) 98802-6259', '85801031', 'Cascavel', 'PR', 'Rua Rio de Janeiro', 'Centro', '2736', '', 'premium.ra@hotmail.com', '2019-08-27 15:26:43', '2019-08-31 11:47:56', '3'),
('5d668451-20e8-4869-8802-4d3ec0b9d769', 'alb32kyp26alb32kyp2628082019101847', '5c364852-a250-4e45-97bb-4761c0b9d769', '82616368000143', 'Irmãos Zorzi Ltda', 'Roberto Zorzi', '', '', '', '(44) 3453-1259', '', '', '', '', '', '', '', '', 'autopecaszorzi@hotmail.com', '2019-08-28 10:40:33', '2019-08-28 10:40:33', NULL),
('5d668778-a170-43f0-9135-4380c0b9d769', 'x7yu521uvfx7yu521uvf28082019105354', '5bfd2290-3dd8-4a9c-82c6-4e0dc0b9d769', '02672503000139', 'CENTRO AUTOMOT WEISS', 'LUIZ CARLOS WEISS', '33511470900', 'Masculino', '25/03/1958', '(45) 3254-8336', '(45) 99804-6026', '85960000', 'Marechal Cândido Rondon', 'PR', 'AVENIDA MARIPA', 'centro', '1029', '', 'centroautomotivoweiss@gmail.com', '2019-08-28 10:54:00', '2019-08-28 10:54:00', NULL),
('5d67c5a0-3d78-4827-9ffc-4c2ac0b9d769', 'q2u56mp4llq2u56mp4ll29082019093120', '5bb3c827-bf7c-4f60-b956-417bc0b9d769', '26956642000152', 'Renovacar Cascavel', 'Ednei Baleiro', '16367549889', 'Masculino', '23/11/1972', '(45) 3306-6361', '(45) 3306-6359', '85805530', 'Cascavel', 'PR', 'Rua Ernandes de Oliveira', 'Pioneiros Catarinenses', '1497', '', 'renovacarcascavel@gmail.com', '2019-08-29 09:31:28', '2019-08-29 09:31:28', NULL),
('5d67d7dc-2b0c-4848-ac8a-4892c0b9d769', 'k9uih4cax1k9uih4cax129082019104907', '5bbbb8e1-07f8-4fb6-a78e-412ac0b9d769', '77239176000125', 'Chapearia Noroeste ', 'Douglas Fernandes da Silva', '05835391900', 'Masculino', '26/05/1986', '4498520821', '(44) 9852-0821', '87209018', 'Cianorte', 'PR', 'Avenida Rio Branco', 'Zona de Armazém', '928', '', 'dougefla@hotmail.com', '2019-08-29 10:49:16', '2019-08-29 10:49:16', NULL),
('5d680b96-c6a4-4d48-8411-4086c0b9d769', '7viah0pkql7viah0pkql29082019142937', '5bbb3fa0-0954-4eeb-afa2-49afc0b9d769', '14479068000100', 'AUTO MECANICA MASTER', 'L.C.N. VIEIRA MECANICA E AUTO PECAS ME', '03739516984', 'Feminino', '15/09/1980', '(44) 3528-4839', '(44) 9820-2339', '85935000', 'Assis Chateaubriand', 'PR', 'AVENIDA LONDRINA', 'JARDIM PARANA', '195', '', 'vaniagravacolu@hotmail.com', '2019-08-29 14:29:58', '2019-08-29 14:29:58', NULL),
('5d6883b4-df74-4e02-ac13-43c6c0b9d769', 'rwlagp0xfnrwlagp0xfn29082019225215', '5bcf7cbf-8abc-4787-999f-4830c0b9d769', '05053686000100', 'Recuperadora Aut.Viv', 'Edson Asserman Scheleder', '73731293900', 'Masculino', '15/12/69', '(46) 3232-2063', '(46) 99102-5304', '85550000', 'Coronel Vivida', 'PR', 'rua Iguaçu', 'centro', '846', '', 'recvividense@wln.com.br', '2019-08-29 23:02:28', '2019-08-29 23:02:28', NULL),
('5d6907af-9cd8-4069-b773-51e2c0b9d769', 'f2bwxdg3cgf2bwxdg3cg30082019082157', '5bb26136-000c-4dfb-bb46-463cc0b9d769', '21334526000188', 'MAFFISSONI PINTURA E', 'EDISON DOS SANTOS', '00463162994', 'Masculino', '24/07/1978', '(46) 99925-1000', '(46) 99940-6136', '85660000', 'Dois Vizinhos', 'PR', 'AV MARIO DE BARROS ', 'CENTRO SUL', '444', '', 'ALEMAOFERROVELHO@HOTMAIL.COM', '2019-08-30 08:25:35', '2019-08-30 08:25:35', NULL),
('5d691924-6f84-43a8-905a-4c56c0b9d769', 'iyj2uifrb1iyj2uifrb130082019093939', '5bb60dbc-fd20-4d1c-be98-46f2c0b9d769', '04987935000164', 'premier chapeacao', 'rodrigo fernandes', '06934512979', 'Masculino', '26/09/1989', '(45) 3223-1212', '(45) 3223-1212', '85801040', 'Cascavel', 'PR', 'Rua Santa Catarina', 'Centro', '377', '', 'vistoria@premierchapeacao.com.br', '2019-08-30 09:40:04', '2019-08-30 09:40:04', NULL),
('5d691a22-5b90-4ec9-845b-4fa9c0b9d769', 'mxm7u6u8gmmxm7u6u8gm30082019094415', '5bb60dbc-fd20-4d1c-be98-46f2c0b9d769', '04987935000164', 'premier chapeacao', 'rodrigo fernandes', '06934512979', 'Masculino', '26/09/1989', '(45) 3223-1212', '(45) 3223-1212', '85801040', 'Cascavel', 'PR', 'Rua Santa Catarina', 'Centro', '377', '', 'vistoria@premierchapeacao.com.br', '2019-08-30 09:44:18', '2019-08-30 09:44:18', NULL),
('5d691acd-fcb4-4c4a-a97f-4a8dc0b9d769', 'cijgm3mltocijgm3mlto30082019094705', '5bb60dbc-fd20-4d1c-be98-46f2c0b9d769', '04987935000164', 'premier chapeacao', 'rodrigo fernandes', '06934512979', 'Masculino', '26/09/1989', '(45) 3223-1212', '(45) 3223-1212', '85801040', 'Cascavel', 'PR', 'Rua Santa Catarina', 'Centro', '377', '', 'vistoria@premierchapeacao.com.br', '2019-08-30 09:47:09', '2019-08-30 09:47:09', NULL),
('5d692a29-16b0-4221-9fd7-4b2dc0b9d769', 'u2g42qdclau2g42qdcla30082019105233', '5d48689f-0d20-4263-8142-40efc0b9d769', '12085808000106', 'Oestcap Distribuidor', 'Oestcap Distribuidora de Auto Pecas Eireli Me', '', '', '', '(45) 3220-3500', '(45) 3220-3514', '', '', '', '', '', '', '', 'nino@oestcap.com.br', '2019-08-30 10:52:41', '2019-08-30 10:52:41', NULL),
('5d693bbc-62a0-44c8-bb1d-4491c0b9d769', 'uxa98nd0d6uxa98nd0d630082019120729', '5d66743f-7600-456d-b7d4-467ac0b9d769', '85056067000109', 'COPEMA', 'BC COMERCIO DE AUTO PEÇAS LTDA', '', '', '', '(45) 3262-1296', '(45) 3262-1187', '', '', '', '', '', '', '', 'copemaautopecas@hotmail.com', '2019-08-30 12:07:40', '2019-08-30 12:07:40', NULL),
('5d69533a-9e60-436f-bc80-5696c0b9d769', '3fwcp72ajl3fwcp72ajl30082019134733', '5bb60b56-ddfc-4c7b-bf70-4b49c0b9d769', '07258600000101', 'Mecânica Serginho', 'Caliquian Passolongo', '07712622990', 'Feminino', '31/07/1993', '(44) 99924-0474', '(44) 3649-5969', '85950-00', 'Palotina', 'PR', 'Rua Pixinguinha', 'Jardim Dallas', '803', '', 'caliquian@hotmail.com', '2019-08-30 13:47:54', '2019-08-30 13:47:54', NULL),
('5d69640b-9ecc-4f2d-b076-4e7cc0b9d769', 'w79nua8oakw79nua8oak30082019145253', '5c07af27-e128-4231-bcdc-4371c0b9d769', '01878125000181', 'CHAPEAÇÃO MISSIO', 'CESAR MINETTO MISSIO', '39532712968', 'Masculino', '18/01/1959', '(45) 3268-1554', '(45) 3268-1554', '85892000', 'Santa Helena', 'PR', 'RUA ARGENTINA', 'CENTRO', '2045', '', 'c.missio@hotmail.com', '2019-08-30 14:59:39', '2019-08-30 14:59:39', NULL),
('5d69702e-9fac-467b-bdd1-413ac0b9d769', '2gx00bvx722gx00bvx7230082019155100', '5d4347d1-752c-475e-8281-424ac0b9d769', '08783646000102', 'j.m.zorzetto', 'mauro /alcides ', '', '', '', '(46) 9409-3044', '(46) 3224-4814', '', '', '', '', '', '', '', 'xaximrecup2017@gmail.com', '2019-08-30 15:51:26', '2019-08-30 15:51:26', NULL),
('5d697c84-dce8-479c-b359-4fbbc0b9d769', '6nlb8cv7sr6nlb8cv7sr30082019164357', '5bb623ef-09c0-40bd-b80c-4c8ac0b9d769', '05510399000174', 'Nilso Tintas', 'Nilso Mendonça Arruda', '48334898991', 'Masculino', '14/08/1962', '(44) 3528-5484', '(44) 3528-5484', '85935000', 'Assis Chateaubriand', 'PR', 'Rua das Paineiras', 'Centro', '187', '', 'nilsoarruda@yahoo.com.br', '2019-08-30 16:44:04', '2019-08-30 16:44:04', NULL),
('5d697d92-f350-439b-8688-45d6c0b9d769', '2yydy8cnej2yydy8cnej30082019164825', '5d66e27d-eed4-4004-949a-491cc0b9d769', '20695421000191', 'Wolf funilaria e pin', 'Edson wolf', '', '', '', '(44) 99807-6080', '(44) 99999-1720', '', '', '', '', '', '', '', 'edsondaianelipe@gmail.com', '2019-08-30 16:48:34', '2019-08-30 16:48:34', NULL),
('5d6984e0-5e78-43a0-b59c-4afdc0b9d769', 'cdsuyvx95vcdsuyvx95v30082019171127', '5bb6b113-77f4-48bf-b156-4961c0b9d769', '24479478000104', 'Chaveiro móvel', 'Fernando Demétrio', '03367976997', 'Masculino', '14/06/1979', '(45) 99924-8819', '(45) 3054-8644', '85910130', 'Toledo', 'PR', 'Rua Alcebíades Formighieri', 'Vila Pioneiro', 'Toledo', '', 'nando_demetrio@hotmail.com', '2019-08-30 17:19:44', '2019-08-30 17:19:44', NULL),
('5d698f98-16bc-48bd-a6af-45efc0b9d769', 'ajk0ogbhk3ajk0ogbhk330082019180340', '5bc4d394-54b8-4262-9833-4b49c0b9d769', '06048825000162', 'RS CHAPEACAO E PINTU', 'SANDRA MARCIA BASSANEZE KUHN', '86990411953', 'Feminino', '14/10/1974', '(46) 3536-1920', '(46) 3536-7864', '85660-00', 'DOIS VIZINHOS', 'PR', 'AV VER. DORVALINO TOSI 494, 35367864', 'JARDIM MARCANTE', '494', '', 'RSCHAPEACAOEPINTURA@HOTMAIL.COM', '2019-08-30 18:05:28', '2019-08-30 18:05:28', NULL),
('5d6a60e6-aee8-46bf-a87f-455dc0b9d769', '68b483f1ny68b483f1ny31082019085822', '5bb60b56-ddfc-4c7b-bf70-4b49c0b9d769', '07258600000101', 'Mecânica Serginho', 'Caliquian Passolongo', '07712622990', 'Feminino', '31/07/1993', '(44) 99924-0474', '(44) 3649-5969', '85950-00', 'Palotina', 'PR', 'Rua Pixinguinha', 'Jardim Dallas', '803', '', 'caliquian@hotmail.com', '2019-08-31 08:58:30', '2019-08-31 08:58:30', NULL),
('5d6cfbb0-2268-4447-a34b-4db7c0b9d769', 'ax21307w4hax21307w4h02092019082131', '5bcf6367-96a0-4d06-8233-4ea4c0b9d769', '72421936000105', 'Fiorentina Veiculos', 'Ronaldo Godoy Bueno', '09911718964', 'Masculino', '01/08/1995', '4699096421', '(46) 9909-6421', '85660-00', 'Dois Vizinhos', 'PR', 'Rua Paulino Palmas Lima, 44, casa', 'Jardim das Palmas', '44', '', 'godoy398@gmail.com', '2019-09-02 08:23:28', '2019-09-02 08:23:28', NULL),
('5d6d02d3-9b50-447d-881f-466bc0b9d769', 'pp36fbx8b0pp36fbx8b002092019085022', '5bb79890-a3e4-4f5b-8a22-4c66c0b9d769', '12157581000159', 'recuperadora bolzan', 'Carlos paulo bolzan', '65876172987', 'Masculino', '16/11/1965', '(45) 3264-1541', '(45) 99966-1927', '85884000', 'MEDIANEIRA', 'PR', 'Av Pedro soccol', 'centro', '1980', '', 'recuperadorabolzan@hotmail.com', '2019-09-02 08:53:55', '2019-09-02 08:53:55', NULL),
('5d6d087f-88d0-4502-b82b-44b0c0b9d769', 'p5j0rq7bovp5j0rq7bov02092019091657', '5c23a10f-c4ac-4dd2-b4b7-4350c0b9d769', '75405860000104', 'AUTO ARAPONGAS FIAT', 'JEAN FERNANDO CORREIA DOS SANTOS', '09907843989', 'Masculino', '16/07/1997', '(43) 99655-3698', '(43) 3055-3636', '86701010', 'Arapongas', 'PR', 'Rua Uirapuru', 'Centro', '987', '', 'jeanfernando08@hotmail.com', '2019-09-02 09:18:07', '2019-09-02 09:18:07', NULL),
('5d6d0e90-25b8-4b1c-a521-4e7fc0b9d769', 'a4s2xrq87ua4s2xrq87u02092019093848', '5c05254f-d830-4370-b808-ff41c0b9d769', '10645186000190', 'CHAPEAÇÃO DIPLOMATA', 'MARCOS ANTONIO FURTADO ', '02095809920', 'Masculino', '21/01/1977', '(45) 99963-1929', '(32) 28-3799', '85804260', 'CASCAVEL', 'PR', 'AV GRALHA AZUL 345', 'CHAPEAÇÃO DIPLOMATA', '345', '', 'CHAPEACAODIPLOMATA@HOTMAIL.COM', '2019-09-02 09:44:00', '2019-09-02 09:44:00', NULL),
('5d6d0fdd-57f0-408d-8a1f-4fc4c0b9d769', 'mdp5flpiwrmdp5flpiwr02092019094848', '5bbbbcd0-fde0-416b-94f2-4406c0b9d769', '72112352000140', 'Bragião Chapeacao e ', 'Celso Bragião', '45280622915', 'Masculino', '12/08/1961', '(45) 3227-2774', '(45) 99917-0292', '85816540', 'Cascavel', 'PR', 'Avenida Rocha Pombo', 'São Cristóvão', '367', '', 'celsobragiao.me@hotmail.com', '2019-09-02 09:49:33', '2019-09-02 09:49:33', NULL),
('5d6d17e6-6a80-45de-9a8c-4f77c0b9d769', 'jpicdtn8kzjpicdtn8kz02092019102210', '5bbb9663-94fc-45bb-9ac1-2281c0b9d769', '76209634000110', 'KRAMER CENTRO AUTOMO', 'JULIA KRAMER PEROTTI', '04885622956', 'Feminino', '28/02/1986', '(45) 3253-1696', '(45) 99972-7603', '85930000', 'Nova Santa Rosa', 'PR', 'AV. SANTO CRISTO', 'CENTRO', '1150', '', 'kramercentroautomotivo@gmail.com', '2019-09-02 10:23:50', '2019-09-02 10:23:50', NULL),
('5d6d1801-b82c-4923-897c-4e4ac0b9d769', 'e7x8d3a9khe7x8d3a9kh02092019102414', '5bbb9663-94fc-45bb-9ac1-2281c0b9d769', '76209634000110', 'KRAMER CENTRO AUTOMO', 'JULIA KRAMER PEROTTI', '04885622956', 'Feminino', '28/02/1986', '(45) 3253-1696', '(45) 99972-7603', '85930000', 'Nova Santa Rosa', 'PR', 'AV. SANTO CRISTO', 'CENTRO', '1150', '', 'kramercentroautomotivo@gmail.com', '2019-09-02 10:24:17', '2019-09-02 10:24:17', NULL),
('5d6d2491-a040-47d6-b7f9-4e74c0b9d769', 'wa2oonk201wa2oonk20102092019111135', '5bfbdc52-2d90-4359-bc09-df59c0b9d769', '76804111000112', 'CAUS', 'AUTO MECANICA CAUS', '01524306959', 'Feminino', '21/02/74', '(45) 3227-2671', '(45) 99974-3788', '85816270', 'Cascavel', 'PR', 'Rua Ponta Grossa', 'São Cristóvão', '2208', '', 'mecanicacaus@brturbo.com.br', '2019-09-02 11:17:53', '2019-09-02 11:17:53', NULL),
('5d6d2528-d974-4cc9-af85-474fc0b9d769', '0zif93unob0zif93unob02092019111945', '5bb214a5-0354-4e59-b606-4964c0b9d769', '02607058000123', 'David Pinturas', 'David Emerencio Bezerra', '72466197987', 'Masculino', '13/10/1972', '(44) 3542-1926', '(44) 99831-9280', '87345000', 'Campina da Lagoa', 'PR', 'Rua ver Nelson da Silveira', 'Centro', '317', '', 'david_regional@hotmail.com', '2019-09-02 11:20:24', '2019-09-02 11:20:24', NULL),
('5d6d2d90-ed40-4d8c-92fb-414cc0b9d769', 'x1p6avve6sx1p6avve6s02092019115527', '5c0a72c4-4680-477b-b321-47a5c0b9d769', '86864089000150', 'casa do automovel', 'Jose Gilberto Romano', '35055758953', 'Masculino', '29/03/1963', '(44) 3423-1700', '(44) 99102-1814', '87705190', 'Paranavaí', 'PR', 'Avenida Paraná', 'Jardim América', '1561', '', 'casadoautomovel.loja@hotmail.com', '2019-09-02 11:56:16', '2019-09-02 11:56:16', NULL),
('5d6d4600-dbd0-4377-a3ee-4770c0b9d769', 'klmrs7k9elklmrs7k9el02092019133944', '5c10f112-bea0-4fc8-96cb-4e1ec0b9d769', '05530669000109', 'Mecânica do Odair', 'Adair Stanhaus', '62824120991', 'Masculino', '19/09/1965', '(45) 3324-8828', '(45) 99912-6821', '85816390', 'Cascavel', 'PR', 'Rua Estados Unidos', 'Pacaembu', '574', '', 'mec.odair@hotmail.com', '2019-09-02 13:40:32', '2019-09-02 13:40:32', NULL),
('5d6d54c1-7c0c-4844-b6d8-4b10c0b9d769', 'afn67qzvndafn67qzvnd02092019144018', '5be2cafb-e058-422d-a63c-4065c0b9d769', '10519252000185', 'posto gn', 'neimar vinicius arnhold', '97464732049', 'Masculino', '17/11/1978', '(45) 99987-3246', '(45) 3273-1358', '85927000', 'Novo Sarandi (Toledo)', 'PR', 'sao salvador', 'centro', '456', '', 'NEIMARARNHOLD@HOTMAIL.COM', '2019-09-02 14:43:29', '2019-09-02 14:43:29', NULL),
('5d6d5504-725c-482b-8573-4bc0c0b9d769', 'w9b11rnnebw9b11rnneb02092019144222', '5d49cb28-8c8c-466b-bf07-4556c0b9d769', '07347899000162', 'Savioli Martelinho ', 'Sergio Luiz Savioli', '', '', '', '(44) 3639-4716', '(44) 3622-7251', '', '', '', '', '', '', '', 'oficinasavioli@uol.com.br', '2019-09-02 14:44:36', '2019-09-02 14:44:36', NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `tb_pedidos_troca`
--
ALTER TABLE `tb_pedidos_troca`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
