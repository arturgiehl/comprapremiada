<header>
    <div class="bg1">
        <div class="container-fix">
            <div class="row">
<!--                <div class="four columns logout">
                    <?PHP
//                        if($this->Session->check('Cliente')){  
//                            echo $this->Html->link('<i class="fas fa-power-off"></i> Sair','/cadastro/logoff?'.date('sdmYHis'),array('escape'=>false));
//                        }else{
//                            echo $this->Html->link('Cadastro',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register'),array('escape'=>false));
//                        }
                    ?>
                </div>-->
                <div class="seven columns search">
                    &nbsp;
                    <?PHP
//                        $value = isset($_GET['s']) ? $_GET['s']:'';
//                        echo $this->Form->create('Busca', array('url' => array('plugin'=>'buscas','controller'=>'buscas','action'=>'index'),'id'=>'search','type'=>'get'));
//                            echo $this->Form->input('s',array('label'=>false,'div'=>'back-input','value'=>$value,'placeholder'=>'Buscar produto'));
//                            echo $this->Form->submit(' ',array('div'=>false));
//                        echo $this->Form->end();
                    ?>
                </div>
                <div class="five columns right-b1">
                    <?PHP
                        if($this->Session->check('Cliente')){
                            echo '<span style="display: inline-block; margin-right: 15px;">Pontuação: <b>'.$cpontos.'</b></span>';
                            echo $this->Html->link('<i class="fas fa-power-off"></i> Sair','/cadastro/logoff?'.date('sdmYHis'),array('escape'=>false));
                        }else{
                            echo $this->Html->link('<i class="fas fa-user"></i> faça seu <b>login</b> ou <b>cadastre-se</b>',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register'),array('escape'=>false,'class'=>'loginc'));
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fix bgh2">
        <div class="row">
            <div class="three columns logo">
                <?PHP echo $this->Html->link($this->Html->image('logo.png',array('alt'=>'Compra Premiada')),'/',array('escape'=>false)); ?>
            </div>
            <div class="nine columns m-right">
                <div class="openmenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <nav id="bgmenu">
                    <ul>
                        <?PHP
//                            echo '<li class="li-close">'.$this->Html->link($this->Html->image('close.svg',array('alt'=>'Fechar')),'javascript:void(0)',array('escape'=>false,'class'=>'button-close')).'</li>';
//                            echo '<li>'.$this->Html->link('Inicial','/',array('escape'=>false,'id'=>'mn1')).'</li>';
//                                echo '<li class="space"><span>|</span></li>';
                            echo '<li>'.$this->Html->link('Produtos',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false,'id'=>'mn1')).'</li>';
                                echo '<li class="space"><span>|</span></li>';
//                            echo '<li>'.$this->Html->link('Pontuação',array('plugin'=>'clientes','controller'=>'clientes','action'=>'pontos'),array('escape'=>false,'id'=>'mn3')).'</li>';
//                                echo '<li class="space"><span>|</span></li>';
                            echo '<li>'.$this->Html->link('Sobre a Campanha','javascript:void(0)',array('escape'=>false,'id'=>'mn1')).'</li>';
                                echo '<li class="space"><span>|</span></li>';
//                            echo '<li>'.$this->Html->link('Regulamento',array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'index'),array('escape'=>false,'id'=>'mn-n')).'</li>';
                            echo '<li>'.$this->Html->link('Regulamento','javascript:void(0)',array('escape'=>false,'id'=>'mn-n')).'</li>';
                                echo '<li class="space"><span>|</span></li>';
                            echo '<li>'.$this->Html->link('Contato','javascript:void(0)',array('escape'=>false,'id'=>'mn3')).'</li>';
//                                echo '<li class="space"><span>|</span></li>';
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>