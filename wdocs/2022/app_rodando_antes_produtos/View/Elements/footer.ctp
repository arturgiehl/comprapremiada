<footer>
    <div class="row bg-indicacao">
        <div class="container-fix">
            <?PHP echo $this->Form->create('Newsletter',array('url'=>array('plugin'=>'contatos','controller'=>'newsletters','action'=>'index'))); ?>
                <div class="row">
                    <div class="five columns itext">
                        <p class="p1">Não encontrou o que procurava?</p>
                        <p class="p2">Indique o prêmio que você gostaria de encontrar aqui.</p>
                    </div>
                    <div class="five columns">
                        <?PHP echo $this->Form->input('nome',array('div'=>false,'label'=>false,'placeholder'=>'Sugestão')); ?>
                    </div>
                    <div class="two columns">
                        <?PHP echo $this->Form->submit('enviar',array('div'=>false)); ?>
                    </div>
                </div>
            <?PHP echo $this->Form->end(); ?>
        </div>
    </div>
    
    <div class="row bg-concessionarias">
        <div class="container-fix">
            <h3>Concessionárias</h3>
            <?PHP
                foreach ($concessionarias as $concessionaria) {
                    $replace = array('(', ')', ' ', '-');
                    $telefone = str_replace($replace, '', $concessionaria['Conce']['telefone']);
                    
                    echo '<div class="concessionaria">';
                        echo '<h4>'.$concessionaria['Conce']['title'].'</h4>';
                        echo $this->Html->link(''.$concessionaria['Conce']['telefone'],'tel:'.$telefone,array('escape'=>false));
                    echo '</div>';
                }
            ?>
        </div>
    </div>
    
    <div class="bg-low-footer">
        <div class="container-fix">
            <div class="row">
                <div class="three columns logo-footer">
                    <?PHP echo $this->Html->image('logo2021.png',array('alt'=>'Compra premiada')); ?>
                </div>
                <div class="three columns menu-footer">
                    <?PHP  
                        echo $this->Html->link('Produtos',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false));
                        echo $this->Html->link('Regulamento',array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'index'),array('escape'=>false));
                        echo $this->Html->link('Contato',array('plugin'=>'contatos','controller'=>'contatos','action'=>'index'),array('escape'=>false));
                    ?>
                </div>
                <div class="six columns logos-fm">
                    <?PHP  
                        echo $this->Html->image('mopar.png',array('alt'=>'Mopar','class'=>'mopar'));
                        echo $this->Html->image('rede_fipal.png',array('alt'=>'Fipal','class'=>'fipal'));
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <div id="box-whatsapp">
        <?PHP 
            $useragent = $_SERVER['HTTP_USER_AGENT'];
                        
            if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
                echo $this->Html->link($this->Html->image('button_whatsapp.png',array('alt'=>'Fale conosco via WhatsApp','title'=>'Fale conosco via WhatsApp')),'https://api.whatsapp.com/send?phone=5545988018781',array('escape'=>false,'target'=>'_blank'));
            }else{
                echo $this->Html->link($this->Html->image('button_whatsapp.png',array('alt'=>'Fale conosco via WhatsApp','title'=>'Fale conosco via WhatsApp')),'https://web.whatsapp.com/send?phone=5545988018781',array('escape'=>false,'target'=>'_blank'));
            }
        ?>
    </div>
    
    <?PHP
//        echo '<div class="box-acept-cookies">';
//            echo '<div class="box-content">';
//                echo '<p>Utilizamos cookies para melhorar a sua <br> experiência de navegação em nosso site</p>';
//                echo $this->Html->link('OK','javascript:void(0)',array('escape'=>false,'id'=>'acept_bc'));
//            echo '</div>';
//        echo '</div>';
        
        echo '<div class="box-cookies-navegation">';
            echo '<div class="box-content">';
                echo '<p>Nós utilizamos cookies e outras tecnologias para possibilitar e aprimorar sua experiência em nosso site, e ao continuar navegando em nossas páginas você concorda com a coleta e uso de cookies. Para saber mais, visite nosso '.$this->Html->link('Termos de Uso e Política de Privacidade',array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'termo_uso'),array('escape'=>false,'target'=>'_blank')).'. </p>';
                echo $this->Html->link('Aceitar','javascript:void(0)',array('escape'=>false,'class'=>'btn-acept','id'=>'acept_bc'));
            echo '</div>';
        echo '</div>';
    ?>
</footer>