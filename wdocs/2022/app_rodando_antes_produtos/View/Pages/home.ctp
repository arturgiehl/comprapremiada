<?PHP
    if(count($banners) === 1){
        echo '<div class="banner-single">';
            if($banners[0]['Banner']['link']){
                echo '<a href="'.$banners[0]['Banner']['link'].'" target="'.$banners[0]['Banner']['target'].'">';
                    echo $this->Html->image('/'.$banners[0]['Banner']['thumb'],array('alt'=>''));
                echo '</a>';
            }else{
                echo $this->Html->image('/'.$banners[0]['Banner']['thumb'],array('alt'=>''));
            }
        echo '</div>';

        echo '<div class="banner-single-mobile">';
            if($banners[0]['Banner']['link']){
                echo '<a href="'.$banners[0]['Banner']['link'].'" target="'.$banners[0]['Banner']['target'].'">';
                    echo $this->Html->image('/'.$banners[0]['Banner']['thumb_mobile'],array('alt'=>''));
                echo '</a>';
            }else{
                echo $this->Html->image('/'.$banners[0]['Banner']['thumb_mobile'],array('alt'=>''));
            }
        echo '</div>';
    }else{
        echo '<div id="bannerslider">';
            echo '<div class="slider-banner">';
                foreach ($banners as $banner) {
                    echo '<div class="slide">';
                        if($banner['Banner']['link']){
                            echo '<a href="'.$banner['Banner']['link'].'" target="'.$banner['Banner']['target'].'">';
                                echo $this->Html->image('/'.$banner['Banner']['thumb'],array('alt'=>''));
                            echo '</a>';
                        }else{
                            echo $this->Html->image('/'.$banner['Banner']['thumb'],array('alt'=>''));
                        }
                    echo '</div>';
                }
            echo '</div>';
        echo '</div>';

        echo '<div id="bannerslider-mobile">';
            echo '<div class="slider-banner-mobile">';
                foreach ($banners as $banner) {
                    echo '<div class="slide">';
                        if($banner['Banner']['link']){
                            echo '<a href="'.$banner['Banner']['link'].'" target="'.$banner['Banner']['target'].'">';
                                echo $this->Html->image('/'.$banner['Banner']['thumb_mobile'],array('alt'=>''));
                            echo '</a>';
                        }else{
                            echo $this->Html->image('/'.$banner['Banner']['thumb_mobile'],array('alt'=>''));
                        }
                    echo '</div>';
                }
            echo '</div>';
        echo '</div>';
    }
?>
    
<section class="top-content">
    <div class="container-fix">
        <div class="row box-imgs">
            <div class="seven columns bi-left">
                <?PHP
                    echo $this->Html->link($this->Html->image('home/cadastre-se-agora.png',array('alt'=>'Cadastre-se agora e troque por produtos')),array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register','?'=>array('referer'=>'home')),array('escape'=>false)); 
                    echo $this->Html->link($this->Html->image('home/mais-de-100-premios.png',array('alt'=>'Mais de 100 produtos')),array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false,'class'=>'m100')); 
                ?>
            </div>
            <div class="five columns bi-right">
                <?PHP echo $this->Html->link($this->Html->image('home/1-real-1-ponto.png',array('alt'=>'Cadastre-se agora e troque por produtos')),array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false));  ?>
            </div>
        </div>
        
        <h1 class="h1p h1p-home">Veja alguns dos produtos disponíveis</h1>
        <?PHP
            $count = 1;
            foreach ($produtos as $produto) {
                if($produto['Produto']['imagem']){
                    if(file_exists('img/media_cache/fill/265x160/ffffff/'.$produto['Produto']['imagem'].'')){
                        $image = $this->Html->image('media_cache/fill/265x160/ffffff/'.$produto['Produto']['imagem'].'',array('alt'=>$produto['Produto']['title']));
                    }else{
                        $image = $this->Fill->image($produto['Produto']['imagem'],265,160,'ffffff',array('alt'=>$produto['Produto']['title']));
                    }
                }else{
                    $image = $this->Html->image('noimg265x160.jpg',array('alt'=>'Imagem indisponível'));
                }

                if($count === 1) echo '<div class="row">';
                    echo '<div class="three columns produto-list">';
                        echo $this->Html->link($image,array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false));
                        echo $this->Html->link($produto['Produto']['title'],array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false,'class'=>'title'));
//                            echo $this->Html->link(number_format($produto['Produto']['preco'], 0, ',', '.').' pontos <i class="fas fa-shopping-basket"></i>',array('plugin'=>'produtos','controller'=>'produtos','action'=>'view','slug'=>$produto['Produto']['slug']),array('escape'=>false,'class'=>'price'));
                        echo '<p class="price">'.number_format($produto['Produto']['preco'], 0, ',', '.').' pontos <i class="fas fa-shopping-basket"></i></p>';
                        echo '<div class="row buttons">';
                            if($this->Session->check('Cliente')){  
                                echo $this->Html->link('<i class="fas fa-shopping-cart"></i> Incluir no carrinho',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'add','idp'=>$produto['Produto']['id']),array('escape'=>false,'class'=>'addcar add-orc')); 
                            }else{
                                echo $this->Html->link('<i class="fas fa-shopping-cart"></i> Incluir no carrinho',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register','?'=>array('referer'=>'home')),array('escape'=>false)); 
                            }
                        echo '</div>';
                    echo '</div>';
                if($count === 4){
                    echo '</div>';
                    $count = 0;
                }
                $count++;
            }
            if($count > 1 && $count <=4) echo '</div>';

            echo $this->element('Painel.paginator'); 
        ?>

        <div class="row vie-all">
            <?PHP echo $this->Html->link('Ver mais <i class="fas fa-plus"></i>',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false));  ?>
        </div>
    </div>
</section>
    