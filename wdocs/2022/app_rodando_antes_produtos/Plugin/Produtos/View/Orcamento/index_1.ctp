<?PHP
    if(isset($produtos) && count($produtos) > 0){
        echo '<input type="hidden" id="thisbase" value="'.$this->base.'">';
        
        echo '<div class="title_content">FINALIZAR ORÇAMENTO</div>';
        echo '<div class="inside">';

            echo '<div class="cabecalho_orc">';
                echo '<div class="item_cabecalho" style="width: 170px;">FOTO</div>';
                echo '<div class="item_cabecalho" style="width: 430px; text-align: left;">PRODUTO</div>';
                echo '<div class="item_cabecalho" style="width: 120px;">QTDE</div>';
                echo '<div class="item_cabecalho" style="width: 135px;">OPÇOES</div>';
            echo '</div>';
            echo '<div class="clear"></div>';
        
            echo '<div id="products-orc">';
                foreach ($produtos as $produto) {
                    $quantidade = $this->requestAction(array('plugin'=>'produtos','controller'=>'orcamento','admin'=>false,'action'=>'get_quantidade',$produto['Produto']['id']));

                    if($produto['Produto']['thumb']) 
                        $imagem = $this->Fill->image($produto['Produto']['thumb'], 100, 70,'FFF', array()); 
                    else 
                        $imagem = $this->Fill->image('/img/noimage.jpg', 100, 70,'FFF', array()); ;

                    $unidades = '';
                    if($produto['Produto']['unidades']) $unidades = $produto['Produto']['unidades'].' unidades';

                    echo '<div class="div-orc">';
                        echo $imagem;
                            echo '<div class="tit-orc">
                                '.$produto['Produto']['title'].'<br>
                                <b>'.$unidades.'</b>
                             </div>';
                        echo '<div class="qtd-orc">
                                <div class="loadinput" id="loadinput'.$produto['Produto']['id'].'"></div> <br> 
                                <div class="divinput"><input type="text" style="width: 30px; padding-left: 10px; height: 40px;" value="'.$quantidade.'" id="qtd'.$produto['Produto']['id'].'" readonly="true" /></div> 
                                <div class="arrowup" id="'.$produto['Produto']['id'].'"></div> 
                                <div class="arrowdown" id="'.$produto['Produto']['id'].'"></div> 
                             </div>';
                        echo '<div class="excluir-orc" id="'.$produto['Produto']['id'].'">EXCLUIR</div>';
                    echo '</div>';
                    echo '<input type="hidden" id="verify_'.$produto['Produto']['id'].'" value="'.$produto['Produto']['id'].'">';
                    echo '<script type="text/javascript">$(document).ready(function(){ $("#products-orc").show(); });</script>';
                }
            echo '</div>';
            
            echo '<div class="add_more">'.$this->Html->link('+ ADICIONAR MAIS PRODUTOS AO ORÇAMENTO',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false)).'</div>';
            echo '<div class="clear"></div>';
            
            echo $this->Form->create('Orcamento',array('id'=>'contato_orcamento'));
                echo $this->Form->input('nome',array('label'=>'Nome'));
                    echo '<div class="clear"></div>';
                echo $this->Form->input('email',array('label'=>'E-mail'));
                    echo '<div class="clear"></div>';
                echo $this->Form->input('telefone',array('label'=>'Telefone','id'=>'telefone'));
                    echo '<div class="clear"></div>';
                echo $this->Form->input('endereco',array('label'=>'Endereço'));
                    echo '<div class="clear"></div>';
                echo $this->Form->input('cidade',array('label'=>'Cidade/Estado'));
                    echo '<div class="clear"></div>';
                echo $this->Form->input('pagamento',array('label'=>'Pretensão de pagamento'));

                echo '<div class="clear"></div>';
                echo $this->Form->input('mensagem',array('label'=>'Mensagem','type'=>'textarea'));
                echo $this->Form->submit('ENVIAR ORÇAMENTO',array('class'=>'content_contact_button','div'=>false));
            echo $this->Form->end();
            
        echo '</div>';
    }else{
        echo '<div class="clear40"></div>';
        echo '<h1>Nenhum produto adicionado ao seu orçamento!</h1>';
        echo '<div class="clear20"></div>';
        echo '<div class="add_more">'.$this->Html->link('+ ADICIONAR PRODUTOS AO ORÇAMENTO',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false)).'</div>';
        echo '<div class="clear40"></div>';
        echo '<div class="clear40"></div>';
        echo '<div class="clear40"></div>';
        echo '<div class="clear20"></div>';
    }
?>

<script type="text/javascript">
$(document).ready(function(){ 
    $("#select-products").hide(); 
    $(".ajl").hide(); 
    $(".loadinput").hide(); 
});

jQuery(function($){
    $('#telefone').mask('(99) 9999.9999');
    
    $('.arrowup').click(function () {
        var idp = this.id;
        $("#qtd"+idp+"").hide();
        $("#loadinput"+idp+"").show();
        var url = $("#thisbase").val()+"/produtos/orcamento/add_quantidade";
        $.get(url,{ 
                   calculo: 'mais',
                   produto_id: this.id,
                   qtd_atual: $("#qtd"+idp+"").val()
                  },function(data) {
                        $("#loadinput"+idp+"").hide();
                        $("#qtd"+idp+"").show();
                        $("#qtd"+idp+"").val(data); 
                });
    });
    
    $('.arrowdown').click(function () {
        var idp = this.id;
        if($("#qtd"+idp+"").val() === '1'){
            return false;
        }
        $("#qtd"+idp+"").hide();
        $("#loadinput"+idp+"").show();
        var url = $("#thisbase").val()+"/produtos/orcamento/add_quantidade";
        $.get(url,{ 
                   calculo: 'menos',
                   produto_id: this.id,
                   qtd_atual: $("#qtd"+idp+"").val()
                  },function(data) {
                        $("#loadinput"+idp+"").hide();
                        $("#qtd"+idp+"").show();
                        $("#qtd"+idp+"").val(data); 
                });
    });
    
    $('.licategory').click(function () {
        $(".ajl").show(); 
        $("#select-products").hide(); 
        var url = $("#thisbase").val()+"/produtos/orcamento/get_product";
        $.get(url,{ 
                   categoria_id: this.id
                  },function(data) {
                        $(".ajl").hide(); 
                        $("#select-products").show(); 
                        $("#select-products").html(data); 
                });
    });
    
    $('.excluir-orc').click(function () {
        var now = new Date(); 
        var hs = now.getHours()+""+now.getMinutes()+""+now.getSeconds();
        if(confirm("Excluir este produto?")){
            $(".ajp").show(); 
            var url = $("#thisbase").val()+"/produtos/orcamento/remove_product?"+hs;
            $.get(url,{ 
                       produto_id: this.id
                      },function(data) {
                            $(".ajp").hide(); 
                            $("#products-orc").show(); 
                            $("#products-orc").html(data); 
                    });
        }
    });
    
});

</script>