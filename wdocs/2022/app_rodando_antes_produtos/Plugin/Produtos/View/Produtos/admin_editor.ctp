<link rel="stylesheet" href="<?PHP echo $this->base ?>/selectize/selectize.css">
<script src="<?PHP echo $this->base ?>/selectize/selectize.js"></script>

<?PHP
echo $this->Html->script('jquery.maskMoney.js',array('inline'=>true));

echo $this->Form->create('Produto',array('url'=>array('action'=>'add')));

    $destaque = array('0'=>'Não', '1'=>'Sim');
    $ativo = array('1'=>'Sim', '0'=>'Não');
?>

<fieldset class="box" id="box_categoria">
    <legend>Ativo:</legend>
    <?PHP 
        $ativo = array('1'=>'Sim', '0'=>'Não');
        echo $this->Form->input('ativo',array('type'=>'select','options'=>$ativo,'label'=>false)); 
    ?>
</fieldset>

<fieldset class="box" id="box_categoria">
    <legend>Categoria:</legend>
    <?PHP echo $this->Form->input('cat_id',array('type'=>'select','options'=>$categorias,'label'=>false,'required'=>'required','id'=>'select_categoria')); ?>
</fieldset>

<div id="loader" style="display: none;">Carregando Sub Categorias.... <br> </div>

<!--<div id="box_sub_categorias">
    <?PHP // if(isset($sub_categorias) && count($sub_categorias) > 0){ ?>
        <fieldset class="box">
            <legend>Sub Categoria:</legend>
            <?PHP // echo $this->Form->input('subcategoria_id',array('type'=>'select','options'=>$sub_categorias,'label'=>false,'name'=>'data[Produto][subcategoria_id]')); ?>
        </fieldset>
    <?PHP // } ?>
</div>-->

<fieldset class="box">
    <legend>Notícia</legend>
    <?PHP
        echo $this->Form->input('title',array('label'=>'Título'));
        echo $this->Form->input('preco',array('label'=>'Pontos','id'=>'preco'));
        echo $this->Form->input('texto',array('label'=>'Texto','class'=>'ckeditor'));
//        echo $this->Form->input('text',array('label'=>'Texto','class'=>'ckeditor'));
        
    ?>
</fieldset>

<?PHP
echo $this->element('Painel.image',array('label'=>'Imagem Destaque','name'=>'imagem'));
echo $this->element('Painel.gallery',array('label'=>'Galeria de Imagens'));
echo $this->element('Painel.videos',array('label'=>'Vídeo'));

//echo $this->element('Painel.downloads',array('label'=>'Arquivos para Download',array('extensions'=>'pdf,rar,zip,xls,xlsx,doc,docx,ppt,pptx')));
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>


<script>
var base = '<?= $this->base ?>';
    
$(document).ready(function(){
    $("#preco").maskMoney({showSymbol:true, decimal:",", thousands:"."});

//    $('#select_categoria').change(function () { 
//        $('#loader').show();
//        var value_input = this.value;
//        var now = new Date(); 
//        var hs = now.getDate()+""+now.getHours()+""+now.getMinutes()+""+now.getSeconds();
//
//        $.ajax({
//            url: base+"/admin/produtos/produtos/get_sub_categorias?rand="+hs,
//            type: "post",
//            data: { catid: value_input } ,
//            success: function (response) {
//                $(document).ajaxComplete(function() {
//                    $('#box_sub_categorias').html(response);
//                    $('#loader').hide();
//                });             
//            },
//            error: function(jqXHR, textStatus, errorThrown) {
//               console.log(textStatus, errorThrown);
//            }
//        });
//    });
    
    $('#palavras_chave').selectize({
        persist: false,
        createOnBlur: true,
        create: true,
        plugins:['remove_button'],
        delimiter: '; '
    });

});
</script>