<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; " />
        <title>Pedido</title>
        <style type="text/css">
            body {
                width: 100%;
                font-family: Verdana, Geneva, sans-serif;
                border: 1px solid transparent;
                display: table;
                line-height: 1.6;
                background-color: #f6f6f6;
                color: #494B4B;
            }
            .content {
                width: 650px; 
                margin-top: 40px; 
                margin: 0 auto;
                display: block;
                font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; 
                box-sizing: border-box; 
                background: #fff; 
                padding: 20px; 
                border: 1px solid #e9e9e9;
            }
            .maintitle {
                font-size: 18px;
                color: #FF3B3F;
                font-weight: bold;	
                text-align: center;
            }
            .title2 {
                margin-top: 20px;
                font-size: 16px;
                text-align: left;
                font-weight: bold;
            }
            #corpo {
                margin-top: 10px;
                margin-bottom: 10px;
                font-size: 15px;
                margin-left: 10px;
            }
            .titulo {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <div class="maintitle">Pedido de troca - <?PHP echo date('d/m/Y H:i'); ?> </div>

            <table id="corpo">
                <tr class="linha">
                    <td class="titulo">CNPJ:</td>
                    <td class="descricao"><?PHP echo $data['cnpj']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Nome Fantasia:</td>
                    <td class="descricao"><?PHP echo $data['nome_fantasia']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Nome:</td>
                    <td class="descricao"><?PHP echo $data['nome']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">CPF:</td>
                    <td class="descricao"><?PHP echo $data['cpf']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Sexo:</td>
                    <td class="descricao"><?PHP echo $data['sexo']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Data Nascimento:</td>
                    <td class="descricao"><?PHP echo $data['data_nascimento']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Telefone 1:</td>
                    <td class="descricao"><?PHP echo $data['telefone1']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Telefone 2:</td>
                    <td class="descricao"><?PHP echo $data['telefone2']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">CEP:</td>
                    <td class="descricao"><?PHP echo $data['cep']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Cidade:</td>
                    <td class="descricao"><?PHP echo $data['cidade']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Estado:</td>
                    <td class="descricao"><?PHP echo $data['estado']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Endereço:</td>
                    <td class="descricao"><?PHP echo $data['endereco']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Bairro:</td>
                    <td class="descricao"><?PHP echo $data['bairro']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">Número:</td>
                    <td class="descricao"><?PHP echo $data['numero']; ?></td>
                </tr>
                <tr class="linha">
                    <td class="titulo">E-mail:</td>
                    <td class="descricao"><?PHP echo $data['email']; ?></td>
                </tr>
            </table>
            
            <table cellspacing=0 cellpadding=4 style="width: 100%; margin-top: 20px">
                <thead style="text-align: left; font-size: 14px;">
                    <tr>
                      <th style="border-bottom: 1px solid #ccc; width: 40%">Produto</th>
                      <th style="border-bottom: 1px solid #ccc">Quantidade</th>
                      <th style="border-bottom: 1px solid #ccc">Ponto Unitário</th>
                      <th style="border-bottom: 1px solid #ccc">Total</th>
                    </tr>
                </thead>
                <tbody style="font-size: 13px">
                    <?PHP
                        $total_pedido = 0;
                        foreach ($produtos as $produto) {
                            $total_pedido = $total_pedido + $produto['Produto']['preco'] * $produto['ProdutoSessao']['quantidade'];
                            echo '<tr>';
                                echo '<td style="border-top: 1px solid #ccc;">'.$produto['Produto']['title'].'</td>';
                                echo '<td style="border-top: 1px solid #ccc;">'.$produto['ProdutoSessao']['quantidade'].'</td>';
                                echo '<td style="border-top: 1px solid #ccc;">'.number_format($produto['Produto']['preco'], 0, ',', '.').'</td>';
                                echo '<td style="border-top: 1px solid #ccc;">'.number_format($produto['Produto']['preco'] * $produto['ProdutoSessao']['quantidade'], 0, ',', '.').'</td>';
                            echo '</tr>';
                        }
                        echo '<tr>';
                            echo '<td style="border-top: 1px solid #ccc;">&nbsp;</td>';
                            echo '<td style="border-top: 1px solid #ccc;">&nbsp;</td>';
                            echo '<td style="border-top: 1px solid #ccc;">&nbsp;</td>';
                            echo '<td style="border-top: 1px solid #ccc;">'.number_format($total_pedido, 0, ',', '.').'</td>';
                        echo '</tr>';
                    ?>
                </tbody>
            </table>
            
            <table id="corpo">
                <tr class="linha">
                    <td class="titulo">Observação:</td>
                    <td class="descricao"><?PHP echo $data['observacao']; ?></td>
                </tr>
            </table>
            <?PHP 
//                if(count($produtos) > 0){ 
////                    echo '<div class="title2">Produtos</div>';
//                    echo '<table id="corpo">';
//                        echo '<tr class="linha">';
//                                echo '<td class="title2">Produto</td>';
//                                echo '<td class="title2" style="padding-left: 10px;">Quantidade</td>';
//                        echo '</tr>';
//                        foreach ($produtos as $produto) {
//                            echo '<tr class="linha">';
//                                echo '<td class="titulo">'.$produto['Produto']['title'].'</td>';
//                                echo '<td class="titulo" style="padding-left: 10px;">'.$produto['ProdutoSessao']['quantidade'].'</td>';
//                            echo '</tr>';
//                        }
//                    echo '</table>';
//                } 
             ?>
        </div>
    </body>
</html>