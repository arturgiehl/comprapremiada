<?PHP
class OrcamentoCliente extends ProdutosAppModel {
    
    public $useTable  = 'pedidos_troca';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(OrcamentoCliente.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(OrcamentoCliente.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $belongsTo = array(
        'Cliente'=>array(
            'className'=>'Clientes.Cliente',
            'foreignKey'=>'cliente_id'
        ),
    ); 
    
}