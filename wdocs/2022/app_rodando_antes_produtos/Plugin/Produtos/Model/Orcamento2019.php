<?PHP
class Orcamento2019 extends ProdutosAppModel {
    
    public $useTable  = 'pedidos_troca_2019';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Orcamento2019.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Orcamento2019.modified,'%d/%m/%Y %H:%i')",
    );
    
}