<?PHP
class ProdutosAppController extends AppController{
    
    public $uses = array(
        'Produtos.Produto',
        'Produtos.CategoriaProduto',
        'Produtos.ProdutoSessao',
        'Produtos.Orcamento',
        'Produtos.OrcamentoCliente',
        'Produtos.Orcamento2018',
        'Produtos.Orcamento2019',
        'Produtos.Orcamento2020',
        'Produtos.Orcamento20202',
        'Produtos.SubCategoria',
        'Clientes.Cliente',
    );
    
}