<?PHP
Configure::write('Painel.menu.Produtos',array(
    'Gerenciar'=>array('plugin'=>'produtos','controller'=>'produtos','action'=>'index','admin'=>true),
    'Adicionar Produto'=>array('plugin'=>'produtos','controller'=>'produtos','action'=>'add','admin'=>true),
    'separator',
    'Gerenciar Categorias'=>array('plugin'=>'produtos','controller'=>'categoria_produtos','action'=>'index','admin'=>true),
    'Adicionar Nova'=>array('plugin'=>'produtos','controller'=>'categoria_produtos','action'=>'add','admin'=>true),
//    'separator',
//    'Gerenciar Sub Categorias'=>array('plugin'=>'produtos','controller'=>'sub_categorias','action'=>'index','admin'=>true),
//    'Adicionar Sub Categoria'=>array('plugin'=>'produtos','controller'=>'sub_categorias','action'=>'add','admin'=>true),
));