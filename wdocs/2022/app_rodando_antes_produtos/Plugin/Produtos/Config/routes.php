<?PHP
Router::connect('/produtos',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'));
Router::connect('/produtos/page/:page',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('page'=>'[0-9]+'));

Router::connect('/produtos/categoria/:categoria',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('categoria'=>'[a-zA-Z0-9-_]+'));
Router::connect('/produtos/categoria/:categoria/page/:page',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('categoria'=>'[a-zA-Z0-9-_]+','page'=>'[0-9]+'));

Router::connect('/produtos/categoria/:categoria/:subslug',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('categoria'=>'[a-zA-Z0-9-_]+','subslug'=>'[a-zA-Z0-9-_]+'));
Router::connect('/produtos/categoria/:categoria/:subslug/page/:page',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('categoria'=>'[a-zA-Z0-9-_]+','subslug'=>'[a-zA-Z0-9-_]+','page'=>'[0-9]+'));

Router::connect('/produtos/:slug',array('plugin'=>'produtos','controller'=>'produtos','action'=>'view'),array('slug'=>'[a-zA-Z0-9-_]+'));

Router::connect('/load',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'loader'));
Router::connect('/enviar-pedido-troca',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'send'));
Router::connect('/adicionar-no-carrinho/:idp',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'add'),array('idp'=>'[a-zA-Z0-9-_]+'));
Router::connect('/remover-do-carrinho/:idp',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'delete_orc'),array('idp'=>'[a-zA-Z0-9-_]+'));
Router::connect('/meu-carrinho',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index'));


