<fieldset class="box">
    <legend>Departamentos do Formulário de Contato</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar',array('action'=>'add')); ?>
    </nav>    
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('title','Título')?></td>
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody class="sortable">
            <?PHP foreach($posts as $post){ ?>
            <tr data-id="<?PHP echo $post['Departamento']['id']; ?>">
                <td><?PHP echo $post['Departamento']['title']?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['Departamento']['id']))?></td>
                <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['Departamento']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>

<script type="text/javascript">
    var base='<?= $this->base ?>';
    jQuery(function($){
        $(".sortable").sortable({
            stop:function(evt,ui){
                $(".sortable>tr").each(function(i,e){
                    var id=$(e).attr('data-id');
                    var index=$(e).index();
                    $.getJSON(base+'/admin/departamentos/departamentos/order/'+id+'/'+index,function(data){
                    });
                });
            }
        });
    });
</script>