<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="robots" CONTENT="noindex, nofollow">
        <title>Sistema Administrativo - ADMX Digital</title>
        <script type="text/javascript">
            var base = '<?= $this->base ?>';
        </script>
        <?PHP
        echo $this->fetch('meta');
        echo $this->Html->css('http://fonts.googleapis.com/css?family=Roboto+Slab|Open+Sans');
        echo $this->Html->css('Painel.reset');
        echo $this->Html->css('Painel.admin.less','stylesheet/less');
        echo $this->Html->css('Painel.images.less','stylesheet/less');
        echo $this->Html->css('Painel.videos.less','stylesheet/less');
        echo $this->Html->css('Painel.downloads.less','stylesheet/less');
        echo $this->Html->css('Painel.galleries.less','stylesheet/less');
//        echo $this->Html->css('/fancybox/jquery.fancybox');
        echo $this->fetch('css');

        echo $this->Html->script(array(
            'Painel.less',
            'Painel./ckeditor/ckeditor',
            'Painel.jquery',
            'Painel.jquery.ui.min',
//            'Painel.jquery.upload',
            'Painel.admin',
            'Painel.downloads',
            'Painel.videos',
            'Painel.images',
            'Painel.jquery.price_format.1.8.min',
//            'Painel.jquery.mask',
            //CONSTRUCTING
//            'Painel./js/galleries',
//            '/fancybox/jquery.fancybox',
        ));
        //FETCH
        echo $this->fetch('script');
        ?>
    </head>

    <body>

        <nav id="mainmenu">
            <?PHP echo $this->Html->image('Painel.logo.png'); ?>
            <div class="usuario">
                <span class="user">Usuário: <span><?PHP echo $this->Session->read('Auth.User.username') ?></span></span>
                <?PHP echo $this->Html->link('logout','/logout', array('class'=>'logout')); ?>
                <div class="user-pannel">
                    <?PHP
                    echo $this->Locker->link('alterar dados',array('plugin'=>'painel','controller'=>'usuarios','action'=>'data',$this->Session->read('Auth.User.id')));
                    echo $this->Locker->link('alterar senha',array('plugin'=>'painel','controller'=>'usuarios','action'=>'password'));
                    ?>
                </div>
            </div>

            <ul class="menu">
                <?PHP
                $menus = Configure::read('Painel.menu');
                foreach ($menus as $key=>$menu):
                    $current = false;
                    $plus = '';
                    foreach ($menu as $k=>$v):
                        if ($k == 'separator' || $v == 'separator') {
                            if ($plus != '') $plus.='<li><hr></li>';
                        }
                        else $plus.=$this->Locker->link("<li>$k</li>", $v, array('escape'=>false));
                        if ($this->here == $this->Html->url($v)) {
                            $current = true;
                        }
                    endforeach;
                    if ($plus != "") {
                        $more = $current ? 'open' : '';
                        echo '<li class="li ' . $more . '"><a href="javascript:void(0);">' . $key . '</a>';
                        echo "<ul>$plus</ul>";
                        echo "</li>";
                    }
                endforeach;
                echo $this->Html->link('Desenvolvimento: AMEXCOM','http://www.amexcom.com.br',array('class'=>'developer','escape'=>false,'target'=>'_blank'));
                ?>
            </ul>
        </nav>
        <div id="contents"><?PHP echo $this->fetch('content'); ?></div>
        <?php
        if (Configure::read('read') >= 4)
            echo $this->element('sql_dump');
        ?>
        <script type="text/javascript">
            jQuery(function($){
                $("fieldset.languaged").css('display','none');
                $("fieldset[data-language=ptb]").css('display','block');
                $(".languages>a").click(function(){
                    var lang=$(this).attr('href').split('#')[1];
                    $("fieldset.languaged").css('display','none');
                    $("fieldset[data-language="+lang+"]").css('display','block');
                    $('.languages>a').removeClass('active');
                    $(this).addClass('active');
                    return false;
                });
                
                $("fieldset.sectorized").css('display','none');
                $("fieldset[data-sector=contact]").css('display','block');
                $(".sectors>a").click(function(){
                    var sector=$(this).attr('href').split('#')[1];
                    $("fieldset.sectorized").css('display','none');
                    $("fieldset[data-sector="+sector+"]").css('display','block');
                    $('.sectors>a').removeClass('active');
                    $(this).addClass('active');
                    if(sector == 'locale'){
                        $('#map_canvas').empty();
                        getMap();
                    }
                    return false;
                });
                
//                $('tbody td.activate a').live('click',function(){
//                    var url=$(this).attr('href');
//                    var $this=$(this);
//                    $.ajax({
//                        url:url,
//                        success:function(data){
//                            $this.parent().html(data);
//                        }
//                    })
//                    return false;
//                });
            });
        </script>
    </body>
</html>