<?PHP
if(!isset($label)) $label="Imagem destaque";
$data=$this->data;
$data=array_shift($data);
$path=(isset($data[$name]) && !empty($data[$name])) ? $data[$name]:'';
$image=isset($path) && !empty($path) ? "/media/crop/100x100/$path" : 'Painel.noimage.png';

if(isset($reduce) && $reduce === 'banner'){
    $reduce = $reduce;
}else{
    $reduce = 'reduce';
}
?>

<fieldset class="box image file" data-name="<?=$name?>" id="box_image">
    <legend><?=$label?></legend>
    <?PHP
        echo $this->Form->input("$name-picker",array('type'=>'file','label'=>'Selecione uma imagem','data-extensions'=>'jpg,png,gif,jpeg','data-maxsize'=>8000,'reduce'=>$reduce));
        echo $this->Form->hidden($name,array('value'=>$path));
        echo $this->Html->image($image);
        echo '<div id="progress"><div class="loading"></div></div>';
    ?>
</fieldset>