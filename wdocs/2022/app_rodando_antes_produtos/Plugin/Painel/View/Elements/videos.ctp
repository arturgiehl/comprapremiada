<?PHP
if(!isset($label)) $label=__("Galeria de vídeos");
?>

<fieldset class="box image videos">
    <legend><?=$label?></legend>
    <nav class="buttons">
        <a href="javascript:void(0);" class="selector"><?=__("Adicionar vídeo do youtube")?></a>
    </nav>
    
    <div class="items">
        <?PHP
        //pre($this->data);
        if(isset($this->data['Video']) && !empty($this->data['Video'])){
            $i=0;
            foreach($this->data['Video'] as $v):
        ?>
        <div class="item">
            <a class="remove" href="javascript:void(0);">x</a>
            <img src="//i2.ytimg.com/vi/<?=$v['yid']?>/hqdefault.jpg" height="100">
            <!--<input type="text" name="data[Video][<?=$i?>][title]" placeholder="Título do vídeo" value="<?=$v['title']?>">-->
            <input type="hidden" name="data[Video][<?=$i?>][yid]" value="<?=$v['yid']?>">
        </div>
        <?PHP
            $i++;
            endforeach;
        }
        ?>
    </div>
</fieldset>