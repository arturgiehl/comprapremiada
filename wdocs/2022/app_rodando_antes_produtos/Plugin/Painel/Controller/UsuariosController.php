<?PHP
class UsuariosController extends PainelAppController {
    
    public $components=array('Session');
    public $uses=array('Painel.Gallery');
    
    ##################################################################################################################    
    public function beforeFilter(){
        parent::beforeFilter();
        $groups=$this->Locker->list_groups(true);
        $this->set(compact('groups'));
    }    
    ##################################################################################################################
    public function login() {        
        $teste=$this->Session->read('Painel.redirect');
        $this->layout = false;        
        if($this->Locker->loggedIn()){            
            $this->redirect($this->Locker->loginRedirect);
        }        
        if ($this->request->data && ($this->request->is('post') || $this->request->is('put'))) {
            if ($this->Locker->login()){
                $_SESSION['KCEDITOR']['disabled']=false;
                if($this->Session->read('Painel.redirect')) $this->redirect($this->Session->read('Painel.redirect'));
                else $this->redirect($this->Locker->loginRedirect);
            } else {
                $this->set('message', $this->Locker->message);
            }
        }
    }
    public function recover(){
        $this->layout = false;
        if ($this->request->data && ($this->request->is('post') || $this->request->is('put'))) {
            $email = $this->request->data['User']['email'];
            $user = $this->User->find('first',array('conditions'=>array('User.email'=>$email)));
            if (count($user) > 0){
                $date = date('d/m/Y H:m:s');
                $newpasswd = md5($date.$email);
                $hashpasswd = Security::hash($newpasswd, NULL, true);
                $this->User->data = $user;
                $this->User->data['User']['password'] = $hashpasswd;
                $user = $this->User->data;
                if($this->User->save($this->User->data)){
                    App::uses('CakeEmail','Network/Email');
                    $mail=new CakeEmail('smtp');
                    $mail->template('Painel.recover');
                    $mail->to($user['User']['email']);
                    $mail->emailFormat('html');
                    $mail->subject("Pedido de nova senha' | em $date");
                    $mail->viewVars(array('data'=> $user['User']));
                    $mail->send();
                    echo '<script type="text/javascript">';
                    echo 'alert("Nova senha enviada para o e-mail cadastrado.");';
                    echo 'window.location.href="'.$this->base.'/admin";';
                    echo '</script>';
                    exit;
                } else {
                    echo '<script type="text/javascript">';
                    echo 'alert("Erro! Tente novamente mais tarde.");';
                    echo 'window.location.href="'.$this->base.'/admin";';
                    echo '</script>';
                    exit;
                }
            } else {
                $this->set('message','E-mail não cadastrado.');
            }
        }
    }
    ##################################################################################################################
    public function logout() {
        $this->autoRender=false;
        if($this->Locker->loggedIn()){       
            session_destroy();
            $redirect=$this->Locker->logoutRedirect;
            $this->Locker->logout();
//            $this->redirect($redirect.'/admin');
            $this->redirect('/admin');
        } else {            
            $this->redirect('/');
        }
    }
    ##################################################################################################################
    public function admin_index(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('User.created' => 'DESC');
        $this->paginate['limit'] =20;
//        $this->paginate['conditions'] = array('Activate.user_id IS NULL');
        $users=$this->paginate('User');
        $this->set('title','Usuários cadastrados');
        $this->set('groups',$this->Locker->config->groups);
        $this->set(compact('users'));
    }
    ##################################################################################################################
    public function admin_filter($group){
        if(!isset($this->Locker->config->groups[$group]['name'])){
            $this->redirect(array('action'=>'index'));
        }
        $group_name=$this->Locker->config->groups[$group]['name'];
        $this->layout="Painel.admin";
        $this->view='admin_index';
        $this->paginate['order']='User.active ASC, User.username DESC';
        $this->paginate['limit']=20;
        $this->paginate['conditions']=array(
            'Activate.user_id IS NULL',
            'group'=>$group,
         );
        $users=$this->paginate('User');
        $this->set('title','Grupo de usuários: '.$group_name);
        $this->set('groups',$this->Locker->config->groups);
        $this->set(compact('users','group_name'));
    }
    ##################################################################################################################
    public function admin_inactive($id=null){        
        if($id){
            if($this->Activate->deleteAll(array('user_id'=>$id))){
                $this->redirect(env('HTTP_REFERER'));
            }
        }        
        $this->layout="Painel.admin";
        $this->paginate['limit']=20;
        $this->paginate['conditions']=array('Activate.user_id IS NOT NULL');
        $users=$this->paginate('User');
        $this->set('title','Usuários inativos');
        $this->set(compact('users'));
    }
    ##################################################################################################################
    public function admin_blocked($id=null){
        $this->layout="Painel.admin";
        $this->view="admin_index";
        $this->paginate['limit']=20;
        $this->paginate['conditions']=array('User.active = 0');
        $users=$this->paginate('User');
        $this->set('title','Usuários bloqueados');
        $this->set(compact('users'));
    }
    ##################################################################################################################
    public function admin_add() {
        $this->layout = "Painel.admin";
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->User->save($this->request->data)){
                $id=$this->User->id;
                $this->Activate->deleteAll(array('user_id'=>$id));
                $this->redirect(array('action'=>'admin_index'));
            }
        }
    }
    ##################################################################################################################
    public function admin_edit($id){
        $this->layout = "Painel.admin";
        $this->data = $this->User->read('*',$id);   
    }
    ##################################################################################################################
    public function admin_delete($id){
        if($this->Locker->user('id')==$id){
            header('Content-Type: text/html; charset=utf-8;');
            $this->Locker->alert(__('Você não pode excluir seu próprio usuário'));
            $this->Locker->js_redirect(env('HTTP_REFERER'));
            exit;
        }
        $this->autoRender=false;
        if($this->User->delete($id)){
            $this->Activate->deleteAll(array('user_id'=>$id));
            $this->redirect(env('HTTP_REFERER'));
        }
    }
    ##################################################################################################################
    public function admin_activate($id){
        if($this->Locker->user('id')==$id){
            header('Content-Type: text/html; charset=utf-8;');
            $this->Locker->alert(__('Você não pode bloquear seu próprio usuário'));
            $this->Locker->js_redirect(env('HTTP_REFERER'));
            exit;
        }
        $this->autoRender=false;
        $this->User->recursive=-1;
        $user=$this->User->read(array('active'),$id);
        $act=$user['User']['active']?0:1;
        $this->User->save(array('id'=>$id,'active'=>$act,));
        $this->redirect(env('HTTP_REFERER'));
    }
    ##################################################################################################################
    public function admin_data(){
//        pre($this->User->validate);
    }
    ##################################################################################################################
    public function admin_password(){
        $this->layout="Painel.admin";
        $this->User->recursive=-1;
        unset($this->User->validate['username'],$this->User->validate['email'],$this->User->validate['name']);
        if($this->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->User->change_password($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
}