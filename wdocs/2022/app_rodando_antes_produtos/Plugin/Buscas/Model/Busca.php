<?PHP
class Busca extends BuscasAppModel {
    
    var $useTable = 'buscas';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Busca.created,'%d/%m/%Y %H:%i')",
    );
    
}