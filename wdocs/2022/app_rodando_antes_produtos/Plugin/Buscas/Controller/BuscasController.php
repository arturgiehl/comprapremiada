<?PHP
class BuscasController extends BuscasAppController{

    public $paginate = array('limit'=>16);
    
    public function open(){
        $this->autoRender = true;
        $this->layout = false;
    }

    public function index(){
        $this->Seo->title('Busca');
        
        $busca = strip_tags($_GET['s']);

        $save['Busca'] = array('id'=>'', 'title'=>$busca, 'ip'=>$this->getIP());
        $this->Busca->save($save);

//        $posts = $this->Produto->search($busca);
        $posts = $this->Produto->find('all',array(
            'conditions' => array(
                    'OR' => array(
                        'Produto.title LIKE' => "%".$busca."%", 
                        'Produto.texto LIKE' => "%".$busca."%",
                        'Produto.preco LIKE' => "%".$busca."%",
                    ),
                    'AND' => array('Produto.ativo' => '1')
                )
        ));
        
        $this->set('posts',$posts);    
        $this->set('categoria','');    
        $this->set('value_so','');  
        $this->set('list_categorias',$this->CategoriaProduto->find('all',array('order'=>array('title ASC'))));
    }
    
    public function admin_index(){
        $this->layout = 'Painel.admin';
        
        $this->paginate['fields'] = array('*', 'COUNT(Busca.title) AS total');
        $this->paginate['group'] = array('title ORDER BY COUNT(Busca.title) DESC');
        $posts = $this->paginate('Busca');
        
        $this->set('posts',$posts);
        $this->set('total',$this->Busca->find('count'));
    }
    
    public function getIP(){  
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  
            $ip = $_SERVER['HTTP_CLIENT_IP'];  
        }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){  
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
        }else  {  
            $ip = $_SERVER['REMOTE_ADDR'];  
        }  
        return $ip;  
    }
    
    public function admin_report(){
        $this->autoRender = false;
        $dados = $this->Busca->find('all', array('order'=>'Busca.created DESC') );
        
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: application/octet-stream");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment; filename=\"Buscas - ".date('d-m-Y').".csv\";" );
        header("Content-Transfer-Encoding: binary");
        echo "\xEF\xBB\xBF";
        echo "Busca;IP;Data;\n";
        foreach($dados as $dado){
            echo $dado['Busca']['title'].';'.$dado['Busca']['ip'].';'.$dado['Busca']['cdate']."\n";
        }
    }
    
}