<div class="view-ajax-phone">
    <p>Cascavel I (sede)</p>
    <a href="tel:4533243030"> &nbsp; (45) <b>3324-3030</b> </a>
    
    <p>Cascavel II</p>
    <a href="tel:4533241010"> &nbsp; (45) <b>3324-1010</b> </a>
    <a href="https://api.whatsapp.com/send?phone=5545998259293" target="_blank" class="whatsapp">(45) <b> 9 9825-9293 </b></a>
    
    <p>Foz do Iguaçu</p>
    <a href="tel:4535722020"> &nbsp; (45) <b>3572-2020</b> </a>
    <a href="https://api.whatsapp.com/send?phone=5545999230960" target="_blank" class="whatsapp">(45) <b> 9 9923-0960 </b></a>
    
    <p>Medianeira</p>
    <a href="tel:4532402202"> &nbsp; (45) <b>3240-2202</b> </a>
    <a href="https://api.whatsapp.com/send?phone=5545999714552" target="_blank" class="whatsapp">(45) <b> 9 9971-4552 </b></a>
    
    <p>Toledo</p>
    <a href="tel:4532520330"> &nbsp; (45) <b>3252-0330</b> </a>
    <a href="https://api.whatsapp.com/send?phone=5545999369874" target="_blank" class="whatsapp">(45) <b> 9 9936-9874 </b></a>
</div>