<fieldset class="box">
    <legend>Cliques Telefones (<?PHP echo $total; ?>)</legend>
    <table class="tables">
        <thead>
            <tr>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Data')?></td>
                <!--<td>&nbsp;</td>-->
                
                <!--<td class="delete">&nbsp;</td>-->
            </tr>
        </thead>
        <tbody>
            <?PHP foreach($posts as $post){ ?>
            <tr>
                <td><?PHP echo $post['Telefone']['cdate']?></td>
                <!--<td> <div class="viewgeoip" id="<?PHP // echo $post['Telefone']['ip']; ?>">ver localização</div> </td>-->
                
                <td><?PHP // echo $this->Html->link('Excluir',array('action'=>'delete',$post['Telefone']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>

<?PHP echo $this->Form->hidden('thisbase',array('id'=>'thisbase','value'=>$this->base));  ?>

<script type="text/javascript">
$('.viewgeoip').click(function () {
    var now = new Date(); 
    var hs = now.getHours()+""+now.getMinutes()+""+now.getSeconds();
    var ip = this.id;

    $.fancybox({
        'type':'ajax', 
        'href': $("#thisbase").val()+"/telefones/telefones/geoip?rand="+hs+"&ip="+ip,
        'titleShow'  : false,
        'cache'	: false,
        'transitionIn'  : 'elastic',
        'transitionOut' : 'elastic',
        'autoscale' : true,
        'scrolling'   : 'no',
        afterLoad: function(){
        }
    });
});
</script>

<style>
    .viewgeoip {
        font-size: 16px;
        color: #069;
        cursor: pointer;
    }
    .viewgeoip:hover {
        text-decoration: underline;
    }
</style>