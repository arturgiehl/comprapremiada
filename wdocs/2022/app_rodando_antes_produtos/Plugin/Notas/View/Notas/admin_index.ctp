<fieldset class="box">
    <legend>Notas Emitidas (<?PHP echo $total; ?> Registros)</legend>
    <?PHP
        $value = !empty($_GET['s']) ? $_GET['s'] : '';
        $options=array('type' => 'get', 'id' => 'search');
        if(!empty($url)) $options['url']=$url;
        echo $this->Form->create('Search',array('url'=>array('plugin'=>'notas', 'controller'=>'notas', 'action'=>'index'),'type' => 'get'));
        echo $this->Form->input('s', array('div'=>false,'label'=>false,'placeholder'=>'BUSCA','value'=>$value));
        echo $this->Form->end(array('label' => 'OK', 'div' => false));
    ?>
    <ul class="menu menu-destaque">
        <li><a href="javascript:void(0);">Filtro por canceladas</a>
            <ul>
                <li><?PHP echo $this->Html->link('Todas',array('action'=>'index')); ?></li>
                <?PHP
                    echo $this->Html->tag('li',$this->Html->link('Sim',array('action'=>'index','c','1')));
                    echo $this->Html->tag('li',$this->Html->link('Não',array('action'=>'index','c','0')));
                ?>
            </ul>
        </li>
    </ul>
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('cliente_codigo','Código')?></td>
                <td><?PHP echo $this->Paginator->sort('cliente_nome','Nome')?></td>
                <td><?PHP echo $this->Paginator->sort('cliente_cnpj','CNPJ')?></td>
                <td><?PHP echo $this->Paginator->sort('nf_numero','NF Número')?></td>
                <td><?PHP echo $this->Paginator->sort('nf_numero','Data Emissão')?></td>
                <td><?PHP echo $this->Paginator->sort('filial','Filial')?></td>
                
                <td><?PHP echo $this->Paginator->sort('ponto_dobro','Ponto')?></td>
                <td><?PHP echo $this->Paginator->sort('valor_nf','Valor')?></td>
                
                <td><?PHP echo $this->Paginator->sort('cancelada','Cancelada')?></td>
                <td><?PHP echo $this->Paginator->sort('manual','Manual')?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Importada')?></td>
            </tr>
        </thead>
        <tbody>
            <?PHP 
                $manual = array(''=>'', '1'=>'Sim');
                $ponto_dobro = array(''=>'', '1'=>'Sim');
                
                foreach($posts as $post){ 
            ?>
             <tr data-id="<?PHP echo $post['Nota']['id']?>">
                <td><?PHP echo $post['Nota']['cliente_codigo']?></td>
                <td><?PHP echo $post['Nota']['cliente_nome']?></td>
                <td><?PHP echo $post['Nota']['cliente_cnpj']?></td>
                <td><?PHP echo $post['Nota']['nf_numero']?></td>
                <td><?PHP echo $post['Nota']['data_emissao']?></td>
                <td><?PHP echo $post['Nota']['filial']?></td>
                
                <td><?PHP echo $post['Nota']['ponto_dobro'].'x'; ?></td>
                <td><?PHP echo number_format($post['Nota']['valor_nf'], 2, ',', '.'); ?></td>
                
                <td><?PHP echo $this->Html->image('nota_'.$post['Nota']['cancelada'].'.png',array('alt'=>'')); ?></td>
                <td><?PHP echo $manual[$post['Nota']['manual']]; ?></td>
                <td><?PHP echo $post['Nota']['cdate']?></td>
            </tr>
            <?PHP }?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>
