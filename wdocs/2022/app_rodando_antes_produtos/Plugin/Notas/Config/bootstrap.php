<?PHP
Configure::write('Painel.menu.Notas',array(
    'Emitidas'=>array('plugin'=>'notas','controller'=>'notas','action'=>'index','admin'=>true),
    'Canceladas'=>array('plugin'=>'notas','controller'=>'notas','action'=>'canceladas','admin'=>true),
    'Adicionar Nota Manual'=>array('plugin'=>'notas','controller'=>'notas','action'=>'manual','admin'=>true),
    
    'separator',
    'Emitidas - 2020 | 2'=>array('plugin'=>'notas','controller'=>'notas','action'=>'index20202','admin'=>true),
    'Canceladas - 2020 | 2'=>array('plugin'=>'notas','controller'=>'notas','action'=>'canceladas20202','admin'=>true),
    'separator',
    'Emitidas - 2020'=>array('plugin'=>'notas','controller'=>'notas','action'=>'index2020','admin'=>true),
    'Canceladas - 2020'=>array('plugin'=>'notas','controller'=>'notas','action'=>'canceladas2020','admin'=>true),
    'separator',
    'Emitidas - 2019'=>array('plugin'=>'notas','controller'=>'notas','action'=>'index2019','admin'=>true),
    'Canceladas - 2019'=>array('plugin'=>'notas','controller'=>'notas','action'=>'canceladas2019','admin'=>true),
    'separator',
    'Emitidas - 2018'=>array('plugin'=>'notas','controller'=>'notas','action'=>'index2018','admin'=>true),
    'Canceladas - 2018'=>array('plugin'=>'notas','controller'=>'notas','action'=>'canceladas2018','admin'=>true),
));