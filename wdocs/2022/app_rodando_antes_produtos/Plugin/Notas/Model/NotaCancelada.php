<?PHP
class NotaCancelada extends NotasAppModel{
    
    var $name = 'NotaCancelada';
    var $useTable = 'notas_canceladas';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(NotaCancelada.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(NotaCancelada.modified,'%d/%m/%Y %H:%i')",
    );
    
}