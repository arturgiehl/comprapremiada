<?PHP
class NotaCancelada20202 extends NotasAppModel{
    
    var $name = 'NotaCancelada20202';
    var $useTable = 'notas_canceladas_2020_2';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(NotaCancelada20202.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(NotaCancelada20202.modified,'%d/%m/%Y %H:%i')",
    );
    
}