<fieldset class="box">
    <legend>Galeria</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar',array('action'=>'add')); ?>
    </nav>    
    <table class="tables">
        <thead>
            <tr>
                <td class="thumb"><a>Foto</a></td>
                <td><?PHP echo $this->Paginator->sort('title','Nome')?></td>
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody class="sortable">
            <?PHP foreach($posts as $post){ ?>
            <tr data-id="<?PHP echo $post['Galeria']['id']; ?>">
                <td><?PHP if($post['Galeria']['imagem']) echo $this->Fill->image($post['Galeria']['imagem'],120,80,'ffffff'); ?></td>
                <td><?PHP echo $post['Galeria']['title']?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['Galeria']['id']))?></td>
                <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['Galeria']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>

<script>
    var base='<?= $this->base ?>';
    jQuery(function($){
        $(".sortable").sortable({
            stop:function(evt,ui){
                $(".sortable>tr").each(function(i,e){
                    var id=$(e).attr('data-id');
                    var index=$(e).index();
                    $.getJSON(base+'/admin/galerias/galerias/order/'+id+'/'+index,function(data){
                    });
                });
            }
        });
    });
</script>