<?PHP
class GaleriasController extends GaleriasAppController{

    public $paginate = array('limit'=>16,'order'=>array('Galeria.order_depoimento' => 'ASC', 'Galeria.created' => 'DESC'));
    
    public function index(){
        $this->Seo->title('Galeria de Fotos');
        
        $posts = $this->Galeria->find('all');
        $this->set('posts',$posts);
    }

    public function admin_index(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('Galeria.order_registro' => 'ASC', 'Galeria.created' => 'DESC');
        $posts = $this->paginate('Galeria');
        $this->set('posts',$posts);
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->Galeria->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        $this->data = $this->Galeria->read('*',$id); 
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->Galeria->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }   
    
    public function admin_order($id,$order){
        $this->autoRender = false;
        $this->Galeria->query('UPDATE tb_galerias SET order_registro = "'.$order.'" WHERE id = "'.$id.'";');
    }
    
}