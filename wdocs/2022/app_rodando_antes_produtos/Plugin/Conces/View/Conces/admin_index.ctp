<fieldset class="box">
    <legend>Concessionárias</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar novo',array('action'=>'add')); ?>
    </nav>    
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('title','Título')?></td>
                <td><?PHP echo $this->Paginator->sort('telefone','Telefone')?></td>
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody class="sortable">
            <?PHP foreach($posts as $post){ ?>
            <tr data-id="<?PHP echo $post['Conce']['id']; ?>">
                <td><?PHP echo $post['Conce']['title']?></td>
                <td><?PHP echo $post['Conce']['telefone']?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['Conce']['id']))?></td>
                <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['Conce']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>

<script>
    var base='<?= $this->base ?>';
    jQuery(function($){
        $(".sortable").sortable({
            stop:function(evt,ui){
                $(".sortable>tr").each(function(i,e){
                    var id=$(e).attr('data-id');
                    var index=$(e).index();
                    $.getJSON(base+'/admin/conces/conces/order/'+id+'/'+index,function(data){
                    });
                });
            }
        });
    });
</script>