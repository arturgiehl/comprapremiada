<fieldset class="box">
    <legend>Cadastros de clientes (<?PHP echo $total; ?> Registros)</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Gerar CSV',array('plugin'=>'clientes','controller'=>'clientes','action'=>'report','admin'=>true)) ?>
    </nav>
    <nav class="pannel" style="margin-right: 120px;">
        <?PHP echo $this->Html->link('Adicionar novo cliente',array('action'=>'add')); ?>
    </nav>    
    <?PHP
        $value = !empty($_GET['s']) ? $_GET['s'] : '';
        $options=array('type' => 'get', 'id' => 'search');
        if(!empty($url)) $options['url']=$url;
        echo $this->Form->create('Search',array('url'=>array('plugin'=>'clientes', 'controller'=>'clientes', 'action'=>'index'),'type' => 'get'));
        echo $this->Form->input('s', array('div'=>false,'label'=>false,'placeholder'=>'BUSCA','value'=>$value));
        echo $this->Form->end(array('label' => 'OK', 'div' => false));
    ?>
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('nome_fantasia','Nome Fantasia'); ?></td>
                <td><?PHP echo $this->Paginator->sort('nome','Nome'); ?></td>
                <td><?PHP echo $this->Paginator->sort('cnpj','CNPJ'); ?></td>
                
<!--                <td><?PHP echo $this->Paginator->sort('total','Total no Período'); ?></td>
                <td><?PHP echo $this->Paginator->sort('pontos_trocados','Trocados no Período'); ?></td>
                <td><?PHP echo $this->Paginator->sort('pontos_trocados','Disponível'); ?></td>-->

                <td style="text-align: center;"><?PHP echo $this->Paginator->sort('ponto_dobro','Ponto em dobro'); ?></td>
                
                <td><?PHP echo $this->Paginator->sort('aprovado','Aprovado'); ?></td>
                <td><?PHP echo $this->Paginator->sort('termos','Termos'); ?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Cadastrado'); ?></td>
                <td class="delete">&nbsp;</td>
                <td class="delete">&nbsp;</td>
                <td class="delete">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP 
                foreach($posts as $post){ 
                    $post = array_shift($post); 
                   
//                    $total_pontos = $this->requestAction(array('plugin'=>'notas','controller'=>'notas','admin'=>false,'action'=>'get_pontos2',$post['cnpj'],$post['id']));
//                    $total_pedidos_ativos = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_pontuacao_trocada',$post['cnpj']));
//                    pre($total_pontos);
//                    pre($pontos);
//                    if($total_pontos['pontos_disponiveis'] < 0){
//                        pre($post);
//                    }
                    
//                    $pontos_clientes = $this->requestAction(array('plugin'=>false,'controller'=>'app','admin'=>false,'action'=>'get_ponto_cliente_disponivel_expirado',$post['cnpj']));
//                    pre($pontos_clientes);
            ?>
            <tr>
                <td><?PHP echo $post['nome_fantasia']; ?></td>
                <td><?PHP echo $post['nome']; ?></td>
                <td><?PHP echo $post['cnpj']; ?></td>
                
<!--                <td><?PHP echo number_format($total_pontos['total_pontos'], 0, '', '.'); ?></td>
                <td><?PHP echo number_format($total_pedidos_ativos, 0, '', '.'); ?></td>
                <td><?PHP echo number_format($total_pontos['pontos_disponiveis'], 0, '', '.'); ?></td>-->
                
                <td style="text-align: center;">
                    <?PHP 
                        echo $this->Html->image('ativo_'.$post['ponto_dobro'].'.png',array('alt'=>'')); 
                        echo '<div style="clear:both; height:7px;"></div>';
                        if($post['ponto_dobro']){
                            $textt = 'Remover a pontuação em dobro do cliente '.$post['nome_fantasia'].'?';
                            echo $this->Html->link('Remover importação de ponto em dobro',array('action'=>'remover_importacao_dobro',$post['id']),array('style'=>'color:#000;'),$textt); 
                        }else{
                            $textt = 'Salvar o cliente '.$post['nome_fantasia'].' para importar as notas em dobro?';
                            echo $this->Html->link('Salvar importação em dobro',array('action'=>'salvar_importacao_dobro',$post['id']),array('style'=>'color:green;'),$textt); 
                        }
                    ?>
                </td>
                
                <td><?PHP echo $this->Html->image('ativo_'.$post['aprovado'].'.png',array('alt'=>'')); ?></td>
                <td><?PHP echo $this->Html->image('ativo_'.$post['termos'].'.png',array('alt'=>'')); ?></td>
                <td><?PHP echo $post['cdate']; ?></td>
                
                <td>
                    <?PHP
                        if($post['aprovado']){
                            echo $this->Html->link("Remover",array('action'=>'delete',$post['id']),array('style'=>'color:red;'),'Tem certeza que deseja remover o cadastro?'); 
                        }else{
                            echo $this->Html->link("Remover",array('action'=>'delete',$post['id']),array('style'=>'color:red;'),'Deseja remover o cadastro?'); 
                            echo '&nbsp;&nbsp;&nbsp;';
                            echo $this->Html->link("Aprovar",array('action'=>'aproved',$post['id']),null,"Deseja aprovar o cadastro?"); 
                        }
                    ?>
                </td>
                
                <td><?PHP echo $this->Html->link('Ver Pontuação',array('plugin'=>'clientes','controller'=>'clientes','action'=>'view_ponto',$post['id']),array('target'=>'_blank'))?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['id']),array())?></td>
                <td><?PHP echo $this->Html->link('Ver',array('action'=>'view',$post['id']),array('target'=>'_blank'))?></td>
            </tr>
            <?PHP } ?>
        </tbody>        
    </table>
    <?PHP echo $this->element("Painel.paginator");?>
</fieldset>