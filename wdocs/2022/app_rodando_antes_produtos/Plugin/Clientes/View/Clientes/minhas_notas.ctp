<?PHP
    $total_dentro6 = 0;
    foreach ($pedidos_ativos as $pedido_periodo) {
        $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_periodo['PedidoCliente']['sessao_id']));
        foreach ($produtos_pedido as $ps) {
            $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
        }
    }
?>
<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Minhas Notas',array('plugin'=>'clientes','controller'=>'clientes','action'=>'minhas_notas')); 
                echo $this->element('breadcrumb');
                
                echo '<h1 class="h1p">Histórico de Notas</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix">
        <h2 class="h2-hp">O prazo de validade para utilizar os pontos das notas é de 6 meses.</h2>
        
        <div class="row-table-responsive">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data da emissão</th>
                        <th role="columnheader">Pontos</th>
                        <th role="columnheader">Válido até:</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $total_pontos = 0;
                        foreach ($notas_atuais as $nota) {
                            echo '<tr role="row">';
                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                echo '<td role="cell">'.number_format($nota['Nota']['valor_nf'], 0, ',', '.').'</td>';
//                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                echo '<td role="cell">'.date('d/m/Y', strtotime($nota['Nota']['data_e'].' + 6 months')).'</td>';
                            echo '</tr>';
                            
                            $total_pontos = $total_pontos + $nota['Nota']['valor_nf'];
                        }
                    ?>
                </tbody>
            </table>
        </div>
        
        <div class="bg-low-table">
            <table role="table">
                <tbody role="rowgroup">
                    <tr>
                        <td role="cell"><b>Total:</b></td>
                        <td role="cell"><?PHP echo number_format($total_pontos, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Já utilizado no período:</b></td>
                        <td role="cell">
                            <?PHP 
//                                $utilizado = $pontos_usados - $total_vencidos;
                                echo '-'.number_format($total_dentro6, 0, ',', '.');
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Expirado e não utilizado:</b></td>
                        <td role="cell">
                            <?PHP 
                                echo number_format($pontos_cliente['expirado'], 0, ',', '.'); 
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Disponível para troca:</b></td>
                        <td role="cell">
                            <?PHP 
                                echo number_format($pontos_cliente['disponivel_para_troca'], 0, ',', '.'); 
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
</div>