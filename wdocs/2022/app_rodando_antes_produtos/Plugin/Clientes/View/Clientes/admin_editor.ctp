<?PHP
echo $this->Form->create('Cliente',array('url'=>array('action'=>'add')));
?>

<fieldset class="box">
    <legend>Novo Cliente</legend>
    <?PHP
        echo $this->Form->input('cnpj',array('label'=>'CNPJ'));
        echo $this->Form->input('nome_fantasia',array('label'=>'Nome da empresa'));
        echo $this->Form->input('nome',array('label'=>'Nome solicitante'));
        echo $this->Form->input('telefone1',array('label'=>'Telefone 1','maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);'));
        echo $this->Form->input('telefone2',array('label'=>'Telefone 2','maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);'));
        echo $this->Form->input('qual_vendedor',array('label'=>'Qual vendedor fipal'));
        echo $this->Form->input('email',array('label'=>'E-mail'));
//        pre($this->data);
        if(!isset($this->data['Cliente']['id'])){
            echo $this->Form->input('senha',array('label'=>'Senha','type'=>'password'));
        }
    ?>
</fieldset>

<?PHP
echo $this->Form->hidden('aprovado');
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>

<script>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>47 && tecla<58)) return true;
    else{
        if (tecla==8 || tecla==0) return true;
        else  return false;
    }
}

function mascara(o,f){
    v_obj = o;
    v_fun = f;
    setTimeout("execmascara()",1);
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function mtel(v){
    v=v.replace(/\D/g,"");             
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); 
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    
    return v;
}

function mdata(v){
    v=v.replace(/\D/g,"")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    return v;
}
</script>