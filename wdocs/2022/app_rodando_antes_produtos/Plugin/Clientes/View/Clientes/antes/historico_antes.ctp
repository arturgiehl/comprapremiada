<?PHP
//    $total_nota_antes = 0;
//    foreach ($notas_antes as $nota_a) {
//        $total_nota_antes = $total_nota_antes + $nota_a['Nota']['valor_nf'];
//    }
//    echo number_format($total_nota_antes, 0, ',', '.');
?>
<div class="bg-gray bgpb">
    <div class="row bg-bread bgmtm">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Histórico',array('plugin'=>'clientes','controller'=>'clientes','action'=>'historico')); 
                echo $this->element('breadcrumb');
                
                echo '<h1 class="h1p">Histórico de Trocas</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix">
        <h2 class="h2-hp">O prazo de validade para utilizar os pontos das notas é de 6 meses.</h2>
        
        <h3 class="h3r">Histórico de trocas dentro do período de 6 meses</h3>
        <div class="tbr-pedido">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data de solicitação</th>
                        <th role="columnheader">Status</th>
                        <th role="columnheader">Produtos</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
                    
                        $total_qtd_geral_periodo = 0;
                        $total_pontos_geral_periodo = 0;
                        foreach ($pedidos_ativos as $pa) {
                            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));
                            
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">'.$pa['PedidoCliente']['cdate'].'</td>';
                                echo '<td role="cell" class="td20">'.$status[$pa['PedidoCliente']['status']].'</td>';
                                echo '<td role="cell">';
                                    echo '<table class="table-pdts">';
                                        $total_qtd_periodo = 0;
                                        $total_pontos_periodo = 0;
                                        foreach ($produtos_pedido as $ps) {
//                                            pre($ps);
                                            echo '<tr>';
                                                echo '<td class="tdf">'.$ps['Produto']['title'].'</td>';
                                                echo '<td>'.$ps['PedidoClienteProduto']['quantidade'].'</td>';
                                                echo '<td>'.number_format($ps['PedidoClienteProduto']['pontos'], 0, ',', '.').'</td>';
                                            echo '</tr>';
                                            
                                            $total_pontos_periodo = $total_pontos_periodo + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                                            $total_qtd_periodo = $total_qtd_periodo + $ps['PedidoClienteProduto']['quantidade'];
                                        }
                                        echo '<tr>';
                                            echo '<td class="tdta"><b>Total:</b></td>';
                                            echo '<td><b>'.$total_qtd_periodo.'</b></td>';
                                            echo '<td><b>'.number_format($total_pontos_periodo, 0, ',', '.').'</b></td>';
                                        echo '</tr>';
                                    echo '</table>';
                                echo '</td>';
                            echo '</tr>';
                            
                            $total_qtd_geral_periodo = $total_qtd_geral_periodo + $total_qtd_periodo;
                            $total_pontos_geral_periodo = $total_pontos_geral_periodo + $total_pontos_periodo;
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="total-low-pedidos">
                <div class="bin b1"><b>Total geral: </b></div>
                <div class="bin b2"><b><?PHP echo $total_qtd_geral_periodo; ?></b></div>
                <div class="bin b3"><b><?PHP echo number_format($total_pontos_geral_periodo, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        <h3 class="h3r">Histórico de trocas anteriores a 6 meses</h3>
        <div class="tbr-pedido">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data de solicitação</th>
                        <th role="columnheader">Status</th>
                        <th role="columnheader">Produtos</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        
                        $total_qtd_geral = 0;
                        $total_pontos_geral = 0;
                        foreach ($pedidos_antes as $pa) {
                            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));
                            
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">'.$pa['PedidoCliente']['cdate'].'</td>';
                                echo '<td role="cell" class="td20">'.$status[$pa['PedidoCliente']['status']].'</td>';
                                echo '<td role="cell">';
                                    echo '<table class="table-pdts">';
//                                        echo '<tr>';
//                                            echo '<td><b>Produto</b></td>';
//                                            echo '<td title="Quantidade"><b>Qtd</b></td>';
//                                            echo '<td><b>Ponto</b></td>';
//                                        echo '</tr>';
                                        
                                        $total_qtd = 0;
                                        $total_pontos = 0;
                                        foreach ($produtos_pedido as $ps) {
//                                            pre($ps);
                                            echo '<tr>';
                                                echo '<td class="tdf">'.$ps['Produto']['title'].'</td>';
                                                echo '<td>'.$ps['PedidoClienteProduto']['quantidade'].'</td>';
                                                echo '<td>'.number_format($ps['PedidoClienteProduto']['pontos'], 0, ',', '.').'</td>';
                                            echo '</tr>';
                                            
                                            $total_pontos = $total_pontos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                                            $total_qtd = $total_qtd + $ps['PedidoClienteProduto']['quantidade'];
                                        }
                                        echo '<tr>';
                                            echo '<td class="tdta"><b>Total:</b></td>';
                                            echo '<td><b>'.$total_qtd.'</b></td>';
                                            echo '<td><b>'.number_format($total_pontos, 0, ',', '.').'</b></td>';
                                        echo '</tr>';
                                    echo '</table>';
                                echo '</td>';
                            echo '</tr>';
                            
                            $total_qtd_geral = $total_qtd_geral + $total_qtd;
                            $total_pontos_geral = $total_pontos_geral + $total_pontos;
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="total-low-pedidos">
                <div class="bin b1"><b>Total geral: </b></div>
                <div class="bin b2"><b><?PHP echo $total_qtd_geral; ?></b></div>
                <div class="bin b3"><b><?PHP echo number_format($total_pontos_geral, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        
        
<!--        <h3 class="h3r">Notas válidas emitidas em até 6 meses</h3>
        <div class="row-table-responsive">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data da emissão</th>
                        <th role="columnheader">Pontos</th>
                        <th role="columnheader">Válido até:</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
//                        $total_pontos = 0;
//                        foreach ($notas_atuais as $nota) {
//                            echo '<tr role="row">';
//                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
//                                echo '<td role="cell">'.number_format($nota['Nota']['valor_nf'], 0, ',', '.').'</td>';
//                                echo '<td role="cell">'.date('d/m/Y', strtotime($nota['Nota']['data_e'].' + 6 months')).'</td>';
//                            echo '</tr>';
//                            
//                            $total_pontos = $total_pontos + $nota['Nota']['valor_nf'];
//                        }
                    ?>
                </tbody>
            </table>
        </div>-->
        
<!--        <div class="bg-low-table">
            <table role="table">
                <tbody role="rowgroup">
                    <tr>
                        <td role="cell"><b>Total:</b></td>
                        <td role="cell"><?PHP echo number_format($total_pontos, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Já utilizado no período:</b></td>
                        <td role="cell">
                            <?PHP 
                                echo '-'.number_format($total_pontos_geral_periodo, 0, ',', '.');
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Disponível para troca:</b></td>
                        <td role="cell">
                            <?PHP 
//                                echo number_format($total_pontos - $cliente['pontos_trocados'], 0, ',', '.'); 
                                echo number_format($total_pontos - $total_pontos_geral_periodo, 0, ',', '.'); 
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>-->
        
    </section>
</div>