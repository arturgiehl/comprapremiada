<div class="bg-gray bgpb">
    <section class="container-fix top-content">
        <?PHP
            $this->Html->addCrumb('Inicial','/'); 
            $this->Html->addCrumb('Pontos',array('plugin'=>'clientes','controller'=>'clientes','action'=>'pontos')); 
            echo $this->element('breadcrumb');
        
            echo '<h1 class="h1p">Pontos</h1>';
        ?>
       
        <div class="row">
                <div class="row box-login-cadastro">
                    <h2>Consultar Pontuação</h2>
                    <?PHP
                        $sexo = array(''=>'', 'Masculino'=>'Masculino', 'Feminino'=>'Feminino');
                    
                        echo $this->Form->create('Cliente',array('class'=>'formc','id'=>'formContult','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'pontos')));
                            echo '<div class="row">';
                                echo '<div class="seven columns">'. $this->Form->input('cnpj',array('label'=>'CNPJ <span>(apenas números)</span>','div'=>false,'type'=>'tel','maxlength'=>14,'onkeypress'=>'return SomenteNumero(event)')) .'</div>';
                                echo '<div class="five columns">'. $this->Form->submit('Consultar',array('div'=>false,'class'=>'button-cc')) .'</div>';
                            echo '</div>';

                            if(isset($pontos) && $pontos){
                                echo '<p class="texto">Você possui <b>'.$pontos.'</b> pontos</p>';
                            }
//                            echo '<div class="row row-buttons">';
//                                echo '<p id="msg-add" class="msg-send-form">enviando...</p>';
//                            echo '</div>';

                        echo $this->Form->end(); 
                    ?>
                </div>
        </div>
        
    </section>
</div>
