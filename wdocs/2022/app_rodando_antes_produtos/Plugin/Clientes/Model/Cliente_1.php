<?PHP
class Cliente extends ClientesAppModel {
    
    var $name = 'Cliente';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Cliente.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Cliente.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $validate = array(
        'cnpj'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'nome_fantasia'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'nome'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'cpf'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'data_nascimento'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'telefone1'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'telefone2'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'telefone1'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'cep'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'cidade'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'estado'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'endereco'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'bairro'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'numero'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'qual_vendedor'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'email'=>array('rule'=>'email','message'=>'Não é um e-mail válido'),
        'senha'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
    );
    
    public $actsAs = array(
        'Painel.Slug'=>array('title'=>'slug'),
    );
   
    
}