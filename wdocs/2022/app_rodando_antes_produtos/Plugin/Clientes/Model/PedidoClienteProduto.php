<?PHP
class PedidoClienteProduto extends ClientesAppModel {
    
    public $useTable  = 'produto_session';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(PedidoClienteProduto.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(PedidoClienteProduto.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $belongsTo = array(
        'Produto' => array(
            'className'  => 'Produtos.Produto',
            'foreignKey' => 'produto_id',
            'fields'     => array('id','title','preco')
        ),
    );
    
}