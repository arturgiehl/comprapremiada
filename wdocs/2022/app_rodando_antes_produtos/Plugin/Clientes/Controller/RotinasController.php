<?PHP
 
class RotinasController extends ClientesAppController {

    public function email_mensal_ponto(){
        set_time_limit(0);
        $this->layout = false;
        $this->autoRender = false;
        
        $clientes = $this->Cliente->query(' 
            SELECT  Cliente.id, Cliente.cnpj, Cliente.nome_fantasia, Cliente.nome, Cliente.email, Cliente.pontos_trocados FROM tb_clientes AS Cliente
                WHERE Cliente.id NOT IN
                    ( SELECT cliente_id FROM email_ponto_mensal AS EPM WHERE DATE_FORMAT(EPM.created, "%Y-%m-%d") = curdate() )
                LIMIT 200
        ');
//        pre(count($clientes));
        
        $count_s = 1;
        foreach ($clientes as $cliente) {
            $pontos_disponiveis = $this->get_ponto_cliente_disponivel($cliente['Cliente']['cnpj']);
            
            try {
                $this->send_mail_mensal($cliente,$pontos_disponiveis);
                
                $save_email['EmailPontoMensal'] = array(
                    'id'         => '',
                    'cliente_id' => $cliente['Cliente']['id'],
                    'pontos'     => '',
                );
                if($this->EmailPontoMensal->save($save_email)){
                    echo $count_s++.'<br>';
//                    pre('enviou');
                    sleep(11);
                }
            } catch (Exception $ex) {
                echo '- Não enviou <br>';
            }
            
//            if($this->send_mail_mensal($cliente,$pontos_disponiveis)){
//                $save_email['EmailPontoMensal'] = array(
//                    'id'         => '',
//                    'cliente_id' => $cliente['Cliente']['id'],
//                    'pontos'     => '',
//                );
//                if($this->EmailPontoMensal->save($save_email)){
//                    echo $count_s++.'<br>';
////                    pre('enviou');
//                    sleep(11);
//                }
//            }else{
//                echo '- Não enviou <br>';
//            }
            
//            $count_s++;
//            if($count_s === 4){
//                pre('teste');
//            }
        }
    }

    private function send_mail_mensal($cliente,$pontos_disponiveis){
        App::uses('CakeEmail','Network/Email');
//        $mail = new CakeEmail('smtp');
        $mail = new CakeEmail('smtpsend');
        if($pontos_disponiveis == 0){
            $mail->template('Clientes.pontuacao_mensal_zerada');
        }else{
            $mail->template('Clientes.pontuacao_mensal');
        }
  
//        $mail->to('artur@amexcom.com.br');
        
        $mail->to($cliente['Cliente']['email']);
//        $mail->bcc('artur@amexcom.com.br');
        
        $mail->emailFormat('html');
        $mail->subject('Pontuação disponível - '.date('d/m/Y - H:i').'');
        $mail->viewVars(array(
            'cliente'            => $cliente['Cliente'],
            'pontos_disponiveis' => $pontos_disponiveis
        ));
        if($mail->send()){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function email_ponto_dez_dias(){
        set_time_limit(0);
        $this->layout = false;
        $this->autoRender = false;
        
        $clientes = $this->Cliente->find('all',array(
            'fields'     => array('id','cnpj','nome_fantasia','nome','email','pontos_trocados'),
//            'conditions' => array('Cliente.cnpj' => '00000000000000')
        ));
//        pre($clientes);
        
        foreach ($clientes as $cliente) {
//            pre($cliente);
            $notas = $this->Nota->find('all',array(
                'fields'     => array('id','cliente_nome','data_emissao','valor_nf','data_e'),
                'conditions' => array('Nota.cancelada = 0', 'Nota.cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
//            pre($notas);
            
            if(count($notas) > 0){
                $nota1 = array_shift($notas);
//                $valor_nota_vencer = $nota1['Nota']['valor_nf'];
               
                $total_notas_outras = 0;
                foreach ($notas as $nota) {
                    $total_notas_outras = $total_notas_outras + $nota['Nota']['valor_nf'];
                }
                
                $total_notas_geral = $total_notas_outras + $nota1['Nota']['valor_nf'];
                $valor_disponivel = $total_notas_geral - $cliente['Cliente']['pontos_trocados'];
                
                $data_vencer = date('Y-m-d', strtotime('+10 day', strtotime(date('Y-m-d'))));
                $data_nota_vencer = date('Y-m-d', strtotime('6 months', strtotime($nota1['Nota']['data_e'])));
                
                if($data_nota_vencer === $data_vencer && $valor_disponivel > $nota1['Nota']['valor_nf']){
//                    echo 'tá vencendo '.$cliente['Cliente']['nome_fantasia'].' <br>';
                    if($this->send_mail_dez_dias($cliente,$nota1)){
                        $save_email['EmailPontoDez'] = array(
                            'id'         => '',
                            'cliente_id' => $cliente['Cliente']['id'],
                            'pontos'     => $nota1['Nota']['valor_nf'],
                        );
                        if($this->EmailPontoDez->save($save_email)){
                            echo 'Enviado <br>';
                            sleep(12);
                        }
                    }
                }
            }
        }
        
    }
    
    private function send_mail_dez_dias($cliente,$nota){
        App::uses('CakeEmail','Network/Email');
//        $mail = new CakeEmail('smtp');
        $mail = new CakeEmail('smtpsend');
        $mail->template('Clientes.pontuacao_dez_dias');
  
//        $mail->to('artur@amexcom.com.br');
        
        $mail->to($cliente['Cliente']['email']);
//        $mail->bcc('artur@amexcom.com.br');
        
        $mail->emailFormat('html');
        $mail->subject('Pontuação a vencer - '.date('d/m/Y - H:i').'');
        $mail->viewVars(array(
            'cliente' => $cliente['Cliente'],
            'nota'   => $nota
        ));
        if($mail->send()){
            return true;
        }else{
            return false;
        }
    }
    
}