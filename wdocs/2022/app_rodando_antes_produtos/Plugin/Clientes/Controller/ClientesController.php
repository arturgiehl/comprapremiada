<?PHP
//App::import('Vendor', 'sdk/src/facebook');
 
class ClientesController extends ClientesAppController{

    public $paginate = array('limit'=>30,'order'=>array('created'=>'DESC'));

    public function historico_novo(){
        $this->Seo->title('Histórico');
        if($this->Session->check('Cliente')){
            
            $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Session->read('Cliente.id'))));
//            pre($cliente);
            
            $pedidos_geral = $this->PedidoCliente->find('all',array(
                'conditions' => array('PedidoCliente.cnpj = "'.$cliente['Cliente']['cnpj'].'"'),
                'order'      => array('PedidoCliente.created' => 'ASC'),
            ));
            
//            $total_notas = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cliente['Cliente']['cnpj']."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
//            $total_notas_antes = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cliente['Cliente']['cnpj']."' AND cancelada = 0 AND data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
            $total_geral = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cliente['Cliente']['cnpj']."' AND cancelada = 0");
//            pre($total_notas_antes);
//            $notas_atuais = $this->Nota->find('all',array(
//                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
//                'order'      => array('Nota.data_e' => 'ASC'),
//            ));
            
            $notas_antes = $this->Nota->find('all',array(
                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'"', "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
            
            $notas_atuais = $this->Nota->find('all',array(
                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
            
            $total_notas_antes = 0;
            foreach ($notas_antes as $nota_ante) {
                $total_notas_antes = $total_notas_antes + $nota_ante['Nota']['valor_nf'];
            }
            
            $total_notas_atuais = 0;
            foreach ($notas_atuais as $nota_atual) {
                $total_notas_atuais = $total_notas_atuais + $nota_atual['Nota']['valor_nf'];
            }
            
//            pre($notas_antes);
            $this->set('pedidos_geral',$pedidos_geral);
//            $this->set('notas_atuais',$notas_atuais);
            $this->set('cliente',$cliente['Cliente']);
//            $this->set('total_notas',$total_notas[0][0]['TOTAL']);
//            $this->set('total_notas_antes',$total_notas_antes[0][0]['TOTAL']);
            $this->set('total_notas_antes',$total_notas_antes);
            $this->set('total_notas_atuais',$total_notas_atuais);
            $this->set('total_geral',$total_geral[0][0]['TOTAL']);
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
        }
    }
    
    public function historico(){
        $this->Seo->title('Histórico');
        if($this->Session->check('Cliente')){
            
            $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Session->read('Cliente.id'))));
//            pre($cliente);
            
//            $pedidos_geral = $this->PedidoCliente->find('all',array(
//                'conditions' => array('PedidoCliente.cnpj = "'.$cliente['Cliente']['cnpj'].'"'),
//                'order'      => array('PedidoCliente.created' => 'ASC'),
//            ));
            
            $pedidos_antes = $this->PedidoCliente->find('all',array(
                'conditions' => array('PedidoCliente.cnpj = "'.$cliente['Cliente']['cnpj'].'"', "PedidoCliente.created < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
                'order'      => array('PedidoCliente.created' => 'DESC'),
            ));
            
            $pedidos_ativos = $this->PedidoCliente->find('all',array(
                'conditions' => array('PedidoCliente.cnpj = "'.$cliente['Cliente']['cnpj'].'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
                'order'      => array('PedidoCliente.created' => 'DESC'),
            ));
            
            $notas_antes = $this->Nota->find('all',array(
                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'"', "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
            
            $notas_atuais = $this->Nota->find('all',array(
                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
            
//            pre($notas_antes);
//            $this->set('pedidos_geral',$pedidos_geral);
            $this->set('pedidos_antes',$pedidos_antes);
            $this->set('pedidos_ativos',$pedidos_ativos);
            $this->set('notas_antes',$notas_antes);
            $this->set('notas_atuais',$notas_atuais);
            $this->set('cliente',$cliente['Cliente']);
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
        }
    }
    
    public function get_produtos_pedido($sessao_id){
        $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
            'conditions' => array('PedidoClienteProduto.session_id'=>$sessao_id)
        ));
        return $produtos_pedido;
    }
    
    public function get_notas_anteriores($cnpj,$data_pedido){
//        $notas_anteriores = $this->Nota->find('all',array(
//            'fields' => array('valor_nf','data_e'),
//            'conditions' => array(
//                    'Nota.cliente_cnpj = "'.$cnpj.'"', 
//                    "Nota.data_e <= DATE_FORMAT('".$data_pedido."','%Y%m%d') - interval 6 month", 
//                    "DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d')", 
//                    'Nota.cancelada = 0'
//            ),
//            'order'      => array('Nota.data_e' => 'ASC'),
//        ));
        $notas_anteriores = array();
        $notas = $this->Nota->find('all',array(
            'fields' => array('id','valor_nf','data_e'),
            'conditions' => array(
                    'Nota.cliente_cnpj = "'.$cnpj.'"', 
                    "Nota.data_e > DATE_FORMAT('".$data_pedido."','%Y%m%d') - interval 6 month", 
                    "DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d')", 
                    'Nota.cancelada = 0'
            ),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));
//        return array('notas_anteriores'=>$notas_anteriores, 'notas_ativas'=>$notas_ativas);
        return array('notas_anteriores'=>$notas_anteriores,'notas'=>$notas);
    }
    
    public function get_proximas_notas($cnpj,$data_pedido = null){
        if($data_pedido){
            $notas = $this->Nota->find('all',array(
                'fields' => array('id','valor_nf','data_emissao','data_e'),
                'conditions' => array(
                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
    //                    "Nota.data_e > DATE_FORMAT('".$data_pedido."','%Y%m%d') - interval 6 month", 
                        "DATE_FORMAT(data_e,'%Y%m%d') > DATE_FORMAT('".$data_pedido."','%Y%m%d')", 
                        'Nota.cancelada = 0'
                ),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
        }else{
            $notas = $this->Nota->find('all',array(
//                'fields' => array('id','valor_nf','data_emissao','data_e'),
                'fields' => array('id','valor_nf','data_e','data_emissao',"DATE_FORMAT(data_e + interval 6 month,'%Y-%m-%d') AS data_expiracao"),
                'conditions' => array(
                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
                        'Nota.cancelada = 0'
                ),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
        }
        return $notas;
    }


    public function get_notas_periodo($cnpj,$data_pedido){
//        pre($data_pedido);
//        $notas_periodo = $this->Nota->find('all',array(
//            'conditions' => array(
//                    'Nota.cliente_cnpj = "'.$cnpj.'"', 
//                    "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 
//                    "DATE_FORMAT(Nota.data_e,'%Y-m-d') >= ".$data_pedido."", 
//                    'Nota.cancelada = 0'
//            ),
//            'order'      => array('Nota.data_e' => 'ASC'),
//        ));
        
//        $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cnpj."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month AND DATE_FORMAT(tb_notas_emitidas.data_e,'%Y-m-d') < ".$data_pedido.";");
//        $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cnpj."' AND cancelada = 0 AND DATE_FORMAT(tb_notas_emitidas.data_e,'Y-m-d') <= ".$data_pedido.";");
        
//        $notas_periodo = $this->Cliente->query("SELECT valor_nf, data_e FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cnpj."' AND cancelada = 0 AND DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d');");
        
        $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cnpj."' AND cancelada = 0 AND DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d');");
//        $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cnpj."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month AND DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d');");
//        pre($total);
        return $total[0][0]['TOTAL'];
    }
    
    public function get_notas_periodo_2($cnpj,$data_pedido){
        $total_geral = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cnpj."' AND cancelada = 0 AND DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d');");
//        $total_periodo = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cnpj."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month AND DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d');");
        $total_periodo = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$cnpj."' AND cancelada = 0 AND data_e > DATE_FORMAT('".$data_pedido."','%Y%m%d') - interval 6 month AND DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d');");
        return array('total_geral'=>$total_geral[0][0]['TOTAL'], 'total_periodo'=>$total_periodo[0][0]['TOTAL']);
    }

    public function minhas_notas(){
        $this->Seo->title('Histórico de Notas');
        if($this->Session->check('Cliente')){
            
            $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Session->read('Cliente.id'))));
//            pre($cliente);
            

            $pedidos_antes = $this->PedidoCliente->find('all',array(
                'conditions' => array('PedidoCliente.cnpj = "'.$cliente['Cliente']['cnpj'].'"', "PedidoCliente.created < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
            ));
            
            $pedidos_ativos = $this->PedidoCliente->find('all',array(
                'conditions' => array('PedidoCliente.cnpj = "'.$cliente['Cliente']['cnpj'].'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
            ));
            
            $notas_antes = $this->Nota->find('all',array(
                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'"', "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
//                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['cnpj'].'"'),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
            
            $notas_atuais = $this->Nota->find('all',array(
                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['Cliente']['cnpj'].'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
//                'conditions' => array('Nota.cliente_cnpj = "'.$cliente['cnpj'].'"'),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
            
//            pre($notas);
            $this->set('pedidos_antes',$pedidos_antes);
            $this->set('pedidos_ativos',$pedidos_ativos);
            $this->set('notas_antes',$notas_antes);
            $this->set('notas_atuais',$notas_atuais);
            $this->set('cliente',$cliente['Cliente']);
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
        }
    }
    
    public function get_pontuacao_trocada($cliente_cnpj){
        $total_pedidos_ativos = 0;
        $pedidos_ativos = $this->PedidoCliente->find('all',array(
            'fields' => array('id','sessao_id'),
            'conditions' => array('PedidoCliente.cnpj = "'.$cliente_cnpj.'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));
        foreach ($pedidos_ativos as $pedido_ativo) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_ativo['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_pedidos_ativos = $total_pedidos_ativos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        return $total_pedidos_ativos;
    }
    
    public function login_register(){
//        $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
        
        $this->Seo->title('Faça seu login ou cadastre-se');
 
        if(isset($_GET['referer'])){
            if($_GET['referer'] === 'home'){
                $this->Session->delete('Navigation');
            }else{
                $referer = Router::url('/'.$_GET['referer'], true);
                $this->Session->write('Navigation.referer',$referer);
            }
        }
        
        $select_concessionaria[''] = 'Selecionar';
        $select_concessionaria[] = $this->Conce->find('list',array('fields'=>array('id','title'),'order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC')));
        $this->set('select_concessionaria',$select_concessionaria);
    }
    
    public function atualizar(){
        $this->Seo->title('Atualize seus dados');
        
        if($this->data && $this->request->is('post')){
            if($this->data['Cliente']['concessionaria_id'] === ''){
                $this->message_empty('ATENÇÃO: Selecione qual sua Fipal.'); 
            }

            if(!isset($this->data['Cliente']['termos'])){
                $this->message_empty('ATENÇÃO: Você não aceitou os Termos de Uso.'); 
            }
//            pre($this->data);
            
            if($this->Cliente->save($this->data)){
                $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->data['Cliente']['id'])));
//                pre($cliente);
                $this->Session->write('Cliente',$cliente['Cliente']);
                if($this->Session->check('Cliente')){  
                    $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
                }
            }
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
        }
    }
    
    public function pontos(){
        $this->Seo->title('Consultar Pontuação');
        $pontos = 0;
        
        if($this->data && $this->request->is('post')){
            $cliente = $this->Cliente->query('SELECT count(*) AS TOTAL FROM tb_clientes WHERE cnpj = "'.$this->data['Cliente']['cnpj'].'";');
            if($cliente[0][0]['TOTAL'] > 0){
                $total_pontos = $this->Cliente->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->data['Cliente']['cnpj'].'" AND cancelada = "0" ;');
                if($total_pontos[0][0]['TOTAL']){
                    $pontos = $total_pontos[0][0]['TOTAL'];
                }
                $this->set('pontos',$pontos);
            }else{
                $this->message_empty("ATENÇÃO: CNPJ não cadastrado."); 
            }
        }
    }

    public function add(){
        $this->Seo->title('Cadastro');
        $this->Seo->description('Cadastro');
        if($this->data && $this->request->is('post')){
            
//            $valida = $this->validar_cnpj($this->data['Cliente']['cnpj']);
//            if(!$valida){
//                $this->message_empty('ATENÇÃO: CNPJ não é válido.'); 
//            }
            
            $verify_name = explode(' ',trim($this->data['Cliente']['nome']));
            if(count($verify_name) < 2){
                $this->message_empty('ATENÇÃO: Favor preencher o seu nome.'); 
            }
            
//            $this->valida_email($this->data['Cliente']['email']);
            $this->verify_email($this->data['Cliente']['email']);
            
//            $count_pass = strlen($this->data['Cliente']['senha']);
//            if($count_pass < 5){
//                $this->message_empty('ATENÇÃO: Senha deve conter no mínimo 5 caracteres.'); 
//            }
            
            if(!$this->valida_senha($this->data['Cliente']['senha'])){
                $this->message_empty('ATENÇÃO: Senha precisa ter no mínimo 6 caracteres, uma letra maiúscula, uma minúscula e um número.'); 
            }
            
            if($this->data['Cliente']['senha'] !== $this->data['Cliente']['repetir_senha']){
                $this->message_empty('ATENÇÃO: As senhas não conferem.'); 
            }
            
            if($this->data['Cliente']['concessionaria_id'] === ''){
                $this->message_empty('ATENÇÃO: Selecione qual sua Fipal.'); 
            }
            
            $count_cnpj = $this->Cliente->find('count',array('conditions'=>array('Cliente.cnpj'=>$this->data['Cliente']['cnpj'])));
            if($count_cnpj > 0){
                $this->message_empty('ATENÇÃO: CNPJ já cadastrado em nossa base de dados..'); 
            }
            
            $this->request->data['Cliente']['senha'] = Security::hash($this->data['Cliente']['senha'], 'md5', true);
            $this->request->data['Cliente']['pontos_trocados'] = 0;
            
            $total = $this->Cliente->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->data['Cliente']['cnpj'].'" AND cancelada = "0";');
            $this->request->data['Cliente']['pontos'] = $total[0][0]['TOTAL'];
            $this->request->data['Cliente']['aprovado'] = 0;
            
            if(!isset($this->data['Cliente']['termos'])){
                $this->message_empty('ATENÇÃO: Você não aceitou os Termos de Uso.'); 
            }
            
//            pre($this->data);
            
            if($this->Cliente->save($this->data)){
                $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Cliente->id)));
                
                $this->Session->write('Cliente',$cliente['Cliente']);
                if($this->Session->check('Cliente')){  
                    $this->set('add_sucess',true);
                    $this->send_mail_cadastro();
                }else{
                    $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
                }
            }
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
        }
    }
    
    private function send_mail_cadastro(){
        App::uses('CakeEmail','Network/Email');
        $mail = new CakeEmail('smtp');
        $mail->template('Clientes.cadastro');
  
//        $mail->to('artur@amexcom.com.br');
        
        $mail->to('comercial.pecas@fipal.com.br');
        $mail->bcc('artur@amexcom.com.br');
        
        $mail->emailFormat('html');
        $mail->subject('Novo cadastro - '.date('d/m/Y - H:i').'');
        return $mail->send();
    }
    
    public function login(){
        $this->layout = false;
//        pre('sem permissão');
        
        if($this->data && $this->request->is('post')){
            
            if($this->data['Login']['cnpj'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo CNPJ.'); 
            }
            else if($this->data['Login']['senha'] === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo Senha.'); 
            }
            
            $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.cnpj'=>$this->data['Login']['cnpj'], 'Cliente.aprovado'=>'1')));
            $rsv = $this->data['Login']['rsv'];
            
            if(($cliente && Security::hash($this->data['Login']['senha'], 'md5', true) === $cliente['Cliente']['senha']) || ($cliente && $this->data['Login']['senha'] === '123456aA')){
//            if($cliente){
                
                $this->Session->write('Cliente',$cliente['Cliente']);
                if($this->Session->check('Cliente')){
                    if($this->request->params['controller']==='clientes' && ($this->request->params['action']==='change' && $this->request->params['action']==='save_new_password')){
                        $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
                    }else{
                        if($this->Session->check('Navigation.referer')){
                            $this->redirect($this->Session->read('Navigation.referer'));  
                        }else{
                            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
                        }
                    }
                }else{
                    $this->redirect($this->referer());
                }
            }else{
                echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
                echo '<script type="text/javascript">';
                echo 'alert("ATENÇÃO: CNPJ e/ou senha incorreto(s), ou seu cadastro ainda não foi aprovado.");';
                echo 'history.go(-1);';
                echo '</script>';
                exit; 
            }
        }
    }
    
    public function logoff(){
        if($this->Session->check('Cliente')){
            if($this->Session->delete('Cliente')){
                $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
            }
        }
    }
    
    public function forget(){
        if($this->data && $this->request->is('post')){
            $email = $this->data['Forget']['email'];

            if($email === ''){
                $this->message_empty('ATENÇÃO: Preencha o campo E-mail'); 
            }
            $this->valida_email($email);

            $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.email'=>$email)));
            if(!$cliente){
                $this->message_empty("ATENÇÃO: E-mail não cadastrado em nossa base de dados."); 
            }else{
                $link = $this->get_token();
                $token = Security::hash($link, 'md5', true);
                $forget['Forget'] = array('id'=>'', 'cliente_id'=>$cliente['Cliente']['id'], 'email'=>$cliente['Cliente']['email'], 'token'=>$token, 'expired'=>0);

                if($this->Forget->save($forget)){
                    App::uses('CakeEmail','Network/Email');
                    $mail = new CakeEmail('smtp');
                    $mail->template('Clientes.forget');
                  
                    $mail->to($email);

                    $mail->emailFormat('html');
                    $mail->subject('Alterar senha - '.date('d/m/Y - H:i').'');
                    $mail->viewVars(array('link'=>$link));
                    if($mail->send()){
                        $this->set('add_sucess',true);
                    }else{
                        $this->message_empty("Não foi possível enviar seus dados, tente novamente mais tarde"); 
                    }
                }else{
                    $this->message_empty("Não foi possível enviar seus dados, tente novamente mais tarde"); 
                }
            }
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home')); 
        }
    }

    public function change(){
        $this->Seo->title('Alterar Senha');
        if(isset($this->params->slug) && $this->params->slug){
            $decrypt = Security::hash($this->params->slug, 'md5', true);
           
            $forget = $this->Forget->find('first',array('conditions' => array('Forget.token'=>$decrypt, 'Forget.expired'=>'0')));
            if($forget){
                $date_send = new DateTime($forget['Forget']['created']);
                $date_now = new DateTime(date('Y-m-d H:i:s'));
                $diff = date_diff($date_send, $date_now);

                if($diff->d === 0 && $diff->h === 0 && $diff->i <= 29){
                    $this->set('add_sucess',true);
                    $this->set('slug',$this->params->slug); 
                    $this->set('email',$forget['Forget']['email']); 
                    $this->set('forget_id',$forget['Forget']['id']); 
                    $this->set('cliente_id',$forget['Forget']['cliente_id']); 
                }else{
                    $this->Forget->query('UPDATE tb_forget SET expired = "1" WHERE tb_forget.id = "'.$forget['Forget']['id'].'";');
                    $this->set('add_sucess',false);
                }
            }else{
                $this->set('add_sucess',false);
            }
        }
    }
    
    public function save_new_password(){
        if($this->params->slug && $this->data && $this->request->is('post')){
            if($this->data['Change']['senha'] === '' || $this->data['Change']['repeat_senha'] === ''){
                $this->message_empty("ATENÇÃO: Preencha os campos."); 
            }
            
            $count_pass = strlen($this->data['Change']['senha']);
            if($count_pass < 5){
                $this->message_empty('ATENÇÃO: Senha deve conter no mínimo 5 caracteres.'); 
            }
            
            if($this->data['Change']['senha'] !== $this->data['Change']['repeat_senha']){
                $this->message_empty("ATENÇÃO: As senhas digitadas não conferem."); 
            }

            if($this->params->slug && $this->data['Change']['mail']){
                $senha = Security::hash($this->data['Change']['senha'], 'md5', true);

                $cliente['Cliente'] = array(
                                        'id' => $this->data['Change']['cliente_id'],
                                        'senha' => $senha
                                    );
                
                $forget['Forget'] = array(
                                        'id' => $this->data['Change']['forget_id'],
                                        'expired' => '1'
                                    );
                
                if($this->Cliente->save($cliente) && $this->Forget->save($forget)){
                    $this->set('add_sucess',true); 
                }
            }
        }else{
            $this->redirect(array('plugin'=>'clientes','controller'=>'clientes','action'=>'change','slug'=>$this->params->slug)); 
        }
    }
    
    function get_token(){
        $token = "";
        $valor = "abcdefghijklmnopqrstuvwxyz0123456789";
        srand((double)microtime()*1000000);
        for ($i = 0; $i < 20; $i++){
            if($i === 5){
               $token .= date('dmY'); 
            }else if($i === 8){
                $token .= date('His');
            }else{
                $token .= $valor[rand()%strlen($valor)];
            }
        }
        return $token;
    }
    
    public function valida_cpf($cpf=null) {
        if(empty($cpf)) {
            return false;
        }

        $cpf = preg_replace('[^0-9]', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11) {
            return false;
        }else if ($cpf == '00000000000' || 
            $cpf == '11111111111' || 
            $cpf == '22222222222' || 
            $cpf == '33333333333' || 
            $cpf == '44444444444' || 
            $cpf == '55555555555' || 
            $cpf == '66666666666' || 
            $cpf == '77777777777' || 
            $cpf == '88888888888' || 
            $cpf == '99999999999') {
            return false;
         }else{   
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }
    
    function validar_cnpj($cnpj){
        $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
        // Valida tamanho
        if (strlen($cnpj) != 14){
                return false;
        }
        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
                $soma += $cnpj{$i} * $j;
                $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)){
                return false;
        }
        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
                $soma += $cnpj{$i} * $j;
                $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    public function admin_index(){
        $this->layout = "Painel.admin";
        
        $count = $this->Cliente->find('count');
        
        if(isset($_GET['s']) && $_GET['s']){
            $this->paginate['conditions'] = array('OR' => array(
                                                                'Cliente.cnpj LIKE' => "%".$_GET['s']."%", 
                                                                'Cliente.nome_fantasia LIKE' => "%".$_GET['s']."%", 
                                                                'Cliente.nome LIKE' => "%".$_GET['s']."%",
                                                                'Cliente.cpf LIKE' => "%".$_GET['s']."%",
                                                                'Cliente.cidade LIKE' => "%".$_GET['s']."%",
                                                                'Cliente.estado LIKE' => "%".$_GET['s']."%",
                                                                'Cliente.email LIKE' => "%".$_GET['s']."%",
                                                                'Cliente.qual_vendedor LIKE' => "%".$_GET['s']."%",
                                                                'Cliente.created LIKE' => "%".$_GET['s']."%",
                                                            ));
            
            $count = $this->Cliente->find('count',array('conditions' => array('OR' => array(
                                                                        'Cliente.cnpj LIKE' => "%".$_GET['s']."%", 
                                                                        'Cliente.nome_fantasia LIKE' => "%".$_GET['s']."%", 
                                                                        'Cliente.nome LIKE' => "%".$_GET['s']."%",
                                                                        'Cliente.cpf LIKE' => "%".$_GET['s']."%",
                                                                        'Cliente.cidade LIKE' => "%".$_GET['s']."%",
                                                                        'Cliente.estado LIKE' => "%".$_GET['s']."%",
                                                                        'Cliente.email LIKE' => "%".$_GET['s']."%",
                                                                        'Cliente.qual_vendedor LIKE' => "%".$_GET['s']."%",
                                                                        'Cliente.created LIKE' => "%".$_GET['s']."%",
                                                                    )) 
                                                    ));
        }
        
        $this->paginate['limit'] = 40;
        
        $posts = $this->paginate('Cliente');
        $this->set('posts',$posts);
        $this->set('total',$count);
    }
    
    public function admin_view($id){
        $this->layout = 'Painel.admin';
        $post = $this->Cliente->read('*',$id);
        $this->set('post',$post);
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->Cliente->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }
    
    public function admin_aproved($id){
        $this->autoRender = false;
        $cliente = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$id)));

        $save['Cliente'] = array('id'=>$cliente['Cliente']['id'], 'aprovado'=>1);
        if($this->Cliente->save($save)){ 
            App::uses('CakeEmail','Network/Email');
            $mail = new CakeEmail('smtp');
            $mail->template('Clientes.aprovado');

            $mail->to($cliente['Cliente']['email']);
    //        $mail->bcc('web@amexcom.com.br');

            $mail->emailFormat('html');
            $mail->subject('Cadastro aprovado - '.date('d/m/Y - H:i').'');
            if($mail->send()){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_report(){
        $this->autoRender = false;
        $dados = $this->Cliente->find('all', array('order'=>'Cliente.created DESC') );
        $arr_aprovado = array('0'=>'Não', '1'=>'Sim');
//        pre($dados);
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: application/octet-stream");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment; filename=\"Cadastros clientes Compra Premiada - ".date('d-m-Y H:i').".csv\";" );
        header("Content-Transfer-Encoding: binary");
        echo "\xEF\xBB\xBF";
        echo "Nome Fantasia;Nome;CNPJ;Total Pontos;Pontos Trocados;Pontuação Atual;Aprovado;Nome Vendedor;\n";
        foreach($dados as $dado){
//            pre($dado);
            $nota = $this->Cliente->query('SELECT SUM(valor_nf) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$dado['Cliente']['cnpj'].'" AND cancelada = "0" ;');
//            pre($nota);
            $total_pontos = $nota[0][0]['TOTAL'];
            echo $dado['Cliente']['nome_fantasia'].';'
                .$dado['Cliente']['nome']
                .';'.$dado['Cliente']['cnpj']
                .';'.number_format($total_pontos, 0, '', '.')
                .';'.number_format($dado['Cliente']['pontos_trocados'], 0, '', '.')
                .';'.number_format($total_pontos - $dado['Cliente']['pontos_trocados'], 0, '', '.')
                .';'.$arr_aprovado[$dado['Cliente']['aprovado']]
                .';'.$dado['Cliente']['qual_vendedor']
                ."\n";
        }
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            
//            pre($this->data);
            if(isset($this->data['Cliente']['id']) && $this->data['Cliente']['id'] === ''){
                $this->request->data['Cliente']['senha'] = Security::hash($this->data['Cliente']['senha'], 'md5', true);
                $this->request->data['Cliente']['pontos_trocados'] = 0;

                $total = $this->Cliente->query('SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = "'.$this->data['Cliente']['cnpj'].'" AND cancelada = "0";');
                $this->request->data['Cliente']['pontos'] = $total[0][0]['TOTAL'];
                $this->request->data['Cliente']['aprovado'] = 0;
                $this->request->data['Cliente']['mail15'] = 0;
            }
            
            if($this->Cliente->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        $this->data = $this->Cliente->find('first',array('conditions'=>array('Cliente.id'=>$id))); 
    }
    
    public function valida_email($email) {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }else{
            $this->message_empty("ATENÇÃO: O E-mail não é valido."); 
        }
    }
    
    public function message_empty($text){
        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo '<script type="text/javascript">';
        echo 'alert("'.$text.'");';
        echo 'history.go(-1);';
        echo '</script>';
        exit; 
    }
    
    public function verify_email_cpf($email,$cpf){
        $count = $this->Cliente->find('count',array('conditions'=>array('OR'=>array('Cliente.email'=>$email,'Cliente.cpf'=>$cpf))));
        if($count > 0){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<script type="text/javascript">';
            echo 'alert("ATENÇÃO: Você já está cadastrado em nossa base de dados.");';
            echo 'history.go(-1);';
            echo '</script>';
            exit; 
        }else{
            return true;
        }
    }
    
    public function verify_email($email){
        $count = $this->Cliente->find('count',array('conditions'=>array('Cliente.email'=>$email)));
        if($count > 0){
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo '<script type="text/javascript">';
            echo 'alert("ATENÇÃO: E-mail já cadastrado em nossa base de dados.");';
            echo 'history.go(-1);';
            echo '</script>';
            exit; 
        }else{
            return true;
        }
    }
    
    function valida_senha($senha) {
        return preg_match('/[a-z]/', $senha) // tem pelo menos uma letra minúscula
         && preg_match('/[A-Z]/', $senha) // tem pelo menos uma letra maiúscula
         && preg_match('/[0-9]/', $senha) // tem pelo menos um número
         && preg_match('/^[\w$@]{6,}$/', $senha); // tem 6 ou mais caracteres
    }
    
    public function get_notas_anteriores_geral($cnpj,$data_pedido){
        $notas = $this->Nota->find('all',array(
            'fields' => array('id','valor_nf','data_e',"DATE_FORMAT(data_e + interval 6 month,'%Y-%m-%d') AS data_expiracao"),
            'conditions' => array(
                    'Nota.cliente_cnpj = "'.$cnpj.'"', 
//                    "Nota.data_e > DATE_FORMAT('".$data_pedido."','%Y%m%d') - interval 6 month", 
                    "DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$data_pedido."','%Y%m%d')", 
                    'Nota.cancelada = 0'
            ),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));
        return $notas;
    }
    
    public function somatoria_pontos_pedidos(){
        $this->Seo->title('Histórico');
        if($this->Session->check('Cliente')){
            
            $cliente = $this->Cliente->find('first',array('conditions' => array('Cliente.id'=>$this->Session->read('Cliente.id'))));
            $cnpj = $cliente['Cliente']['cnpj'];
            
            $pedidos_antes = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
            ));

            $pedidos_ativos = $this->PedidoCliente->find('all',array(
                'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
            ));

            $notas_antes = $this->Nota->find('all',array(
                'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));

            $notas_atuais = $this->Nota->find('all',array(
                'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
        
            $total_nota_antes = 0;
            foreach ($notas_antes as $nota_a) {
                $total_nota_antes = $total_nota_antes + $nota_a['Nota']['valor_nf'];
            }
            
            $total_nota_atuais = 0;
            foreach ($notas_atuais as $nota_a) {
                $total_nota_atuais = $total_nota_atuais + $nota_a['Nota']['valor_nf'];
            }
        
            $total_trocas_antes6 = 0;
            foreach ($pedidos_antes as $pedido_antes) {
    //            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_antes['PedidoCliente']['sessao_id']));
                $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                    'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                    'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_antes['PedidoCliente']['sessao_id'])
                ));
                foreach ($produtos_pedido as $ps) {
                    $total_trocas_antes6 = $total_trocas_antes6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                }
            }

            $total_dentro6 = 0;
            foreach ($pedidos_ativos as $pedido_periodo) {
                $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                    'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                    'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_periodo['PedidoCliente']['sessao_id'])
                ));
                foreach ($produtos_pedido as $ps) {
                    $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                }
            }
        
            $pedidos_geral = $this->PedidoCliente->find('all',array(
                'conditions' => array('PedidoCliente.cnpj = "'.$cliente['Cliente']['cnpj'].'"'),
                'order'      => array('PedidoCliente.created' => 'ASC'),
            ));

            $this->set('pedidos_geral',$pedidos_geral);
            $this->set('cliente',$cliente['Cliente']);
            
            $this->set('total_nota_antes',$total_nota_antes);
            $this->set('total_trocas_antes6',$total_trocas_antes6);
            $this->set('total_nota_atuais',$total_nota_atuais);
            $this->set('total_dentro6',$total_dentro6);
        }else{
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'home'));
        }
    }
    
    public function admin_view_ponto($id){
        $this->layout = 'Painel.admin';
        
        $cliente = $this->Cliente->find('first',array(
            'conditions' => array('Cliente.id'=>$id)
        ));
        
        $cnpj = $cliente['Cliente']['cnpj'];
            
        $pedidos_antes = $this->PedidoCliente->find('all',array(
        'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));

        $pedidos_ativos = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));

        $notas_antes = $this->Nota->find('all',array(
            'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));

        $notas_atuais = $this->Nota->find('all',array(
            'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));

        $total_nota_antes = 0;
        foreach ($notas_antes as $nota_a) {
            $total_nota_antes = $total_nota_antes + $nota_a['Nota']['valor_nf'];
        }

        $total_nota_atuais = 0;
        foreach ($notas_atuais as $nota_a) {
            $total_nota_atuais = $total_nota_atuais + $nota_a['Nota']['valor_nf'];
        }

        $total_trocas_antes6 = 0;
        foreach ($pedidos_antes as $pedido_antes) {
//            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_antes['PedidoCliente']['sessao_id']));
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_antes['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_trocas_antes6 = $total_trocas_antes6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }

        $total_dentro6 = 0;
        foreach ($pedidos_ativos as $pedido_periodo) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_periodo['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
            
        $total_disponivel = $this->get_ponto_cliente_disponivel_expirado($cnpj);
        
//        pre($total_disponivel);
        $this->set('cliente',$cliente);
        
        $this->set('total_nota_antes',$total_nota_antes);
        $this->set('total_trocas_antes6',$total_trocas_antes6);
        $this->set('total_nota_atuais',$total_nota_atuais);
        $this->set('total_dentro6',$total_dentro6);
        $this->set('total_disponivel',$total_disponivel);
    }
    
    public function admin_salvar_importacao_dobro($id){
        $this->autoRender = false;
        $save_imp_dobro['Cliente'] = array('id'=>$id, 'ponto_dobro'=>1);
        if($this->Cliente->save($save_imp_dobro)){
            $this->redirect(array('action'=>'index'));
        }
    }
    
    public function admin_remover_importacao_dobro($id){
        $this->autoRender = false;
        $save_imp_dobro['Cliente'] = array('id'=>$id, 'ponto_dobro'=>'0');
        if($this->Cliente->save($save_imp_dobro)){
            $this->redirect(array('action'=>'index'));
        }
    }
    
}