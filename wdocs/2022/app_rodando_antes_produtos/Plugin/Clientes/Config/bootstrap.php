<?PHP
Configure::write('Painel.menu.Clientes / Pedidos',array(
    'Cadastros Clientes'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'index','admin'=>true),
    'Pedidos'=>array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index','admin'=>true),
    
    'separator',
    'Pedidos 2020 | 2'=>array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index20202','admin'=>true),
    'separator',
    'Pedidos 2020'=>array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index2020','admin'=>true),
    'separator',
    'Pedidos 2019'=>array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index2019','admin'=>true),
    'separator',
    'Pedidos 2018'=>array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index2018','admin'=>true),
));