<?PHP
echo $this->Form->create('Regulamento',array('url'=>array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'add')));
?>

<fieldset class="box">
    <legend>Regulamento e Termos de Uso</legend>
    <?PHP 
        echo $this->Form->input('texto',array('label'=>'Texto','class'=>'ckeditor')); 
        echo $this->Form->input('termos_de_uso',array('label'=>'Termos de Uso','class'=>'ckeditor')); 
    ?>
</fieldset>

<?PHP
//echo $this->element('Painel.image',array('label'=>'Imagem Destaque','name'=>'imagem'));
//echo $this->element('Painel.videos',array('label'=>'Vídeo'));
//echo $this->element('Painel.gallery',array('label'=>'Imagens'));
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>