<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Regulamento',array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'index')); 
                echo $this->element('breadcrumb');

                echo '<h1 class="h1p">Regulamento</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix top-content">
        <div class="row">
            <div class="row texto">
                <?PHP echo $post['Regulamento']['texto']; ?>
            </div>
        </div>
    </section>
</div>
