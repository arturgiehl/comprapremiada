<?PHP if(isset($add_sucess) && $add_sucess){ ?>
    <div class="container-fix"> 
        <div class="msg-add-sucess"> 
            Dados enviados com sucesso! <br>
            Alguém de nossa equipe logo entrará em contato com você!
        </div>
    </div>
<?PHP }else{ ?>
<script src='https://www.google.com/recaptcha/api.js'></script>
    
<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Contato',array('plugin'=>'contatos','controller'=>'contatos','action'=>'index')); 
                echo $this->element('breadcrumb');

                echo '<h1 class="h1p">Regulamento</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix top-content">
        <div class="row row-contact">
            <div class="six columns">
                <div class="row texto">
                    <p>Dúvidas ou alguma sugestão? Entre em contato conosco.</p>
                </div>
                <?PHP
                    echo $this->Form->create('Contato',array('class'=>'formc','url'=>array('plugin'=>'contatos','controller'=>'contatos','action'=>'index')));
                        echo $this->Form->input('nome',array('label'=>false,'placeholder'=>'Nome','div'=>false,'data-amex-input'=>'name'));
                        echo $this->Form->input('email',array('label'=>false,'placeholder'=>'E-mail','type'=>'email','div'=>false,'data-amex-input'=>'email'));
                        echo $this->Form->input('telefone',array('label'=>false,'placeholder'=>'Telefone','id'=>'telefone','div'=>false,'maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);','data-amex-input'=>'phone'));
                        echo $this->Form->input('cidade',array('label'=>false,'placeholder'=>'Cidade','div'=>false,'data-amex-input'=>'city'));
                        echo $this->Form->input('mensagem',array('label'=>false,'placeholder'=>'Mensagem','div'=>false,'data-amex-input'=>'message'));
                        
                        echo '<div class="g-recaptcha" data-sitekey="6Lek1NEUAAAAANcBrSQpz7HR0191KXLk7hq2sBP0" style="margin-bottom: 10px;"></div>';
                        
                        echo $this->Form->submit('ENVIAR',array('div'=>false,'class'=>'bs-contact'));
                        echo '<div class="row msg-send-form"> Enviando dados...</div>';
                    echo $this->Form->end(); 
                ?>
            </div>
            <div class="six columns right-contact">
                <div class="row texto">
                    <p>Telefones para contato:</p>
                </div>
                <?PHP
                    echo $this->Html->link('<i class="fas fa-phone"></i> <span>(45)</span> 3218-1033','tel:4532181033',array('escape'=>false,'target'=>'_blank'));
                    echo $this->Html->link('<i class="fas fa-phone"></i> <span>(45)</span> 99963-9372','tel:45999639372',array('escape'=>false,'target'=>'_blank'));
                ?>
            </div>
        </div>
    </section>
</div>

<?PHP } ?>