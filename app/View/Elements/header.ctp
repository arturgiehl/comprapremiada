<?PHP
    if($this->Session->check('Cliente')){
//        echo '<div class="bg-top-car">';
//            echo '<div class="container-fix">';
//                echo '<div class="row">';
//                    echo $this->Html->link('<i class="fas fa-shopping-cart"></i> Meu carrinho',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index'),array('escape'=>false,'class'=>'mycar'));
//                    echo $this->Html->link('<i class="fas fa-power-off"></i> Sair','/cadastro/logoff?'.date('sdmYHis'),array('escape'=>false,'class'=>'out'));
//                echo '</div>';
//            echo '</div>';
//        echo '</div>';
        
        echo '<div class="bg-menu-options">';
            echo '<div class="container-fix">';
                echo '<ul>';
//                    echo '<li>'.$this->Html->link('<i class="fas fa-ellipsis-v"></i> Histórico teste',array('plugin'=>'clientes','controller'=>'clientes','action'=>'somatoria_pontos_pedidos'),array('escape'=>false,'class'=>'mycar')).'</li>';
                    
                    echo '<li>'.$this->Html->link('<i class="fas fa-ellipsis-v"></i> Minhas Trocas',array('plugin'=>'clientes','controller'=>'clientes','action'=>'historico'),array('escape'=>false,'class'=>'mycar')).'</li>';
                    echo '<li>'.$this->Html->link('<i class="fas fa-ellipsis-v"></i> Minhas Notas',array('plugin'=>'clientes','controller'=>'clientes','action'=>'minhas_notas'),array('escape'=>false,'class'=>'mycar')).'</li>';
                    echo '<li>'.$this->Html->link('<i class="fas fa-shopping-cart"></i> Meu carrinho',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'index'),array('escape'=>false,'class'=>'mycar')).'</li>';
                    echo '<li class="ll">'.$this->Html->link('<i class="fas fa-power-off"></i> Sair','/cadastro/logoff?'.date('sdmYHis'),array('escape'=>false,'class'=>'out')).'</li>';
                echo '</ul>';
            echo '</div>';
        echo '</div>';
    }
?>

<header>
    <div class="container-fix">
        <div class="row bt1">
            <?PHP
                if($this->Session->check('Cliente')){
                    if((strlen($this->Session->read('Cliente.nome'))) > 37){
                        $cname = substr($this->Session->read('Cliente.nome'),0, 37).'...';
                    }else{
                        $cname = $this->Session->read('Cliente.nome');
                    }
                    
                    echo '<div class="mre-new">';
//                        echo $this->Html->link('<i class="fas fa-user"></i> <span><b class="name">'.$cname.'</b></span> <br> <span> <b>'.$cpontos.'</b> pontos</span>','javascript:void(0)',array('escape'=>false,'class'=>'lp'));
                        
                        if($valor_vencer && $data_vencer){
                            echo '<div class="name-pontos avencer">';
                                echo '<p><b><i class="fas fa-user"></i> '.$cname.'</b></p>';
                                echo '<p><b>'.$cpontos.'</b> pontos disponíveis</p>';
                                echo '<p><b>'.number_format($valor_vencer, 0, ',', '.').'</b> pontos a vencer em '.$data_vencer.'<p>';
                            echo '</div>';
                        }else{
                            echo '<div class="name-pontos">';
                                echo '<p><b><i class="fas fa-user"></i> '.$cname.'</b></p>';
                                echo '<p class="cd"><b>'.number_format($pontos_cliente['disponivel_para_troca'], 0, ',', '.').'</b> pontos disponíveis</p>';
//                                echo '<p class="cex"><b>'.number_format($pontos_cliente['expirado'], 0, ',', '.').'</b> expirado(s) e não utilizado(s)</p>';
                            echo '</div>';
                        }
                        
//                        echo '<div class="name-pontos">';
//                            echo '<p><b><i class="fas fa-user"></i> '.$cname.'</b></p>';
//                            echo '<p><b>'.$cpontos.'</b> pontos disponíveis</p>';
//                        echo '</div>';
                        
                    echo '</div>';
                }else{
                    echo '<div class="mre mre2">';
                        echo $this->Html->link('<i class="fas fa-user"></i> Faça seu login ou cadastre-se',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register'),array('escape'=>false,'class'=>'loginc'));
                    echo '</div>';
                }
            
               
//                echo '<div class="mre mre2">';
//                    echo '&nbsp;';
//                echo '</div>';
                
            ?>
        </div>
        <div class="row">
            <div class="two columns logo">
                <?PHP echo $this->Html->link($this->Html->image('logo2021.png',array('alt'=>'Compra Premiada')),'/',array('escape'=>false)); ?>
            </div>
            <div class="ten columns right-top">
                <div class="openmenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <nav id="bgmenu">
                    <ul>
                        <?PHP
                            echo '<li class="li-close">'.$this->Html->link($this->Html->image('close.svg',array('alt'=>'Fechar')),'javascript:void(0)',array('escape'=>false,'class'=>'button-close')).'</li>';
                            
                            echo '<li>'.$this->Html->link('Produtos',array('plugin'=>'produtos','controller'=>'produtos','action'=>'index'),array('escape'=>false,'class'=>'afirst')).'</li>';
                            echo '<li>'.$this->Html->link('Ponto em Dobro',array('plugin'=>'ponto_dobro','controller'=>'pecas','action'=>'index'),array('escape'=>false)).'</li>';
                            echo '<li>'.$this->Html->link('Regulamento',array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'index'),array('escape'=>false)).'</li>';
                            echo '<li>'.$this->Html->link('Galeria de Fotos',array('plugin'=>'galerias','controller'=>'galerias','action'=>'index'),array('escape'=>false)).'</li>';
                            echo '<li>'.$this->Html->link('Contato',array('plugin'=>'contatos','controller'=>'contatos','action'=>'index'),array('escape'=>false,'id'=>'mn3')).'</li>';
                        ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>