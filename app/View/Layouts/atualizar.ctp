<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-N2VMLT6');</script> 
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?PHP echo $title_for_layout?></title>
        <?PHP
            echo $this->Html->charset('utf-8');
            echo $this->Html->meta('canonical',$canonical,array('rel'=>'canonical','type'=>null,'title'=>null));
            echo $this->Html->meta(Router::url('/',true).'favicon.png', Router::url('/',true).'img/favicon.png', array('type'=>'icon'));
        ?>
        <!--<meta property="fb:app_id" content="54545454545454" />--> 
        <meta property="og:locale" content="pt_BR">
        <meta property="og:site_name" content="comprapremiadafipal">
        <meta property="og:url" content="<?PHP echo $this->Html->url(false,true); ?>" />  
        <?PHP if(isset($og_title)){ ?> 
            <meta property="og:title" content="<?PHP echo $og_title; ?>" /> 
        <?PHP }else{ ?>   
            <meta property="og:title" content="Compra Premiada" /> 
        <?PHP } ?>   
        <?PHP if(isset($og_description)){ ?> 
            <meta property="og:description" content="<?PHP echo $og_description; ?>" /> 
        <?PHP }else{ ?>  
            <meta property="og:description" content="Fipal Compra Premiada Reparadores" /> 
        <?PHP } ?>   
        <?PHP if(isset($og_image)){ ?> 
            <meta property="og:image" content="<?PHP echo $this->Html->url('/'.$og_image,true); ?>" /> 
        <?PHP }else{ ?>   
            <meta property="og:image" content="<?PHP echo $this->Html->url('/img/share.jpg',true); ?>" /> 
        <?PHP } ?>   
        <meta property="og:type" content="website" />
    
        <?PHP 
//            echo $this->Html->css('/css/styles');
            echo $this->Less->css('styles-v4'); 
            echo $this->Html->css('https://use.fontawesome.com/releases/v5.0.6/css/all.css'); 
        ?>
        <script>
            var base = '<?= $this->base ?>';
        </script>
    </head>
    <body>
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2VMLT6"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 

        <?PHP 
            echo $this->element('header'); 
    
        ?>
        
        <div class="bg-gray bgpb">
            <div class="row bg-bread">
                <div class="container-fix">
                    <?PHP
                        $this->Html->addCrumb('Inicial','/'); 
                        $this->Html->addCrumb('Atualizar dados',array('plugin'=>'clientes','controller'=>'clientes','action'=>'atualizar')); 
                        echo $this->element('breadcrumb');

        //                echo '<h1 class="h1p">Faça seu login ou cadastre-se</h1>';
                        echo '<h1 class="h1p">Atualize seus dados</h1>';
                    ?>
                </div>
            </div>
            <section class="container-fix content-mt">
                <div class="row">

                    <div class="row box-login-cadastro blc2 row-atualizar">
                        <!--<h2>Atualize seus dados</h2>-->
                        <?PHP
//                            pre($data_cliente);
                            
                            echo $this->Form->create('Cliente',array('class'=>'formc','id'=>'formAdd','url'=>array('plugin'=>'clientes','controller'=>'clientes','action'=>'atualizar')));
                                echo $this->Form->hidden('id',array('value'=>$data_cliente['id']));
                                
                                echo '<div class="row">';
                                    echo '<div class="six columns">'. $this->Form->input('cnpj',array('label'=>'CNPJ <span>(apenas números)</span>','div'=>false,'type'=>'tel','maxlength'=>14,'onkeypress'=>'return SomenteNumero(event)','value'=>$data_cliente['cnpj'])) .'</div>';
                                    echo '<div class="six columns">'. $this->Form->input('nome_fantasia',array('label'=>'Nome Fantasia da Empresa','div'=>false,'value'=>$data_cliente['nome_fantasia'])) .'</div>';
                                echo '</div>';

                                echo '<div class="row">';
                                    echo $this->Form->input('nome',array('label'=>'Nome do Solicitante','div'=>false,'value'=>$data_cliente['nome']));
                                echo '</div>';

                                echo '<div class="row">';
                                    echo '<div class="six columns">'. $this->Form->input('telefone1',array('label'=>'Telefone 1 <span>(apenas números)</span>','div'=>false,'maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);','value'=>$data_cliente['telefone1'])) .'</div>';
                                    echo '<div class="six columns">'. $this->Form->input('telefone2',array('label'=>'Telefone 2 <span>(apenas números)</span>','div'=>false,'maxlength'=>15,'type'=>'tel','onkeyup'=>'mascara(this,mtel);','value'=>$data_cliente['telefone2'])) .'</div>';
                                echo '</div>';

                                echo '<div class="row">';
                                    echo $this->Form->input('qual_vendedor',array('label'=>'Qual seu Vendedor Fipal','div'=>false,'value'=>$data_cliente['qual_vendedor']));
                                echo '</div>';

                                echo '<div class="row">';
                                    echo $this->Form->input('concessionaria_id',array('type'=>'select','options'=>$select_concessionaria,'label'=>'Selecione qual sua Fipal','required'=>'required'));
                                echo '</div>';

                                echo '<label class="label-termos">';
                                    echo '<input type="checkbox" name="data[Cliente][termos]" value="1">';
                                    echo 'Li e estou de acordo com os '.$this->Html->link('Termos de Uso',array('plugin'=>'regulamento','controller'=>'regulamento','action'=>'termo_uso'),array('escape'=>false,'target'=>'_blank')). ' da plataforma';
                                echo '</label>';

                                echo '<div class="row row-buttons">';
                                    echo $this->Form->submit('Atualizar',array('div'=>false,'class'=>'button-add'));
                                    echo '<p id="msg-add" class="msg-send-form">enviando...</p>';
                                echo '</div>';

                            echo $this->Form->end(); 
                        ?>
                    </div>

                </div>

            </section>
        </div>

        
        <?PHP 
            echo $this->element('footer'); 
        ?>
        
        <script src="<?PHP echo $this->Html->url('/js/less.js',true); ?>"></script>
        <script src="<?PHP echo $this->Html->url('/js/jquery.js',true); ?>"></script>
        <script src="<?PHP echo $this->Html->url('/bxslider/jquery.bxslider.min.js',true); ?>"></script>
        <script src="<?PHP echo $this->Html->url('/js/jquery.magnific-popup.min.js',true); ?>"></script>
        <script src="<?PHP echo $this->Html->url('/js/js-v2.js',true); ?>"></script>
    </body>
</html>