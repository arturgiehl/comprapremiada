<div id="popup_cookies">
    <div class="row r-close">
        <a href="javascript:void(0)" class="closepopup"> <?PHP echo $this->Html->image('close.svg',array('alt'=>'Fechar')); ?> </a>
    </div>
    
    <div class="row row-img-cookie">
        <?PHP echo $this->Html->image('icon_cookie_black.png',array('alt'=>'')); ?>
    </div>
    
    <div class="row r-texts">
        <h2>Nós usamos cookies</h2>
        <p>
            Podemos colocá-los para análise dos nossos dados de visitantes, para melhorar o nosso site, mostrar conteúdos personalizados e para lhe proporcionar uma ótima experiência no site. 
            Para mais informações sobre os cookies que utilizamos, leia nossa <?PHP echo $this->Html->link('política de Cookies',$this->Html->url('/politica-de-cookies.pdf',true),array('escape'=>false,'target'=>'_blank')); ?>
        </p>
    </div>
    
    <div class="row r-checkbox">
        <h3>Ativação dos cookies</h3>

        <div class="row rcheckt">
            <div class="six columns">
                <label class="switch"> <input type="checkbox" checked="true" disabled="true" class="check_nes"> <span class="slider round"></span> </label>
                <span class="stitle">Necessário</span>
            </div>
            
            <div class="six columns">
                <label class="switch lb_cga"> <input type="checkbox" name="checkbox_click_ga" id="checkbox_click_ga" value="1"> <span class="slider round"></span> </label>
                <span class="stitle">Desempenho</span>
            </div>
        </div>
        
<!--        <div class="row rcheckt">
            <div class="six columns">
                <label class="switch"> <input type="checkbox"> <span class="slider round"></span> </label>
                <span class="stitle">Funcional</span>
            </div>
            
            <div class="six columns">
                <label class="switch"> <input type="checkbox"> <span class="slider round"></span> </label>
                <span class="stitle">Publicidade</span>
            </div>
        </div>-->
        
        <?PHP
            echo '<div class="row buttnos-an">';
                echo $this->Html->link('Salvar preferências','javascript:void(0)',array('escape'=>false,'id'=>'save_pref_cookies'));
//                echo $this->Html->link('Rejeitar','javascript:void(0)',array('escape'=>false,'id'=>'reject_all_cookies'));
            echo '</div>';
        ?>
    </div>
</div>

<script>
$(document).ready(function(){
    $('.closepopup').click(function(){
        $.magnificPopup.close(); 
    });
    
    if(window.localStorage.getItem('lsc_ga_analytic') !== null) {
        if(window.localStorage.getItem('lsc_ga_analytic') === '1'){
            $('#checkbox_click_ga').prop('checked', true);
        }else{
            $('#checkbox_click_ga').prop('checked', false);
        }
    }
    
    $('#save_pref_cookies').click(function(){
        if($('input[name="checkbox_click_ga"]').is(':checked')){
            localStorage.setItem('lsc_ga_analytic','1');
        }else{
            localStorage.setItem('lsc_ga_analytic','0');
        }
        localStorage.setItem('lsc_show_nav_footer','0');
        $.magnificPopup.close(); 
    });
    
});
</script>