<div class="container-fix">
    <div class="row page-404">
        <div class="row texto">
            <p>404 - Página não encontrada ou inexistente. </p>
        </div>
        <div class="row">
            <?PHP echo $this->Html->link('Ir para a página inicial',array('controller'=>'pages','action'=>'home'),array('escape'=>false));?>
        </div>
    </div>
</div>
