$(document).ready(function(){
            
    if(window.localStorage.getItem('box_cookie_comprafipal_v2') === null) {
        $('.box-cookies-navegation').show();
    }
    
    $('#acept_bc').click(function() {
        localStorage.setItem('box_cookie_comprafipal_v2','aceito');
        $('.box-cookies-navegation').hide();
    });
    
    $(".openmenu, .button-close").click(function(){
        $('#bgmenu').toggle(); 
    });
    
    $(".csm").click(function(){
        $('#submenu'+this.id).slideToggle(); 
    });
    
    $("#fc").click(function(){
        $('#box-fc').slideToggle(); 
    });
    
    $("#pt").click(function(){
        $('#box-pt').slideToggle(); 
    });
    
    $('.slider-banner').bxSlider({
        slideWidth: 1920,
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 0,
        controls : false,
        auto: true,
        speed: '5000'
    });
    
    $('.slider-banner-mobile').bxSlider({
        slideWidth: 650,
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 0,
        controls : false,
        auto: true,
        speed: '5000'
    });
    
    $("#button-forget").click(function(){
        $('#formLogin').hide();
        $('#formForget').show();
    });
        
    $("#button-login").click(function(){
        $('#formForget').hide();
        $('#formLogin').show();
    });
    
    $("#cep").blur(function() {
        if($("#cep").val().length < 8 ){
            alert('ATENÇÃO: Cep deve conter 8 digitos');
            return;
        } 
        
        $("#loadcep").show();
        
        cep = $.trim($("#cep").val());
        $.getJSON("https://viacep.com.br/ws/"+cep+"/json/unicode/")
            .done(function(json) {
                $("#endereco").val(json.logradouro);
                $("#bairro").val(json.bairro);
                $("#cidade").val(json.localidade);
                $("#estado").val(json.uf);
                $("#numero").focus();
                $("#loadcep").hide();
            })
            .fail(function( jqxhr, textStatus, error ) {
                alert('Ocorreu algum erro.');
        });
    });
    
    $(".button-add").click(function(){
        var formID = document.getElementById("formAdd");
        if(formID.checkValidity()) {
            $(this).hide();
            $('#msg-add').show();
        }
    });
    
    $(".bs-orc").click(function(){
        var formID = document.getElementById("formPedido");
        if(formID.checkValidity()) {
            $(this).hide();
            $('.msg-send-form').show();
        }
    });
    
    $(".bs-contact").click(function(){
        var formID = document.getElementById("ContatoIndexForm");
        if(formID.checkValidity()) {
            $(this).hide();
            $('.msg-send-form').show();
        }
    });
    
    $('.bg-gallery, .img-main').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Carregando imagem #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1]
        },
        image: {
            tError: '<a href="%url%">Imagem não carregada.',
        }
    });

    $('.al-left').magnificPopup({
        type: 'image'
    });
    
    $('.img-pd').magnificPopup({
        type: 'image'
    });
    
    $('.add-orc').magnificPopup({
        type: 'ajax',
        closeOnBgClick: true,
        showCloseBtn: true,
        callbacks: {
            open: function() {
                location.hash = 'add';
            },
            close: function(){
                window.history.pushState('', '/', window.location.pathname);
                $.magnificPopup.close(); 
            },
            success: function() {
                
            }
        }
    });
    
    $(".arrowQrd").click(function(){
        var produtoSessaoId = this.id;
        var quantidade = $('#qtd_'+produtoSessaoId).val();
        var sum = $(this).attr('data-sum');
        var produto_id = $(this).attr('data-pi');
          
        $.ajax({
            type: "GET", 
            url: base+"/produtos/orcamento/qtdeup",
            data: { quantidade: quantidade, produtoSessaoId: produtoSessaoId, produtoId: produto_id, sum: sum },
            contentType: "application/json; charset=utf-8",
            cache: false,
            beforeSend: function() {
                $('.popup-loader').magnificPopup({ closeOnBgClick: false, showCloseBtn: false, type: 'ajax', removalDelay: 160, preloader: false, fixedContentPos: false, }).magnificPopup('open');
            },
            error: function() {
                alert('Ocorreu algum erro.');
            },
            success: function(retorno){
                var data = jQuery.parseJSON(retorno);
                $.magnificPopup.close(); 
                
                if(data.ponto == '1'){
                    alert('ATENÇÃO: Você não tem pontos suficientes para aumentar a quantidade do produto.');
                }else{
                    window.location.href = base+"/meu-carrinho";
                }
            } 
        }); 
    });
    
    $('#select-order').change(function() {
        var pagep = $('#pagep').val();
        if(this.value != ""){
            window.location.href = pagep+"?ordenacao="+this.value;
        }else{
            window.location.href = pagep;
        }
    });
                
});

$(window).bind("pageshow", function() {
    $('.msg-send-form').hide();
    $('.button-add, .bs-orc').show();
});

function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>47 && tecla<58)) return true;
    else{
        if (tecla==8 || tecla==0) return true;
        else  return false;
    }
}

function mascara(o,f){
    v_obj = o;
    v_fun = f;
    setTimeout("execmascara()",1);
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function mtel(v){
    v=v.replace(/\D/g,"");             
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); 
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    
    return v;
}

function mdata(v){
    v=v.replace(/\D/g,"")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    return v;
}