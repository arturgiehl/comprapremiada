<section class="top-content">
    <div class="container-fix">
        <h1>Dúvidas Frequentes</h1>
    </div>
</section>

<section class="container-fix">
    <?PHP
        $this->Html->addCrumb('Principal','/'); 
        $this->Html->addCrumb('Dúvidas Frequentes',array('plugin'=>'faqs','controller'=>'faqs','action'=>'index')); 
        echo $this->element('breadcrumb');
    ?>
    <div class="row low-bc bg-faqs">
        <?PHP
            $cf = 1;
            foreach ($posts as $faq) {
                echo '<div class="row faq">';
                    echo '<div class="faq-title" id="'.$faq ['Faq']['id'].'">
                            '.$cf.' - '.$faq['Faq']['title'].' <b id="b_'.$faq ['Faq']['id'].'">+</b>
                         </div>';
                    echo '<div class="faq-text" id="faq-text'.$faq ['Faq']['id'].'">'; 
                        echo $faq['Faq']['resposta'];
                    echo '</div>';    
                echo '</div>';
                
                $cf++;
            }
        ?>
    </div>
    <div class="row row-content-bi">
        <?PHP echo $this->element('box_links'); ?>
    </div>
</section>


<script>
    var element = document.getElementById("mn5");
    element.classList.add("m-active");
</script>

        
