<?PHP
echo $this->Html->script('jquery.maskMoney.js',array('inline'=>true));  

echo $this->Form->create('Nota',array('url'=>array('plugin'=>'notas','controller'=>'notas','action'=>'manual')));
?>

<fieldset class="box">
    <legend>Adicionar Nota Manual</legend>
    <?PHP
        $filiais = array(
                        'FIAT_FP001_EMI' => '001',
                        'FIAT_FP002_EMI' => '002',
                        'FIAT_FP003_EMI' => '003',
                        'FIAT_FP004_EMI' => '004',
                        'FIAT_FP005_EMI' => '005',
                        'FIAT_FP006_EMI' => '006',
                        'FIAT_FP007_EMI' => '007',
                        'FIAT_FP009_EMI' => '009'
                    );
        
        echo $this->Form->input('filial',array('type'=>'select','options'=>$filiais,'label'=>'Filial'));
        echo $this->Form->input('cliente_codigo',array('label'=>'Código do Cliente','maxlength'=>7,'onkeypress'=>'return SomenteNumero(event)'));
        echo $this->Form->input('cliente_nome',array('label'=>'Nome do Cliente'));
        echo $this->Form->input('cliente_cnpj',array('label'=>'CNPJ','onkeypress'=>'return SomenteNumero(event)'));
//        echo $this->Form->input('natureza_codigo',array('label'=>'Natureza Código'));
        echo $this->Form->input('nf_numero',array('label'=>'Número da Nota','maxlength'=>7,'onkeypress'=>'return SomenteNumero(event)'));
        echo $this->Form->input('data_emissao',array('label'=>'Data Emissão','maxlength'=>10,'onkeyup'=>'mascara(this,mdata);','placeholder'=>'99/99/9999'));
        echo $this->Form->input('valor_nf',array('label'=>'Valor da Nota','type'=>'text','id'=>'valor'));
    ?>
</fieldset>

<?PHP
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>

<script>
var base = '<?= $this->base ?>';
    
$(document).ready(function(){
    $("#valor").maskMoney({showSymbol:true, decimal:",", thousands:"."});
});

function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;   
    if((tecla>47 && tecla<58)) return true;
    else{
        if (tecla==8 || tecla==0) return true;
        else  return false;
    }
}

function mascara(o,f){
    v_obj = o;
    v_fun = f;
    setTimeout("execmascara()",1);
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function mtel(v){
    v=v.replace(/\D/g,"");             
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); 
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    
    return v;
}

function mdata(v){
    v=v.replace(/\D/g,"")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    return v;
}
</script>