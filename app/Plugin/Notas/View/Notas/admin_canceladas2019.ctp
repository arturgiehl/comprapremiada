<fieldset class="box">
    <legend>Notas Canceladas 2019</legend>
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('cliente_codigo','Código')?></td>
                <td><?PHP echo $this->Paginator->sort('cliente_nome','Nome')?></td>
                <td><?PHP echo $this->Paginator->sort('cliente_cnpj','CNPJ')?></td>
                <td><?PHP echo $this->Paginator->sort('nf_numero','NF Número')?></td>
                <td><?PHP echo $this->Paginator->sort('nf_numero','Data Emissão')?></td>
                <td><?PHP echo $this->Paginator->sort('valor_nf','Valor')?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Importada')?></td>
                <td class="edit">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP foreach($posts as $post){ ?>
             <tr data-id="<?PHP echo $post['NotaCancelada2019']['id']?>">
                <td><?PHP echo $post['NotaCancelada2019']['cliente_codigo']?></td>
                <td><?PHP echo $post['NotaCancelada2019']['cliente_nome']?></td>
                <td><?PHP echo $post['NotaCancelada2019']['cliente_cnpj']?></td>
                <td><?PHP echo $post['NotaCancelada2019']['nf_numero']?></td>
                <td><?PHP echo $post['NotaCancelada2019']['data_emissao']?></td>
                <td><?PHP echo $post['NotaCancelada2019']['valor_nf']?></td>
                <td><?PHP echo $post['NotaCancelada2019']['cdate']?></td>
            </tr>
            <?PHP }?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>
