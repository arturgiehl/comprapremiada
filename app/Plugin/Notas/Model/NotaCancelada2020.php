<?PHP
class NotaCancelada2020 extends NotasAppModel{
    
    var $name = 'NotaCancelada2020';
    var $useTable = 'notas_canceladas_2020';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(NotaCancelada2020.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(NotaCancelada2020.modified,'%d/%m/%Y %H:%i')",
    );
    
}