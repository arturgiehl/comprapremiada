<?PHP
class Nota2020 extends NotasAppModel{
    
    var $name = 'Nota2020';
    var $useTable = 'notas_emitidas_2020';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Nota2020.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Nota2020.modified,'%d/%m/%Y %H:%i')",
    );
    
}