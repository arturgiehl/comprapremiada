<?PHP
class NotaProdutoCancelada extends NotasAppModel {
    
    var $name = 'NotaProdutoCancelada';
    var $useTable = 'notas_produtos_canceladas';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(NotaProdutoCancelada.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(NotaProdutoCancelada.modified,'%d/%m/%Y %H:%i')"
    );
    
}