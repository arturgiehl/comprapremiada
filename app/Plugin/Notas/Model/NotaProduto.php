<?PHP
class NotaProduto extends NotasAppModel {
    
    var $name = 'NotaProduto';
    var $useTable = 'notas_produtos';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(NotaProduto.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(NotaProduto.modified,'%d/%m/%Y %H:%i')",
    );
    
}