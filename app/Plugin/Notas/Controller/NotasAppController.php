<?PHP
class NotasAppController extends AppController{
    
    public $uses = array(
        'Notas.Nota',
        'Notas.Nota2018',
        'Notas.Nota2019',
        'Notas.Nota2020',
        'Notas.Nota20202',
        'Notas.NotaCancelada',
        'Notas.NotaCancelada2018',
        'Notas.NotaCancelada2019',
        'Notas.NotaCancelada2020',
        'Notas.NotaCancelada20202',
        'Notas.NotaProduto',
        'Notas.NotaProdutoCancelada'
    ); 
    
}