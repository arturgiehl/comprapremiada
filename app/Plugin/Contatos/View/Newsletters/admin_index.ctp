<fieldset class="box">
    <legend>Sugestões (<?PHP echo $total; ?> Registros)</legend>
<!--    <nav class="pannel">
        <?PHP // echo $this->Html->link('Gerar CSV Newsletter',array('plugin'=>'contatos','controller'=>'newsletters','action'=>'report','admin'=>true)) ?>
    </nav>-->
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('nome','Sugestão'); ?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Enviado'); ?></td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP foreach($posts as $post): $post=array_shift($post); ?>
            <tr>
                <td><?PHP echo $post['nome']; ?></td>
                <td><?PHP echo $post['cdate']; ?></td>
                <!--<td><?PHP // echo $this->Html->link("Excluir",array('action'=>'delete',$post['id']),null,"Tem certeza que deseja excluir?"); ?></td>-->
            </tr>
            <?PHP endforeach; ?>
        </tbody>        
    </table>
    <?PHP echo $this->element("Painel.paginator"); ?>
</fieldset>