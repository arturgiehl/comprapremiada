<fieldset class="box">
    <legend>Contatos (<?PHP echo $total; ?> Registros)</legend>
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('nome','Nome'); ?></td>
                <td><?PHP echo $this->Paginator->sort('email','E-mail'); ?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Enviado em'); ?></td>
                <td class="delete">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP foreach($posts as $post){ 
                    $post = array_shift($post); 
            ?>
            <tr>
                <td><?PHP echo $post['nome']; ?></td>
                <td><?PHP echo $post['email']; ?></td>
                <td><?PHP echo $post['cdate']; ?></td>
                <td><?PHP echo $this->Html->link('Ver',array('action'=>'view',$post['id']))?></td>
                <td>
                    <?PHP 
                        if($this->Session->read('Auth.User.username') === 'admxdigital'){
                            echo $this->Html->link("Excluir",array('action'=>'delete',$post['id']),null,"Tem certeza que deseja excluir?"); 
                        }else{
                            echo ' ';
                        }
                    ?>
                </td>
            </tr>
            <?PHP } ?>
        </tbody>        
    </table>
    <?PHP echo $this->element("Painel.paginator");?>
</fieldset>