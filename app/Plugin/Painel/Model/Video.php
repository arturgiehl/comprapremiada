<?PHP
class Video extends PainelAppModel{
    
    public $virtualFields=array(
        'default'=>"CONCAT('http://img.youtube.com/vi/',yid,'/mqdefault.jpg')",
        'link'=>"CONCAT('http://www.youtube.com/watch?v=',yid)",
        'embed'=>"CONCAT('http://www.youtube.com/embed/',yid)",
    );
    
}