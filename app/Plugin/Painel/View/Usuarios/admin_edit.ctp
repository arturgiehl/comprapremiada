<?PHP
echo $this->Form->create('User', array('type' => 'file','url'=>array('controller'=>'usuarios','action'=>'add')));
?>

<fieldset class="box image">
    <legend><?=__('Informações pessoais')?></legend>
    <?PHP
    $user=$this->Session->read('Auth');
    echo $this->Form->input('name', array('label' => 'Nome'));
    echo $this->Form->input('email');
    if($user['User']['group']=='sadmin') echo $this->Form->input('group',array('label'=>'Grupo de acesso','options'=>$groups,'id'=>'_groups'));
    ?>
</fieldset>

<?PHP
//echo $this->element('Painel.image',array('label'=>'Foto','name'=>'foto'));
?>

<fieldset class="box image">
    <legend><?=__('Informações de login')?></legend>
    <?PHP
    echo $this->Form->input('username', array('label' => 'Usuário'));
    echo $this->Form->input('password', array('label' => 'Senha', 'type' => 'password','value'=>''));
    echo $this->Form->input('password_retype', array('label' => 'Redigite a senha', 'type' => 'password','value'=>''));
    ?>
</fieldset>

<!--<fieldset class="box">                   
    <legend>Pós Graduação</legend>
    <?PHP // echo $this->Form->input('UsuarioPos',array('multiple'=>'checkbox','label'=>false,'options'=>$list_pos_graduacao)); ?>
</fieldset>-->

<?PHP
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>

<script type="text/javascript">
    jQuery(function($){
        $('#menu').change(function(){
            var id = $(this).val();
            $('#submenu').empty().append('<option value="0">Carregando...</option>');
            $.ajax({
                url:'<?PHP echo $this->base ?>/configs/submenus/json_get?id='+id,
                dataType:'json',
                data:{id:id},
                success:function(data){
                    $('#submenu').empty();
                    if (Object.keys(data).length > 0){                        
                        $('#submenu').removeAttr('disabled');
                        $('#submenu').append('<option value="0">Nenhum submenu</option>');
                        for(var i in data){
                            $('#submenu').append('<option value="'+data[i].id+'">'+data[i].title+'</option>');
                        }
                    }else{
                        $('#submenu').append('<option value="0">Este menu não possui nenhum submenu.</option>');
                    }
                }
            });
        });
    });
</script>