<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>E-Brief</title>
<?PHP
echo $this->Html->css('Painel.login.less.css?'.time(),'stylesheet/less');
echo $this->Html->script('Painel.less');
?>
</head>
<body>    
    <div id="login">       
        <?PHP echo $this->Html->image('Painel.login/logo.png',array('id'=>'logo')); ?>
        <div class="inner">
            <h1 class="title">Login</h1>            
            <?PHP
            if(isset($message)) echo '<h3>'.$message.'</h3>';
            echo $this->Form->create('User',array('id'=>'login-form'));
            echo $this->Form->input('username',array('label'=>'Usuário','value'=>'','autocomplete'=>'off'));
            echo $this->Form->input('password',array('label'=>'Senha','value'=>'','autocomplete'=>'off'));
            echo $this->Html->link('Esqueci minha senha',array('plugin'=>'painel','controller'=>'usuarios','action'=>'recover','admin'=>false),array('class'=>'recover-password'));
            echo $this->Form->end('Login');
            ?>
        </div>
        <div class="reflex"></div>
    </div>    
</body>
</html>