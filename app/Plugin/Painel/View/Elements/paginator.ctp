<?PHP
if($this->request->params['plugin']=='produtos' && $this->request->params['controller']=='produtos' && $this->request->params['action']=='index' && isset($this->request->params['categoria'])){
    $this->Paginator->options(array('url'=>array('plugin'=>'produtos','controller'=>'produtos','action'=>'index','categoria'=>$this->params['categoria'])));
}

if($this->Paginator && $this->Paginator->hasPage(2)):
    if(!isset($prev)) $prev='«';
    if(!isset($next)) $next='»';
    if(!isset($separator)) $separator=false;
?>
<nav class="paginator">
    <?PHP
        echo $this->Paginator->prev($prev);
        echo $this->Paginator->numbers(array('separator'=>$separator));
        echo $this->Paginator->next($next);
    ?>
</nav>
<?PHP endif; ?>