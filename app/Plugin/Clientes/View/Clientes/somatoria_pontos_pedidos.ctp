<style>
    .tr-red td {
        color: red !important;
        text-decoration: line-through;
    }
    .tr-black td {
        color: #cdcdcd !important;
    }
    .tr-blue td {
        color: blue !important;
    }
</style>
<div class="bg-gray bgpb">
    <div class="row bg-bread bgmtm">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Histórico',array('plugin'=>'clientes','controller'=>'clientes','action'=>'historico')); 
                echo $this->element('breadcrumb');
                
                echo '<h1 class="h1p">Histórico Geral</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix">
        <h2 class="h2-hp">O prazo de validade para utilizar os pontos das notas é de 6 meses.</h2>
        
        
        
        
        
        <h3 class="h3r">Histórico de trocas geral</h3>
        <div class="tbr-pedido">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data de solicitação</th>
                        <th role="columnheader">Status</th>
                        <th role="columnheader">Produtos</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
        <?PHP
            $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
            $total_pontos_geral = 0;

            $saldo_total_notas = 0;
            $saldo_total_usado = 0;

            $total_disponivel = 0;

            $arr_notas_usadas = array();
            $arr_saldos_restantes = array();
            $arr_notas_expiradas = array();

            $data_ultima_nota = '';

            $restante = 0;
            foreach ($pedidos_geral as $pa) {
                $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));

                echo '<tr role="row">';
                    echo '<td role="cell" class="td20">'.$pa['PedidoCliente']['cdate'].'</td>';
                    echo '<td role="cell" class="td20">'.$status[$pa['PedidoCliente']['status']].'</td>';
                    echo '<td role="cell">';
                        echo '<table class="table-pdts">';
                            $total_qtd = 0;
                            $total_pontos = 0;
                            foreach ($produtos_pedido as $ps) {
                                echo '<tr>';
                                    echo '<td class="tdf">'.$ps['Produto']['title'].'</td>';
                                    echo '<td>'.$ps['PedidoClienteProduto']['quantidade'].'</td>';
                                    echo '<td>'.number_format($ps['PedidoClienteProduto']['pontos'], 0, ',', '.').'</td>';
                                echo '</tr>';

                                $total_pontos = $total_pontos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                                $total_qtd = $total_qtd + $ps['PedidoClienteProduto']['quantidade'];
                            }
                            echo '<tr>';
                                echo '<td class="tdta"><b>Total:</b></td>';
                                echo '<td><b>'.$total_qtd.'</b></td>';
                                echo '<td><b>'.number_format($total_pontos, 0, ',', '.').'</b></td>';
                            echo '</tr>';
                        echo '</table>';
                    echo '</td>';
                echo '</tr>';

                $total_pontos_geral = $total_pontos_geral + $total_pontos;

                $expl_date = explode(' ', $pa['PedidoCliente']['created']);

                $notas_anteriores_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_notas_anteriores_geral',$pa['PedidoCliente']['cnpj'],$expl_date[0]));

        //                            pre($notas_anteriores_pedido);

                echo '<tr role="row">';
        //                                echo '<td role="cell" class="td20"></td>';
        //                                echo '<td role="cell" class="td20"></td>';
                    echo '<td role="cell" colspan="3">';
                        echo '<table class="table-notas">';
                            $total_notas = 0;
                            $notas_usadas_pedido = 0;
                            $ponto_restante = 0;


                            
                            foreach ($notas_anteriores_pedido as $nota) {
        //                                            $firstDate  = new DateTime($nota['Nota']['data_e']);
        //                                            $secondDate = new DateTime(date('Y-m-d'));
        //                                            $intvl = $firstDate->diff($secondDate);
        //                                            pre($intvl->m);

                                if(!in_array($nota['Nota']['id'], $arr_notas_usadas)) { 
                                    $arr_notas_usadas[] = $nota['Nota']['id'];


                                    $firstDate  = new DateTime($expl_date[0]);
                                    $secondDate = new DateTime($nota['Nota']['data_e']);
                                    $intvl = $firstDate->diff($secondDate);
                                    
                                    if($intvl->m < 6){
                                        $ponto_restante = 0;


                                        $notas_usadas_pedido = $notas_usadas_pedido + $nota['Nota']['valor_nf'];
   
                                        $class = '';
                                        if($notas_usadas_pedido > $total_pontos && !$arr_saldos_restantes){ 
                                            
                                            $ponto_restante = $notas_usadas_pedido - $total_pontos;

                                            $class = 'class="tr-blue"';
                                            $arr_saldos_restantes[$nota['Nota']['id']] = array(
                                                'id_nota'          => $nota['Nota']['id'],
                                                'valor_total_nota' => $nota['Nota']['valor_nf'],
                                                'valor_restante'   => $ponto_restante,
                                                'data_e'           => $nota['Nota']['data_e'],
                                                'data_expiracao'   => $nota[0]['data_expiracao']
                                            );

                                            echo '<tr '.$class.'>';
                                                echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                                echo '<td>'.number_format($nota['Nota']['valor_nf'] , 0, ',', '.').' - '.number_format($ponto_restante , 0, ',', '.').'</td>';
                                                echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                                echo '<td>'.$nota[0]['data_expiracao'].'</td>';
                                            echo '</tr>';
                                        }
                                        else if($notas_usadas_pedido <= $total_pontos){
                                            $class = 'class="tr-black"';
                                            echo '<tr '.$class.'>';
                                                echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                                echo '<td>'.number_format($nota['Nota']['valor_nf'] , 0, ',', '.').'</td>';
                                                echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                                echo '<td>'.$nota[0]['data_expiracao'].'</td>';
                                            echo '</tr>';
                                        }else{
                                            echo '<tr>';
                                                echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                                echo '<td>'.number_format($nota['Nota']['valor_nf'] , 0, ',', '.').'</td>';
                                                echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                                echo '<td>'.$nota[0]['data_expiracao'].'</td>';
                                            echo '</tr>';
                                        }

                                        

                                        $total_notas = $total_notas + $nota['Nota']['valor_nf'];
                                        $data_ultima_nota = $nota['Nota']['data_e'];
                                    }else{
                                        $arr_notas_expiradas[] = array(
                                            'id_nota'        => $nota['Nota']['id'],
                                            'data_emissao'   => $nota['Nota']['data_e'],
                                            'data_expiracao' => $nota[0]['data_expiracao'],
                                            'valor_nota'     => $nota['Nota']['valor_nf'],
                                            'valor_expirado' => $nota['Nota']['valor_nf'],
                                        );

                                        $class = 'class="tr-red"';
                                        echo '<tr '.$class.'>';
                                            echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                            echo '<td>'.number_format($nota['Nota']['valor_nf'] , 0, ',', '.').'</td>';
        //                                                    echo '<td>'.number_format($notas_usadas_pedido , 0, ',', '.').'</td>';
                                            echo '<td>'.$nota['Nota']['data_e'].'</td>';
                                            echo '<td>'.$nota[0]['data_expiracao'].'</td>';
        //                                                    echo '<td>'.$intvl->m.', '.date('d/m/Y', strtotime("6 months",strtotime($nota['Nota']['data_e']))).'</td>';
                                        echo '</tr>';
                                    }
                                }
                            }

                            
                            if($arr_saldos_restantes && $total_notas < $total_pontos){
                                foreach ($arr_saldos_restantes as $restante) {
//                                    pre($restante);
                                    $firstDate  = new DateTime($expl_date[0]);
                                    $secondDate = new DateTime($restante['data_e']);
                                    $intvl = $firstDate->diff($secondDate);
                                    if($intvl->m < 6){
//                                        pre($restante);
                                        $saldo_restante = ($total_notas + $restante['valor_restante']) - $total_pontos;
                                        $arr_saldos_restantes[$restante['id_nota']] = array(
                                            'id_nota'          => $restante['id_nota'],
                                            'valor_total_nota' => $restante['valor_total_nota'],
                                            'valor_restante'   => $saldo_restante,
                                            'data_e'           => $restante['data_e'],
                                            'data_expiracao'   => $restante['data_expiracao'],
                                        );
                                    }
                                }
                            }
                            
                            $saldo_total_notas = $saldo_total_notas + ($total_notas - $total_pontos_geral); 
                            $saldo_total_usado = $saldo_total_usado + $total_pontos;

                            $total_disponivel = $total_disponivel + ($total_notas - $total_pontos); 

                            echo '<tr><td>&nbsp;</td></tr>';

                            echo '<tr>';
                                echo '<td colspan="3"><b>Total notas: '.number_format($total_notas , 0, ',', '.').'</b></td>';
                            echo '</tr>';
                            echo '<tr>';
                                echo '<td colspan="3"><b>Usado: '.number_format($total_pontos, 0, ',', '.').'</b></td>';
                            echo '</tr>';
                            echo '<tr>';
                                echo '<td colspan="3"><b>Total disponivel: '.number_format($total_disponivel , 0, ',', '.').'</b></td>';
                            echo '</tr>';
                        echo '</table>';
                    echo '</td>';
                echo '</tr>';
            }
            
            
            
            

        //                        pre($arr_saldos_restantes);
        //                        pre($arr_notas_expiradas);
        //                        pre($data_ultima_nota);
            $proximas_notas = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_proximas_notas',$cliente['cnpj'],$data_ultima_nota));
        ?>
                </tbody>
            </table>
        </div>
        
        
        <div class="row">
            <div class="total-low-geral">
                <div class="bin"><b>Disponivel: </b></div>
                <div class="bin "><b><?PHP echo number_format($total_disponivel, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        
        <h3 class="h3r">Notas emitidas a partir de <?PHP echo $data_ultima_nota; ?></h3>
        <div class="row-table-responsive">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data da emissão</th>
                        <th role="columnheader">Pontos</th>
                        <th role="columnheader">Válido até:</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $total_pontos = 0;
                        foreach ($proximas_notas as $nota) {
                            $firstDate  = new DateTime($nota['Nota']['data_e']);
                            $secondDate = new DateTime(date('Y-m-d'));
                            $intvl = $firstDate->diff($secondDate);
                                    
                            if($intvl->m < 6){
                                echo '<tr role="row">';
                                    echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                    echo '<td role="cell">'.number_format($nota['Nota']['valor_nf'], 0, ',', '.').'</td>';
                                    echo '<td role="cell">'.date('d/m/Y', strtotime($nota['Nota']['data_e'].' + 6 months')).'</td>';
                                echo '</tr>';

                                $total_pontos = $total_pontos + $nota['Nota']['valor_nf'];
                            }else{
//                                pre($nota);
                                $arr_notas_expiradas[] = array(
                                    'id_nota'        => $nota['Nota']['id'],
                                    'data_emissao'   => $nota['Nota']['data_e'],
                                    'data_expiracao' => $nota[0]['data_expiracao'],
                                    'valor_nota'     => $nota['Nota']['valor_nf'],
                                    'valor_expirado' => $nota['Nota']['valor_nf'],
                                );
                                
                                echo '<tr class="tr-red" role="row">';
                                    echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                    echo '<td role="cell">'.number_format($nota['Nota']['valor_nf'], 0, ',', '.').'</td>';
                                    echo '<td role="cell">'.date('d/m/Y', strtotime($nota['Nota']['data_e'].' + 6 months')).'</td>';
                                echo '</tr>';
                            }
                        }
                        
                        $total_usado_expirado = 0;
                        if($arr_saldos_restantes){
                            foreach ($arr_saldos_restantes as $restante) {
                                $firstDate  = new DateTime(date('Y-m-d'));
                                $secondDate = new DateTime($restante['data_e']);
                                $intvl = $firstDate->diff($secondDate);
                                if($intvl->m >= 6){
                                    $total_usado_expirado = $total_usado_expirado + $restante['valor_restante'];
                                }
                            }
                        }

                        if($total_usado_expirado < 0){
                            $total_usado_expirado = 0;
                        }
            
//                        pre($arr_notas_expiradas);
                        $total_pontos_expirados = 0;
                        if($arr_notas_expiradas){
                            foreach ($arr_notas_expiradas as $nota_expirada) {
                                $total_pontos_expirados = $total_pontos_expirados + $nota_expirada['valor_expirado'];
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
        
        <div class="bg-low-table">
            <table role="table">
                <tbody role="rowgroup">
                    <tr> <td role="cell"></td><td role="cell"></td><td role="cell"></td> </tr>
                    <tr>
                        <td role="cell"><b>Disponível:</b></td>
                        <td role="cell"><?PHP echo number_format($total_pontos, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        
      
        <div class="row">
            <div class="total-low-geral">
                <div class="bin"><b>Disponivel geral: </b></div>
                <div class="bin "><b><?PHP echo number_format($total_disponivel + $total_pontos, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        <div class="row">
            <div class="total-low-geral">
                <div class="bin"><b>Expirado: </b></div>
                <div class="bin "><b><?PHP echo number_format($total_usado_expirado + $total_pontos_expirados, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        <div class="row">
            <div class="total-low-geral">
                <div class="bin"><b>Disponivel para troca: </b></div>
                <div class="bin "><b><?PHP echo number_format(($total_disponivel + $total_pontos) - $total_usado_expirado, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        <div class="row" style="margin-top: 20px;">
            <div class="three columns">
                <div class="total-low-geral">
                    <div class="bin">Notas antes período:</div>
                    <div class="bin "><b><?PHP echo number_format($total_nota_antes, 0, ',', '.'); ?></b></div>
                </div>
                <div class="total-low-geral">
                    <div class="bin">Notas no período:</div>
                    <div class="bin "><b><?PHP echo number_format($total_nota_atuais, 0, ',', '.'); ?></b></div>
                </div>
                <div class="total-low-geral">
                    <div class="bin">Total:</div>
                    <div class="bin "><b><?PHP echo number_format($total_nota_antes + $total_nota_atuais, 0, ',', '.'); ?></b></div>
                </div>
            </div>
            <div class="six columns">
                <div class="total-low-geral">
                    <div class="bin">Trocas antes período:</div>
                    <div class="bin "><b><?PHP echo number_format($total_trocas_antes6, 0, ',', '.'); ?></b></div>
                </div>
                <div class="total-low-geral">
                    <div class="bin">Trocas no período:</div>
                    <div class="bin "><b><?PHP echo number_format($total_dentro6, 0, ',', '.'); ?></b></div>
                </div>
                <div class="total-low-geral">
                    <div class="bin">Total:</div>
                    <div class="bin "><b><?PHP echo number_format($total_trocas_antes6 + $total_dentro6, 0, ',', '.'); ?></b></div>
                </div>
            </div>
        </div>
        
    </section>
</div>