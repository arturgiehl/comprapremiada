<?PHP
    $total_nota_antes = 0;
    foreach ($notas_antes as $nota_a) {
        $total_nota_antes = $total_nota_antes + $nota_a['Nota']['valor_nf'];
    }
    echo number_format($total_nota_antes, 0, ',', '.');
    
    $total_nota_atuais = 0;
    foreach ($notas_atuais as $nota_a) {
        $total_nota_atuais = $total_nota_atuais + $nota_a['Nota']['valor_nf'];
    }
    
    $total_notas_geral = $total_nota_antes + $total_nota_atuais;
    
    $total_trocas_antes6 = 0;
    $saldo_sobrando_antes = 0;
    foreach ($pedidos_antes as $pa) {
//        pre($pedido_antes);
        $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));
        foreach ($produtos_pedido as $ps) {
            $total_trocas_antes6 = $total_trocas_antes6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
        }
        
        $expl_date = explode(' ', $pa['PedidoCliente']['created']);
        $total_notas_periodo = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_notas_periodo',$pa['PedidoCliente']['cnpj'],$expl_date[0]));
//        $saldo_sobrando_antes = $saldo_sobrando_antes + ($total_notas_periodo - $total_trocas_antes6);
        $saldo_sobrando_antes = $saldo_sobrando_antes + ($total_nota_antes - $total_trocas_antes6);
    }
    
    $total_dentro6 = 0;
    $saldo_sobrando_periodo = 0;
    foreach ($pedidos_ativos as $pa) {
        $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));
        foreach ($produtos_pedido as $ps) {
            $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
        }
        
        $expl_date = explode(' ', $pa['PedidoCliente']['created']);
        $total_notas_periodo = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_notas_periodo_2',$pa['PedidoCliente']['cnpj'],$expl_date[0]));
//        pre($total_notas_periodo);
        $saldo_sobrando_periodo = $saldo_sobrando_periodo + ($total_notas_periodo['total_periodo'] + $saldo_sobrando_antes) - $total_dentro6;
    }
    
    $total_trocas_geral = $total_trocas_antes6 + $total_dentro6;
    
    if($total_trocas_antes6 > 0){
        $expirado = $total_nota_antes - $total_trocas_antes6;
    }else{
        $expirado = $total_nota_antes - ($total_trocas_antes6 + $total_dentro6);
    }
    
    if($expirado < 0){
        $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);
        $show_expirado = 0;
        $ja_utilizado = abs($expirado);
    }else{
        $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6) - $expirado;
        $show_expirado = $expirado;
        $ja_utilizado = $total_dentro6;
    }
    
?>
<div class="bg-gray bgpb">
    <div class="row bg-bread bgmtm">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Histórico',array('plugin'=>'clientes','controller'=>'clientes','action'=>'historico')); 
                echo $this->element('breadcrumb');
                
                echo '<h1 class="h1p">Histórico de Trocas</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix">
        <h2 class="h2-hp">O prazo de validade para utilizar os pontos das notas é de 6 meses.</h2>
        
        
        
        
        
        
        
        
        <h3 class="h3r">Notas válidas emitidas em até 6 meses</h3>
        <div class="row-table-responsive">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data da emissão</th>
                        <th role="columnheader">Pontos</th>
                        <th role="columnheader">Válido até:</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $total_pontos = 0;
                        foreach ($notas_atuais as $nota) {
                            echo '<tr role="row">';
                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                echo '<td role="cell">'.number_format($nota['Nota']['valor_nf'], 0, ',', '.').'</td>';
                                echo '<td role="cell">'.date('d/m/Y', strtotime($nota['Nota']['data_e'].' + 6 months')).'</td>';
                            echo '</tr>';
                            
                            $total_pontos = $total_pontos + $nota['Nota']['valor_nf'];
                        }
                    ?>
                </tbody>
            </table>
        </div>
        
        <div class="bg-low-table">
            <table role="table">
                <tbody role="rowgroup">
                    <tr> <td role="cell"></td><td role="cell"></td><td role="cell"></td> </tr>
                    
                    <tr>
                        <td role="cell"><b>Total de notas no período:</b></td>
                        <td role="cell"><?PHP echo number_format($total_pontos, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Total de notas anteríores a 6 meses:</b></td>
                        <td role="cell"><?PHP echo number_format($total_nota_antes, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Total geral notas:</b></td>
                        <td role="cell"><?PHP echo number_format($total_notas_geral, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                    <tr> <td role="cell"></td><td role="cell"></td><td role="cell"></td> </tr>
                    
                    <tr>
                        <td role="cell"><b>Total trocas anteríores a 6 meses:</b></td>
                        <td role="cell"><?PHP echo number_format($total_trocas_antes6, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Total trocas no período de 6 meses:</b></td>
                        <td role="cell"><?PHP echo number_format($total_dentro6, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td role="cell"><b>Total geral de pontos trocados:</b></td>
                        <td role="cell"><?PHP echo number_format($total_trocas_geral, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                    <tr> <td role="cell"></td><td role="cell"></td><td role="cell"></td> </tr>
                    
<!--                    <tr>
                        <td role="cell"><b>Total geral usado - vencido:</b></td>
                        <td role="cell">
                            <?PHP 
                                echo number_format($total_trocas_geral - $total_nota_antes, 0, ',', '.'); 
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>-->
<!--                    <tr>
                        <td role="cell"><b>Sobra de anteriormente:</b></td>
                        <td role="cell">
                            <?PHP 
//                                if($total_nota_antes && $total_trocas_antes6){
//                                    echo number_format($total_nota_antes - $total_trocas_antes6, 0, ',', '.');
//                                }else{
//                                    echo 0;
//                                }
                                echo number_format($total_nota_antes - $total_trocas_antes6, 0, ',', '.');
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>-->
                    <tr>
                        <td role="cell"><b>Sobra troca geral:</b></td>
                        <td role="cell">
                            <?PHP 
                                echo number_format($total_notas_geral - $total_trocas_geral, 0, ',', '.'); 
                                echo '<br>';
                                echo number_format($total_nota_antes - $total_trocas_geral, 0, ',', '.'); 
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                    <tr> <td role="cell"></td><td role="cell"></td><td role="cell"></td> </tr>
                    
                    <tr>
                        <td role="cell"><b>Expirado:</b></td>
                        <td role="cell">
                            <?PHP 
//                                $expirado = 0;
//                                if(($total_notas_geral - $total_trocas_geral) > $total_pontos){
//                                    $expirado = ($total_notas_geral - $total_trocas_geral) - $total_pontos;
//                                }
//                                echo number_format($expirado, 0, ',', '.');
//                                if($saldo_sobrando_antes < 0){
//                                    echo number_format($total_notas_geral - $total_trocas_geral, 0, ',', '.'); 
//                                }else{
//                                    echo number_format($total_nota_antes - $total_trocas_geral, 0, ',', '.'); 
//                                }
//                                if(($saldo_sobrando_antes + $saldo_sobrando_periodo) > 0){
//                                    echo number_format($total_notas_geral - $total_trocas_geral, 0, ',', '.'); 
//                                }else{
//                                    echo number_format($total_nota_antes - $total_trocas_antes6, 0, ',', '.'); 
//                                }
                                
//                                echo number_format($total_trocas_geral - $saldo_sobrando_antes, 0, ',', '.');
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td role="cell"><b>Disponível para troca periodo:</b></td>
                        <td role="cell">
                            <?PHP 
                                
                                if($total_pontos > $total_dentro6){
                                    echo number_format($total_pontos - $total_dentro6, 0, ',', '.'); 
                                }else{
                                    echo number_format($total_pontos - ($total_trocas_geral - $total_nota_antes), 0, ',', '.');
//                                    echo number_format($total_pontos, 0, ',', '.');
                                }
                                
                                
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
        
        <h3 class="h3r">Histórico de trocas dentro do período de 6 meses</h3>
        <h3 class="h3r">Histórico de trocas</h3>
        <div class="tbr-pedido">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data de solicitação</th>
                        <th role="columnheader">Status</th>
                        <th role="columnheader">Produtos</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
                        $total_qtd_geral_periodo = 0;
                        $total_pontos_geral_periodo = 0;
                        foreach ($pedidos_ativos as $pa) {
                            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));
                            
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">'.$pa['PedidoCliente']['cdate'].'</td>';
                                echo '<td role="cell" class="td20">'.$status[$pa['PedidoCliente']['status']].'</td>';
                                echo '<td role="cell">';
                                    echo '<table class="table-pdts">';
                                        $total_qtd_periodo = 0;
                                        $total_pontos_periodo = 0;
                                        foreach ($produtos_pedido as $ps) {
//                                            pre($ps);
                                            echo '<tr>';
                                                echo '<td class="tdf">'.$ps['Produto']['title'].'</td>';
                                                echo '<td>'.$ps['PedidoClienteProduto']['quantidade'].'</td>';
                                                echo '<td>'.number_format($ps['PedidoClienteProduto']['pontos'], 0, ',', '.').'</td>';
                                            echo '</tr>';
                                            
                                            $total_pontos_periodo = $total_pontos_periodo + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                                            $total_qtd_periodo = $total_qtd_periodo + $ps['PedidoClienteProduto']['quantidade'];
                                        }
                                        echo '<tr>';
                                            echo '<td class="tdta"><b>Total:</b></td>';
                                            echo '<td><b>'.$total_qtd_periodo.'</b></td>';
                                            echo '<td><b>'.number_format($total_pontos_periodo, 0, ',', '.').'</b></td>';
                                        echo '</tr>';
                                    echo '</table>';
                                echo '</td>';
                            echo '</tr>';
                            
                            $total_qtd_geral_periodo = $total_qtd_geral_periodo + $total_qtd_periodo;
                            $total_pontos_geral_periodo = $total_pontos_geral_periodo + $total_pontos_periodo;
                            
                            $expl_date = explode(' ', $pa['PedidoCliente']['created']);
                            $total_notas_periodo = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_notas_periodo_2',$pa['PedidoCliente']['cnpj'],$expl_date[0]));
//                            pre($total_notas_periodo);
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">Notas total: '.number_format($total_notas_periodo['total_geral'], 0, ',', '.').'</td>';
                                echo '<td role="cell" class="td20">Notas periodo: '.number_format($total_notas_periodo['total_periodo'], 0, ',', '.').'</td>';
                                echo '<td role="cell" class="td20">Saldo: '.number_format($total_notas_periodo['total_geral'] - $total_pontos_geral_periodo, 0, ',', '.').'</td>';
                            echo '</tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="total-low-pedidos">
                <div class="bin b1"><b>Total geral: </b></div>
                <div class="bin b2"><b><?PHP echo $total_qtd_geral_periodo; ?></b></div>
                <div class="bin b3"><b><?PHP echo number_format($total_pontos_geral_periodo, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        <h3 class="h3r">Histórico de trocas anteriores a 6 meses</h3>
        <div class="tbr-pedido">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data de solicitação</th>
                        <th role="columnheader">Status</th>
                        <th role="columnheader">Produtos</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
                        $total_qtd_geral = 0;
                        $total_pontos_geral = 0;
                        foreach ($pedidos_antes as $pa) {
                            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));
                            
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">'.$pa['PedidoCliente']['cdate'].'</td>';
                                echo '<td role="cell" class="td20">'.$status[$pa['PedidoCliente']['status']].'</td>';
                                echo '<td role="cell">';
                                    echo '<table class="table-pdts">';
                                        $total_qtd = 0;
                                        $total_pontos = 0;
                                        foreach ($produtos_pedido as $ps) {
                                            echo '<tr>';
                                                echo '<td class="tdf">'.$ps['Produto']['title'].'</td>';
                                                echo '<td>'.$ps['PedidoClienteProduto']['quantidade'].'</td>';
                                                echo '<td>'.number_format($ps['PedidoClienteProduto']['pontos'], 0, ',', '.').'</td>';
                                            echo '</tr>';
                                            
                                            $total_pontos = $total_pontos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                                            $total_qtd = $total_qtd + $ps['PedidoClienteProduto']['quantidade'];
                                        }
                                        echo '<tr>';
                                            echo '<td class="tdta"><b>Total:</b></td>';
                                            echo '<td><b>'.$total_qtd.'</b></td>';
                                            echo '<td><b>'.number_format($total_pontos, 0, ',', '.').'</b></td>';
                                        echo '</tr>';
                                    echo '</table>';
                                echo '</td>';
                            echo '</tr>';
                            
                            $total_qtd_geral = $total_qtd_geral + $total_qtd;
                            $total_pontos_geral = $total_pontos_geral + $total_pontos;
                            
                            $expl_date = explode(' ', $pa['PedidoCliente']['created']);
                            $total_notas_periodo = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_notas_periodo_2',$pa['PedidoCliente']['cnpj'],$expl_date[0]));
//                            pre($total_notas_periodo);
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">Notas total: '.number_format($total_notas_periodo['total_geral'], 0, ',', '.').'</td>';
                                echo '<td role="cell" class="td20">Notas periodo: '.number_format($total_notas_periodo['total_periodo'], 0, ',', '.').'</td>';
                                echo '<td role="cell" class="td20">Saldo: '.number_format($total_notas_periodo['total_geral'] - $total_pontos_geral, 0, ',', '.').'</td>';
                            echo '</tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="total-low-pedidos">
                <div class="bin b1"><b>Total geral: </b></div>
                <div class="bin b2"><b><?PHP echo $total_qtd_geral; ?></b></div>
                <div class="bin b3"><b><?PHP echo number_format($total_pontos_geral, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        
        
        <h3 class="h3r">Histórico de trocas geral</h3>
        <div class="tbr-pedido">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data de solicitação</th>
                        <th role="columnheader">Status</th>
                        <th role="columnheader">Produtos</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
                        $total_qtd_geral = 0;
                        $total_pontos_geral = 0;
                        
                        $saldo_total = 0;
                        $saldo_total_usado = 0;
                        
                        $saldo_remanescente = 0;
                        $saldo_total_expirado = 0;
                        $remanescente = 0;
                        
                        foreach ($pedidos_geral as $pa) {
                            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pa['PedidoCliente']['sessao_id']));
                            
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">'.$pa['PedidoCliente']['cdate'].'</td>';
                                echo '<td role="cell" class="td20">'.$status[$pa['PedidoCliente']['status']].'</td>';
                                echo '<td role="cell">';
                                    echo '<table class="table-pdts">';
                                        $total_qtd = 0;
                                        $total_pontos = 0;
                                        foreach ($produtos_pedido as $ps) {
                                            echo '<tr>';
                                                echo '<td class="tdf">'.$ps['Produto']['title'].'</td>';
                                                echo '<td>'.$ps['PedidoClienteProduto']['quantidade'].'</td>';
                                                echo '<td>'.number_format($ps['PedidoClienteProduto']['pontos'], 0, ',', '.').'</td>';
                                            echo '</tr>';
                                            
                                            $total_pontos = $total_pontos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                                            $total_qtd = $total_qtd + $ps['PedidoClienteProduto']['quantidade'];
                                        }
                                        echo '<tr>';
                                            echo '<td class="tdta"><b>Total:</b></td>';
                                            echo '<td><b>'.$total_qtd.'</b></td>';
                                            echo '<td><b>'.number_format($total_pontos, 0, ',', '.').'</b></td>';
                                        echo '</tr>';
                                    echo '</table>';
                                echo '</td>';
                            echo '</tr>';
                            
                            $total_qtd_geral = $total_qtd_geral + $total_qtd;
                            $total_pontos_geral = $total_pontos_geral + $total_pontos;
                            
                            $expl_date = explode(' ', $pa['PedidoCliente']['created']);
                            $total_notas_periodo = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_notas_periodo_2',$pa['PedidoCliente']['cnpj'],$expl_date[0]));
                            
//                            $remanescente = 0;
//                            if(($total_notas_periodo['total_geral'] - $total_pontos) > 0){
//                                $remanescente = $total_notas_periodo['total_geral'] - $total_pontos;
//                            }
//                            $remanescente = $remanescente + ($total_notas_periodo['total_geral'] - $total_pontos_geral);
//                            $remanescente = $remanescente + ($total_notas_periodo['total_geral'] - $total_pontos_geral);
                            $remanescente = $total_notas_periodo['total_geral'] - $total_pontos_geral;
                               
                            $saldo_total = $saldo_total + ($total_notas_periodo['total_geral'] - $total_pontos_geral);
                                    
                            echo '<tr role="row">';
                                echo '<td role="cell" class="td20">';
                                    echo 'nt: '.number_format($total_notas_periodo['total_geral'], 0, ',', '.').' &nbsp;&nbsp;';
                                    echo 'np: '.number_format($total_notas_periodo['total_periodo'], 0, ',', '.').' ';
                                    echo 'rem: '.number_format($remanescente, 0, ',', '.').' ';
//                                    echo 'ex: '.number_format($expirado, 0, ',', '.').' <br> ';
//                                    echo 'total: '.number_format($saldo_total, 0, ',', '.');
                                echo '</td>';
//                                echo '<td role="cell" class="td20">Usado: '.number_format($total_notas_periodo['total_periodo'] - $total_pontos, 0, ',', '.').'</td>';
//                                echo '<td role="cell" class="td20">Saldo: '.number_format($total_notas_periodo['total_geral'] - $total_pontos_geral, 0, ',', '.').'</td>';
                                echo '<td role="cell" class="td20">Usado: '.number_format($total_pontos, 0, ',', '.').'</td>';
                                echo '<td role="cell" class="td20">Saldo: '.number_format($saldo_total, 0, ',', '.').'</td>';
                            echo '</tr>';
                        }
//                        pre($remanescente);
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="total-low-pedidos">
                <div class="bin b1"><b>Total geral: </b></div>
                <div class="bin b2"><b><?PHP echo $total_qtd_geral; ?></b></div>
                <div class="bin b3"><b><?PHP echo number_format($total_pontos_geral, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        <div class="row">
            <div class="total-low-pedidos">
                <div class="bin"><b>Total disponivel: </b></div>
                <div class="bin b3"><b><?PHP echo number_format($saldo_total, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        <div class="row">
            <div class="total-low-pedidos">
                <div class="bin"><b>Remaa: </b></div>
                <div class="bin b3"><b><?PHP echo number_format($remanescente, 0, ',', '.'); ?></b></div>
            </div>
        </div>
        
        
        
        
        
    </section>
</div>