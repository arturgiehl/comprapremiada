<?PHP
    $total_nota_antes = 0;
    foreach ($notas_antes as $nota_a) {
        $total_nota_antes = $total_nota_antes + $nota_a['Nota']['valor_nf'];
    }
//    echo number_format($total_nota_antes, 0, ',', '.');
    
    $total_nota_atuais = 0;
    foreach ($notas_atuais as $nota_a) {
        $total_nota_atuais = $total_nota_atuais + $nota_a['Nota']['valor_nf'];
    }
    
    $total_trocas_antes6 = 0;
    foreach ($pedidos_antes as $pedido_antes) {
        $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_antes['PedidoCliente']['sessao_id']));
        foreach ($produtos_pedido as $ps) {
            $total_trocas_antes6 = $total_trocas_antes6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
        }
    }
    
    $total_dentro6 = 0;
    foreach ($pedidos_ativos as $pedido_periodo) {
        $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_periodo['PedidoCliente']['sessao_id']));
        foreach ($produtos_pedido as $ps) {
            $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
        }
    }
    
    if($total_trocas_antes6 > 0){
        $expirado = $total_nota_antes - $total_trocas_antes6;
    }else{
        $expirado = $total_nota_antes - ($total_trocas_antes6 + $total_dentro6);
//        $expirado = $total_nota_antes;
    }
//    pre($total_trocas_antes6);
    
    if($expirado < 0){
        $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);
        $show_expirado = 0;
        $ja_utilizado = abs($expirado);
//        pre($expirado);
//        $ja_utilizado = $total_dentro6;
    }else{
        $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6) - $expirado;
        $show_expirado = $expirado;
        $ja_utilizado = $total_dentro6;
    }
    
?>

<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Minhas Notas',array('plugin'=>'clientes','controller'=>'clientes','action'=>'minhas_notas')); 
                echo $this->element('breadcrumb');
                
                echo '<h1 class="h1p">Histórico de Notas</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix">
        <h2 class="h2-hp">O prazo de validade para utilizar os pontos das notas é de 6 meses.</h2>
        
        <div class="row-table-responsive">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Data da emissão</th>
                        <th role="columnheader">Pontos</th>
                        <th role="columnheader">Válido até:</th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $total_pontos = 0;
                        foreach ($notas_atuais as $nota) {
                            echo '<tr role="row">';
                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                echo '<td role="cell">'.number_format($nota['Nota']['valor_nf'], 0, ',', '.').'</td>';
//                                echo '<td role="cell">'.$nota['Nota']['data_emissao'].'</td>';
                                echo '<td role="cell">'.date('d/m/Y', strtotime($nota['Nota']['data_e'].' + 6 months')).'</td>';
                            echo '</tr>';
                            
                            $total_pontos = $total_pontos + $nota['Nota']['valor_nf'];
                        }
                    ?>
                </tbody>
            </table>
        </div>
        
        <div class="bg-low-table">
            <table role="table">
                <tbody role="rowgroup">
                    <tr>
                        <td role="cell"><b>Total de notas no período:</b></td>
                        <td role="cell"><?PHP echo number_format($total_pontos, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
<!--                    <tr>
                        <td role="cell"><b>Total trocas anteríores a 6 meses:</b></td>
                        <td role="cell"><?PHP // echo number_format($total_trocas_antes6, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Total trocas no período de 6 meses:</b></td>
                        <td role="cell"><?PHP // echo number_format($total_dentro6, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>-->
                    <tr>
                        <td role="cell"><b>Já utilizado no período de 6 meses:</b></td>
                        <td role="cell">
                            <?PHP 
                                echo '-'.number_format($ja_utilizado, 0, ',', '.');
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Disponível para troca:</b></td>
                        <td role="cell">
                            <?PHP 
                                echo number_format($disponivel, 0, ',', '.'); 
                            ?>
                        </td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                    
                    <tr>
                        <td role="cell"></td>
                        <td role="cell"></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Total de notas anteríores a 6 meses:</b></td>
                        <td role="cell"><?PHP echo number_format($total_nota_antes, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Total geral de notas:</b></td>
                        <td role="cell"><?PHP echo number_format($total_nota_antes + $total_pontos, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td role="cell"><b>Pontos trocados anteríor 6 meses:</b></td>
                        <td role="cell"><?PHP echo number_format($total_trocas_antes6, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td role="cell"><b>Total geral de pontos trocados:</b></td>
                        <td role="cell"><?PHP echo number_format($total_trocas_antes6 + $total_dentro6, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                    <tr>
                        <td role="cell"><b>Pontos expirados e não utilizados:</b></td>
                        <td role="cell"><?PHP echo number_format($show_expirado, 0, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        
    </section>
</div>