<?PHP 
//    pre($post['Cliente']);
?>

<fieldset class="box">
    <legend>Cliente</legend>    
    <dl class="description">
        <dt>CNPJ:</dt><dd><?PHP echo $cliente['Cliente']['cnpj']?></dd>
        <dt>Nome Fantasia:</dt><dd><?PHP echo $cliente['Cliente']['nome_fantasia']?></dd>
        <dt>Nome:</dt><dd><?PHP echo $cliente['Cliente']['nome']?></dd>
    </dl>    
</fieldset>

<fieldset class="box box-inline">
    <legend>Notas emitidas</legend>    
    <dl class="description">
        <dt>Notas antes do período:</dt><dd><?PHP echo number_format($total_nota_antes, 0, ',', '.'); ?></dd>
        <dt>Notas dentro do período:</dt><dd><?PHP echo number_format($total_nota_atuais, 0, ',', '.'); ?></dd>
        <dt>Total geral:</dt><dd><?PHP echo number_format($total_nota_antes + $total_nota_atuais, 0, ',', '.'); ?></dd>
    </dl>    
</fieldset>

<fieldset class="box box-inline">
    <legend>Trocas realizadas</legend>    
    <dl class="description">
        <dt>Trocas antes do período:</dt><dd><?PHP echo number_format($total_trocas_antes6, 0, ',', '.'); ?></dd>
        <dt>Trocas dentro do período:</dt><dd><?PHP echo number_format($total_dentro6, 0, ',', '.'); ?></dd>
        <dt>Total geral:</dt><dd><?PHP echo number_format($total_trocas_antes6 + $total_dentro6, 0, ',', '.'); ?></dd>
    </dl>    
</fieldset>

<fieldset class="box box-inline">
    <legend>Pontuação</legend>    
    <dl class="description">
        <dt>Expirado e não utilizado:</dt><dd><?PHP echo number_format($total_disponivel['expirado'], 0, ',', '.'); ?></dd>
        <dt>Disponível para troca:</dt><dd><?PHP echo number_format($total_disponivel['disponivel_para_troca'], 0, ',', '.'); ?></dd>
    </dl>    
</fieldset>


<style type="text/css">
    dt { 
        width: 120px !important;
    }
    .box-inline {
        width: 44%;
        /*padding-top: 20px !important;*/
        display: inline-block;
        float: left;
        margin-right: 1%;
    }

    .box-inline dt {
        width: 55% !important;
    }
    .box-inline dd {
        width: 40% !important;;
    }
</style>