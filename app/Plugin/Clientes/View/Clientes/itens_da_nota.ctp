<?PHP
//    $total_dentro6 = 0;
//    foreach ($pedidos_ativos as $pedido_periodo) {
//        $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_periodo['PedidoCliente']['sessao_id']));
//        foreach ($produtos_pedido as $ps) {
//            $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
//        }
//    }
?>
<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Itens da Nota',array('plugin'=>'clientes','controller'=>'clientes','action'=>'itens_da_nota')); 
                echo $this->element('breadcrumb');
                
                echo '<h1 class="h1p">Itens da Nota</h1>';
            ?>
        </div>
    </div>
    <section class="container-fix">
        <h2 class="h2-hp">Itens da nota <?PHP echo $this->params->nota; ?></h2>
        
        <div class="row-table-responsive tbr-itens">
            <table role="table">
                <thead role="rowgroup">
                    <tr role="row">
                        <th role="columnheader">Código Item</th>
                        <th role="columnheader">Ponto em dobro</th>
                        <th role="columnheader">Valor:</th>
                        <th role="columnheader"> </th>
                    </tr>
                </thead>
                <tbody role="rowgroup">
                    <?PHP
                        $total_pontos = 0;
                        foreach ($itens_nota as $item_nota) {
//                            pre($item_nota);
                            $ponto_dobro = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_ponto_dobro',$this->params->nota,$this->Session->read('Cliente.cnpj'),$item_nota['NotaProduto']['codigo_item']));
                            
                            if($ponto_dobro){
                                echo '<tr role="row">';
                                    echo '<td role="cell">'.$item_nota['NotaProduto']['codigo_item'].'</td>';
                                    echo '<td role="cell" style="color:green;">2x</td>';
                                    echo '<td role="cell">R$ '.number_format($item_nota['NotaProduto']['valor_item'] * 2, 2, ',', '.').'</td>';
                                echo '</tr>';

                                $total_pontos = $total_pontos + ($item_nota['NotaProduto']['valor_item'] * 2);
                            }else{
                                echo '<tr role="row">';
                                    echo '<td role="cell">'.$item_nota['NotaProduto']['codigo_item'].'</td>';
                                    echo '<td role="cell">1x</td>';
                                    echo '<td role="cell">R$ '.number_format($item_nota['NotaProduto']['valor_item'], 2, ',', '.').'</td>';
                                echo '</tr>';

                                $total_pontos = $total_pontos + $item_nota['NotaProduto']['valor_item'];
                            }
                        }
                    ?>
                </tbody>
            </table>
        </div>
        
        <div class="bg-low-table">
            <table role="table">
                <tbody role="rowgroup">
                    <tr>
                        <!--<td role="cell"><b>Total:</b></td>-->
                        <td role="cell"><b>Total:</b> R$ <?PHP echo number_format($total_pontos, 2, ',', '.'); ?></td>
                        <td role="cell">&nbsp;</td>
                        <td role="cell">&nbsp;</td>
                        <td role="cell">&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
</div>