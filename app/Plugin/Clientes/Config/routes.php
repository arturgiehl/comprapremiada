<?PHP
Router::connect('/historico-geral',array('plugin'=>'clientes','controller'=>'clientes','action'=>'somatoria_pontos_pedidos'),array());

Router::connect('/historico',array('plugin'=>'clientes','controller'=>'clientes','action'=>'historico'),array());
Router::connect('/historico-de-notas',array('plugin'=>'clientes','controller'=>'clientes','action'=>'minhas_notas'),array());

Router::connect('/itens-da-nota/:nota',array('plugin'=>'clientes','controller'=>'clientes','action'=>'itens_da_nota'),array('nota'=>'[a-zA-Z0-9-_]+'));

Router::connect('/atualize-seus-dados',array('plugin'=>'clientes','controller'=>'clientes','action'=>'atualizar'),array());
Router::connect('/consultar-pontuacao',array('plugin'=>'clientes','controller'=>'clientes','action'=>'pontos'),array());
Router::connect('/cadastro/login-cadastro',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_register'),array());

Router::connect('/cadastro/cadastrar',array('plugin'=>'clientes','controller'=>'clientes','action'=>'add'),array());
Router::connect('/cadastro/login',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login'),array());
Router::connect('/cadastro/login-social',array('plugin'=>'clientes','controller'=>'clientes','action'=>'login_social'),array());
Router::connect('/cadastro/logoff',array('plugin'=>'clientes','controller'=>'clientes','action'=>'logoff'),array());

Router::connect('/cadastro/solicitar-nova-senha',array('plugin'=>'clientes','controller'=>'clientes','action'=>'forget'),array());

Router::connect('/cadastro/alterar-senha',array('plugin'=>'clientes','controller'=>'clientes','action'=>'change'),array());
Router::connect('/cadastro/alterar-senha/:slug',array('plugin'=>'clientes','controller'=>'clientes','action'=>'change'),array('slug'=>'[a-zA-Z0-9-_]+'));
Router::connect('/cadastro/salvar-nova-senha/:slug',array('plugin'=>'clientes','controller'=>'clientes','action'=>'save_new_password'),array('slug'=>'[a-zA-Z0-9-_]+'));

Router::connect('/rotinas/aviso-mensal-pontuacao',array('plugin'=>'clientes','controller'=>'rotinas','action'=>'email_mensal_ponto'),array());
Router::connect('/rotinas/aviso-vencimento-ponto-dez-dias',array('plugin'=>'clientes','controller'=>'rotinas','action'=>'email_ponto_dez_dias'),array());