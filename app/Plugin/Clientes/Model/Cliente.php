<?PHP
class Cliente extends ClientesAppModel {
    
    var $name = 'Cliente';
    var $useTable = 'clientes';
    
    public $virtualFields = array(
        'cdate' => "DATE_FORMAT(Cliente.created,'%d/%m/%Y %H:%i')",
        'mdate' => "DATE_FORMAT(Cliente.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $validate = array(
        'cnpj'              => array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'nome_fantasia'     => array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'nome'              => array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'telefone1'         => array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'qual_vendedor'     => array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'email'             => array('rule'=>'email','message'=>'Não é um e-mail válido'),
        'senha'             => array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'concessionaria_id' => array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
    );
    
    public $actsAs = array(
        'Painel.Slug' => array('title'=>'slug'),
    );
   
    public $belongsTo = array(
        'Conce' => array(
            'className'  => 'Conces.Conce',
            'foreignKey' => 'concessionaria_id',
            'fields'     => array('id','title','telefone','slug')
        ),
    ); 
    
}