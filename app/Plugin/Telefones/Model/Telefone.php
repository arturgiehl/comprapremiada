<?PHP
class Telefone extends TelefonesAppModel{
    
    var $name = 'Telefone';
    var $useTable = 'contagem_telefones';
    
    public $virtualFields=array(
        'cdate'=>"DATE_FORMAT(Telefone.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Telefone.modified,'%d/%m/%Y %H:%i')",
    );
    
}