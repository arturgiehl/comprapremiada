<div class="open-search">
    <?PHP
        $value = isset($_GET['s']) ? $_GET['s']:'';
        echo $this->Form->create('Busca', array('url' => array('plugin'=>'buscas','controller'=>'buscas','action'=>'index'),'id'=>'search','type'=>'get'));
            echo $this->Form->input('s',array('label'=>false,'div'=>'back-input','value'=>$value,'placeholder'=>'Buscar'));
            echo $this->Form->submit('ok',array('div'=>false));
        echo $this->Form->end();
    ?>
</div>