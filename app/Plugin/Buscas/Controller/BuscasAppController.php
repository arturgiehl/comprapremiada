<?PHP
class BuscasAppController extends AppController{
    
    public $uses = array(
                    'Buscas.Busca',
                    'Produtos.Produto',
                    'Produtos.CategoriaProduto'
                );
    
}