<?PHP
class Orcamento20202 extends ProdutosAppModel {
    
    public $useTable  = 'pedidos_troca_2020_2';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Orcamento20202.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Orcamento20202.modified,'%d/%m/%Y %H:%i')",
    );
    
}