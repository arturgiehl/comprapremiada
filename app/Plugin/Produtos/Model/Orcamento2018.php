<?PHP
class Orcamento2018 extends ProdutosAppModel {
    
    public $useTable  = 'pedidos_troca_2018';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Orcamento2018.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Orcamento2018.modified,'%d/%m/%Y %H:%i')",
    );
    
}