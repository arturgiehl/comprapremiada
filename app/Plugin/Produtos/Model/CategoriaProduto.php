<?PHP
class CategoriaProduto extends ProdutosAppModel{

    var $useTable = 'produtos_categoria';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(CategoriaProduto.created,'%d/%m/%Y')",
        'mdate'=>"DATE_FORMAT(CategoriaProduto.modified,'%d/%m/%Y')",
    );
    
    public $validate = array(
        'title'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
    );
    
    public $actsAs = array(
        'Painel.Gallery',
        'Painel.Slug'=>array('title'=>'slug'),
    );
    
//    public $hasMany = array (
//        'SubCategoria' => array(
//            'className' => 'Produtos.SubCategoria',
//            'foreignKey' => 'categoria_id',
//            'order' => array('SubCategoria.title'=>'ASC')
//        )
//    );
    
}