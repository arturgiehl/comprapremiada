<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; " />
        <title>Compra Premiada Fipal</title>

        <style type="text/css">
            body {
                -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6;
            }
            body {
                background-color: #f6f6f6;
            }
        </style>
    </head>
    <body style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background: #f6f6f6; margin: 0; padding: 0;">
        <table class="body-wrap" style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; width: 100%; background: #f6f6f6; margin: 0; padding: 0;">
            <tr style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; margin: 0; padding: 0;">
                <td style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; vertical-align: top; margin: 0; padding: 0;" valign="top"></td>
                <td class="container" width="700" style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;" valign="top">
                    <div class="content" style="font-family: 'Gautami', 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 15px; background: #fff; margin: 0; padding: 20px; margin-top: 40px; margin-bottom: 40px; border: 1px solid #e9e9e9;">
                        <p style="text-align: center; font-weight: bold; font-size: 18px; margin-top: 10px; margin-top: 0px;">
                            <b>Status do Pedido</b>
                        </p>
                        <p>
                            Olá, <?PHP echo $cliente['nome']; ?>, 
                        </p>
                        <p>seu pedido de troca foi encaminhado para compra.</p>
                        
                        <table cellspacing=0 cellpadding=4 style="width: 100%; margin-top: 20px">
                            <thead style="text-align: left; font-size: 14px;">
                                <tr>
                                  <th style="border-bottom: 1px solid #ccc; width: 40%">Produto</th>
                                  <th style="border-bottom: 1px solid #ccc">Quantidade</th>
                                  <th style="border-bottom: 1px solid #ccc">Ponto Unitário</th>
                                  <th style="border-bottom: 1px solid #ccc">Total</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 13px">
                                <?PHP
                                    $total_pedido = 0;
                                    foreach ($produtos as $produto) {
                                        $total_pedido = $total_pedido + $produto['Produto']['preco'] * $produto['ProdutoSessao']['quantidade'];
                                        echo '<tr>';
                                            echo '<td style="border-top: 1px solid #ccc;">'.$produto['Produto']['title'].'</td>';
                                            echo '<td style="border-top: 1px solid #ccc;">'.$produto['ProdutoSessao']['quantidade'].'</td>';
                                            echo '<td style="border-top: 1px solid #ccc;">'.number_format($produto['Produto']['preco'], 0, ',', '.').'</td>';
                                            echo '<td style="border-top: 1px solid #ccc;">'.number_format($produto['Produto']['preco'] * $produto['ProdutoSessao']['quantidade'], 0, ',', '.').'</td>';
                                        echo '</tr>';
                                    }
                                    echo '<tr>';
                                        echo '<td style="border-top: 1px solid #ccc;">&nbsp;</td>';
                                        echo '<td style="border-top: 1px solid #ccc;">&nbsp;</td>';
                                        echo '<td style="border-top: 1px solid #ccc;">&nbsp;</td>';
                                        echo '<td style="border-top: 1px solid #ccc;">'.number_format($total_pedido, 0, ',', '.').'</td>';
                                    echo '</tr>';
                                ?>
                            </tbody>
                        </table>
                        
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>
