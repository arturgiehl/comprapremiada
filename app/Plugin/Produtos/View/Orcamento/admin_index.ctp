<fieldset class="box">
    <legend>Pedidos</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Gerar CSV',array('plugin'=>'produtos','controller'=>'orcamento','action'=>'report','admin'=>true)) ?>
    </nav>
    <?PHP
        $value = !empty($_GET['s']) ? $_GET['s'] : '';
        $options=array('type' => 'get', 'id' => 'search');
        if(!empty($url)) $options['url']=$url;
        echo $this->Form->create('Search',array('url'=>array('plugin'=>'produtos', 'controller'=>'orcamento', 'action'=>'index'),'type' => 'get'));
        echo $this->Form->input('s', array('div'=>false,'label'=>false,'placeholder'=>'BUSCA','value'=>$value));
        echo $this->Form->end(array('label' => 'OK', 'div' => false));
    ?>
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('nome','Nome'); ?></td>
                <td><?PHP echo $this->Paginator->sort('email','E-mail'); ?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Enviado'); ?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Status'); ?></td>
                <td class="delete">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP 
                $status = array(''=>'Aguardando', '2'=>'Encaminhado para compra', '3'=>'Entregue');
                foreach($posts as $post){ 
                    $post = array_shift($post); 
                    if($post['nome'] && $post['email']){
            ?>
            <tr>
                <td><?PHP echo $post['nome']; ?></td>
                <td><?PHP echo $post['email']; ?></td>
                <td><?PHP echo $post['cdate']; ?></td>
                <td><?PHP echo $status[$post['status']]; ?></td>
                <td><?PHP echo $this->Html->link('Imprimir',array('action'=>'impression',$post['id']),array('target'=>'_blank')); ?></td>
                <td><?PHP echo $this->Html->link('Ver',array('action'=>'view',$post['id']),array('target'=>'_blank')); ?></td>
            </tr>
            <?PHP 
                    }
                } 
            ?>
        </tbody>        
    </table>
    <?PHP echo $this->element("Painel.paginator"); ?>
</fieldset>