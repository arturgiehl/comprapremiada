<?PHP
echo $this->Form->create('CategoriaProduto',array('url'=>array('action'=>'add')));
?>

<fieldset class="box">
    <legend>Categoria</legend>
    <?PHP echo $this->Form->input('title',array('label'=>'Título')); ?>
</fieldset>

<?PHP
echo $this->element('Painel.image',array('label'=>'Ícone (PNG sem fundo 64x64)','name'=>'icone'));
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>