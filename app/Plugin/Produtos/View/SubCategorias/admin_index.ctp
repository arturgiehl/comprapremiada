<fieldset class="box">
    <legend>Sub Categorias</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar',array('action'=>'add')) ?>
    </nav>
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('title','Título')?></td>
                <td><?PHP echo $this->Paginator->sort('categoria_id','Categoria')?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Criado')?></td>
                <td class="created"><?PHP echo $this->Paginator->sort('modified','Modificado')?></td>
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?PHP foreach($posts as $post){ ?>
            <tr>
                <td><?PHP echo $post['SubCategoria']['title']?></td>
                <td><?PHP echo $post['Categoria']['title']?></td>
                <td><?PHP echo $post['SubCategoria']['cdate']?></td>
                <td><?PHP echo $post['SubCategoria']['mdate']?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['SubCategoria']['id']))?></td>
                <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['SubCategoria']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>
