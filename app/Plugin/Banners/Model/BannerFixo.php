<?PHP
class BannerFixo extends BannersAppModel {

    var $useTable = 'banners_fixos';
    
    public $virtualFields = array(
        'cdate' => "DATE_FORMAT(BannerFixo.created,'%d/%m/%Y %H:%i')",
        'mdate' => "DATE_FORMAT(BannerFixo.modified,'%d/%m/%Y %H:%i')",
    );

}