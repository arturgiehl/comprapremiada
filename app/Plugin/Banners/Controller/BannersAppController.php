<?PHP
class BannersAppController extends AppController {
    
    public $uses = array(
        'Banners.Banner',
        'Banners.BannerFixo'
    );
    
    public $helpers = array('Media.Crop','Media.Resize','Media.Fill');
    
}