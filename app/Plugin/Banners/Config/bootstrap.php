<?PHP
Configure::write('Painel.menu.Banners',array(
    'Gerenciar Banners'=>array('plugin'=>'banners','controller'=>'banners','action'=>'index','admin'=>true),
    'Adicionar Banner'=>array('plugin'=>'banners','controller'=>'banners','action'=>'add','admin'=>true),
    'Banners Fixos'=>array('plugin'=>'banners','controller'=>'banner_fixo','action'=>'edit','admin'=>true),
));