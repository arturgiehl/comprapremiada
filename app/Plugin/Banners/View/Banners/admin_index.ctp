<fieldset class="box">
    <legend>Banners</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar Banner',array('plugin'=>'banners','controller'=>'banners','action'=>'add','admin'=>true)) ?>
    </nav>
    <table class="tables">
        <thead>
            <tr>
                <td class="thumb"><a>Banner</a></td>
                <td class="created"><?PHP echo $this->Paginator->sort('created','Criado')?></td>
                <td class="modified"><?PHP echo $this->Paginator->sort('modified','Modificado')?></td>
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody class="sortable">
            <?PHP foreach($posts as $post): ?>
            <tr data-id="<?PHP echo $post['Banner']['id']?>">
                <td><?PHP echo $this->Crop->image($post['Banner']['thumb'],120,80)?></td>
                <td><?PHP echo $post['Banner']['cdate']?></td>
                <td><?PHP echo $post['Banner']['mdate']?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['Banner']['id']))?></td>
                <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['Banner']['id']),false,'Tem certeza?')?></td>
            </tr>
            <?PHP endforeach; ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>

<script type="text/javascript">
    var base='<?PHP echo  $this->base ?>';
    jQuery(function($){
        $(".sortable").sortable({
            stop:function(evt,ui){
                $(".sortable>tr").each(function(i,e){
                    var id=$(e).attr('data-id');
                    var index=$(e).index();
                    $.getJSON(base+'/admin/banners/banners/order/'+id+'/'+index,function(data){
                    });
                });
            }
        });
    });
</script>