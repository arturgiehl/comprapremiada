<?PHP
echo $this->Form->create('BannerFixo',array('url'=>array('plugin'=>'banners','controller'=>'banner_fixo','action'=>'add')));
    $target = array('_self' => 'Mesma Aba', '_blank' => 'Outra Aba');

    echo $this->element('Painel.image',array('label'=>'Banner esquerdo de cima (680x300)','name'=>'banner_l1','reduce'=>'banner'));
    echo $this->Form->input('link_bl1',array('label'=>'Link <span style="font-size: 13px;">(deixar em branco se não houver)</span>'));    
    echo '<div style="clear:both; height: 30px;"></div>';
    
    echo $this->element('Painel.image',array('label'=>'Banner esquerdo de baixo (680x300)','name'=>'banner_l2','reduce'=>'banner'));
    echo $this->Form->input('link_bl2',array('label'=>'Link <span style="font-size: 13px;">(deixar em branco se não houver)</span>'));    
    echo '<div style="clear:both; height: 30px;"></div>';
    
    echo $this->element('Painel.image',array('label'=>'Banner direito (470x650)','name'=>'banner_r','reduce'=>'banner'));
    echo $this->Form->input('link_br',array('label'=>'Link <span style="font-size: 13px;">(deixar em branco se não houver)</span>'));    
    echo '<div style="clear:both;"></div>';
    
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
