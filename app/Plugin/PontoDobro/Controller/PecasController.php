<?PHP
class PecasController extends PontoDobroAppController {

    public $paginate = array('limit'=>16,'order'=>array('Peca.order_depoimento' => 'ASC', 'Peca.created' => 'DESC'));
    
    public function index(){        
        $this->Seo->title('Ponto em dobro');
        
        $this->paginate['limit']      = 15;
        $this->paginate['order']      = array('Peca.order_registro' => 'ASC', 'Peca.created' => 'DESC');
        $this->paginate['conditions'] = array('Peca.ativo' => '1');
        
        if(isset($this->params->page)){ 
            $this->paginate['page'] = $this->params->page;   
        }
        
        $posts = $this->paginate('Peca');
        $texto_pd = $this->TextoPd->find('first');
        
        $this->set('posts',$posts);
        $this->set('texto_pd',$texto_pd);
    }

    public function admin_index(){
        $this->layout = 'Painel.admin';
        $this->paginate['order'] = array('Peca.order_registro' => 'ASC', 'Peca.created' => 'DESC');
        $posts = $this->paginate('Peca');
        $this->set('posts',$posts);
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            if($this->Peca->save($this->request->data)){
                $this->redirect(array('action'=>'index'));
            }
        }
    }
    
    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        $this->data = $this->Peca->read('*',$id); 
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->Peca->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }   
    
    public function admin_order($id,$order){
        $this->autoRender = false;
        $this->Peca->query('UPDATE tb_pecas_ponto_dobro SET order_registro = "'.$order.'" WHERE id = "'.$id.'";');
    }
    
}