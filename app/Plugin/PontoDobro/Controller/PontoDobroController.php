<?PHP
App::import('Vendor', 'PHPExcel/Classes/PHPExcel');
App::import('Vendor', 'PHPExcel/Classes/PHPExcel/IOFactory');

class PontoDobroController extends PontoDobroAppController {
    
    public $components = array('Painel.Uploading','Help');
    public $paginate = array('limit'=>30,'order'=>array('created'=>'DESC'));
        
    public function admin_index(){
        $this->layout = 'Painel.admin';
        
        $this->paginate['order'] = array('PontoDobro.data_inicial' => 'DESC');
        $posts = $this->paginate('PontoDobro');
        
        $this->set('posts',$posts);
    }
    
    public function admin_add(){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        if($this->request->data && ($this->request->is('post') || $this->request->is('put'))){
            
            $expld_di = explode('/',$this->data['PontoDobro']['data_inicial']);
            if(!checkdate($expld_di[1], $expld_di[0], $expld_di[2])){
                $this->Help->message_empty('ATENÇÃO: Data inicial não é válida.'); 
            }
            
            $expld_df = explode('/',$this->data['PontoDobro']['data_final']);
            if(!checkdate($expld_df[1], $expld_df[0], $expld_df[2])){
                $this->Help->message_empty('ATENÇÃO: Data final não é válida.'); 
            }
            
            $this->request->data['PontoDobro']['data_inicial']  = $this->Help->change_save_date($this->data['PontoDobro']['data_inicial']);
            $this->request->data['PontoDobro']['data_final']  = $this->Help->change_save_date($this->data['PontoDobro']['data_final']);
            
//            pre($this->data);
            
            $this->Uploading->allowed = array('csv','CSV','txt','TXT','xlsx','XLSX');
            if(isset($this->data['Arquivo']['name']) && $this->data['Arquivo']['name']){
                $arquivo = $this->Uploading->send_file('files/produtos_notas', $this->data['Arquivo']);
                if($arquivo){
                    
                    $path_parts = pathinfo($arquivo);
                    $arr_codigos = array();
                    
                    if(isset($path_parts['extension']) && ($path_parts['extension'] === 'xlsx' || $path_parts['extension'] === 'xls')){
                        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                        $objReader->setReadDataOnly(true);
                        $objPHPExcel = $objReader->load($arquivo);

                        $sheet = $objPHPExcel->getActiveSheet(); 
                        $highestRow = $sheet->getHighestRow(); 
                        $highestColumn = $sheet->getHighestColumn();

                        $rowData = array();
                        for ($row = 1; $row <= $highestRow; $row++){ 
                            $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,NULL,TRUE,FALSE);
                        }
                        
                        foreach ($rowData as $rd) {
                            if(trim($rd[0][0]) != '' && trim($rd[0][0]) != 'CÓDIGO'){
                                $arr_codigos[] = trim($rd[0][0]);
                            }
                        }
                    }else{
                        $link_file = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . $arquivo;
                        $read_file = fopen($link_file, 'r');
                        while(!feof($read_file)){
                            $linha = fgets($read_file, 1024);
                            $codigo = str_replace(array('"'), '', $linha);
                            if(trim($codigo) != '' && trim(utf8_encode($codigo)) != 'CÓDIGO'){
                                $arr_codigos[] = trim($codigo);
                            }
                        }
                    }
                }else{
                    $this->Help->message_empty("ATENÇÃO: Arquivo enviado não é CSV, TXT ou XLSX."); 
                }
            }
            
//            pre($this->data);
            if($this->PontoDobro->save($this->request->data)){
                $campanha_id = $this->PontoDobro->id;
                
                if(isset($this->data['PD']) && count($this->data['PD']) > 0){
                    $truncate = $this->PontoDobroProduto->query('DELETE FROM tb_ponto_dobro_produtos WHERE campanha_id = "'.$campanha_id.'"; ');
                            
                    foreach ($this->data['PD'] as $key => $codigo) {
                        if(trim($codigo) != ''){
                            $savepdp['PontoDobroProduto'] = array('id'=>'', 'campanha_id'=>$campanha_id, 'codigo'=>$codigo);
                            $this->PontoDobroProduto->save($savepdp);
                        }
                    }
                }
                
                if(isset($arr_codigos) && count($arr_codigos) > 0){
                    foreach ($arr_codigos as $key => $codigo) {
                        $savepdp['PontoDobroProduto'] = array('id'=>'', 'campanha_id'=>$campanha_id, 'codigo'=>$codigo);
                        $this->PontoDobroProduto->save($savepdp);
                    }
                }
            
                $this->redirect(array('action'=>'index'));
            }
        }
    }

    public function admin_edit($id){
        $this->layout = 'Painel.admin';
        $this->view = 'admin_editor';
        
        $registro = $this->PontoDobro->find('first',array('conditions'=>array('PontoDobro.id'=>$id)));
        
        if($registro['PontoDobro']['data_inicial']){
            $registro['PontoDobro']['data_inicial'] = $this->Help->show_mask_date($registro['PontoDobro']['data_inicial']);
        }
        
        if($registro['PontoDobro']['data_final']){
            $registro['PontoDobro']['data_final'] = $this->Help->show_mask_date($registro['PontoDobro']['data_final']);
        }
        
        $pd_codigos = $this->PontoDobroProduto->find('all',array(
            'conditions' => array('PontoDobroProduto.campanha_id' => $registro['PontoDobro']['id'])
        ));
        
        $this->data = $registro; 
        $this->set('pd_codigos',$pd_codigos);
    }
    
    public function admin_delete($id){
        $this->autoRender = false;
        if($this->PontoDobro->delete($id)){
            $this->redirect(array('action'=>'index'));
        }
    }   
    
//    public function teste_np(){
//        $this->autoRender = false;
//        
//        $notas = $this->Nota->find('all',array(
//            'fields'     => array('nf_numero','data_emissao','data_e','valor_nf'),
//            'conditions' => array('Nota.data_e >= "2022-02-07"', 'Nota.cancelada = 0')
//        ));
//        
//        $countline = 1;
//        $arr_notas_off = array();
//        
//        foreach ($notas as $nota) {
////            pre($nota);
//            $total_np = $this->NotaProduto->query("SELECT codigo_item, ROUND(SUM(valor_item), 2) AS TOTAL FROM tb_notas_produtos WHERE nf_numero = '".$nota['Nota']['nf_numero']."'; ");
//            
//            if($total_np[0][0]['TOTAL']){
//                $valor_nota = $nota['Nota']['valor_nf'];
//                $valor_np = $total_np[0][0]['TOTAL'];
//
//                $dif = '';
//                if($valor_nota != $valor_np){
//                    $dif = ' &nbsp; - &nbsp<span style="color:red;">'.number_format($valor_nota - $valor_np, 2, ',', '.').'</span>';
//                }
//                
//                $codigos = '';
//                foreach ($total_np as $np) {
//                    $codigos .= 'Cod: '.$np['tb_notas_produtos']['codigo_item'].' | ';
//                }
//                
//                echo '&nbsp;&nbsp; '.$countline.' - '.$nota['Nota']['nf_numero'].' &nbsp; | &nbsp; '.number_format($valor_nota, 2, ',', '.').' - '.number_format($valor_np, 2, ',', '.').$dif.' --------------- '.$codigos;
//                echo '<br><br>';
//                $countline++;
//            }else{
//                $arr_notas_off[] = $nota;
//            }
//        }
//        
//        echo '<br> '.count($arr_notas_off);
//    }
    
    public function teste_np(){
        $this->autoRender = false;
        
        $notas = $this->Nota->find('all',array(
            'fields'     => array('nf_numero','data_emissao','data_e','valor_nf'),
            'conditions' => array('Nota.data_e >= "2022-02-07"', 'Nota.cancelada = 0')
        ));
        
        $countline = 1;
        $arr_notas_off = array();
        
        foreach ($notas as $nota) {
//            pre($nota);
//            $total_np = $this->NotaProduto->query("SELECT codigo_item, ROUND(SUM(valor_item), 2) AS TOTAL FROM tb_notas_produtos WHERE nf_numero = '".$nota['Nota']['nf_numero']."'; ");
            $nprodutos = $this->NotaProduto->find('all',array(
                'fields'     => array('nf_numero','codigo_item','valor_item'),
                'conditions' => array('NotaProduto.nf_numero' => $nota['Nota']['nf_numero'])
            ));
            
            $total_pnotas = 0;
            $codigos = '';
            foreach ($nprodutos as $np) {
//                pre($np);
                $total_pnotas = $total_pnotas + $np['NotaProduto']['valor_item'];
                $codigos .= '<span style="font-size:13px;">'.$np['NotaProduto']['codigo_item'].'</span> - '.number_format($np['NotaProduto']['valor_item'], 2, ',', '.').' | ';
            }
            $codigos = rtrim($codigos, '| ');
            
            if($total_pnotas){
                $valor_nota = $nota['Nota']['valor_nf'];
                $valor_np = $total_pnotas;

                $dif = '';
                if($valor_nota != $valor_np){
                    $dif = ' &nbsp; - &nbsp<span style="color:red;">'.number_format($valor_nota - $valor_np, 2, ',', '.').'</span>';
                }
                
                echo '&nbsp;&nbsp; '.$countline.' - '.$nota['Nota']['nf_numero'].' &nbsp; | &nbsp; '.number_format($valor_nota, 2, ',', '.').' - '.number_format($valor_np, 2, ',', '.').$dif.' ------------------------- '.$codigos;
                echo '<br><br>';
                $countline++;
            }else{
                $arr_notas_off[] = $nota;
            }
        }
        
        echo '<br> '.count($arr_notas_off);
    }
    
    public function salvar_ponto_dobro(){
        $this->autoRender = false;

        $campanhas = $this->PontoDobro->find('all',array(
            'conditions' => array('PontoDobro.data_inicial <= "'.date('Y-m-d').'"', 'PontoDobro.data_final >= "'.date('Y-m-d').'"', 'PontoDobro.ativo = "1"')
//            'conditions' => array('PontoDobro.data_inicial <= DATE_FORMAT(NOW(),"%Y%m%d") ', 'PontoDobro.ativo = "1"')
        ));
//        pre($campanhas);
        
        foreach ($campanhas as $campanha) {
            if(count($campanha['PontoDobroProduto']) > 0){
                foreach ($campanha['PontoDobroProduto'] as $produto) {
                    
                    $nprodutos = $this->NotaProduto->find('all',array(
//                        'fields'     => array('nf_numero','codigo_item','valor_item'),
                        'conditions' => array('NotaProduto.codigo_item' => $produto['codigo'])
                    ));
                    
                    foreach ($nprodutos as $np) {
                        $nota = $this->Nota->find('first',array(
                            'conditions' => array(
                                'Nota.cliente_cnpj' => $np['NotaProduto']['cliente_cnpj'], 
                                'Nota.nf_numero'    => $np['NotaProduto']['nf_numero']
                            )
                        ));
                        
                        if($nota){
                            $count_save_np = $this->PontoDobroNpSalvo->find('count',array(
                                'conditions' => array(
                                    'PontoDobroNpSalvo.campanha_id'  => $campanha['PontoDobro']['id'],
                                    'PontoDobroNpSalvo.nota_id'      => $nota['Nota']['id'],
                                    'PontoDobroNpSalvo.cliente_cnpj' => $nota['Nota']['cliente_cnpj'],
                                    'PontoDobroNpSalvo.codigo_item'  => $np['NotaProduto']['codigo_item'],
                                )
                            ));
                            
                            $data_nota     = $nota['Nota']['data_e'];
                            $data_inicio_c = $campanha['PontoDobro']['data_inicial'];
                            $data_final_c  = $campanha['PontoDobro']['data_final'];
                            
                            if($count_save_np === 0 && strtotime($data_nota) >= strtotime($data_inicio_c) && strtotime($data_nota) <= strtotime($data_final_c)){
                                $novo_valor_nota = $nota['Nota']['valor_nf'] + $np['NotaProduto']['valor_item'];

                                $arr_novo_valor_nota['Nota'] = array(
                                    'id'       => $nota['Nota']['id'],
                                    'valor_nf' => $novo_valor_nota
                                );

                                $arr_np_dobro['PontoDobroNpSalvo'] = array(
                                    'id'           => '',
                                    'campanha_id'  => $campanha['PontoDobro']['id'],
                                    'nota_id'      => $nota['Nota']['id'],
                                    'nf_numero'    => $nota['Nota']['nf_numero'],
                                    'cliente_cnpj' => $nota['Nota']['cliente_cnpj'],
                                    'codigo_item'  => $np['NotaProduto']['codigo_item'],
                                    'valor_item'   => $np['NotaProduto']['valor_item'],
                                );

                                if($this->Nota->save($arr_novo_valor_nota) && $this->PontoDobroNpSalvo->save($arr_np_dobro)){
                                    echo 'salvo | ';
                                }
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    
    public function remover_ponto_dobro(){
        $this->autoRender = false;
        
        $notas_produtos = $this->NotaProduto->find('all',array(
            'conditions' => array('NotaProduto.natureza' => 'DV02')
        ));
        
        foreach ($notas_produtos as $np) {
            $ponto_dobro_salvo = $this->PontoDobroNpSalvo->find('first',array(
                'conditions' => array(
                    'PontoDobroNpSalvo.nf_numero'   => $np['NotaProduto']['nf_numero'],
                    'PontoDobroNpSalvo.codigo_item' => $np['NotaProduto']['codigo_item'],
                    'PontoDobroNpSalvo.ativo'       => '1',
                )
            ));
            
            if($ponto_dobro_salvo){
//                pre($ponto_dobro_salvo);
                
                $nota = $this->Nota->find('first',array('conditions'=>array('Nota.id'=>$ponto_dobro_salvo['PontoDobroNpSalvo']['nota_id'])));
                
                $diminui_valor = $nota['Nota']['valor_nf'] - $ponto_dobro_salvo['PontoDobroNpSalvo']['valor_item'];
                
                $save_nova['Nota'] = array(
                    'id'       => $ponto_dobro_salvo['PontoDobroNpSalvo']['nota_id'],
                    'valor_nf' => $diminui_valor
                );
                
                $update_pn_salvo['PontoDobroNpSalvo'] = array(
                    'id'    => $ponto_dobro_salvo['PontoDobroNpSalvo']['id'],
                    'ativo' => '0',
                );
                
                if($this->Nota->save($save_nova) && $this->PontoDobroNpSalvo->save($update_pn_salvo)){
                    echo 'Atualizado | ';
                }
            }
        }
        
    }
    
    
}