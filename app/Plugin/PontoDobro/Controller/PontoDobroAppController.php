<?PHP
class PontoDobroAppController extends AppController {
    
    public $uses = array(
        'PontoDobro.PontoDobro',
        'PontoDobro.PontoDobroProduto',
        'PontoDobro.PontoDobroNpSalvo',
        'PontoDobro.Peca',
        'PontoDobro.TextoPd',
        'Notas.Nota',
        'Notas.NotaProduto'
    );
    
}