<fieldset class="box">
    <legend>Produtos/Peças ponto em dobro</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar',array('action'=>'add')); ?>
    </nav>    
    <table class="tables">
        <thead>
            <tr>
                <td class="thumb"><a>Imagem</a></td>
                <td><?PHP echo $this->Paginator->sort('title','Título')?></td>
                <td><?PHP echo $this->Paginator->sort('ativo','Ativo')?></td>
                <td class="edit">&nbsp;</td>
                <td class="delete">&nbsp;</td>
            </tr>
        </thead>
        <tbody class="sortable">
            <?PHP foreach($posts as $post){ ?>
                <tr data-id="<?PHP echo $post['Peca']['id']; ?>">
                    <td><?PHP if($post['Peca']['imagem']) echo $this->Fill->image($post['Peca']['imagem'],120,80,'ffffff'); ?></td>
                    <td><?PHP echo $post['Peca']['title']; ?></td>
                    <td><?PHP echo $this->Html->image('ativo_'.$post['Peca']['ativo'].'.png',array('alt'=>'')); ?></td>
                    <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['Peca']['id']))?></td>
                    <td><?PHP echo $this->Html->link('Excluir',array('action'=>'delete',$post['Peca']['id']),false,'Tem certeza?')?></td>
                </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>

<script>
    var base='<?= $this->base ?>';
    jQuery(function($){
        $('.sortable').sortable({
            stop:function(evt,ui){
                $('.sortable>tr').each(function(i,e){
                    var id    = $(e).attr('data-id');
                    var index = $(e).index();
                    $.getJSON(base+'/admin/ponto_dobro/pecas/order/'+id+'/'+index,function(data){
                    });
                });
            }
        });
    });
</script>