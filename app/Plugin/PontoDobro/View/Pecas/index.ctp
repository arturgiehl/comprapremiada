<div class="bg-gray bgpb">
    <div class="row bg-bread">
        <div class="container-fix">
            <?PHP
                $this->Html->addCrumb('Inicial','/'); 
                $this->Html->addCrumb('Ponto em Dobro',array('plugin'=>'ponto_dobro','controller'=>'pecas','action'=>'index')); 
                echo $this->element('breadcrumb');

                echo '<h1 class="h1p">Ponto em Dobro</h1>';
            ?>
        </div>
    </div>
    
    <section class="container-fix content-ponto-dobro">
        <?PHP   
            if($texto_pd['TextoPd']['texto']){
                echo '<div class="row texto">';
                    echo $texto_pd['TextoPd']['texto'];
                echo '</div>';
            }
            
            if(count($posts) > 0){
                foreach ($posts as $peca) {
                    
                    $image = '';
                    if($peca['Peca']['imagem']){
                        if(file_exists('img/media_cache/fill/300x220/ffffff/'.$peca['Peca']['imagem'].'')){
                            $image = $this->Html->image('media_cache/fill/300x220/ffffff/'.$peca['Peca']['imagem'].'',array('alt'=>$peca['Peca']['title']));
                        }else{
                            $image = $this->Fill->image($peca['Peca']['imagem'],300,220,'ffffff',array('alt'=>$peca['Peca']['title']));
                        }
                    }
                        
                    echo '<div class="row list-pd">';
                        if($image){
                            echo '<div class="three columns imgpd">';
                                echo $this->Html->link($image,'/'.$peca['Peca']['imagem'],array('escape'=>false,'class'=>'img-pd','title'=>$peca['Peca']['title']));
                            echo '</div>';

                            echo '<div class="nine columns info-right">';
                                echo '<h2>'.$peca['Peca']['title'].'</h2>';
                                echo '<div class="row texto-pd">';
                                    echo $peca['Peca']['texto'];
                                echo '</div>';
                            echo '</div>';
                        }else{
                            echo '<h2>'.$peca['Peca']['title'].'</h2>';
                            echo '<div class="row texto-pd">';
                                echo $peca['Peca']['texto'];
                            echo '</div>';
                        }
                    echo '</div>';
                }
                
                echo $this->element('Painel.paginator'); 
            }else{
                echo '<div class="row texto"> <p><b>Nenhum registro encontrado.</b></p> </div>';
            }
        ?>
    </section>
</div>
