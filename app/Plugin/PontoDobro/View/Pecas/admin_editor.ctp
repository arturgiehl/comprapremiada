<?PHP
echo $this->Form->create('Peca',array('url'=>array('action'=>'add')));
?>

<fieldset class="box">
    <legend>Produto/Peça em dobro</legend>
    <?PHP
        echo $this->Form->input('title',array('label'=>'Título'));
        echo $this->Form->input('texto',array('label'=>'Descrição','class'=>'ckeditor'));
    ?>
</fieldset>

<fieldset class="box" id="box_categoria">
    <legend>Ativo:</legend>
    <?PHP 
        $ativo = array('1'=>'Sim', '0'=>'Não');
        echo $this->Form->input('ativo',array('type'=>'select','options'=>$ativo,'label'=>false)); 
    ?>
</fieldset>

<?PHP
echo $this->element('Painel.image',array('label'=>'Imagem','name'=>'imagem'));
//echo $this->element('Painel.gallery',array('label'=>'Imagens'));
//echo $this->element('Painel.videos',array('label'=>'Vídeos'));
echo $this->Form->hidden('id');
echo $this->Form->end('Enviar');
?>