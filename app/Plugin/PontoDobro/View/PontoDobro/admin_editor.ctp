<script src="<?PHP echo $this->Html->url('/datepicker/jquery-ui-1.9.0.js',true); ?>"></script>
<link rel="stylesheet" href="<?PHP echo $this->base ?>/datepicker/jquery-ui-1.9.0.css">

<style>
    .content-codes {
        width: 100%;
        max-width: 280px;
        background-color: #FFF;
        padding: 20px;
        border: 1px solid #ccc;
        border-radius: 10px;
    }
    .content-codes p {
        margin-bottom: 15px;
        text-align: center;
    }
    .line-inpt {
        margin-bottom: 10px;
    }
    .line-inpt input {
        width: 100%;
        height: 30px;
    }
    .button-add {
        margin: 0 auto;
        color: #FFF;
        text-align: center;
        font-weight: bold;
        font-size: 14px;
        line-height: 14px;
        padding: 10px 20px 10px 20px;
        border-radius: 5px;
        cursor: pointer;
        margin-top: 20px;
        display: inline-block;
        background-color: #0065CA;
        /*box-shadow: 0px 16px 40px -6px rgb(0 28 72 / 15%);*/
    }
    .import-file {
        width: 100%;
        max-width: 705px;
        background-color: #FFF;
        padding: 20px;
        border: 1px solid #ccc;
        margin-top: 25px;
        border-radius: 10px;
    }
    .import-file label {
        margin-top: 0px;
        margin-bottom: 15px;
        font-size: 14px;
    }
</style>
<?PHP

echo $this->Form->create('PontoDobro',array('type'=>'file','url'=>array('plugin'=>'ponto_dobro','controller'=>'ponto_dobro','action'=>'add')));

    $ativo = array('1'=>'Sim', '0'=>'Não');
?>
<fieldset class="box">
    <legend>Nova campanha</legend>
    <?PHP
        echo $this->Form->input('title',array('label'=>'Título'));
        
        echo $this->Form->input('data_inicial',array('label'=>'Data Inicial','maxlength'=>10,'type'=>'text','onkeyup'=>'mascara(this,mdata);','placeholder'=>'99/99/9999','class'=>'datepick'));
        echo $this->Form->input('data_final',array('label'=>'Data Final','maxlength'=>10,'type'=>'text','onkeyup'=>'mascara(this,mdata);','placeholder'=>'99/99/9999','class'=>'datepick'));
        echo $this->Form->input('ativo',array('type'=>'select','options'=>$ativo,'label'=>'Ativo')); 
    ?>
</fieldset>

<div class="content-codes">
    <p>Para remover um código deixe o campo em branco</p>
    <div class="load_codes" id="load_codes">
        <?PHP
            if(isset($this->data['PontoDobro']['id']) && isset($pd_codigos) && count($pd_codigos) > 0){
                foreach ($pd_codigos as $codigo) {
                    echo '<div class="line-inpt"> <input name="data[PD][]" placeholder="Código do produto" type="text" value="'.$codigo['PontoDobroProduto']['codigo'].'"> </div>';
                }
            }else{
                for ($index = 0; $index < 5; $index++) {
                    echo '<div class="line-inpt"> <input name="data[PD][]" placeholder="Código do produto" type="text"> </div>';
                }
            }
        ?>
    </div>

    <div class="button-add">
        Adicionar mais códigos
    </div>
</div>

<div class="import-file">
    <?PHP
        echo '<div class="input-wrapper">';
            echo '<label for="input-file">Importar arquivo CSV ou TXT (um código por linha)</label>';
            echo '<input id="input-file" type="file" name="data[Arquivo]" value="" />';
            echo '<span id="file-name"></span>';
        echo '</div>';
    ?>
</div>

<?PHP
    echo $this->Form->hidden('id');
echo $this->Form->end('Salvar');
?>

<script>
    var base = '<?= $this->base ?>';
    
    $(document).ready(function(){
        $('.button-add').click(function(){
            $('#load_codes').append('<div class="line-inpt"> <input name="data[PD][]" placeholder="Código do produto" type="text"> </div>');
        });
    });

    function SomenteNumero(e){
        var tecla=(window.event)?event.keyCode:e.which;   
        if((tecla>47 && tecla<58)) return true;
        else{
            if (tecla==8 || tecla==0) return true;
            else  return false;
        }
    }

    function mascara(o,f){
        v_obj = o;
        v_fun = f;
        setTimeout("execmascara()",1);
    }

    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mdata(v){
        v=v.replace(/\D/g,"")
        v=v.replace(/(\d{2})(\d)/,"$1/$2")
        v=v.replace(/(\d{2})(\d)/,"$1/$2")
        return v;
    }
    
    
// ------------------------------------------------ datepicker  ------------------------------------------------
    var matched, browser;
    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
    }

    // Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
        browser.webkit = true;
    } else if ( browser.webkit ) {
        browser.safari = true;
    }

    jQuery.browser = browser;

    $(function() {
        $('.datepick').datepicker({
            dateFormat: 'dd/mm/yy',
//            minDate: 0,
        });

        $.datepicker.regional['pt'] = {
            closeText: 'Fechar',
            prevText: 'Anterior',
            nextText: 'Seguinte',
            currentText: 'Hoje',
            monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
            'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
            'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'S&aacute;bado'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'S&aacute;b'],
            dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'S&aacute;b'],
            weekHeader: 'Sem',
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['pt']);

    });
// ------------------------------------------------ datepicker  ------------------------------------------------
</script>
