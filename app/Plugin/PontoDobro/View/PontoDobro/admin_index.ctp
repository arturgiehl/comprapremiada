
<fieldset class="box">
    <legend>Campanhas ponto em dobro</legend>
    <nav class="pannel">
        <?PHP echo $this->Html->link('Adicionar',array('action'=>'add')); ?>
    </nav>    
    
    <table class="tables">
        <thead>
            <tr>
                <td><?PHP echo $this->Paginator->sort('title','Título')?></td>
                <td><?PHP echo $this->Paginator->sort('data_inicial','Data Inicial')?></td>
                <td><?PHP echo $this->Paginator->sort('data_final','Data Final')?></td>
                <td><?PHP echo $this->Paginator->sort('ativo','Ativo')?></td>
                <td class="edit">&nbsp;</td>
                <!--<td class="delete">&nbsp;</td>-->
            </tr>
        </thead>
        <tbody>
            <?PHP 
                $destaque = array('0'=>'Não', '1'=>'Sim');
                foreach($posts as $post){ 
            ?>
            <tr>
                <td><?PHP echo $post['PontoDobro']['title']?></td>
                <td><?PHP echo $post['PontoDobro']['ds_inicial']?></td>
                <td><?PHP echo $post['PontoDobro']['ds_final']?></td>
                <td><?PHP echo $this->Html->image('ativo_'.$post['PontoDobro']['ativo'].'.png',array('alt'=>'')); ?></td>
                <td><?PHP echo $this->Html->link('Editar',array('action'=>'edit',$post['PontoDobro']['id']))?></td>
                <!--<td><?PHP // echo $this->Html->link('Excluir',array('action'=>'delete',$post['Produto']['id']),false,'Tem certeza?')?></td>-->
            </tr>
            <?PHP } ?>
        </tbody>
    </table>
    <?PHP echo $this->element('Painel.paginator');?>
</fieldset>