<?PHP
class PontoDobroProduto extends PontoDobroAppModel {
    
    var $name = 'PontoDobroProduto';
    var $useTable = 'ponto_dobro_produtos';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(PontoDobroProduto.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(PontoDobroProduto.modified,'%d/%m/%Y %H:%i')",
    );
    
}