<?PHP
class Peca extends PontoDobroAppModel {
    
    var $useTable = 'pecas_ponto_dobro';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(Peca.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(Peca.modified,'%d/%m/%Y %H:%i')",
    );
    
    public $actsAs = array(
//        'Painel.Gallery',
//        'Painel.Videos',
        'Painel.Slug' => array('title'=>'slug'),
    );
    
}