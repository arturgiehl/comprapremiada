<?PHP
class TextoPd extends PontoDobroAppModel {
    
    var $useTable = 'texto_ponto_dobro';
    
    public $virtualFields = array(
        'cdate'=>"DATE_FORMAT(TextoPd.created,'%d/%m/%Y %H:%i')",
        'mdate'=>"DATE_FORMAT(TextoPd.modified,'%d/%m/%Y %H:%i')",
    );
    
//    public $actsAs = array(
//        'Painel.Gallery',
//        'Painel.Videos'
//    );
}