<?PHP
class PontoDobro extends PontoDobroAppModel {
    
    var $name = 'PontoDobro';
    var $useTable = 'ponto_dobro_campanha';
    
    public $virtualFields = array(
        'cdate'      => "DATE_FORMAT(PontoDobro.created,'%d/%m/%Y %H:%i')",
        'mdate'      => "DATE_FORMAT(PontoDobro.modified,'%d/%m/%Y %H:%i')",
        'ds_inicial' => "DATE_FORMAT(PontoDobro.data_inicial,'%d/%m/%Y')",
        'ds_final'   => "DATE_FORMAT(PontoDobro.data_final,'%d/%m/%Y')",
    );
    
    public $validate = array(
        'title'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'data_inicial'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
        'data_final'=>array('rule'=>'notBlank','message'=>'Não deixe este campo em branco'),
    );
    
    public $actsAs = array(
        'Painel.Slug'=>array('title'=>'slug')
    );
    
    public $hasMany = array(
        'PontoDobroProduto'=>array(
            'className'  => 'PontoDobro.PontoDobroProduto',
            'foreignKey' => 'campanha_id'
        )
    );
    
}