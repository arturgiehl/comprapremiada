<?php
Router::connect('', array('controller'=>'pages','action'=>'home'));
Router::connect('/', array('controller'=>'pages', 'action'=>'home'));
Router::connect('/pagina-indisponivel', array('controller'=>'pages', 'action'=>'notfound'));

Router::connect('/cookies', array('controller'=>'pages', 'action'=>'popup_cookies'));
Router::connect('/loadga', array('controller'=>'pages', 'action'=>'load_ga'));

####################################################################
CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';