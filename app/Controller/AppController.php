<?PHP
App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $components = array(
        'Painel.Locker',
        'Seo.Seo',
        'Session',
    );
    
    public $helpers = array(
        'Media.Crop',
        'Media.Fill',
        'Less.Less',
        'Session',
    );
    
    public $uses = array(
        'Clientes.Cliente',
        'Clientes.PedidoCliente',
        'Clientes.PedidoClienteProduto',
        'Notas.Nota',
        'Produtos.Orcamento',
        'Conces.Conce'
    );
     
    public function beforeFilter() {
        parent::beforeFilter();
        
        $cpontos = 0;
        $valor_vencer = 0;
        $total_vencidos = 0;
        $pontos_usados = 0;
        $data_vencer = '';
        $pontos_cliente = array();
        
        if($this->Session->check('Cliente')){
            $data_cliente = $this->Session->read('Cliente');
            
            if(isset($data_cliente['termos']) && $data_cliente['termos'] === '1'){
//                $cpontos = $this->get_ponto_cliente_disponivel($data_cliente['cnpj']);
                
                $pontos_cliente = $this->get_ponto_cliente_disponivel_expirado($data_cliente['cnpj']);
            }else{
//                pre($this->params['plugin']);
                if($this->params['plugin'] == 'regulamento' && $this->params['controller'] == 'regulamento' && $this->params['action'] == 'termo_uso'){
             
                }else{
                    $this->layout = 'atualizar';
                    $select_concessionaria[''] = 'Selecionar';
                    $select_concessionaria[] = $this->Conce->find('list',array('fields'=>array('id','title'),'order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC')));
                    $this->set('select_concessionaria',$select_concessionaria);
                    $this->set('data_cliente',$data_cliente);
                }
            }
        }
        
//        $this->set('cpontos',$cpontos);
        $this->set('pontos_cliente',$pontos_cliente);
        $this->set('valor_vencer',$valor_vencer);
        $this->set('data_vencer',$data_vencer);
        $this->set('total_vencidos',$total_vencidos);
        $this->set('pontos_usados',$pontos_usados);
        $this->set('concessionarias',$this->Conce->find('all',array('order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC'))));
    }

    public function get_ponto_cliente_disponivel($cnpj){
        $pedidos_antes = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));
        
        $pedidos_ativos = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));

        $notas_antes = $this->Nota->find('all',array(
            'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));
        
        $notas_atuais = $this->Nota->find('all',array(
            'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));
        
        $total_nota_antes = 0;
        foreach ($notas_antes as $nota_a) {
            $total_nota_antes = $total_nota_antes + $nota_a['Nota']['valor_nf'];
        }

        $total_nota_atuais = 0;
        foreach ($notas_atuais as $nota_a) {
            $total_nota_atuais = $total_nota_atuais + $nota_a['Nota']['valor_nf'];
        }

        $total_trocas_antes6 = 0;
        foreach ($pedidos_antes as $pedido_antes) {
//            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_antes['PedidoCliente']['sessao_id']));
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_antes['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_trocas_antes6 = $total_trocas_antes6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        
        $total_dentro6 = 0;
        foreach ($pedidos_ativos as $pedido_periodo) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_periodo['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        
        $expirado = 0;
        if(($total_trocas_antes6) > 0){
            $saldo = $total_nota_antes - $total_trocas_antes6;
        }
        else if(($total_trocas_antes6 + $total_dentro6) < $total_nota_atuais){
            $saldo = $total_nota_antes;
//            pre('aa');
        }
        else{
            $saldo = $total_nota_antes - ($total_trocas_antes6 + $total_dentro6);
//            pre('bb');
        }
//        pre($saldo);
//        pre($expirado);
        if($saldo < 0){
            $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);
        }else{
            $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6) - $saldo;
        }
    
        return $disponivel;
    }
    
    public function get_pontuacao_disponivel(){
        $total_pedidos_ativos = 0;
        $pedidos_ativos = $this->PedidoCliente->find('all',array(
            'fields' => array('id','sessao_id'),
            'conditions' => array('PedidoCliente.cnpj = "'.$this->Session->read('Cliente.cnpj').'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));
        foreach ($pedidos_ativos as $pedido_ativo) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_ativo['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_pedidos_ativos = $total_pedidos_ativos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$this->Session->read('Cliente.cnpj')."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
        $total_disponivel = $total[0][0]['TOTAL'] - $total_pedidos_ativos;
        return $total_disponivel;
    }


    public function afterFilter() {
        if($this->response->statusCode() == '404'){
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'notfound')); 
        }
    }

    public function get_pontos_usados($cliente_id = null) {
        $pedidos = $this->Orcamento->find('all',array(
            'conditions' => array('Orcamento.cliente_id = "'.$cliente_id.'"', "DATE_FORMAT(Orcamento.created,'%Y%m%d') >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;")
        ));
//        pre($pedidos);
        
        $total_pontos = 0;
        foreach ($pedidos as $pedido) {
            $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        Produto.preco,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$pedido['Orcamento']['sessao_id'].'"');
            foreach ($produtos_session as $ps) {
                $total_pontos = $total_pontos + ($ps['Produto']['preco'] * $ps['ProdutoSessao']['quantidade']);
            }
        }
        return $total_pontos;
    }
    
    public function get_ponto_cliente_disponivel_expirado($cnpj){
        $arr_notas_usadas = array();
        $arr_saldos_restantes = array();
        $arr_notas_expiradas = array();
        
        $data_ultima_nota = '';
        $total_pontos_geral = 0;
        $saldo_total_notas = 0;
        $saldo_total_usado = 0;
        $total_disponivel = 0;
        
        $pedidos_geral = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"'),
            'order'      => array('PedidoCliente.created' => 'ASC'),
        ));

        foreach ($pedidos_geral as $pa) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'conditions' => array('PedidoClienteProduto.session_id'=>$pa['PedidoCliente']['sessao_id'])
            ));
            
            $total_qtd = 0;
            $total_pontos = 0;
            foreach ($produtos_pedido as $ps) {
                $total_pontos = $total_pontos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
                $total_qtd = $total_qtd + $ps['PedidoClienteProduto']['quantidade'];
            }
            
            $total_pontos_geral = $total_pontos_geral + $total_pontos;
            $expl_date = explode(' ', $pa['PedidoCliente']['created']);
            
            $notas_anteriores_pedido = $this->Nota->find('all',array(
                'fields' => array('id','valor_nf','data_e',"DATE_FORMAT(data_e + interval 6 month,'%Y-%m-%d') AS data_expiracao"),
                'conditions' => array(
                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
                        "DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$expl_date[0]."','%Y%m%d')", 
                        'Nota.cancelada = 0'
                ),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
            
            $total_notas = 0;
            $notas_usadas_pedido = 0;
            $ponto_restante = 0;
            
            foreach ($notas_anteriores_pedido as $nota) {
                if(!in_array($nota['Nota']['id'], $arr_notas_usadas)) { 
                    $arr_notas_usadas[] = $nota['Nota']['id'];

                    $firstDate  = new DateTime($expl_date[0]);
                    $secondDate = new DateTime($nota['Nota']['data_e']);
                    $intvl = $firstDate->diff($secondDate);

                    if($intvl->m < 6){
                        $ponto_restante = 0;
                        $notas_usadas_pedido = $notas_usadas_pedido + $nota['Nota']['valor_nf'];
                        if($notas_usadas_pedido > $total_pontos && !$arr_saldos_restantes){ 
                            $ponto_restante = $notas_usadas_pedido - $total_pontos;
                            $arr_saldos_restantes[$nota['Nota']['id']] = array(
                                'id_nota'          => $nota['Nota']['id'],
                                'valor_total_nota' => $nota['Nota']['valor_nf'],
                                'valor_restante'   => $ponto_restante,
                                'data_e'           => $nota['Nota']['data_e'],
                                'data_expiracao'   => $nota[0]['data_expiracao']
                            );
                        }

                        $total_notas = $total_notas + $nota['Nota']['valor_nf'];
                        $data_ultima_nota = $nota['Nota']['data_e'];
                    }else{
                        $arr_notas_expiradas[] = array(
                            'id_nota'        => $nota['Nota']['id'],
                            'data_emissao'   => $nota['Nota']['data_e'],
                            'data_expiracao' => $nota[0]['data_expiracao'],
                            'valor_nota'     => $nota['Nota']['valor_nf'],
                            'valor_expirado' => $nota['Nota']['valor_nf'],
                        );
                    }
                }
            }
            
            if($arr_saldos_restantes && $total_notas < $total_pontos){
                foreach ($arr_saldos_restantes as $restante) {
                    $firstDate  = new DateTime($expl_date[0]);
                    $secondDate = new DateTime($restante['data_e']);
                    $intvl = $firstDate->diff($secondDate);
                    if($intvl->m < 6){
                        $saldo_restante = ($total_notas + $restante['valor_restante']) - $total_pontos;
                        $arr_saldos_restantes[$restante['id_nota']] = array(
                            'id_nota'          => $restante['id_nota'],
                            'valor_total_nota' => $restante['valor_total_nota'],
                            'valor_restante'   => $saldo_restante,
                            'data_e'           => $restante['data_e'],
                            'data_expiracao'   => $restante['data_expiracao'],
                        );
                    }
                }
            }
            
            $saldo_total_notas = $saldo_total_notas + ($total_notas - $total_pontos_geral); 
            $saldo_total_usado = $saldo_total_usado + $total_pontos;
            $total_disponivel = $total_disponivel + ($total_notas - $total_pontos); 
        }
        
        if($data_ultima_nota){
            $proximas_notas = $this->Nota->find('all',array(
                'fields' => array('id','valor_nf','data_emissao','data_e'),
                'conditions' => array(
                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
                        "DATE_FORMAT(data_e,'%Y%m%d') > DATE_FORMAT('".$data_ultima_nota."','%Y%m%d')", 
                        'Nota.cancelada = 0'
                ),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
        }else{
            $proximas_notas = $this->Nota->find('all',array(
                'fields' => array('id','valor_nf','data_e','data_emissao',"DATE_FORMAT(data_e + interval 6 month,'%Y-%m-%d') AS data_expiracao"),
                'conditions' => array(
                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
                        'Nota.cancelada = 0'
                ),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
        }
        
        $total_proximas_notas = 0;
        foreach ($proximas_notas as $nota) {
            $firstDate  = new DateTime($nota['Nota']['data_e']);
            $secondDate = new DateTime(date('Y-m-d'));
            $intvl = $firstDate->diff($secondDate);
            if($intvl->m < 6){
                $total_proximas_notas = $total_proximas_notas + $nota['Nota']['valor_nf'];
            }else{
                $data_expiracao = '';
                if(isset($nota[0]['data_expiracao'])){
                    $data_expiracao = $nota[0]['data_expiracao'];
                }
        
                $arr_notas_expiradas[] = array(
                    'id_nota'        => $nota['Nota']['id'],
                    'data_emissao'   => $nota['Nota']['data_e'],
                    'data_expiracao' => $data_expiracao,
                    'valor_nota'     => $nota['Nota']['valor_nf'],
                    'valor_expirado' => $nota['Nota']['valor_nf'],
                );
            }
        }
        
        
        $total_usado_expirado = 0;
        if($arr_saldos_restantes){
            foreach ($arr_saldos_restantes as $restante) {
                $firstDate  = new DateTime(date('Y-m-d'));
                $secondDate = new DateTime($restante['data_e']);
                $intvl = $firstDate->diff($secondDate);
                if($intvl->m >= 6){
                    $total_usado_expirado = $total_usado_expirado + $restante['valor_restante'];
                }
            }
        }

        if($total_usado_expirado < 0){
            $total_usado_expirado = 0;
        }

        $total_pontos_expirados = 0;
        if($arr_notas_expiradas){
            foreach ($arr_notas_expiradas as $nota_expirada) {
                $total_pontos_expirados = $total_pontos_expirados + $nota_expirada['valor_expirado'];
            }
        }
        
        $disponivel_geral = $total_disponivel + $total_proximas_notas;
        $expirado = $total_usado_expirado + $total_pontos_expirados;
        $disponivel_para_troca = ($total_disponivel + $total_proximas_notas) - $total_usado_expirado;
        
        return array(
            'disponivel_geral' => $disponivel_geral,
            'expirado' => $expirado,
            'disponivel_para_troca' => $disponivel_para_troca
        );
    }
    
    
}