<?PHP
App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $components = array(
        'Painel.Locker',
        'Seo.Seo',
        'Session',
    );
    
    public $helpers = array(
        'Media.Crop',
        'Media.Fill',
        'Less.Less',
        'Session',
    );
    
    public $uses = array(
        'Clientes.Cliente',
        'Clientes.PedidoCliente',
        'Clientes.PedidoClienteProduto',
        'Notas.Nota',
        'Produtos.Orcamento',
        'Conces.Conce'
    );
     
    public function beforeFilter() {
        parent::beforeFilter();
        
        $cpontos = 0;
        $valor_vencer = 0;
        $total_vencidos = 0;
        $pontos_usados = 0;
        $data_vencer = '';
        $pontos_cliente = array();
        
        if($this->Session->check('Cliente')){
            $data_cliente = $this->Session->read('Cliente');
            
            if(isset($data_cliente['termos']) && $data_cliente['termos'] === '1'){
//                $cpontos = $this->get_ponto_cliente_disponivel($data_cliente['cnpj']);
                
                $pontos_cliente = $this->get_ponto_cliente_disponivel_expirado($data_cliente['cnpj']);
//                pre($pontos_cliente);
            }else{
//                pre($this->params['plugin']);
                if($this->params['plugin'] == 'regulamento' && $this->params['controller'] == 'regulamento' && $this->params['action'] == 'termo_uso'){
             
                }else{
                    $this->layout = 'atualizar';
                    $select_concessionaria[''] = 'Selecionar';
                    $select_concessionaria[] = $this->Conce->find('list',array('fields'=>array('id','title'),'order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC')));
                    $this->set('select_concessionaria',$select_concessionaria);
                    $this->set('data_cliente',$data_cliente);
                }
            }
        }
        
//        $this->set('cpontos',$cpontos);
        $this->set('pontos_cliente',$pontos_cliente);
        $this->set('valor_vencer',$valor_vencer);
        $this->set('data_vencer',$data_vencer);
        $this->set('total_vencidos',$total_vencidos);
        $this->set('pontos_usados',$pontos_usados);
        $this->set('concessionarias',$this->Conce->find('all',array('order'=>array('Conce.order_registro'=>'ASC', 'Conce.created'=>'DESC'))));
    }

    public function get_ponto_cliente_disponivel($cnpj){
        $pedidos_antes = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));
        
        $pedidos_ativos = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));

        $notas_antes = $this->Nota->find('all',array(
            'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));
        
        $notas_atuais = $this->Nota->find('all',array(
            'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));
        
        $total_nota_antes = 0;
        foreach ($notas_antes as $nota_a) {
            $total_nota_antes = $total_nota_antes + $nota_a['Nota']['valor_nf'];
        }

        $total_nota_atuais = 0;
        foreach ($notas_atuais as $nota_a) {
            $total_nota_atuais = $total_nota_atuais + $nota_a['Nota']['valor_nf'];
        }

        $total_trocas_antes6 = 0;
        foreach ($pedidos_antes as $pedido_antes) {
//            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_antes['PedidoCliente']['sessao_id']));
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_antes['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_trocas_antes6 = $total_trocas_antes6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        
        $total_dentro6 = 0;
        foreach ($pedidos_ativos as $pedido_periodo) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_periodo['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        
        $expirado = 0;
        if(($total_trocas_antes6) > 0){
            $saldo = $total_nota_antes - $total_trocas_antes6;
        }
        else if(($total_trocas_antes6 + $total_dentro6) < $total_nota_atuais){
            $saldo = $total_nota_antes;
//            pre('aa');
        }
        else{
            $saldo = $total_nota_antes - ($total_trocas_antes6 + $total_dentro6);
//            pre('bb');
        }
//        pre($saldo);
//        pre($expirado);
        if($saldo < 0){
            $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);
        }else{
            $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6) - $saldo;
        }
    
        return $disponivel;
    }
    
    public function get_pontuacao_disponivel(){
        $total_pedidos_ativos = 0;
        $pedidos_ativos = $this->PedidoCliente->find('all',array(
            'fields' => array('id','sessao_id'),
            'conditions' => array('PedidoCliente.cnpj = "'.$this->Session->read('Cliente.cnpj').'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));
        foreach ($pedidos_ativos as $pedido_ativo) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_ativo['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_pedidos_ativos = $total_pedidos_ativos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        $total = $this->Cliente->query("SELECT ROUND(SUM(valor_nf), 2) AS TOTAL FROM tb_notas_emitidas WHERE cliente_cnpj = '".$this->Session->read('Cliente.cnpj')."' AND cancelada = 0 AND data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;");
        $total_disponivel = $total[0][0]['TOTAL'] - $total_pedidos_ativos;
        return $total_disponivel;
    }


    public function afterFilter() {
        if($this->response->statusCode() == '404'){
            $this->redirect(array('plugin'=>false,'controller'=>'pages','action'=>'notfound')); 
        }
    }

    public function get_pontos_usados($cliente_id = null) {
        $pedidos = $this->Orcamento->find('all',array(
            'conditions' => array('Orcamento.cliente_id = "'.$cliente_id.'"', "DATE_FORMAT(Orcamento.created,'%Y%m%d') >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month;")
        ));
//        pre($pedidos);
        
        $total_pontos = 0;
        foreach ($pedidos as $pedido) {
            $produtos_session = $this->Orcamento->query('SELECT
                                                        Produto.id,
                                                        Produto.title,
                                                        Produto.imagem,
                                                        Produto.preco,
                                                        ProdutoSessao.id,
                                                        ProdutoSessao.quantidade
                                                    FROM tb_produtos AS Produto    
                                                        INNER JOIN tb_produto_session AS ProdutoSessao
                                                            ON ProdutoSessao.produto_id = Produto.id AND ProdutoSessao.session_id = "'.$pedido['Orcamento']['sessao_id'].'"');
            foreach ($produtos_session as $ps) {
                $total_pontos = $total_pontos + ($ps['Produto']['preco'] * $ps['ProdutoSessao']['quantidade']);
            }
        }
        return $total_pontos;
    }
    
    public function get_ponto_cliente_disponivel_expirado($cnpj){
        $pedidos_antes = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));
        
        $pedidos_ativos = $this->PedidoCliente->find('all',array(
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"', "PedidoCliente.created >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month"),
        ));

        $notas_antes = $this->Nota->find('all',array(
            'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e < DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));
        
        $notas_atuais = $this->Nota->find('all',array(
            'conditions' => array('Nota.cliente_cnpj = "'.$cnpj.'"', "Nota.data_e >= DATE_FORMAT(NOW(),'%Y%m%d') - interval 6 month", 'Nota.cancelada = 0'),
            'order'      => array('Nota.data_e' => 'ASC'),
        ));
        
        $total_nota_antes = 0;
        foreach ($notas_antes as $nota_a) {
            $total_nota_antes = $total_nota_antes + $nota_a['Nota']['valor_nf'];
        }

        $total_nota_atuais = 0;
        foreach ($notas_atuais as $nota_a) {
            $total_nota_atuais = $total_nota_atuais + $nota_a['Nota']['valor_nf'];
        }

        $total_trocas_antes6 = 0;
        foreach ($pedidos_antes as $pedido_antes) {
//            $produtos_pedido = $this->requestAction(array('plugin'=>'clientes','controller'=>'clientes','admin'=>false,'action'=>'get_produtos_pedido',$pedido_antes['PedidoCliente']['sessao_id']));
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_antes['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_trocas_antes6 = $total_trocas_antes6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        
        $total_dentro6 = 0;
        foreach ($pedidos_ativos as $pedido_periodo) {
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'fields' => array('PedidoClienteProduto.quantidade','PedidoClienteProduto.pontos','Produto.id','Produto.title','Produto.preco'),
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido_periodo['PedidoCliente']['sessao_id'])
            ));
            foreach ($produtos_pedido as $ps) {
                $total_dentro6 = $total_dentro6 + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }
        }
        
        $expirado = 0;
//        if(($total_trocas_antes6) > 0){
//            $saldo = $total_nota_antes - $total_trocas_antes6;
//        }
//        else if(($total_trocas_antes6 + $total_dentro6) < $total_nota_atuais){
//            $saldo = $total_nota_antes;
////            pre('aa');
//        }
//        else{
//            $saldo = $total_nota_antes - ($total_trocas_antes6 + $total_dentro6);
////            pre('bb');
//        }
        
        $saldo = $total_nota_antes - ($total_trocas_antes6 + $total_dentro6);
        if($saldo < 0){
            $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);
            if($total_dentro6 > $total_nota_atuais){
                $expirado = $disponivel;
            }
        }else{
            $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6) - $saldo;
            $expirado = $saldo;
        }
        
//        $saldo = (($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6) - $total_nota_antes);
//        if($saldo < 0){
//            $expirado = ($total_nota_antes) - ($total_trocas_antes6 + $total_dentro6);
////            $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);
//            if($total_dentro6 > $total_nota_atuais){
////                $expirado = $disponivel;
//                $expirado = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);;
//            }
//        }else{
//            $disponivel = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6) - $saldo;
//            $expirado = $saldo;
//        }
        
//        if($total_nota_antes > 0 && $total_trocas_antes6 === 0){
//            $expirado = $total_nota_antes;
////            pre($saldo);
//        }else{
//            $expirado = $total_nota_antes - ($total_trocas_antes6 + $total_dentro6);
//            if($total_dentro6 > $total_nota_atuais){
//                $expirado = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);
//            }
//        }
            
//        $disponivel_geral = ($total_nota_antes + $total_nota_atuais) - ($total_trocas_antes6 + $total_dentro6);
//        $disponivel_para_troca = $disponivel_geral - $expirado;
            
        
// ------------------------------ acha os pontos disponiveis por pedido e nota ------------------------------ //
        $pedidos_geral = $this->PedidoCliente->find('all',array(
            'fields'     => array('id','sessao_id','created','cdate'),
            'conditions' => array('PedidoCliente.cnpj = "'.$cnpj.'"'),
            'order'      => array('PedidoCliente.created' => 'ASC'),
        ));
        
        $arr_notas_usadas = array();
        $total_disponivel = 0;
        $total_notas_expiradas = 0;
        $data_ultima_nota = null;
        
        foreach ($pedidos_geral as $pedido) {
//            pre($pedido);
//            $notas_antes_6_12 = $this->Nota->find('all',array(
//                'fields' => array('id','valor_nf','data_e'),
//                'conditions' => array(
//                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
//                        "Nota.data_e > DATE_FORMAT('".$expl_date[0]."','%Y%m%d') - interval 12 month", 
//                        "Nota.data_e < DATE_FORMAT('".$expl_date[0]."','%Y%m%d') - interval 6 month", 
//                        "DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$expl_date[0]."','%Y%m%d')", 
//                        'Nota.cancelada = 0'
//                ),
//                'order'      => array('Nota.data_e' => 'ASC'),
//            ));
//            $total_notas_antes_12 = 0;
//            pre($notas_antes_6_12);
            
            $expl_date = explode(' ', $pedido['PedidoCliente']['created']);
            $notas_antes_pedido = $this->Nota->find('all',array(
                'fields' => array('id','valor_nf','data_e'),
                'conditions' => array(
                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
                        "Nota.data_e > DATE_FORMAT('".$expl_date[0]."','%Y%m%d') - interval 6 month", 
                        "DATE_FORMAT(data_e,'%Y%m%d') <= DATE_FORMAT('".$expl_date[0]."','%Y%m%d')", 
                        'Nota.cancelada = 0'
                ),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
//            pre($notas_antes_pedido);
            
            $total_notas = 0;
            foreach ($notas_antes_pedido as $nota_ap) {
//                $firstDate  = new DateTime($nota_ap['Nota']['data_e']);
//                $secondDate = new DateTime(date('Y-m-d'));
//                $intvl = $firstDate->diff($secondDate);
                                            
                if(!in_array($nota_ap['Nota']['id'], $arr_notas_usadas)) { 
                    $arr_notas_usadas[] = $nota_ap['Nota']['id'];
                    $total_notas = $total_notas + $nota_ap['Nota']['valor_nf'];
                    $data_ultima_nota = $nota_ap['Nota']['data_e'];
                }
            }
            
            $produtos_pedido = $this->PedidoClienteProduto->find('all',array(
                'conditions' => array('PedidoClienteProduto.session_id'=>$pedido['PedidoCliente']['sessao_id'])
            ));
            $total_pontos = 0;
            foreach ($produtos_pedido as $ps) {
                $total_pontos = $total_pontos + ($ps['PedidoClienteProduto']['pontos'] * $ps['PedidoClienteProduto']['quantidade']);
            }

            $total_disponivel = $total_disponivel + ($total_notas - $total_pontos);
        }
        
        if($data_ultima_nota){
            $notas_depois_pedido = $this->Nota->find('all',array(
                'fields' => array('id','valor_nf','data_emissao','data_e'),
                'conditions' => array(
                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
                        "DATE_FORMAT(data_e,'%Y%m%d') > DATE_FORMAT('".$data_ultima_nota."','%Y%m%d')", 
                        'Nota.cancelada = 0'
                ),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
        }else{
            $notas_depois_pedido = $this->Nota->find('all',array(
                'fields' => array('id','valor_nf','data_emissao','data_e'),
                'conditions' => array(
                        'Nota.cliente_cnpj = "'.$cnpj.'"', 
                        'Nota.cancelada = 0'
                ),
                'order'      => array('Nota.data_e' => 'ASC'),
            ));
        }
        
        $total_pontos_notas_depois = 0;
        foreach ($notas_depois_pedido as $nota) {
            $total_pontos_notas_depois = $total_pontos_notas_depois + $nota['Nota']['valor_nf'];
        }
        
        $disponivel_para_troca = $total_disponivel + $total_pontos_notas_depois;
        // ------------------------------ -------------------------------------------------- ------------------------------ //
// 
//        pre($total_disponivel);
//        $disponivel_para_troca = $disponivel - $expirado;
        
//        pre($disponivel);
        return array(
            'disponivel_para_troca' => $disponivel_para_troca, 
//            'disponivel_teste'      => $disponivel, 
            'expirado'              => $expirado,
            'total_nota_antes'      => $total_nota_antes,
            'total_nota_atuais'     => $total_nota_atuais,
            'total_trocas_antes6'   => $total_trocas_antes6,
            'total_dentro6'         => $total_dentro6,
        );
    }
    
    
}