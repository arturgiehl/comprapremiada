<?php
App::uses('AppController', 'Controller');

class PagesController extends AppController {

    public function admin_index() {
        $this->layout = "Painel.admin";
        exit;
    }
     
    public $uses = array(
        'Banners.Banner',
        'Banners.BannerFixo',
        'Produtos.Produto',
        'Notas.Nota',
        'Notas.NotaCancelada'
    );
    
    public function home(){
        $banners_fixo = $this->get_banners_fixo();
        
        $this->set('banners',$this->Banner->find('all',array('order'=>array('Banner.order_banner'=>'ASC','Banner.created'=>'DESC'))));
        $this->set('banners_fixo',$banners_fixo);
        $this->set('produtos',$this->Produto->find('all',array('conditions'=>array('Produto.ativo = "1"'),'order'=>'RAND()','limit'=>12)));
//        pre(Configure::version());
    }
    
    public function get_banners_fixo(){
        if(Cache::read('ch_banners_fixos')) {
            $ch_banners_fixos = Cache::read('ch_banners_fixos');
        }else{
            $ch_banners_fixos = $this->BannerFixo->find('first');
            Cache::write('ch_banners_fixos',$ch_banners_fixos);
        }
        return $ch_banners_fixos;
    }

    public function notfound(){
        $this->Seo->title('Nada encontrado');
    }
    
    public function change(){
        $this->layout = false;
        $this->autoRender = false;
        
        $this->Produto->recursive = -1;
        $produtos = $this->Produto->find('all',array('fields'=>array('id','title','preco')));
//        pre($produtos);
        
        foreach ($produtos as $produto) {
            $pontuacao = $produto['Produto']['preco'] * 2;
            $save_np = array(
                'id'    => $produto['Produto']['id'],
                'preco' => $pontuacao
            );
            $this->Produto->save($save_np);
        }
//        $count = count($produtos);
        
        
        echo 'salvo.';
    }

    public function verifica_canceladas(){
        $this->layout = false;
        $this->autoRender = false;
        
        $count = 1;
        $notas_canceladas = $this->NotaCancelada->find('all');
        foreach ($notas_canceladas as $notacancelada) {
            $nota = $this->Nota->find('first',array('conditions'=>array('Nota.nf_numero'=>$notacancelada['NotaCancelada']['nf_numero'])));
            if($nota){
                pre('ixee');
            }else{
                echo $count.' - foi. <br>';
            }
            $count++;
        }
    }
    
    public function soma(){
        $this->layout = false;
        $this->autoRender = false;
        
//        $notas = $this->Nota->query('SELECT valor_nf FROM tb_notas_emitidas_2018 AS Nota;');
//        $notas = $this->Nota->query('SELECT valor_nf FROM tb_notas_emitidas_2019 AS Nota;');
        $notas = $this->Nota->query('SELECT valor_nf FROM tb_notas_emitidas_2020 AS Nota;');
        
        $total = 0;
        foreach ($notas as $nota) {
            $total = $total + $nota['Nota']['valor_nf'];
        }
        
//        pre('R$ '.number_format($total, 2, ',', '.'));
    }
    
    public function popup_cookies(){
        $this->autoRender = true;
        $this->layout = false;
    }
    
    public function load_ga(){
        $this->layout = false;
    }
    
}